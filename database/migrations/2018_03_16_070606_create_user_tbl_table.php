<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tbl', function (Blueprint $table) {
            $table->increments('Use_Id',11);
            $table->string('Use_Name');
            $table->tinyInteger('Use_Type')->default('1')->comment = "1=Admin 2=Teacher 3=Driver";
            $table->text('Use_Password');
            $table->text('Use_Address');
            $table->string('Use_City');
            $table->string('Use_State');
            $table->string('Use_Country');
            $table->bigInteger('Use_Phone_No')->unsigned()->nullable();
            $table->bigInteger('Use_Mobile_No')->unsigned()->nullable();
            $table->tinyInteger('Use_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Use_CreatedBy')->unsigned()->nullable();
            $table->datetime('Use_CreatedAt');
            $table->integer('Use_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Use_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tbl');
    }
}
