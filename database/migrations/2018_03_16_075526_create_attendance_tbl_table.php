<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_tbl', function (Blueprint $table) {
            $table->increments('Att_Id',11);
            $table->integer('Att_Bra_Id')->unsigned();
            $table->integer('Att_Cla_Id')->unsigned();
            $table->integer('Att_Cla_Section');
            $table->date('Att_Month');
            $table->tinyInteger('Att_List')->comment = "1=Present 0=Absent";
            $table->integer('Att_CreatedBy')->unsigned()->nullable();
            $table->datetime('Att_CreatedAt');
            $table->integer('Att_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Att_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_tbl');
    }
}
