<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_tbl', function (Blueprint $table) {
            $table->increments('Brn_Id',11);
            $table->integer('Brn_Zon_Id')->unsigned();
            $table->string('Brn_Name');
            $table->text('Brn_Code',3);
            $table->text('Brn_Email');
            $table->bigInteger('Brn_Mobile_No')->unsigned();
            $table->text('Brn_Address');
            $table->tinyInteger('Brn_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Brn_CreatedBy')->unsigned()->nullable();
            $table->datetime('Brn_CreatedAt');
            $table->integer('Brn_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Brn_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_tbl');
    }
}
