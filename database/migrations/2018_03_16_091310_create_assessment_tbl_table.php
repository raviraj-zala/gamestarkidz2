<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_tbl', function (Blueprint $table) {
            $table->increments('Ass_Id',11);
            $table->date('Ass_Month');
            $table->integer('Ass_Rate');
            $table->integer('Ass_CreatedBy')->unsigned()->nullable();
            $table->datetime('Ass_CreatedAt');
            $table->integer('Ass_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Ass_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_tbl');
    }
}
