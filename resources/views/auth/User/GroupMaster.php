<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{
    const CREATED_AT = 'Grp_Mas_CreatedAt';
	const UPDATED_AT = 'Grp_Mas_UpdatedAt';

	protected $primaryKey = "Grp_Mas_Id";
	protected $table = "group_master_tbl";

	public function Group_Member(){
		return $this->hasMany('App\Model\GroupDetail','Grp_Det_Mas_Id','Grp_Mas_Id');
	}
}
