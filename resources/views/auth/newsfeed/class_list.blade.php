<div class="box box-primary">
    <div class="line-1 branch-line">
        <div class="mangement-btn user-text">
            <h2 class="box-title"><b>Select Class</b></h2>
        </div>
        <div class="user-checkall">
            <a href="javascript:void(0);" onclick="selectAll(true,'checkAllClass')">Check All </a>|
            <a href="javascript:void(0);" onclick="unselectAll(false,'uncheckAllClass')">UnCheck All</a>
        </div>
    </div>

{{--    @php--}}
{{--        $classes_details = \App\Model\ClassTbl::where('Cla_Bra_Id',$data->request('Brn_Id'))->get();--}}
{{--    @endphp--}}

    @php $i=0; @endphp
    <div class="branch-box" style="width: 100%;">
        <div class="branch-box" style="width: 100%;">
            <div>
                @foreach($data as $detail)
                    <div class="col-md-3">
                        <input type="checkbox" name="class_id[]" id="chckbox.{{ $i }}" class="class_id css-checkbox {{ 'branch'.$detail->Cla_Id }} {{ 'brn'.$detail->Cla_Bra_Id }} " value="{{ $detail->Cla_Id }} {{ $detail->Cla_Id }}" @if(old('branch')) @foreach(old('branch') as $b) @if($b == $detail->Cla_Bra_Id ) checked="true" @endif @endforeach @endif >
                        <label class="css-label" for="chckbox.{{ $i }}">{{ $detail->Cla_Class }}</label>
                    </div>
                    @php $i++; @endphp
                @endforeach
            </div>
        </div>
    </div>
</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-btn">
				<input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" ></a>
			</div>
		</div>
	</div>


<script type="text/javascript">
    $("#submitBtn").on('click',function(e){
        var title = $("#title").val();
        var description = $("#description").val();
        var branchError = $("#branchError").val();
        var user_type = $("#user_type").val();
        var checkCount = 0;
        var imageOrVideoURL = $("#img_vdo_url").val();
        if(user_type=="1" || user_type=="5"){
            var checkedNum = $('input[name="class_id[]"]:checked').length;
            $("#titleError").html("");
            $("#descriptionError").html("");
            if(title=="" || title.trim()=="" ){
                $("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
                $("#title").focus();
                return false;
                e.preventDefault();
            }else if(description=="" || description.trim()=="" ){
                $("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
                $("#description").focus();
                return false;
                e.preventDefault();
            }else if(!empty(imageOrVideoURL)){
                if (!preg_match("/http:\\/\\/(?:www\\.)?youtube.*watch\\?v=([a-zA-Z0-9\\-_]+)/", $img_vdo_url)) {
                    $("#imageOrVideoURLError").html("<label class='error' for='imageOrVideoURL'><span class='text-danger'>Please enter valid URL</span></label>");
                    $("#imageOrVideoURLError").focus();
                    return false;
                    e.preventDefault();
                }
            } else if(branchError=="") {
                $("#branchError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
                $("#description").focus();
                return false;
                e.preventDefault()
            }

            else if(checkedNum=="0"){
                alert('Please at least check one of the class');
                return false;
                e.preventDefault();
            }

        }else if(user_type=="2"){
            var tag_type = $("#tagType").val();
            $("#titleError").html("");
            $("#descriptionError").html("");
            if(title=="" || title.trim()=="" ){
                $("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
                $("#title").focus();
                return false;
                e.preventDefault();
            }else if(description=="" || description.trim()=="" ){
                $("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
                $("#description").focus();
                return false;
                e.preventDefault();
            }else if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$img_vdo_url) ){
                $("#img_vdo_url").html("<label class='error' for='imageOrVideoURL'><span class='text-danger'>Please enter valid URL</span></label>");
                $("#img_vdo_url").focus();
                return false;
                e.preventDefault();
            }
            else if(tag_type == "class"){
                var checkedClass = $('input[name="class_id[]"]:checked').length;
                if(checkedClass=="0")
                {
                    alert('Please at least check one of the class');
                    return false;
                    e.preventDefault();
                }
            }else if(tag_type == "student"){
                var checkedStudent = $('input[name="student_id[]"]:checked').length;
                if(checkedStudent=="0")
                {
                    alert('Please at least check one of the student');
                    return false;
                    e.preventDefault();
                }
            }
        }
    });

    function selectAll(bool,type)
    {
        if(type == "checkAllClass"){
            $("input.class_id").each(function(i){
                $(this).prop('checked', true);
            });
        } else if(type == "checkAllClass"){
            $("input.class_id").each(function(i){
                $(this).prop('checked', true);
            });
        }
    }

    function unselectAll(bool,type)
    {
        if(type == "uncheckAllClass"){
            $("input.class_id").each(function(i){
                $(this).prop('checked', false);
            });
        } else if(type == "uncheckAllClass"){
            $("input.class_id").each(function(i){
                $(this).prop('checked', false);
            });
        }
    }

</script>