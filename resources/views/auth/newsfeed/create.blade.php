@extends('auth.Master.master')

@section('title','Blog Create')

@section('site_header')

@section('sidebar')

@section('content')
	<div class="line-1">
		<div class="mangement-btn user-text">
			<h2><img src="{{ asset('images/') }}" alt="">Create Blog</h2>
		</div>
	</div>
	<div class="clearfix"></div>

	<form name="create-leave" role="form" method="POST" action="{{ url('save_blog') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" id="user_type" value="{{{ Auth::user()->Use_Type }}}">
		<div class="form-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<div class="form-box">
							<div class="form-text form-box-width">
								<h5>Title<span>*</span> :</h5>
							</div>
							<div class="form-typ-box zone-box">
								<input type="text" name="title" class="branch-control" id="title" value="{{ old('title') }}">
								<div id="titleError"></div>
								@if ($errors->has('title'))
									<label class="error" for="imageOrVideo">
									<span class="text-danger">
										{{ $errors->first('title') }}
									</span>
									</label>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-box">
							<div class="form-text form-box-width">
								<h5>Description<span>*</span> :</h5>
							</div>
							<div class="form-typ-box zone-box">
								<textarea class="branch-control" name="description" id="description" rows="5">{{ old('description') }}</textarea>
								<div id="descriptionError"></div>
								@if ($errors->has('description'))
									<label class="error" for="imageOrVideo">
									<span class="text-danger">
										{{ $errors->first('description') }}
									</span>
									</label>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-5">
						<div class="form-box">
							<div class="form-text form-box-width">
								<h5>Image/Video URL :</h5>
							</div>
							<div class="form-typ-box zone-box browser-box">
								<input name="imageOrVideoURL" class="browser-btn" {{ $errors->has('imageOrVideoURL') ? ' is-invalid' : '' }}
								type="text" >
								<div id="imageOrVideoURLError"></div>
								@if ($errors->has('imageOrVideoURL'))
									<label class="error" for="imageOrVideoURL">
								<span class="text-danger">
									{{ $errors->first('imageOrVideoURL') }}
								</span>
									</label>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-5">

						<div class="form-box">
							<div class="form-text form-box-width">
								<h5>Image/Video :</h5>
							</div>
							<div class="form-typ-box zone-box browser-box">
								<input name="imageOrVideo[]" class="browser-btn {{ $errors->has('imageOrVideo') ? ' is-invalid' : '' }}" type="file" 	accept="image/*,video/*" multiple="true">
								@if ($errors->has('imageOrVideo'))
									<label class="error" for="imageOrVideo">
								<span class="text-danger">
									{{ $errors->first('imageOrVideo') }}
								</span>
									</label>
								@endif
							</div>
						</div>

					</div>
				</div>



				@if(Auth::user()->Use_Type == "2")
					<input type="hidden" name="status" value="0">
					<div class="row">
						<div class="col-lg-5">
							<div class="form-box">
								<div class="form-text form-box-width">
									<h5>Tag <span>*</span> :</h5>
								</div>
								<div class="form-typ-box zone-box">
									<div class="btn-group bootstrap-select show-tick branch-control">
										<select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('tag_type') ? 'is-invalid' : '' }}" name="tag_type" id="tagType">
											<option value="class" selected="">Class</option>
											<option value="student">Student</option>
										</select>
										@if ($errors->has('tag_type'))
											<label class="error" for="tag_type">
			                    <span class="text-danger">
			                        {{ $errors->first('tag_type') }}
			                    </span>
											</label>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				@else
					<input type="hidden" name="tag_type" value="class">
					<div class="row">
						<div class="col-lg-5">
							<div class="form-box">
								<div class="form-text form-box-width">
									<h5>Status <span>*</span> :</h5>
								</div>
								<div class="form-typ-box zone-box">
									<div class="btn-group bootstrap-select show-tick branch-control">
										<select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="13">
											<option value="1" >Active</option>
											<option value="0">In-Active</option>
										</select>
										@if ($errors->has('status'))
											<label class="error" for="status">
			                    <span class="text-danger">
			                        {{ $errors->first('status') }}
			                    </span>
											</label>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif

			<!-- Select Multiple Class For Social Activity  -->
				@if(Auth::user()->Use_Type == "1" || Auth::user()->Use_Type == "5")
					<div class="row">
						<div class="col-lg-5">

							<div class="form-box">
								<div class="form-text form-box-width">
									<h5>Branch :</h5>
								</div>
								<div class="form-typ-box zone-box">
									<div class="btn-group bootstrap-select show-tick form-control select2 select2-hidden-accessible">
										<select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
											<option value="none">-- Select --</option>
											@foreach($branch as $data)
												<option name="{{ $data->Brn_Id }}" value="{{ $data->Brn_Id }}" @if(isset($task->Tas_Branch)) @if($task->Tas_Branch==$data->Brn_Id) selected @endif @endif>{{ $data->Brn_Name }}</option>
											@endforeach
										</select>
										@if (Session::has('branch'))
											<label class="error" for="branch">
										<span class="text-danger">
											{{ Session::get('branch') }}
										</span>
											</label>
										@endif
									</div>
									<div id="branchError"></div>
								</div>
							</div>

						</div>
					</div>
				@endif

			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div id="setclass"></div>
				<div id="branchError" style="float: left;"></div>
			</div>
		</div>



		{{--	<div class="row">--}}
		{{--		<div class="col-lg-12">--}}
		{{--			<div class="form-btn">--}}
		{{--				<input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" ></a>--}}
		{{--			</div>--}}
		{{--		</div>--}}
		{{--	</div>--}}
	</form>



	<script type="text/javascript">
		$(document).ready(function(){
			$("#submitBtn").on('click',function(e){
				var title = $("#title").val();
				var description = $("#description").val();
				var user_type = $("#user_type").val();
				var imageOrVideoURL = $("#img_vdo_url").val();
				if(user_type=="1" || user_type=="5"){
					var checkedNum = $('input[name="class_id[]"]:checked').length;
					$("#titleError").html("");
					$("#descriptionError").html("");
					if(title=="" || title.trim()=="" ){
						$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
						$("#title").focus();
						return false;
						e.preventDefault();
					}else if(description=="" || description.trim()=="" ){
						$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
						$("#description").focus();
						return false;
						e.preventDefault();
					}else if(!empty(imageOrVideoURL)){
						if (!preg_match("/http:\\/\\/(?:www\\.)?youtube.*watch\\?v=([a-zA-Z0-9\\-_]+)/", $img_vdo_url)) {
							$("#imageOrVideoURLError").html("<label class='error' for='imageOrVideoURL'><span class='text-danger'>Please enter valid URL</span></label>");
							$("#imageOrVideoURLError").focus();
							return false;
							e.preventDefault();
						}
					}
					else if(checkedNum=="0"){
						alert('Please at least check one of the class');
						return false;
						e.preventDefault();
					}

				}else if(user_type=="2"){
					var tag_type = $("#tagType").val();
					$("#titleError").html("");
					$("#descriptionError").html("");
					if(title=="" || title.trim()=="" ){
						$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
						$("#title").focus();
						return false;
						e.preventDefault();
					}else if(description=="" || description.trim()=="" ){
						$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
						$("#description").focus();
						return false;
						e.preventDefault();
					}else if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$img_vdo_url) ){
						$("#img_vdo_url").html("<label class='error' for='imageOrVideoURL'><span class='text-danger'>Please enter valid URL</span></label>");
						$("#img_vdo_url").focus();
						return false;
						e.preventDefault();
					}
					else if(tag_type == "class"){
						var checkedClass = $('input[name="class_id[]"]:checked').length;
						if(checkedClass=="0")
						{
							alert('Please at least check one of the class');
							return false;
							e.preventDefault();
						}
					}else if(tag_type == "student"){
						var checkedStudent = $('input[name="student_id[]"]:checked').length;
						if(checkedStudent=="0")
						{
							alert('Please at least check one of the student');
							return false;
							e.preventDefault();
						}
					}
				}
			});
			$("#tagType").on('change',function(e){
				var tag_type = $("#tagType").val();
				if(tag_type=="class")
				{
					$("#studentList").css('display','none');
					$("#classList").css('display','block');
				}else if(tag_type=="student"){
					$("#classList").css('display','none');
					$("#studentList").css('display','block');
				}
			});

			$("#branch").on('change',function(){
				debugger;
				var branchId = $(this).val();
				if(("#branch")!="none") {
					var settings = {
						url: "{{ url('student/getStudentClass') }}",
						"method": "POST",
						data: {
							_token: '{{ csrf_token() }}',
							Cla_Bra_Id: branchId
						},
					};

					$.ajax(settings).done(function (data) {
						debugger;
						$("#setclass").html("Loading...");
						if (data == "false") {
							$("#setclass").html("Something went wrong please try again later.");
						} else {
							$("#setclass").html(data);
						}
					}).fail(function (response) {
						debugger;
						console.log(response);
					});
				}
				else{
					$("#branchError").html('<label class="text-danger">Please select branch.<label>');
					$("#setclass").html('Please select branch.');
				}
			});
		});


		function selectAll(bool,ids)
		{
			$("input.brn"+ids).each(function(i){
				$(this).prop('checked', true);
			});
		}
		function unselectAll(bool,ids)
		{
			$("input.brn"+ids).each(function(i){
				$(this).prop('checked', false);
			});
		}

		function setText(){
			var chkArray = [];
			/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
			$(".css-checkbox:checked").each(function() {
				chkArray.push($(this).val());
			});

			/* we join the array separated by the comma */
			var selected;
			selected = chkArray.join(',') ;

			/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
			if(selected.length > 0){
				return true;
			}else{
				alert("Please at least check one of the Branch");
				return false;
			}
		}
	</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')