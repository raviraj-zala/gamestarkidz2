@extends('auth.Master.master')
@section('title','Class Management')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('images/') }}" alt="">{{ $group_detail->Grp_Mas_Name }}</h2>    

	</div>
</div>

<div class="clearfix"></div>

<div class="row">

	@php
		$chatLists = \App\Model\Group::select('group_chat_tbl.*','user_tbl.Use_Name')
					->leftjoin('user_tbl','group_chat_tbl.Grp_Cha_Sender_Id','=','user_tbl.Use_Id')
					->where('group_chat_tbl.Grp_Cha_Mas_Id',$group_detail->Grp_Mas_Id)
					->orderby('group_chat_tbl.Grp_Cha_CreatedAt','ASC')
					->get();
	@endphp
	<div class="col-lg-12">
		<div class="main-chat-box" id="main_chat_box_id">
			@forelse($chatLists as $chatList)
			<div class="row">
				

				<div class="col-lg-12">
					@if($chatList->Grp_Cha_Sender_Id == Auth::user()->Use_Id)
						<div class="pull-right chat-list">
							<div class="chat-box-left">
					@else
						<div class="pull-left chat-list">
							<div class="chat-box-right">
					@endif
								<div class="chat-user-name">
									{{ $chatList->Use_Name }}
								</div>
								@if($chatList->Grp_Cha_Type=="0")
									{{ $chatList->Grp_Cha_Message }}
								@elseif($chatList->Grp_Cha_Type=="1")
									<video width="auto" height="240px" controls>
										<source src="{{ url('/chat/video/'.$chatList->Grp_Cha_Video) }}" type="video/mp4">
									</video>
								@elseif($chatList->Grp_Cha_Type=="2")
									<img src="{{ url('/chat/image/'.$chatList->Grp_Cha_Image) }}" id="" style="width:250px!important;height:240px;">
								@elseif($chatList->Grp_Cha_Type=="3")
									<audio controls>
										<source src="{{ url('/chat/audio/'.$chatList->Grp_Cha_Audio) }}" type="audio/ogg">
									</audio>
								@elseif($chatList->Grp_Cha_Type=="4")
									<a target="_blank" href="{{ url('/chat/docs/'.$chatList->Grp_Cha_Document) }}">Document</a>
								@endif

								<div class="chat-create-time">
									{{ date('d M H:i a',strtotime($chatList->Grp_Cha_CreatedAt)) }}
								</div>

							</div>
						</div>
				</div>
			</div>
			@empty
			<div class="col-lg-12">
				No chat yet.
			</div>
			@endforelse
		</div>
		<form method="POST" enctype="multipart/form-data" id="group_form" name="group_form">
			<input type="hidden" name="sender_id" id="sender_id" value="{{ Auth::user()->Use_Id }}">
			<input type="hidden" name="_token" value="{{csrf_token ()}}">
			<input type="hidden" name="group_master_id" id="group_master_id" value="{{ $group_detail->Grp_Mas_Id }}">
			<div id="pop-up" class="pop-up">
				<div class="image-upload images_chat_div">
				    <label for="file-input" class="vdo_back" title="Image">
				        <img src="{{url('images/picture.png')}}" class="vdo-width">
				    </label>
				    <input id="file-input" name="image_file" type="file" class="image_file" accept="image/*">
				</div>
				<div class="image-upload video_chat_div">
					<label for="file-input-video" class="vdo_back" title="Video">
						<img src="{{url('images/video-camera.png')}}" class="vdo-width image_chat">
					</label>
					<input type="file" id="file-input-video" name="video_file" name="" class="" accept="video/mp4,video/x-m4v,video/*">
				</div>
				<div class="image-upload video_chat_div">
					<label for="file-input-audio" class="vdo_back" title="Audio">
						<img src="{{url('images/music-player.png')}}" class="vdo-width image_chat">
					</label>
					<input type="file" id="file-input-audio" name="audio_file" class="" accept=".mp3,audio/*">
				</div>
				<div class="image-upload video_chat_div">
					<label for="file-input-file" class="vdo_back" title="File">
						<img src="{{url('images/file.png')}}" class="vdo-width image_chat">
					</label>
					<input type="file" id="file-input-file" name="audio_file" class="" accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.pdf">
				</div>
			</div>
		</form>
			{{-- New chat box --}}
		<div class="main-chat-box">
			<div class="row">
				<div class="col-lg-12">
					<div class="message-box">
						<input type="file" name="file" class="form-control" id="file_include">
						<input type="text" name="text_message" class="form-control" id="message_id" placeholder="Message.." >
						<i class="fa fa-paperclip" aria-hidden="true" id="pin"></i>
						<input type="button" name="send" value="SEND" id="send_button" class="btn">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.image-upload > input
	{
	    display: none;
	}
	
	.image-upload img
	{
	    width: 80px;
	    cursor: pointer;
	}
	.image_chat{
		width: 9%;
	}
	.image_file{
		position: absolute;
	}
	.pop-up{
		border-radius: 4px;
		display: inline-flex;
    	width: 77%;
    	margin-left: 10px;
	}
	.fa-paperclip{
		font-size: 200%;
	}
	.chat-user-name {
		font-size: 10px;
		text-align: end;
		padding-bottom: 5px;
		font-style: italic;
	}
	.chat-box-right{
		display: inline-block;
		padding: 0px 8px 0px 8px;
		margin: 6px 6px;
		background-color: #ff975b;
		color: #fff;
		border-radius: 0px 10px 10px 10px;
	}
	.chat-box-left {
		display: inline-block;
		padding: 0px 8px 0px 8px;
		margin: 6px 6px;
		background-color: #ff975b;
		color: #fff;
		border-radius: 10px 0px 10px 10px;
	}
	.main-chat-box {
		padding: 0px 0px;
		height: 500px;
		overflow-x: hidden;
		margin-top: 15px;
		margin-left: 10px;
		margin-right: 10px;
		width: 100%;
		overflow-y: auto;
	}
	.chat-create-time {
		font-size: 10px;
		display: block;
		text-align: end;
		width: 100%;
		padding-top: 4px;
		font-style: italic;
	}
	#file_include{
		display: none;
	}
	#message_id{
		width: 80%;
		margin-right: 10px;
	}
	#send_button{
		width: 20%;
		background-color: #ff975b;
		color: #fff;
	}
	.message-box{
		width: 100%;
		display: inline-flex;
	}
	.vdo_back{
		background-color: #f2975c;
	    border: none;
	    color: white;
	    height: 50px;
	    width: 50px;
	    padding: 12px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	    margin: 4px 2px;
	    cursor: pointer;
	    border-radius: 50%;
	}
	.vdo-width{
		width:26px !important;
	}
</style>
<script type="text/javascript" src="http://{{ request()->getHost() }}:8000/socket.io/socket.io.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var sender_id='{{ Auth::user()->Use_Id }}';
		var socket = io.connect(window.location.hostname+":8000/");
		var Grp_Mas_Id= $("#group_master_id").val();
		var text_message='',group_master_id=Grp_Mas_Id,datetime='';

		$('#main_chat_box_id').animate({
	      scrollTop: $('#main_chat_box_id')[0].scrollHeight
	    }, 2000);
	    length_image=0;
	    $("#file-input").change(function () {
	    	if ($(this).val() != '') {
			    img_group = this;
		    	console.log(img_group);
        	}
		    var files = $(this)[0].files;
			length_image = files.length;
			imageUpload(img_group);
		});
		length_video=0;
		$("#file-input-video").change(function () {
	    	if ($(this).val() != '') {
			    video_group = this;
        	}
		    var files = $(this)[0].files;
		    console.log(files);
			length_video = files.length;
			videoUpload(video_group);
		});
		length_audio=0;
		$("#file-input-audio").change(function () {
	    	if ($(this).val() != '') {
			    audio_group = this;
        	}
		    var files = $(this)[0].files;
			length_audio = files.length;
			audioUpload(audio_group);
		});
		length_file=0;
		$("#file-input-file").change(function () {
	    	if ($(this).val() != '') {
			    file_group = this;
        	}
		    var files = $(this)[0].files;
			length_file = files.length;
			console.log(file_group);
			fileUpload(file_group);
		});
	    /* send message */
	    $("#send_button").on('click',function(e){
	    	text_message = $("#message_id").val().trim();
	    	if(text_message!=''){
	    		m_chat_type = 0;
		    	datetime = yyyymmdd();
		    	var Grp_Mas_Id= $("#group_master_id").val();
		    	/* send message socket */
		    	socket.emit("send group msg",{senderId:sender_id,groupMasterId:Grp_Mas_Id,message:text_message,chatType:m_chat_type,datetime:datetime});
		    	$("#message_id").val('');
	    	}
	    });
	
	    /* enter key to send message */ 
	    $('#message_id').keypress(function(e) {
	    	var key = e.which;
		    if (key == 13) // the enter key code
		    {
		    	$('input[name = send]').click();
		      	text_message = $("#message_id").val().trim();
	    		if(text_message!=''){
			    	datetime = yyyymmdd();
			    	/* send message socket */
			    	socket.emit("send group msg",{senderId:sender_id,groupMasterId:group_master_id,message:text_message,chatType:m_chat_type,datetime:datetime});
			    	$("#message_id").val('');
		    	}
		    	e.preventDefault();
		    }
	  	});
	  	/* Pin to open menu */ 
	    $('#message_id').on('keyup',function(){
			if($(this).val().length >= 0){
				$("#pop-up").hide();
			} else{
				$("#pop-up").show();
			}
		});
		$("#pop-up").hide();
		$('#pin').on('click',function(){
			$("#pop-up").toggle();
		});	  	
	});
	function imageUpload(img_group){
		if (length_image!=0) {
    			datetime = yyyymmdd();
		    	m_chat_type = 2;
		    	var form_data = new FormData();
		    	var file  =  img_group.files[0];
		    	var user_id = $("#sender_id").val();
		    	var group_master_id = $("#group_master_id").val();
    			form_data.append('image',file);
    			form_data.append('_token', '{{csrf_token()}}');
    			form_data.append('sender_id', user_id);
    			form_data.append('group_master_id', group_master_id);
    			form_data.append('m_chat_type', 2);
		    	datetime = yyyymmdd();
		    	$.ajax({
                    type : "POST",
                    url : "{{ route('group_img_upload') }}",
                    data : form_data,
		            contentType: false,
		            cache: false,
		            processData:false,
                    success : function(response){
						length='0';
	                	var socket = io.connect(window.location.hostname+":8000/");
	                	socket.emit("send group msg",{senderId:response['Grp_Cha_Sender_Id'],groupMasterId:response['Grp_Cha_Mas_Id'],fileName:response['Grp_Cha_Image'] ,chatType:2,datetime:datetime});
	                	$('#main_chat_box_id').animate({
					      	scrollTop: $('#main_chat_box_id')[0].scrollHeight
					    }, 1000);
	                	$("#file-input").val('');
	                    },
                    error: function(error){
                    	console.log(error);
                    }
                });   
    	}
    }
    function videoUpload(video_group){
    	if (length_video!=0) {
    			datetime = yyyymmdd();
		    	m_chat_type = 1;
		    	var form_data = new FormData();
		    	var file  =  video_group.files[0];
		    	var user_id = $("#sender_id").val();
		    	var group_master_id = $("#group_master_id").val();
    			form_data.append('video',file);
    			form_data.append('_token', '{{csrf_token()}}');
    			form_data.append('sender_id', user_id);
    			form_data.append('group_master_id', group_master_id);
    			form_data.append('m_chat_type', 1);
		    	datetime = yyyymmdd();
		    	$.ajax({
                    type : "POST",
                    url : "{{ route('group_video_upload') }}",
                    data : form_data,
		            contentType: false,
		            cache: false,
		            processData:false,
                    success : function(response){
						length='0';
                    	var file = response;
	                	var socket = io.connect(window.location.hostname+":8000/");
	                	socket.emit("send group msg",{senderId:response['Grp_Cha_Sender_Id'],groupMasterId:response['Grp_Cha_Mas_Id'],fileName:response['Grp_Cha_Video'] ,chatType:1,datetime:datetime});
	                	$("#file-input-video").val('');
	                	$('#main_chat_box_id').animate({
					      	scrollTop: $('#main_chat_box_id')[0].scrollHeight
					    }, 1000);
                    },
                    error: function(error){
                    	console.log(error);
                    }
                });   
	    	}
    }
    function audioUpload(audio_group){
    	if (length_audio!=0) {
		    	m_chat_type = 3;
		    	var form_data = new FormData();
		    	var file  =  audio_group.files[0];
		    	var user_id = $("#sender_id").val();
		    	var group_master_id = $("#group_master_id").val();
    			form_data.append('audio',file);
    			form_data.append('_token', '{{csrf_token()}}');
    			form_data.append('sender_id', user_id);
    			form_data.append('group_master_id', group_master_id);
    			form_data.append('m_chat_type', 3);
    			datetime = yyyymmdd();
		    	$.ajax({
                    type : "POST",
                    url : "{{ route('group_audio_upload') }}",
                    data : form_data,
		            contentType: false,
		            cache: false,
		            processData:false,
                    success : function(response){
						length='0';
                    	var file = response;
	                	var socket = io.connect(window.location.hostname+":8000/");
	                	socket.emit("send group msg",{senderId:response['Grp_Cha_Sender_Id'],groupMasterId:response['Grp_Cha_Mas_Id'],fileName:response['Grp_Cha_Audio'] ,chatType:3,datetime:datetime});
	                	$("#file-input-audio").val('');
	                	$('#main_chat_box_id').animate({
					      	scrollTop: $('#main_chat_box_id')[0].scrollHeight
					    }, 1000);
                    },
                    error: function(error){
                    	console.log(error);
                    }
                });   
	    	}
    }
	function fileUpload(file_group){
		if (length_file!=0) {
		    	m_chat_type = 4;
		    	var form_data = new FormData();
		    	var file  =  file_group.files[0];
		    	var user_id = $("#sender_id").val();
		    	var group_master_id = $("#group_master_id").val();
    			form_data.append('file',file);
    			form_data.append('_token', '{{csrf_token()}}');
    			form_data.append('sender_id', user_id);
    			form_data.append('group_master_id', group_master_id);
    			form_data.append('m_chat_type', 4);
    			datetime = yyyymmdd();
		    	$.ajax({
                    type : "POST",
                    url : "{{ route('group_file_upload') }}",
                    data : form_data,
		            contentType: false,
		            cache: false,
		            processData:false,
                    success : function(response){
                    	console.log(response);
						length='0';
                    	var file = response;
	                	var socket = io.connect(window.location.hostname+":8000/");
	                	socket.emit("send group msg",{senderId:response['Grp_Cha_Sender_Id'],groupMasterId:response['Grp_Cha_Mas_Id'],fileName:response['Grp_Cha_Document'] ,chatType:response['Grp_Cha_Type'],datetime:datetime});
	                	$("#file-input-file").val('');
	                	$('#main_chat_box_id').animate({
					      	scrollTop: $('#main_chat_box_id')[0].scrollHeight
					    }, 1000);
                    },
                    error: function(error){
                    	console.log(error);
                    }
                });                
	    	}
	}
	/* meesage get socket */
		var group_master_id = {{ Request::get('id') }};
		var socket = io.connect(window.location.hostname+":8000/");
    	socket.on("read_group_msg",function(res_data){
    		if(res_data[0].Grp_Cha_Mas_Id==group_master_id){
    		
	    		console.log(res_data);	
	    		if(res_data[0].Grp_Cha_Type==0){
	    			var sender_id = $("#sender_id").val();
	    			msg = res_data[0].Grp_Cha_Message;
	    			dtime = getFormattedDate(res_data[0].Grp_Cha_CreatedAt);
	    			name_sender = res_data[0].Use_Name;
	    			console.log(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id);
	    			if(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id){
		    			$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-right chat-list">'+
									'<div class="chat-box-left">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
											msg+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			} else{
						$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-left chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
											msg+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}

					$('#main_chat_box_id').animate({
				      scrollTop: $('#main_chat_box_id')[0].scrollHeight
				    }, 1000);
	    		} else{
					console.log("else");
	    		}
	    		if (res_data[0].Grp_Cha_Type==2) {
	    			var sender_id = $("#sender_id").val();
	    			img = res_data[0].Grp_Cha_Image;
	    			image = '/chat/image/'+ img;
	    			dtime = getFormattedDate(res_data[0].Grp_Cha_CreatedAt);
	    			name_sender = res_data[0].Use_Name;
					if(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id){
						console.log(sender_id);
	    				$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-right chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<img src ='+image+' width="250px" height="240px" >'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');

	    			} else{
	    				$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-left chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<img src ='+image+' width="250px" height="240px" >'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}
	    			$('#main_chat_box_id').animate({
				      scrollTop: $('#main_chat_box_id')[0].scrollHeight
				    }); 
	    		}
	    		// Video
	    		if (res_data[0].Grp_Cha_Type==1) {
	    			var sender_id = $("#sender_id").val();
	    			video = res_data[0].Grp_Cha_Video;
	    			video = '/chat/video/'+ video;
	    			dtime = getFormattedDate(res_data[0].Grp_Cha_CreatedAt);
	    			name_sender = res_data[0].Use_Name;
	    			if(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id){
	    				console.log("if");
	    			$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-right chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<video width="auto" height="240px" controls="">'+
										'<source src ='+ video +'>'+'</video>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}else{
	    				console.log("else");
	    				$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-left chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<video width="auto" height="240px" controls="">'+
										'<source src ='+ video +'>'+'</video>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}
	    			$('#main_chat_box_id').animate({
				      scrollTop: $('#main_chat_box_id')[0].scrollHeight
				    }); 
	    		}
	    		// Audio
	    		if (res_data[0].Grp_Cha_Type==3) {
	    			var sender_id = $("#sender_id").val();
	    			audio = res_data[0].Grp_Cha_Audio;
	    			audio = '/chat/audio/'+ audio;
	    			dtime = getFormattedDate(res_data[0].Grp_Cha_CreatedAt);
	    			name_sender = res_data[0].Use_Name;
	    			if(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id){
	    			$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-right chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<audio controls="">'+
										'<source src ='+ audio +'>'+'</audio>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}else{
	    				$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-left chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+
										'<audio width="auto" height="240px" controls="">'+
										'<source src ='+ audio +'>'+'</audio>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}
	    			$('#main_chat_box_id').animate({
				      scrollTop: $('#main_chat_box_id')[0].scrollHeight
				    }); 
	    		}
	    		// File
	    		if (res_data[0].Grp_Cha_Type==4) {
	    			var sender_id = $("#sender_id").val();
	    			file = res_data[0].Grp_Cha_Document;
	    			file = '/chat/docs/'+ file;
	    			dtime = getFormattedDate(res_data[0].Grp_Cha_CreatedAt);
	    			name_sender = res_data[0].Use_Name;
	    			if(res_data[0].Grp_Cha_Sender_Id.toString() == sender_id){
	    			$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-right chat-list">'+
									'<div class="chat-box-left">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+'<a target="_blank" href='+ file +'>'+'Document'+'</a>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}else{
	    				$("#main_chat_box_id").append('<div class="row">'+
							'<div class="col-lg-12">'+
								'<div class="pull-left chat-list">'+
									'<div class="chat-box-right">'+
										'<div class="chat-user-name">'+name_sender+'</div>'+'<a target="_blank" href='+ file +'>'+'Document'+'</a>'+
										'<div class="chat-create-time">'+dtime+'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
	    			}
	    			$('#main_chat_box_id').animate({
				      scrollTop: $('#main_chat_box_id')[0].scrollHeight
				    }); 
	    		}
    		}

			res_data='';
		});
	function yyyymmdd() {
	    var x = new Date();
	    var y = x.getFullYear().toString();
	    var mo = (x.getMonth() + 1).toString();
	    var d = x.getDate().toString();
	    var h = x.getHours();
	    var m = x.getMinutes();
	    var s = x.getSeconds();

	    (d.length == 1) && (d = '0' + d);
	    (mo.length == 1) && (mo = '0' + mo);
	    (h.length == 1) && (h = '0' + h);
	    (m.length == 1) && (m = '0' + m);
	    (s.length == 1) && (s = '0' + s);
	    var yyyymmdd = y +'-'+ mo +'-'+ d +' '+ h +':'+m+':'+s;
	    return yyyymmdd;
	}
	function getFormattedDate(input){
	    var indiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
		x = new Date(indiaTime);
	    const monthNames = ['Jan','Feb','Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	    var d = x.getDate().toString();
	    var mo = monthNames[x.getMonth()];
	    var h = x.getHours();
	    var m = x.getMinutes();
	    var ampm = h >= 12 ? 'pm' : 'am';
	    result = d+' '+mo+' '+h+':'+m+' '+ampm;
	    return result;
	}
	// $(document).ready(function () {
	  // Handler for .ready() called.
	  // setTimeout( function() {
	  //   $('#main_chat_box_id').animate({
	  //     scrollTop: $('#main_chat_box_id')[0].scrollHeight
	  //   }, 2000);
	  // }, 3000);
	// });
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')