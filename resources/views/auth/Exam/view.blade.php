@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text">
   <h2><img src="{{ asset('images/report_img.png') }}" alt="">Student Result</h2>   
 </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <!-- <form name="insert-report" role="form" method="POST" action="#" enctype="multipart/form-data"> -->
        <!-- {{ csrf_field() }} -->
        <div class="branch-form">
            <input type="hidden" id="branch_id" name="student_id" >
            <input type="hidden" id="student_gr_no" name="student_gr_no" >
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="branch-form">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Branch <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="branch_id" style="z-index: auto"  id="branch_id" value="{{$class_mod->branch}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Class <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                            <input type="text" name="class" id="class" value="{{$class_mod->class}}" style="z-index: auto" readonly>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                          <h5>Section <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                        <input type="text" name="section" style="z-index: auto" id="section" value="{{$class_mod->Cla_Sec_Id}}" readonly>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Exam Year <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="title" id="title" tabindex="3" value="{{ $class_mod->exam_year }}" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Title <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="title" id="title" tabindex="3" value="{{ $class_mod->exam_title }}" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Subject <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="subject" id="subject" tabindex="3" value="{{$class_mod->Exam_Subject}}" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Status <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="status" id="status" tabindex="3" value="@if($class_mod->status==0) Active @else In-Active @endif" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Exam Type <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="status" id="status" tabindex="3" value="@if($class_mod->Exm_Type==1) Objective @else Subjective @endif" readonly>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <!-- </form> -->
</div>  

<div id="progress_report_data">
    
    <div class="line-1">        
        <div class="mangement-btn user-text">
            <h2><img src="{{ asset('images/report_img.png') }}" alt="">Student List</h2>      
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">

        @if(Session::has('success'))
        <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="main-btn">
               
            </div>
        </div>
    </div>
    <div class="table-form">   
        <table id="example2">
            <tbody>
                <tr>
                    <th class="td-left">Student GR No.</th>   
                    <th class="td-left">Student Name</th>
                    <th class="td-left">Total Question</th>
                    <th class="td-left">Total Attempt</th>

                    <th  @if($class_mod->Exm_Type==0) style="display: none;" @endif class="td-left">Correct Answer</th>
                    <th  class="td-left">Total Marks</th>
                    <th>Obtain Marks</th>
                    <th  @if($class_mod->Exm_Type==0) style="display: none;" @endif class="td-left">Student Percentage</th>

                    <th class="td-left">View</th>
                </tr>
                <?php $i = 1; ?>
                @if(count($students))
                @forelse($students as $student)
                <tr>
                <?php $student_persentage = round(( $student->stu_marks/$student->total_marks ) * 100, 2); ?>
                    <input type="hidden" name="Att_Cla_Id" value="{{ $student->Std_Cla_Id }}" >
                    <td>{{$student->Std_Gr_No}}</td>
                    <td class="td-left">{{ $student->Std_Name }}</td>
                    <td>{{$student->total_ques}}</td>
                    <td>{{$student->attempt}}</td>
                    <td  @if($class_mod->Exm_Type==0) style="display: none;" @endif>{{$student->correct_ans}}</td>
                    <td>  {{ $student->total_marks }} </td>
                    <td>  {{ $student->stu_marks }} </td>
                    <td  @if($class_mod->Exm_Type==0) style="display: none;" @endif>  {{ $student_persentage }} % </td>

                    <td><a href="{{ url('student_ans_sheet',$student->Stu_Exam_Id) }}">
                            <img src="{{ asset('images/notepad.png') }}" alt="notepad">
                        </a>
                    </td>
                </tr>
                <?php $i++; ?>
                @empty
                <tr><td colspan="9">No Data Found.</td></tr>
                @endforelse
                @else
                    <tr><td colspan="9">No Data Found.</td></tr>
                @endif
            </tbody>
        </table>
    </div>
    <!-- @if(count($students)>0)
        <div>
            <div class="form-btn branch-form-btn">
                <input type="submit" value="Update" tabindex="3" onClick="return setText()" id="progressSubmitForm"><a href="{{ url('list_Attendance') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
            </div>    
        </div>
    @endif -->
<!-- </form> -->


    <div class="container"
         style="margin-top: 100px;
            @if($class_mod->Exm_Type == 0 )
                 display: none;
            @endif
                 "
            >
        <div class="row"
             style="
             @forelse($students as $student)
                 @empty
                         display: none;
             @endforelse
               ">
            <div class="col-md-6 col-md-offset-1" style="margin: 0px; margin-top: 10px;">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Student Percentage </h3>
                    </div>
                    <div class="panel-body" align="center">
                        <div id="student_pie_chart" style="width:450px; height:450px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-1" style="margin: 0px; margin-top: 10px;">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> All Over Percentage </h3>
                    </div>
                    <div class="panel-body" align="center">
                        <div id="all_pie_chart" style="width:450px; height:450px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
@push('footer_css')
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2020.3.1118/styles/kendo.common.min.css" />
@push('footer_script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="http://kendo.cdn.telerik.com/2020.3.1118/js/kendo.all.min.js"></script>
<script type="text/javascript">

var analytics = <?php echo $students; ?>

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(StudentDrawChart);
    google.charts.setOnLoadCallback(AllStudentDrawChart);
    //Student Pie Chart Start
    @php
    $new_student = array();
    $count = count($students);
    foreach($students as $student) {
        $student_percentage = round(( $student->stu_marks/$student->total_marks ) * 100, 2);
        $student = array (
          'category' => $student->Std_Name,
          'value' => $student_percentage
        );
        array_push($new_student, $student); 
    }
    
    @endphp
    function StudentDrawChart() {
        var StudentData = @php echo json_encode($new_student); @endphp;
        console.log(StudentData);
        $("#student_pie_chart").kendoChart({
            legend: {
                visible: false
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            series: [{
                type: "pie",
                startAngle: 150,
                data: StudentData
            }],
            tooltip: {
                visible: true,
                format: "{0}%"
            }   
        });
    }
    //Student Pie Chart End
    
    //All Over Student Pie Chart Start
    @php
    $new_all = array();
    foreach($students as $student) {
        $all_percentage = round(( $student->stu_marks/$student->total_marks ) * 100, 2);
        array_push($new_all, $all_percentage); 
    }
    //$all_average = array();
    if(!empty($new_all)) {
        $student_average = round(( array_sum($new_all) / $count ), 2);
        if($student_average == 100) {
            $school_average = 100;
            $all_average = array (
                ['category' => 'School','value' => $school_average],
            );
        } else if($student_average == 0) {
            $school_average = 0;
            $all_average = array (
                ['category' => 'School','value' => $school_average],
            );
        } else {
            $school_average = round((100 - $student_average),2);
            $all_average = array (
                ['category' => 'Student','value' => $student_average],
                ['category' => 'School','value' => $school_average],
            );
        }
    } else {
        $all_average = array (
            ['category' => 'Student','value' => 0],
            ['category' => 'School','value' => 0],
        );
    }
    //array_push($all_average, $new_all);
    @endphp
    
    function AllStudentDrawChart() {
        var AllStudentData = @php echo json_encode($all_average); @endphp;
        console.log(AllStudentData);
        $("#all_pie_chart").kendoChart({
            legend: {
                visible: false
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            series: [{
                type: "pie",
                startAngle: 150,
                data: AllStudentData
            }],
            tooltip: {
                visible: true,
                format: "{0}%"
            }   
        });
    }
    //All Over Student Pie Chart End

$(function () {
    /*$("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });*/
    $("#presentall").click(function () {
        if ($("#presentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', true);
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $("#absentall").click(function () {
        if ($("#absentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', true);
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $('.present-all').click(function(){
        // var presentid = $('input[name="present_id[]"]').val();
         var presentid = this.value;
         console.log(presentid);
         if(presentid == 'checked' ){
            $('.absent-all'+presentid).attr('checked',true);
         } else {
            $('.absent-all'+presentid).attr('checked',false);
         }
         // console.log(id);
    });
    $('.absent-all').click(function(){
        var absentid = this.value;
        if(absentid == 'checked'){
            $('.present-all'+absentid).attr('checked',true);
         } else {
            $('.present-all'+absentid).attr('checked',false);
         }
        // console.log(absentid);
        // var absentid = $('input[name="absent_id[]"]').val();
    });
});
    function setText()
    {
        var chkArray = [];
        
         // look for all checkboes that have a class 'chk' attached to it and check if it was checked 
        $(".present-all:checked").each(function() {

            console.log($(this).val());
            chkArray.push($(this).val());
        });
         // we join the array separated by the comma 
        var selected;
        selected = chkArray.join(',') ;
        
         // check if there is selected checkboxes, by default the length is 1 as it contains one single comma 
        if(selected.length > 0)
        {
            return true;
        }else{
             alert("Please at least check one of the student"); 
             return false;
        }
    }
    /* $("#section").on('change', function () {
            var type = $("#section").val();
            if (type === "2") {
                $("#attendancereport").css('display', 'none');
            } else {
                $("#attendancereport").css("display", "block");
            }
        });*/
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
