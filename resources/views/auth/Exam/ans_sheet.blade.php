@extends('auth.Master.master')

@section('title','Add Questions')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
  @if(Session::has('success'))
    <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      {{ Session::get('success') }}
    </div>
  @endif

  @if(Session::has('error'))
    <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      {{ Session::get('error') }}
    </div>
  @endif 
    <div class="mangement-btn user-text">
       <h2><img src="{{ asset('images/event-management.png') }}" alt="">Student Answer Sheet</h2>      
    </div>
</div>

<div class="form-section">
{{--    <form name="add-student" role="form" method="GET" action="{{ url('save_question') }}" enctype="multipart/form-data" id="studentForm">--}}
        {{ csrf_field() }}

    @php
       $eam_stype = $student->Exm_Type;
    @endphp
        <div class="left-form">
            <input type="hidden" name="id" value="@if(isset($class_mod)){{$class_mod->Que_Cla_Id}}@endif">

            <div class="form-box">
                <div class="form-text">
                  <h5>Student Name <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                    <input type="text" name="class" id="class" class="branch-control" value="@if(isset($student->Std_Name)){{ $student->Std_Name }}@endif" readonly>
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                    <input type="text" name="class" id="class" class="branch-control" value="@if(isset($student->Cla_Id)){{ $student->Cla_Id }}@endif" readonly>
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                    <input type="text" name="section" id="section" class="branch-control" value="@if(isset($student->Cla_Sec_Id)){{ $student->Cla_Sec_Id }}@endif" readonly>
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Title* :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="section" id="section" class="branch-control" value="@if(isset($student->Exam_Title)){{ $student->Exam_Title }}@endif" readonly>
                    
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Subject* :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="subject" id="subject" class="branch-control" required value="@if(isset($student->Exam_Subject)){{ $student->Exam_Subject}}@endif">
                </div>
            </div>


            <!-- <div class="form-box" @if(($student->Exm_Type =0 )) style="display: none;" @endif>
                <div class="form-text">
                    <h5>Correct Answer</h5>
                </div>
                <div class="form-typ-box">
                    <input
                            id="subject"
                            class="branch-control"
                            value="{{$student->correct_ans}}">

                </div>
            </div> -->

            <div class="form-box">
                <div class="form-text">
                    <h5>Subject* :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="subject" id="subject" class="branch-control" required value="@if(isset($student->Exam_Subject)){{ $student->Exam_Subject}}@endif">
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Exam Type :</h5>
                </div>
                <div class="form-typ-box">

{{--                    {{ dd($student->Exm_Status) }}--}}
                    @if($student->Exm_Status == 1)
                    <input type="text" name="subject" id="subject" class="branch-control"
                           required value="Objective">
                        @endif

                    @if($student->Exm_Status == 0)
                    <input type="text" name="subject" id="subject" class="branch-control"
                           required value="Subjective">
                    @endif
                </div>
            </div>
  
        </div>
        <div class="clearfix"></div>


        <div class="progress_report_data" style="margin-top: 50px;">
            <div class="clearfix"></div>
            @if($eam_stype==1)
                <div class="table-form">
                    <div id="appendRow"></div>
                    <table id="ques">
                        @php $i=0; @endphp
                            @foreach($ques_mod as $data)
                                <tr id="question_row[{{$i}}]">
                                    <td class="td-left table-td-style">
                                        <h6 class="que_lable " id="qlable_{{$i}}" >Question:- {{($i+1)}}</h6>
    {{--                                    <input id="question[{{$i}}]" type="checkbox" name="question[]" value="question_{{$i}}" class="css-checkbox question-all delete-check"><label for="question[{{$i}}]" class="css-label table-ckeckbox delete-check"></label>--}}
                                        <textarea raw="4" name="que[]" class="text-style textarea-width" placeholder="Question {{($i+1)}}">@if(isset($data->Que_Question)){{ $data->Que_Question }}@endif</textarea><br>

                                        @if($data->Std_Ans=='A')
                                            <input id="answerA[{{$i}}]"
                                                   type="checkbox"
                                                   name="id1[]"
                                                   value="A_{{$i}}"
                                                   class="css-checkbox present-all check-style "
                                                    @if(isset($data->Que_Ans) && ($data->Std_Ans=='A')) checked @endif
                                            >
                                                <label for="answerA[{{$i}}]" class="css-label table-ckeckbox check-style">
    {{--                                                A--}}

                                                </label>

                                        <input type="text"
                                               name="option1[]"
                                               class="text-style"
                                               style="@if(($data->Que_Ans == 'A')) border-color: green  @else border-color: red !important; @endif"
                                               placeholder="Option A"
                                               value="@if(isset($data->Que_Option_1)){{ $data->Que_Option_1 }}@else '' @endif"
                                        >
                                        @endif

                                        @if($data->Std_Ans=='B')

                                        <input id="answerB[{{$i}}]"
                                               type="checkbox"
                                               name="id2[]"
                                               value="B_{{$i}}"
                                               class="css-checkbox present-all check-style1 "
                                               @if(isset($data->Que_Ans) && ($data->Std_Ans=='B')) checked @endif
                                        >
                                            <label for="answerB[{{$i}}]" class="css-label table-ckeckbox check-style1">
    {{--                                            B--}}
                                            </label>

                                        <input type="text"
                                               name="option2[]"
                                               class="text-style"
                                               style="@if(($data->Que_Ans == 'B')) border-color: green !important;  @else border-color: red !important; @endif"
                                               placeholder="Option B "
                                               value="@if(isset($data->Que_Option_2)){{ $data->Que_Option_2 }}@else '' @endif"
                                        >
                                        @endif

                                            @if($data->Std_Ans=='C')

                                        <input id="answerC[{{$i}}]"
                                               type="checkbox"
                                               name="id3[]" value="C_1"
                                               class="css-checkbox present-all check-style1"
                                               @if(isset($data->Que_Ans) && ($data->Std_Ans=='C')) checked @endif>

                                        <label for="answerC[{{$i}}]" class="css-label table-ckeckbox check-style1">
    {{--                                        C--}}
                                        </label>

                                        <input type="text"
                                               name="option3[]"
                                               class="text-style @if(($data->Que_Ans == 'C')) Yes @else Mo @endif"
                                               style="@if(($data->Que_Ans == 'C')) border-color: green !important;  @else border-color: red !important; @endif"
                                               placeholder="Option C"
                                               value="@if(isset($data->Que_Option_3)){{ $data->Que_Option_3 }}
                                               @else '' @endif"
                                        >
                                            @endif

                                        @if($data->Std_Ans=='D')

                                        <input id="answerD[{{$i}}]"
                                               type="checkbox"
                                               name="id4[]"
                                               value="D_1"
                                               class="css-checkbox present-all check-style1"
                                               @if(isset($data->Que_Ans) && ($data->Std_Ans=='D')) checked @endif
                                        >
                                        <label for="answerD[{{$i++}}]" class="css-label table-ckeckbox check-style1">
    {{--                                        D--}}
                                        </label>


                                        <input type="text"
                                               name="option4[]"
                                               class="text-style @if(($data->Que_Ans == 'D')) Yes @else Mo @endif"
                                               style="@if(($data->Que_Ans == 'D')) border-color: green !important;  @else border-color: red !important; @endif"
                                               placeholder="Option D "
                                               value="@if(isset($data->Que_Option_4)){{ $data->Que_Option_4 }}@else '' @endif"
                                        >
                                         @endif
                                        <br>

                                        <label class="css-label"
                                                style="padding: 5px 10px; background-color: white; margin-left: 70px; margin-top: 10px;">
                                            @if(($data->Que_Ans == 'A')) 
                                            Correct Answer: {{$data->Que_Ans}} {{$data->Que_Option_1}}
                                            @elseif(($data->Que_Ans == 'B'))
                                            Correct Answer: {{$data->Que_Ans}} {{$data->Que_Option_2}}
                                            @elseif(($data->Que_Ans == 'C'))
                                            Correct Answer: {{$data->Que_Ans}} {{$data->Que_Option_3}}
                                            @else
                                            Correct Answer: {{$data->Que_Ans}} {{$data->Que_Option_4}}
                                            @endif
                                        </label>

                                    </td>
                                </tr>
                            @php $i++; @endphp
                            @endforeach
                    </table>
                </div>
            @endif



            @if($eam_stype==0)
                <div id="subjective">

                    <div class="table-form">
                        <div id="appendSubRow"></div>


                        <form  method="POST" action="{{ url('save_mark') }}">
                            @csrf
                        <table id="subques">

                            <input type="hidden" name="stu_exam_id" value="{{ $student->Stu_Exam_Id }}">
                            <input type="hidden" name="exm_status" value="{{ $student->Exm_Status }}">
                            <input type="hidden" name="exam_title" value="{{ $student->Exam_Title }}">
                            <input type="hidden" name="exam_subject" value="{{ $student->Exam_Subject }}">
                            <input type="hidden" name="que_cla_id" value="{{ $student->Que_Cla_Id }}">
                            <input type="hidden" name="stu_id" value="{{ $student->Stu_Id }}">
                            @php $i=0; @endphp


                             @foreach($ques_mod as $data)
                                    <tr id="sub_question_row[{{$i}}]">
                                        <td class="td-left table-td-style">
                                            <div class="tbl_ques_wrapper">
                                              <h6 class="que_lable_one" id="q_sub_lable_{{$i}}"> Question:- {{($i+1)}}</h6>
                                              <input id="subquestion[{{$i}}]" type="checkbox" name="subquestion[]" value="subquestion_{{($i+1)}}" class="css-checkbox question-all delete-check"><label for="subquestion[{{$i}}]" class="css-label table-ckeckbox delete-check ques1_chkbx"></label>
                                              <textarea raw="4" name="subque[]" id="subtext_{{$i}}" class="text-style textarea-width" placeholder="Question {{($i+1)}}">@if(isset($data->Que_Question)){{ $data->Que_Question }}@endif</textarea><br>
                                            </div>
                                            
                                            <div class="table_ans_wrapper">
                                              <h6 class="que_lable_ans ans_wrapper" id="q_sub_lable_{{$i}}"> Answer:- {{($i+1)}}</h6>
                                              <textarea raw="4" name="subqueans[]" id="subtext_{{$i}}" class="text-style textarea-width txt_marign" placeholder="Question {{($i+1)}}">@if(isset($data->Que_Question)){{ $data->Std_Ans }}@endif</textarea><br>
                                            </div>

                                            <div id="sub_optionError_{{($i-1)}}"></div>

                                            <div class="marks_wrapper">
                                              <h6 class="que_lable_ans " id="mlable_{{$i}}"> Total Marks for Question {{($i+1)}}:- @if(isset($data->Que_Marks)){{ $data->Que_Marks }} @endif</h6>

                                              <h6 class="que_lable_ans " id="mlable_{{$i}}"> Give Marks for Answer:- {{($i+1)}}:- </h6>
                                              <input type="number" maxlength="3"
                                              name="submarks[]" class="text-style only_number_allow" placeholder="For Answer:- {{($i+1)}}" value="{{$data->Std_Disc_Ans}}"><br>
                                            </div>

                                        </td>
                                    </tr>

                                @php $i++; @endphp
                            @endforeach


    {{--                        @endif--}}
                        </table>

                        <div class="row">
                            <div class="form-btn branch-form-btn">
                                <input value="Save" type="submit" tabindex="11" id="submitForm">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            @endif



{{--            <div class="row">--}}
{{--                <div class="form-btn branch-form-btn">--}}
{{--                    <!-- <input value="save" type="submit" tabindex="11" id="submitForm"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="12" ></a> -->--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
{{--    </form>--}}



</div>
<style type="text/css">
    #ques{border-top: 2px solid #ff975b!important;}
  .que_lable_one{color:#ff975b; margin-left:30px; margin-top:30px; position: absolute;}
  .que_lable{color:#ff975b; margin-left:10px; margin-top:50px;}
  .que_mark {color: #ff975b;  margin-left: 10px;  margin-top: 10px;}

  .tbl_ques_wrapper { position: relative; }
  .ques1_chkbx { position: absolute; top: 100%; left: -20px; }
  .ans_wrapper {position: absolute;}
  .marks_wrapper { display: flex; flex-direction: row; align-items: center; }
  
  .que_lable_ans{color:#ff975b; margin-left:30px; margin-top:30px;}
    .table-form tr .table-td-style{background-color: #f7f7f7!important;color:#fff!important; border-left: 1px solid #fff; border-right: 1px solid #fff;}
    .check-style{margin-left: 80px!important;}
    .check-style1{margin-left: 30px!important;}
    .delete-check{margin-left: 20px!important; margin-top: -70px!important;}
    .text-style{width: 15%; height: auto; margin: 10px; display: inline-block; color: #ff975b; padding: 5px; border: 1px solid #ff975b; margin-top: 40px; margin-left: 30px;}
    .lable-style{display: inline-block;font-size: 16px;font-weight: 500;margin-left: 716px;text-transform: capitalize;color:#ff975b;}
    .textarea-width{width:80%!important; margin-left: 140px; margin-top:20px; border: 1px solid #ff975b;}
  .txt_marign{margin-left: 140px;}
    ::-webkit-input-placeholder {color: #ff975b; font-size:font-size: 14px;font-weight: 400;text-transform: capitalize;}
    :-ms-input-placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
    ::placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
</style>


@endsection

@section('footer')

@push('footer_script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('js/index.js') }}"></script>


@endpush
@section('footer_link_and_scripts')