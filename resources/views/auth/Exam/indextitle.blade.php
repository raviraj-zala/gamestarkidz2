@extends('auth.Master.master')

@section('title','Exam Management')

@section('site_header')

@section('sidebar')

@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('error') }}
            </div>
        @endif
    </div>
    <div class="line-1">
        <div class="mangement-btn user-text">
            <h2><img src="{{ asset('images/class-management.png') }}" alt="branch-img">Exam Title</h2>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="search-box">
                <input id="btnSearch" name="Cla_Name" placeholder="Search" type="search" />
            </div>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="main-btn">
                <ul>
                    @if(Auth::user()->Use_Type == "1" || Auth::user()->Use_Type == "5")
                        <li>
                            <input class="create"
                                   value="Create Exam Title"
                                   type="button" onClick="document.location.href='{{ url('create_exam_title') }}'">
                        </li>
                @endif

                    <li><input type="submit" value="Delete" class="delete" id="btnDelete" ></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="table-form">
        <table id="example2">
            <tbody><tr>
                <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
                    <label for="selectall" class="css-label table-ckeckbox">
                        <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
                <th class="td-left">Exam Title</th>
                <th class="td-left">Exam Year</th>
                <th>Status</th>
                <th>Edit</th>
            </tr>

            @if($title)
                <?php $i=1; ?>
                @forelse($title as $data)
                    <tr>

{{--                        <td><input id="title{{ $i }}" type="checkbox"--}}
{{--                                   onClick="checkbox_checked()" name="titles[]"--}}
{{--                                   value="{{ $data->exam_id }}" class="css-checkbox check-all"><label for="title{{ $i }}" class="css-label table-ckeckbox"></label></td>--}}
                        <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->exam_id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>

                        <td class="td-left">{{ $data->exam_title}}</td>
                        <td class="td-left">{{ $data->exam_year}}</td>
                        <td>
                            @if($data->exam_status == 1)
                                Active
                            @elseif($data->exam_status == 0)
                                In-Active
                            @endif
                        </td>
                        <td><a href="{{ url('edit_exam_title',$data->exam_id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a></td>
                    </tr>
                    <?php $i++; ?>
                @empty
                    <tr><td colspan="7" align="center"> No Data Found</td></tr>
                @endforelse
            @else
                No Data Found
            @endif


            </tbody></table>
    </div>


    <div class="paggination-section">
        @if ($title->lastPage() > 1)
            <ul>
                @if ($title->currentPage() != 1 && $title->lastPage() >= 5)
                    <li><a href="{{ $title->url($title->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
                @endif
                @if($title->currentPage() != 1)
                    <li>
                        <a  href="{{ $title->url($title->currentPage()-1) }}" >
                            <
                        </a>
                    </li>
                @endif
                @for($i = max($title->currentPage()-2, 1); $i <= min(max($title->currentPage()-2, 1)+4,$title->lastPage()); $i++)
                    <li>
                        <a class="{{ ($title->currentPage() == $i) ? 'active' : '' }}" href="{{ $title->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor
                @if ($title->currentPage() != $title->lastPage())
                    <li>
                        <a href="{{ $title->url($title->currentPage()+1) }}" >
                            >
                        </a>
                    </li>
                @endif
                @if ($title->currentPage() != $title->lastPage() && $title->lastPage() >= 5)
                    <li>
                        <a href="{{ $title->url($title->lastPage()) }}" >
                            >>
                        </a>
                    </li>
                @endif
            </ul>
        @endif
    </div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
</script>

<script type="text/javascript">
$('#btnSearch').on('change',function(){
    var Cla_Name = $(this).val();
    if(Cla_Name!=""){
        $.ajax({
            type : "POST",
            url : "{{ url('search_class') }}",
            data : { 
                    _token:     '{{ csrf_token() }}',
                    Cla_Name : Cla_Name
                },
            success : function(data){
                window.location.reload();
            },
            error: function(error){
            }
        });
    }else{
        window.location = "{{ url('Class Mgmt.') }}"
    }
});


$('#btnDelete').on('click',function(){
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete=confirm('Delete selected records???');
        if(ch_delete){
            var ids = [];
            $('input:checked').each(function(i){
                ids[i] = $(this).val();
            });
            $.ajax({
                type : "POST",
                url : "{{ url('delete_exam') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cls_Id : ids
                },
                success : function(data){
                    // $("input[type='checkbox']").each(function(i){
                    //     $(this).prop('checked', false);
                    // });
                    window.location.reload();
                }
            });
        }
    }else{
        alert('Please select atleast one record!');
    }
});



</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')

<!-- http://www.codeinhouse.com/how-to-delete-multiple-rows-using-checkbox-in-php-laravel-using-eloquent-javascript/ -->