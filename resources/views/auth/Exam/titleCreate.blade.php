@extends('auth.Master.master')

@section('title','Add Questions')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
       <h2><img src="{{ asset('images/event-management.png') }}" alt="">@if(isset($class_mod)) Edit @else Add @endif Exam Title</h2>
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif
<style>
    .ui-datepicker-trigger{
        border:none;
        background:none;
    }
</style>
<div class="form-section">
    <form name="add-student" role="form" method="POST" action="{{ url('save_exam') }}" enctype="multipart/form-data" id="studentForm">
        {{ csrf_field() }}
        <div class="left-form">
            <input type="hidden" name="id" value="@if(isset($class_mod)){{$class_mod->Que_Cla_Id}}@endif">
{{--            <div class="form-box">--}}
{{--                <div class="form-text">--}}
{{--                  <h5>Branch * :</h5>--}}
{{--                </div>   --}}
{{--                <div class="form-typ-box">--}}
{{--                    <div class="btn-group bootstrap-select show-tick form-control">--}}
{{--                        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">--}}
{{--                            @if(isset($class_mod))--}}
{{--                                @foreach($branch as $data)--}}
{{--                                    <option value="{{ $data->Brn_Id }}" @if(isset($class_mod)) @if($class_mod->Cla_Bra_Id == $data->Brn_Id) selected @endif @endif>{{ $data->Brn_Name }}</option>--}}
{{--                                @endforeach--}}
{{--                            @else--}}
{{--                                <option value="none">-- Select --</option>--}}
{{--                                @foreach($branch as $data)--}}
{{--                                    <option value="{{ $data->Brn_Id }}" >{{ $data->Brn_Name }}</option>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </select>--}}
{{--                        @if (Session::has('branch'))--}}
{{--                            <label class="error" for="branch">--}}
{{--                                <span class="text-danger">--}}
{{--                                    {{ Session::get('branch') }}--}}
{{--                                </span>--}}
{{--                            </label>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    <div id="branchError"></div>--}}
{{--                </div>    --}}
{{--            </div>--}}

{{--            <div class="form-box">--}}
{{--                <div class="form-text">--}}
{{--                  <h5>Class <span>*</span> :</h5>--}}
{{--                </div>   --}}
{{--                <div class="form-typ-box ">--}}
{{--                <div class="btn-group bootstrap-select show-tick form-control">--}}
{{--                <div class="select-box" >--}}
{{--                  <select class="branch-cls" name="class" id="class" tabindex="2">--}}
{{--                    @if(isset($class_mod))--}}
{{--                        @foreach($class_id as $data)--}}
{{--                            <option value="{{ $data->Cla_branchId }}" @if($class_mod->Cla_Id == $data->Cla_branchId) selected @endif>{{ $data->Cla_Class }}</option>--}}
{{--                        @endforeach--}}
{{--                    @else--}}
{{--                        <option value="none">-- Select--</option>--}}
{{--                    @endif--}}
{{--                  </select>--}}
{{--                </div>--}}
{{--                </div>--}}
{{--                <div id="classError"></div>--}}
{{--                @if (Session::has('class'))--}}
{{--                <label class="error" for="class">--}}
{{--                    <span class="text-danger">--}}
{{--                        {{ Session::get('class') }}--}}
{{--                    </span>--}}
{{--                    </label>--}}
{{--                @endif--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="form-box">--}}
{{--                <div class="form-text">--}}
{{--                  <h5>Section <span>*</span> :</h5>--}}
{{--                </div>   --}}
{{--                <div class="form-typ-box ">--}}
{{--                    <div class="btn-group bootstrap-select show-tick form-control">--}}
{{--                        <div class="select-box" >--}}
{{--                            <select class="class-sec" name="section" id="section" tabindex="3">--}}
{{--                                @if(isset($class_mod))--}}
{{--                                    @foreach($class_id as $data)--}}
{{--                                        <option value="{{ $data->Cla_Section }}" @if($class_mod->Cla_Sec_Id  == $data->Cla_Bra_Id) selected @endif>{{ $data->Cla_Section }}</option>--}}
{{--                                    @endforeach--}}
{{--                                @else--}}
{{--                                    <option value="none">-- Select--</option>--}}
{{--                                @endif--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div id="sectionError"></div>--}}
{{--                    @if (Session::has('section'))--}}
{{--                        <label class="error" for="section">--}}
{{--                            <span class="text-danger">--}}
{{--                                {{ Session::get('section') }}--}}
{{--                            </span>--}}
{{--                        </label>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="form-box">
                <div class="form-text">
                    <h5>Exam Title* :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="title" id="title" class="branch-control" value="@if(isset($class_mod)){{ $class_mod->Exam_Title }}@else{{ old('title') }}@endif">
                    <div id="titleError"></div>
                    @if ($errors->has('title'))
                        <label class="error" for="title">
                            <span class="text-danger">
                                {{ $errors->first('title') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>


            <div class="form-box">
                <div class="form-text">
                    <h5>Year<span>*</span> :</h5>
                </div>
                <div class="form-typ-box" >

                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="date" class="form-control"  id="datepicker" autocomplete="off"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div id="dateError"></div>
                    @if (Session::has('date'))
                        <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>



            <div class="form-box">
                <div class="form-text">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="4">
                        <option value="1" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==1)) selected  @endif>Active</option>
                        <option value="0" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==0)) selected  @endif>In-Active</option>
                    </select>
                    </div>
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                </div>    
            </div>



        </div>
        <div class="clearfix"></div>
        <div class="progress_report_data">

            <div class="clearfix"></div>

            <div class="row">
                <div class="form-btn branch-form-btn">
                    <input value="save" type="submit" tabindex="11" id="submitForm"><a href="{{ url("/Exam Mgmt.") }}"><input value="Cancel" type="button" tabindex="12" ></a>
                </div>
            </div>
        </div>
    </form>
</div>
<style type="text/css">
    .que_lable{color: #ff975b;  margin-left: 10px;  margin-top: 50px;}
    .error_text{color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;display: none; margin-left: 60px;}
    #ques{border-top: 2px solid #ff975b!important;}
    .table-form tr .table-td-style{background-color: #f7f7f7!important;color:#fff!important; border-left: 1px solid #fff; border-right: 1px solid #fff;}
    .check-style{margin-left: 80px!important;}
    .check-style1{margin-left: 30px!important;}
    .delete-check{margin-left: 20px!important; margin-top: -70px!important;}
    .text-style{width: 15%; height: auto; margin: 10px; display: inline-block; color: #ff975b; padding: 5px; border: 1px solid #ff975b;}
    .lable-style{display: inline-block;font-size: 16px;font-weight: 500;margin-left: 716px;text-transform: capitalize;color:#ff975b;}
    .textarea-width{width:90%!important; margin-left: 10px; margin-top: 20px; border: 1px solid #ff975b;}
    ::-webkit-input-placeholder {color: #ff975b; font-size:font-size: 14px;font-weight: 400;text-transform: capitalize;}
    :-ms-input-placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
    ::placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
</style>
@endsection

@section('footer')

@push('footer_script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('js/index.js') }}"></script>
  <script src="{{ url('js/clockpicker.js') }}"></script>


{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>--}}
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>



  <script type="text/javascript">
$(document).ready(function(){




    var brnErr,clsErr,secErr;
    var i=2;
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^abcdABCD]/g, '');
    });





    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        var title = $("#title").val();
        var subject = $("#subject").val();
        var date = $('#datepicker').val();
        var duedate = $('#duedatepicker').val();
        var time = $('#time').val();
        
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault();
            return false;
        }else{
            if(class_val=="none")
            {
                alert("Please Select  Class...");
                e.preventDefault();
                return false;
                
            }else{
                if(section=="none"){
                    alert("Please Select  Section...");
                    e.preventDefault();
                    return false;
                }else{
                    if(title=="none"){
                        alert("Please Enter Title...");
                        e.preventDefault();
                        return false;
                    }else{
                        if(subject=="none"){
                            alert("Please Enter Subject...");
                            e.preventDefault();
                            return false;
                        }else{
                            if(date==""){
                                $("#dateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Date.</span></label>");
                                e.preventDefault();
                                return false;
                            }else {
                                if(duedate==""){
                                    $("#duedateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Due Date.</span></label>");
                                    e.preventDefault();
                                    return false;
                                }else {
                                    if(time==""){
                                        $("#timeError").html("<label for='time' class='error'><span class='text-danger'>Please Enter Time.</span></label>");
                                        e.preventDefault();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    });


    $("#submitForm").click(function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        var title = $("#title").val();
        var subject = $("#subject").val();
        var date = $('#datepicker').val();
        var duedate = $('#duedatepicker').val();
        var time = $('#time').val();
        if(branch=="none"){
            $("#branchError").html("<label for='branch' class='error'><span class='text-danger'>Please select branch.</span></label>");
            e.preventDefault();
            return false;
        }else{
            $("#branchError").html("");
            if(class_val=="none")
            {
                $("#classError").html("<label for='class' class='error'><span class='text-danger'>Please select class.</span></label>");
                e.preventDefault();
                return false;
                
            }else{
                if(section=="none"){
                    $("#sectionError").html("<label for='branch' class='error'><span class='text-danger'>Please select section.</span></label>");
                    e.preventDefault();
                    return false;
                }else {
                    if(title==""){
                        $("#titleError").html("<label for='title' class='error'><span class='text-danger'>Please enter title.</span></label>");
                        e.preventDefault();
                        return false;
                    }else {
                        if(subject==""){
                            $("#subjectError").html("<label for='subject' class='error'><span class='text-danger'>Please enter subject.</span></label>");
                            e.preventDefault();
                            return false;
                        }else {
                            var sel = document.getElementById('exam_type');
                            if (sel.options[sel.selectedIndex].value == 1) {

                                $("textarea:input[name='que[]']").each(function( key, value ){
                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                        $(this).css("border","1px solid red");
                                        $(this).focus();
                                        e.preventDefault();
                                        return false;
                                    }else {
                                        $(this).css("border","1px solid #ff975b");
                                        $("input:text[name='option1[]']").each(function( key, value ){
                                            if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                $(this).css("border","1px solid red");
                                                $(this).focus();
                                                e.preventDefault();
                                                return false;
                                            }else {
                                                $(this).css("border","1px solid #ff975b");
                                                $("input:text[name='option2[]']").each(function( key, value ){
                                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                        $(this).css("border","1px solid red");
                                                        $(this).focus();
                                                        e.preventDefault();
                                                        return false;
                                                    }else {
                                                        $(this).css("border","1px solid #ff975b");
                                                        $("input:text[name='option3[]']").each(function( key, value ){
                                                            if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                                $(this).css("border","1px solid red");
                                                                $(this).focus();
                                                                e.preventDefault();
                                                                return false;
                                                            }else {
                                                                $(this).css("border","1px solid #ff975b");
                                                                $("input:text[name='option4[]']").each(function( key, value ){
                                                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                                        $(this).css("border","1px solid red");
                                                                        $(this).focus();
                                                                        e.preventDefault();
                                                                        return false;
                                                                    }else{
                                                                        if(date==""){
                                                                            $("#dateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Date.</span></label>");
                                                                            e.preventDefault();
                                                                            return false;
                                                                        }else {
                                                                            if(duedate==""){
                                                                                $("#duedateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Due Date.</span></label>");
                                                                                e.preventDefault();
                                                                                return false;
                                                                            }else {
                                                                                if(time==""){
                                                                                    $("#timeError").html("<label for='time' class='error'><span class='text-danger'>Please Enter Time.</span></label>");
                                                                                    e.preventDefault();
                                                                                    return false;
                                                                                }else{
                                                                                    $(this).css("border","1px solid #ff975b");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else if(sel.options[sel.selectedIndex].value == 0){

                                $("textarea:input[name='subque[]']").each(function( key, value ){
                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                        $(this).css("border","1px solid red");
                                        $(this).focus();
                                        e.preventDefault();
                                        return false;
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }


    });

    $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                console.log(data);
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select--</option>');
                     $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });

    $("#class").on('change',function(){
        var idAndClass = $(this).val();
        var classIdName = idAndClass.split(",");
        console.log(classIdName);
        $.ajax({
            type : "POST",
            url : "{{ url('student/getSection') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Class : classIdName[1],
                Cla_Bra_Id : classIdName[0]
            },
            dataType : "JSON",
            success : function(data){
                    $(".class-sec option").each(function() {
                        $(this).remove();
                    });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                     });
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#section").append(items);
            },
            error :function(error){
                console.log(error);
            }
        });
    });


    $(function() {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY',
            });

    });
});
</script>


@endpush
@section('footer_link_and_scripts')