@extends('auth.Master.master')

@section('title','Exam Management')

@section('site_header')

@section('sidebar')

@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('error') }}
            </div>
        @endif
    </div>


<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('images/class-management.png') }}" alt="branch-img">Exam Management</h2>      
    </div>
</div>

<div class="clearfix"></div>
    <div class="row">

        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="search-box">
                <input id="btnSearch" name="Cla_Name" placeholder="Search" type="search" />
          </div>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="main-btn">
            <ul>
                @if(Auth::user()->Use_Type == "1" || Auth::user()->Use_Type == "5")
                <li>
                    <input class="create" value="create question" type="button" onClick="document.location.href='{{ url('create_question') }}'">
                </li>
                @endif
                              <li><input type="submit" value="Delete" class="delete" id="btnDelete" ></li>
            </ul>
          </div>
        </div>
      </div>

<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Title</th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Student</th>
      <th class="td-left">Student Given Exam</th>
      <th class="td-left">Section</th>
      <th class="td-left">Exam Type</th>
      <th>Status</th>
      <th>Edit</th>
      <th>View</th>
    </tr>

@if($class)
    <?php $i=1; ?>
    @forelse($class as $data)
    <tr>
        @php
            $Cla_Id=$data->Cla_Id;
            $Cla_Id = explode(',', $Cla_Id);
            $Cla_Bra_Id = $Cla_Id[0];
            $Cla_Class = $Cla_Id[1];
            $Std_Cla_Id = \App\Model\ClassTbl::select('Cla_Id')->where('Cla_Bra_Id',$Cla_Bra_Id)->where('Cla_Class',$Cla_Class)->value('Cla_Id');
            $exam_student = \App\Model\Student::where('Std_Cla_Id',$Std_Cla_Id)->count();
            $student_exam = \App\Model\StudentExamAns::where('stu_exam_ans_tbl.Que_Cla_Id',$data->Que_Cla_Id)->count();
            $not_student_exam = $exam_student - $student_exam;
        @endphp
        <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Que_Cla_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
        <td class="td-left">{{ $data->exam_title."- ".$data->Exam_Subject }}</td>
        <td class="td-left">{{ $data->Brn_Name }}</td>
        <td class="td-left">{{ $Cla_Class }}</td>
        <td class="td-left">{{ $exam_student }}</td>
        <td class="td-left">{{ $student_exam }}</td>
        <td >{{ $data->Cla_Sec_Id }}</td>
        <td>
             @if($data->Exm_Type == 1)
                Objective
            @elseif($data->Exm_Type == 0)
                Subjective
            @endif
        </td>
        <td>
            @if($data->Que_Cla_Status == 1)
                Active
            @elseif($data->Que_Cla_Status == 0)
                In-Active
            @endif
        </td>
        <td><a href="{{ url('edit_question',$data->Que_Cla_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a></td>
        <td><a href="{{ url('view_student_record',$data->Que_Cla_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
    <?php $i++; ?>
    @empty
        <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
@else
    No Data Found
@endif    

  
  </tbody></table>
</div>

<div class="paggination-section">
@if ($class->lastPage() > 1)
    <ul>
        @if ($class->currentPage() != 1 && $class->lastPage() >= 5)
            <li><a href="{{ $class->url($class->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($class->currentPage() != 1)
            <li>
                <a  href="{{ $class->url($class->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($class->currentPage()-2, 1); $i <= min(max($class->currentPage()-2, 1)+4,$class->lastPage()); $i++)
                <li>
                    <a class="{{ ($class->currentPage() == $i) ? 'active' : '' }}" href="{{ $class->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($class->currentPage() != $class->lastPage())
            <li>
                <a href="{{ $class->url($class->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($class->currentPage() != $class->lastPage() && $class->lastPage() >= 5)
            <li>
                <a href="{{ $class->url($class->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
</script>

<script type="text/javascript">
$('#btnSearch').on('change',function(){
    var Cla_Name = $(this).val();
    if(Cla_Name!=""){
        $.ajax({
            type : "POST",
            url : "{{ url('search_class') }}",
            data : { 
                    _token:     '{{ csrf_token() }}',
                    Cla_Name : Cla_Name
                },
            success : function(data){
                window.location.reload();
            },
            error: function(error){
            }
        });
    }else{
        window.location = "{{ url('Class Mgmt.') }}"
    }
});


$('#btnDelete').on('click',function(){
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete=confirm('Delete selected records???');
        if(ch_delete){
            var ids = [];
            $('input:checked').each(function(i){
                ids[i] = $(this).val();
            });   
            $.ajax({
                type : "POST",
                url : "{{ url('delete_question') }}",
                data : { 
                        _token:     '{{ csrf_token() }}',
                        Cls_Id : ids
                    },
                success : function(data){
                    // $("input[type='checkbox']").each(function(i){
                    //     $(this).prop('checked', false);
                    // });
                    window.location.reload();
                }
            });
        }
    }else{
        alert('Please select atleast one record!');
    }
});

$('#titleBtnDelete').on('click',function(){
    var j = $("input:checked").length;
    if (j > 0)
    {
        var til_delete=confirm('Delete selected records?');
        if(til_delete){
            var titles = [];
            $('input:checked').each(function(i){
                titles[i] = $(this).val();
            });
            $.ajax({
                type : "POST",
                url : "{{ url('delete_exam') }}",
                data : {
                        _token:     '{{ csrf_token() }}',
                        Exm_Id : titles
                    },
                success : function(data){

                    window.location.reload();
                }
            });
        }
    }else{
        alert('Please select atleast one record!');
    }
});

$("input.btnActInact").on('click',function(){
    var n = $("input:checked").length;
    var val = $(this).val();
    var ids = [];
    var ch_sts=false;
    var status=null;
    if( n >0 )
    {
        $('input:checked').each(function(i){ ids[i] = $(this).val(); });
        if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
        if(status==1) { ch_sts=confirm('Activate selected records???'); }
        else if(status==0) { ch_sts=confirm('In-activate selected records???'); }

        if(ch_sts){
            $.ajax({
                type: "POST",
                url: "{{ url('classStatus') }}",
                data: { 
                    id: ids, 
                    status: status,
                    _token:     '{{ csrf_token() }}'
                },
                success: function(data) {
                    $("input[type='checkbox']").each(function(i){
                            $(this).prop('checked', false);
                    });
                    window.location.reload();
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }
    }
    else
    {
        alert('Please select atleast one record!');
    }
});
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')

<!-- http://www.codeinhouse.com/how-to-delete-multiple-rows-using-checkbox-in-php-laravel-using-eloquent-javascript/ -->