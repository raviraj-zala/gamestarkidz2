@extends('auth.Master.master')

@section('title','Add Questions')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
       <h2><img src="{{ asset('images/event-management.png') }}" alt="">@if(isset($class_mod)) Edit @else Add @endif Questions</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif
<style>
    .ui-datepicker-trigger{
        border:none;
        background:none;
    }
</style>
<div class="form-section">
    <form name="add-student" role="form" method="GET" action="{{ url('save_question') }}" enctype="multipart/form-data" id="studentForm">
        {{ csrf_field() }}
        <div class="left-form">
            <input type="hidden" name="id" value="@if(isset($class_mod)){{$class_mod->Que_Cla_Id}}@endif">
            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                            @if(isset($class_mod))
                                @foreach($branch as $data)
                                    <option value="{{ $data->Brn_Id }}" @if(isset($class_mod)) @if($class_mod->Cla_Bra_Id == $data->Brn_Id) selected @endif @endif>{{ $data->Brn_Name }}</option>
                                @endforeach
                            @else
                                <option value="none">-- Select --</option>
                                @foreach($branch as $data)
                                    <option value="{{ $data->Brn_Id }}" >{{ $data->Brn_Name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if (Session::has('branch'))
                            <label class="error" for="branch">
                                <span class="text-danger">
                                    {{ Session::get('branch') }}
                                </span>
                            </label>
                        @endif
                    </div>
                    <div id="branchError"></div>
                </div>    
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    @if(isset($class_mod))
                        @foreach($class_id as $data)
                            <option value="{{ $data->Cla_branchId }}" @if($class_mod->Cla_Id == $data->Cla_branchId) selected @endif>{{ $data->Cla_Class }}</option>
                        @endforeach
                    @else
                        <option value="none">-- Select--</option>
                    @endif
                  </select>
                </div>
                </div>
                <div id="classError"></div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select class="class-sec" name="section" id="section" tabindex="3">
                                @if(isset($class_mod))
                                    @foreach($class_id as $data)
                                        <option value="{{ $data->Cla_Section }}" @if($class_mod->Cla_Sec_Id  == $data->Cla_Bra_Id) selected @endif>{{ $data->Cla_Section }}</option>
                                    @endforeach
                                @else
                                    <option value="none">-- Select--</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div id="sectionError"></div>
                    @if (Session::has('section'))
                        <label class="error" for="section">
                            <span class="text-danger">
                                {{ Session::get('section') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Exam Year* :</h5>
                </div>
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select  name="exam_year" id="exam_year" tabindex="3">
                                        <option value="none">-- Select --</option>
                                        @foreach($title as $data)
                                            <option value="{{ $data->exam_year }}" >{{ $data->exam_year }}</option>
                                        @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="titleError"></div>
                    @if (Session::has('exam_year'))
                        <label class="error" for="title">
                            <span class="text-danger">
                                {{ Session::get('exam_year') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Title* :</h5>
                </div>
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select class="exam_title"  name="title" id="title" tabindex="3">
                                        <option value="none">-- Select --</option>
                                        @foreach($title as $data)
                                            <option value="{{ $data->exam_id }}" >{{ $data->exam_title }}</option>
                                        @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="titleError"></div>
                    @if (Session::has('title'))
                        <label class="error" for="title">
                            <span class="text-danger">
                                {{ Session::get('title') }}
                            </span>
                        </label>
                    @endif
                </div>

            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Subject* :</h5>
                </div>

                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select class="class-sub" name="subject" id="subject" tabindex="3">

                                @if(isset($class_mod))
                                        <option value=" "> </option>
                                        <option  name="subject" id="subject_field" value="none">Subject Not Found</option>
                                @else
                                    <option value="none">-- Select--</option>
                                @endif

                            </select>
                        </div>
                    </div>
                    <div id="subjectError"></div>
                    @if (Session::has('subject'))
                        <label class="error" for="subject">
                            <span class="text-danger">
                                {{ Session::get('subject') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>



            <div class="form-box">
                <div class="form-text">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="4">
                        <option value="1" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==1)) selected  @endif>Active</option>
                        <option value="0" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==0)) selected  @endif>In-Active</option>
                    </select>
                    </div>
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                </div>    
            </div>


            <div class="form-box">
                <div class="form-text">
                    <h5>Start Date<span>*</span> :</h5>
                </div>
                <div class="form-typ-box" >
{{--                    <input type="text"--}}
{{--                           name="date"--}}
{{--                           id="datepicker"--}}
{{--                           tabindex="3"--}}
{{--                           readonly>--}}
{{--                    <span class="input-group-addon">--}}
{{--                        <span class="glyphicon glyphicon-calendar"></span>--}}
{{--                    </span>--}}

                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="date" class="form-control"  id="datepicker" autocomplete="off"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div id="dateError"></div>
                    @if (Session::has('date'))
                        <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>End Date<span>*</span> :</h5>
                </div>
                <div class="form-typ-box">
{{--                    <input type="text"--}}
{{--                           name="duedate"--}}
{{--                           id="duedatepicker"--}}
{{--                           tabindex="3"--}}
{{--                           readonly>--}}
{{--                    <i class='fa fa-calendar'></i>--}}
{{--                    <span class="input-group-addon">--}}
{{--                        <span class="glyphicon glyphicon-calendar"></span>--}}
{{--                    </span>--}}

                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' name="duedate" class="form-control"  id="duedatepicker" autocomplete="off"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div id="duedateError"></div>
                    @if (Session::has('date'))
                        <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Exam Type<span>*</span> :</h5>
                </div>
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                        <select id="exam_type" class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('exam_type') ? 'is-invalid' : '' }}" name="exam_type" required tabindex="4">
                            <option value="1" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==1)) selected  @endif>Objective</option>
                            <option value="0" @if(isset($class_mod) && ($class_mod->Que_Cla_Status==0)) selected  @endif>Subjective</option>
                        </select>
                    </div>
                    @if ($errors->has('exam_type'))
                        <label class="error" for="exam_type">
                            <span class="invalid-feedback">
                                {{ $errors->first('exam_type') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>



            <div class="form-box" >
                <div class="form-text">
                    <h5>Duration <span>*</span> :</h5>
                </div>

                <div class="form-typ-box datetimepicker3">

                    <input type="text"
{{--                           class=""--}}
                           name="time"
                           tabindex="3"
                           id="time"
                           autocomplete="off"
                           >
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                    <div id="timeError"></div>

                    @if (Session::has('time'))
                        <label class="error" for="time">
                        <span class="text-danger">
                            {{ Session::get('time') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>




        </div>
        <div class="clearfix"></div>
        <div class="progress_report_data">
            <div class="line-1">        
                <div class="mangement-btn user-text">
                    <h2><img src="{{ asset('images/report_img.png') }}" alt="">Questions</h2>      
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="objective">
                <div class="row">
                    <!-- <div class="col-md-4 col-sm-4 col-xs-12"></div> -->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="main-btn">
                            <ul>
                                <li>
                                    <input class="create" value="Add" type="button" id="addQuestion">
                                </li>
                                <li>
                                    <input type="button" value="delete" class="delete" id="deleteQuestion" >
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="table-form">
                    <div id="appendRow"></div>
                    <table id="ques">
                        @php $i=0; @endphp
                        @if(!isset($class_mod))
                            <tr id='question_row[{{$i}}]'>
                                <td class="td-left table-td-style">
                                    <h6 class="que_lable " id="qlable_{{$i}}">Question:- {{($i+1)}}</h6>
                                    <input id="question[{{$i}}]" type="checkbox" name="question[]" value="question_{{$i}}" class="css-checkbox question-all delete-check"><label for="question[{{$i}}]" class="css-label table-ckeckbox delete-check"></label>
                                    <textarea raw="4" name="que[]" class="text-style textarea-width" id="text_{{$i}}" placeholder="Question {{($i+1)}}"></textarea><br>
                                    <input id="answerA[{{$i}}]" type="checkbox" name="id1[]" value="A_{{$i}}" class="css-checkbox present-all check-style"><label for="answerA[{{$i}}]" class="css-label table-ckeckbox check-style"></label>
                                    <input type="text" name="option1[]" class="text-style" placeholder="Option A">
                                    <input id="answerB[{{$i}}]" type="checkbox" name="id2[]" value="B_{{$i}}" class="css-checkbox present-all check-style1"><label for="answerB[{{$i}}]" class="css-label table-ckeckbox check-style1"></label>
                                    <input type="text" name="option2[]" class="text-style" placeholder="Option B">
                                    <input id="answerC[{{$i}}]" type="checkbox" name="id3[]" value="C_{{$i}}" class="css-checkbox present-all check-style1"><label for="answerC[{{$i}}]" class="css-label table-ckeckbox check-style1"></label>
                                    <input type="text" name="option3[]" class="text-style" placeholder="Option C">
                                    <input id="answerD[{{$i}}]" type="checkbox" name="id4[]" value="D_{{$i}}" class="css-checkbox present-all check-style1"><label for="answerD[{{$i}}]" class="css-label table-ckeckbox check-style1"></label>
                                    <input type="text" name="option4[]" class="text-style" placeholder="Option D"><br>
                                    <h6 class="que_mark " id="mlable_{{$i}}">Marks:- </h6>
                                    <input type="number" maxlength="3" name="objmarks[]" class="text-style only_number_allow" placeholder="For Question:- {{($i+1)}}"><br>
                                    <div id="optionError_{{$i}}"></div>
                                </td>
                            </tr>
                        @else
                            @foreach($ques_mod as $data)
                                <tr id="question_row[{{$i}}]">
                                    <td class="td-left table-td-style">
                                        <h6 class="que_lable" id="qlable_{{$i}}">Question:- {{($i+1)}}</h6>
                                        <input id="question[{{$i}}]" type="checkbox" name="question[]" value="question_{{($i+1)}}" class="css-checkbox question-all delete-check"><label for="question[{{$i}}]" class="css-label table-ckeckbox delete-check"></label>
                                        <textarea raw="4" name="que[]" id="text_{{$i}}" class="text-style textarea-width" placeholder="Question {{($i+1)}}">@if(isset($data->Que_Question)){{ $data->Que_Question }}@endif</textarea><br>
                                        <input id="answerA[{{$i}}]" type="checkbox" name="id1[]" value="A_{{$i}}" class="css-checkbox present-all check-style" @if(isset($data->Que_Ans) && ($data->Que_Ans=='A')) checked @endif><label for="answerA[{{$i}}]" class="css-label table-ckeckbox check-style"></label>
                                        <input type="text" name="option1[]" class="text-style" placeholder="Option A" value="@if(isset($data->Que_Option_1)){{ $data->Que_Option_1 }}@else '' @endif">
                                        <input id="answerB[{{$i}}]" type="checkbox" name="id2[]" value="B_{{$i}}" class="css-checkbox present-all check-style1" @if(isset($data->Que_Ans) && ($data->Que_Ans=='B')) checked @endif><label for="answerB[{{$i}}]" class="css-label table-ckeckbox check-style1"></label>
                                        <input type="text" name="option2[]" class="text-style" placeholder="Option B" value="@if(isset($data->Que_Option_2)){{ $data->Que_Option_2 }}@else '' @endif">
                                        <input id="answerC[{{$i}}]" type="checkbox" name="id3[]" value="C_{{$i}}" class="css-checkbox present-all check-style1" @if(isset($data->Que_Ans) && ($data->Que_Ans=='C')) checked @endif><label for="answerC[{{$i}}]" class="css-label table-ckeckbox check-style1"></label>
                                        <input type="text" name="option3[]" class="text-style" placeholder="Option C" value="@if(isset($data->Que_Option_3)){{ $data->Que_Option_3 }}@else '' @endif">
                                        <input id="answerD[{{$i}}]" type="checkbox" name="id4[]" value="D_{{$i}}" class="css-checkbox present-all check-style1" @if(isset($data->Que_Ans) && ($data->Que_Ans=='D')) checked @endif><label for="answerD[{{$i++}}]" class="css-label table-ckeckbox check-style1"></label>
                                        <input type="text" name="option4[]" class="text-style" placeholder="Option D" value="@if(isset($data->Que_Option_4)){{ $data->Que_Option_4 }}@else '' @endif"><br>
                                        <h6 class="que_mark " id="mlable_{{$i}}">Marks:- </h6>
                                        <input type="number" maxlength="3" name="objmarks[]" class="text-style only_number_allow" placeholder="For Question:- {{($i+1)}}"><br>

                                        <div id="optionError_{{($i-1)}}"></div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>


            <div id="subjective" style="display: none">
                <div class="row">
                    <!-- <div class="col-md-4 col-sm-4 col-xs-12"></div> -->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="main-btn">
                            <ul>
                                <li>
                                    <input class="create" value="Add" type="button" id="addSubQuestion">
                                </li>
                                <li>
                                    <input type="button" value="delete" class="delete" id="deleteSubQuestion" >
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="table-form">
                    <div id="appendSubRow"></div>
                    <table id="subques">
                        @php $i=0; @endphp
                        @if(!isset($class_mod))
                            <tr id='sub_question_row[{{$i}}]'>
                                <td class="td-left table-td-style">
                                    <h6 class="que_lable " id="q_sub_lable_{{$i}}">Question:- {{($i+1)}}</h6>
                                    <input id="subquestion[{{$i}}]" type="checkbox" name="subquestion[]" value="subquestion_{{$i}}" class="css-checkbox question-all delete-check"><label for="subquestion[{{$i}}]" class="css-label table-ckeckbox delete-check"></label>
                                    <textarea raw="4" name="subque[]" class="text-style textarea-width" id="subtext_{{$i}}" placeholder="Question {{($i+1)}}"></textarea><br>
                                    <h6 class="que_mark " id="mlable_{{$i}}">Marks:- </h6>
                                    <input type="number" maxlength="3" name="submarks[]" class="text-style only_number_allow" placeholder="For Question:- {{($i+1)}}"><br>

                                    <div id="sub_optionError_{{$i}}"></div>
                                </td>
                            </tr>
                        @else
                            @foreach($ques_mod as $data)
                                <tr id="sub_question_row[{{$i}}]">
                                    <td class="td-left table-td-style">
                                        <h6 class="que_lable" id="q_sub_lable_{{$i}}">Question:- {{($i+1)}}</h6>
                                        <input id="subquestion[{{$i}}]" type="checkbox" name="subquestion[]" value="subquestion_{{($i+1)}}" class="css-checkbox question-all delete-check"><label for="subquestion[{{$i}}]" class="css-label table-ckeckbox delete-check"></label>
                                        <textarea raw="4" name="subque[]" id="subtext_{{$i}}" class="text-style textarea-width" placeholder="Question {{($i+1)}}">@if(isset($data->Que_Question)){{ $data->Que_Question }}@endif</textarea><br>
                                        <h6 class="que_mark " id="mlable_{{$i}}">Marks:- </h6>
                                        <input type="number" maxlength="3" name="submarks[]" class="text-style only_number_allow" placeholder="For Question:- {{($i+1)}}"><br>

                                        <div id="sub_optionError_{{($i-1)}}"></div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>


            <div class="row">
                <div class="form-btn branch-form-btn">
                    <input value="save" type="submit" tabindex="11" id="submitForm"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="12" ></a>
                </div>
            </div>
        </div>
    </form>
</div>
<style type="text/css">
    .que_lable{color: #ff975b;  margin-left: 10px;  margin-top: 50px;}
    .que_mark {color: #ff975b;  margin-left: 10px;  margin-top: 10px;}
    .error_text{color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;display: none; margin-left: 60px;}
    #ques{border-top: 2px solid #ff975b!important;}
    .table-form tr .table-td-style{background-color: #f7f7f7!important;color:#fff!important; border-left: 1px solid #fff; border-right: 1px solid #fff;}
    .check-style{margin-left: 80px!important;}
    .check-style1{margin-left: 30px!important;}
    .delete-check{margin-left: 20px!important; margin-top: -70px!important;}
    .text-style{width: 15%; height: auto; margin: 10px; display: inline-block; color: #ff975b; padding: 5px; border: 1px solid #ff975b;}
    .lable-style{display: inline-block;font-size: 16px;font-weight: 500;margin-left: 716px;text-transform: capitalize;color:#ff975b;}
    .textarea-width{width:90%!important; margin-left: 10px; margin-top: 20px; border: 1px solid #ff975b;}
    ::-webkit-input-placeholder {color: #ff975b; font-size:font-size: 14px;font-weight: 400;text-transform: capitalize;}
    :-ms-input-placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
    ::placeholder {color: #ff975b;font-size: 14px;font-weight: 400;text-transform: capitalize;}
</style>
@endsection

@section('footer')

@push('footer_script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('js/index.js') }}"></script>
  <script src="{{ url('js/clockpicker.js') }}"></script>


{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>--}}
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>



  <script type="text/javascript">
$(document).ready(function(){
    $('#exam_type').change(function(){
        //alert($(this).val());
        if($(this).val()==1){
            $('#objective').show();
            $('#subjective').hide();
        }
        else{
            $('#subjective').show();
            $('#objective').hide();
        }

    });



    var brnErr,clsErr,secErr;
    var i=2;
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^abcdABCD]/g, '');
    });

    $(document).on('click', '.present-all', function(){
         var id = $(this).attr("id");
         var values = $(this).val();
         var value = id.replace('answerA[','');
         value = value.replace('answerB[','');
         value = value.replace('answerC[','');
         value = value.replace('answerD[','');
         value = value.replace(']','');
         var checkboxes = document.getElementById(id);
         var check = checkboxes.checked;
         if(check == true ){
            if(values.indexOf("A") !=-1){
                document.getElementById('answerA['+value+']').checked=true;
                document.getElementById('answerB['+value+']').checked=false;
                document.getElementById('answerC['+value+']').checked=false;
                document.getElementById('answerD['+value+']').checked=false;
            }else if(values.indexOf('B') !=-1){
                document.getElementById('answerA['+value+']').checked=false;
                document.getElementById('answerB['+value+']').checked=true;
                document.getElementById('answerC['+value+']').checked=false;
                document.getElementById('answerD['+value+']').checked=false;
            }else if(values.indexOf('C') !=-1){
                document.getElementById('answerA['+value+']').checked=false;
                document.getElementById('answerB['+value+']').checked=false;
                document.getElementById('answerC['+value+']').checked=true;
                document.getElementById('answerD['+value+']').checked=false;
            }else{
                document.getElementById('answerA['+value+']').checked=false;
                document.getElementById('answerB['+value+']').checked=false;
                document.getElementById('answerC['+value+']').checked=false;
                document.getElementById('answerD['+value+']').checked=true;
            }
        }else{
           document.getElementById(id).checked=true; 
        }
    });


    $(document).on('click', '#addQuestion', function(){  
        var table = document.getElementById("ques");
        var rows = table.getElementsByTagName("tr");
        var j = rows.length;
        var lastRow = table.rows[ table.rows.length - 1 ];
        i = lastRow.id;
        i = i.replace(']','');
        i = i.replace('question_row[','');
        i=parseInt(i);
        i=i+1;
        que_id=j+1;        
        if(j<10){
            $('#ques').append('<tr id="question_row['+i+']"><td class="td-left table-td-style"><h6 class="que_lable" id="qlable_'+i+'">Question:- '+que_id+'</h6><input id="question['+i+']" type="checkbox" name="question[]" value="question_'+i+'" class="css-checkbox question-all delete-check"><label for="question['+i+']" class="css-label table-ckeckbox delete-check"></label><textarea raw="4" name="que[]" class="text-style textarea-width" placeholder="Question '+que_id+'" id="text_'+i+'"></textarea><br><input id="answerA['+i+']" type="checkbox" name="id1[]" value="A_'+i+'" class="css-checkbox present-all check-style"><label for="answerA['+i+']" class="css-label table-ckeckbox check-style"></label><input type="text" name="option1[]" class="text-style" placeholder="Option A"><input id="answerB['+i+']" type="checkbox" name="id2[]" value="B_'+i+'" class="css-checkbox present-all check-style1"><label for="answerB['+i+']" class="css-label table-ckeckbox check-style1"></label><input type="text" name="option2[]" class="text-style" placeholder="Option B"><input id="answerC['+i+']" type="checkbox" name="id3[]" value="C_'+i+'" class="css-checkbox present-all check-style1"><label for="answerC['+i+']" class="css-label table-ckeckbox check-style1"></label><input type="text" name="option3[]" class="text-style" placeholder="Option C"><input id="answerD['+i+']" type="checkbox" name="id4[]" value="D_'+i+'" class="css-checkbox present-all check-style1"><label for="answerD['+i+']" class="css-label table-ckeckbox check-style1"></label><input type="text" name="option4[]" class="text-style" placeholder="Option D"> <h6 class="que_mark " id="qlable_'+i+'">Marks:- </h6> <input type="number" name="objmarks[]" class="text-style only_number_allow" maxlength="3" placeholder="For Question:- '+que_id+'"><br><div id="optionError_'+i+'"></div></td></tr>');
        }
    });

    $(document).on('click', '#addSubQuestion', function(){
        var table = document.getElementById("subques");
        var rows = table.getElementsByTagName("tr");
        var j = rows.length;
        var lastRow = table.rows[ table.rows.length - 1 ];
        i = lastRow.id;
        i = i.replace(']','');
        i = i.replace('sub_question_row[','');
        i=parseInt(i);
        i=i+1;
        que_id=j+1;
        if(j<10){
            $('#subques').append('<tr id="sub_question_row['+i+']"><td class="td-left table-td-style"><h6 class="que_lable" id="q_sub_lable_'+i+'">Question:- '+que_id+'</h6><input id="subquestion['+i+']" type="checkbox" name="subquestion[]" value="subquestion_'+i+'" class="css-checkbox question-all delete-check"><label for="subquestion['+i+']" class="css-label table-ckeckbox delete-check"></label><textarea raw="4" name="subque[]" class="text-style textarea-width" placeholder="Question '+que_id+'" id="text_'+i+'"></textarea> <h6 class="que_mark " id="mlable_'+i+'">Marks:- </h6> <input type="number" name="submarks[]" class="text-style only_number_allow_add" maxlength="3" placeholder="For Question:- '+que_id+'"> <br><div id="sub_optionError_'+i+'"></div></td></tr>');
        }
    });


    var delete_ids =[];
    $(document).on('click', '#deleteQuestion', function(){
        checkboxes = document.getElementsByName('question[]');
        var table = document.getElementById("ques");
        var rows = table.getElementsByTagName("tr");
        var i = rows.length;
        if(checkboxes.length>1){
            $("table").find('input[name="question[]"]').each(function(){
                if($(this).is(":checked")){
                    id = $(this).attr('id');
                    id = id.replace('question[','');
                    id = id.replace(']','');
                    delete_ids.push('text_'+id);
                    $(this).parents("tr").remove();
                    var m=1;
                    for(j=0;j<=i;j++){
                        if(delete_ids.indexOf('text_'+j)!==-1){
                        }else{
                            console.log('j'+j);
                            $('#text_'+j).attr('placeholder','Question '+m);
                            $('#qlable_'+j).text('Question:- '+m);
                            m++;
                        }
                    }
                }
            });
        }
    });


    var delete_sub_ids =[];
    $(document).on('click', '#deleteSubQuestion', function(){
        checkboxes = document.getElementsByName('subquestion[]');
        var table = document.getElementById("subques");
        var rows = table.getElementsByTagName("tr");
        var i = rows.length;
        // alert('hello');
        if(checkboxes.length>1){
            $("table").find('input[name="subquestion[]"]').each(function(){
                if($(this).is(":checked")){
                    id = $(this).attr('id');
                    id = id.replace('subquestion[','');
                    id = id.replace(']','');
                    delete_sub_ids.push('text_'+id);
                    $(this).parents("tr").remove();
                    var m=1;
                    for(j=0;j<=i;j++){
                        if(delete_sub_ids.indexOf('text_'+j)!==-1){
                        }else{
                            console.log('j'+j);
                            $('#text_'+j).attr('placeholder','Question '+m);
                            $('#q_sub_lable_'+j).text('Question:- '+m);
                            m++;
                        }
                    }
                }
            });
        }
    });


    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        var title = $("#title").val();
        var subject = $("#subject").val();
        var date = $('#datepicker').val();
        var duedate = $('#duedatepicker').val();
        var time = $('#time').val();
        
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault();
            return false;
        }else{
            if(class_val=="none")
            {
                alert("Please Select  Class...");
                e.preventDefault();
                return false;
                
            }else{
                if(section=="none"){
                    alert("Please Select  Section...");
                    e.preventDefault();
                    return false;
                }else{
                    if(title=="none"){
                        alert("Please Enter Title...");
                        e.preventDefault();
                        return false;
                    }else{
                        if(subject=="none"){
                            alert("Please Enter Subject...");
                            e.preventDefault();
                            return false;
                        }else{
                            if(date==""){
                                $("#dateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Date.</span></label>");
                                e.preventDefault();
                                return false;
                            }else {
                                if(duedate==""){
                                    $("#duedateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Due Date.</span></label>");
                                    e.preventDefault();
                                    return false;
                                }else {
                                    if(time==""){
                                        $("#timeError").html("<label for='time' class='error'><span class='text-danger'>Please Enter Time.</span></label>");
                                        e.preventDefault();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    });

    $("#deleteQuestion").click(function(e){
        var n = $("input:checked").length;
        if(n > 0){
            var confirmVal = window.confirm("Are you sure that you want to delete this question?");
            if(confirmVal == true){
                e.preventDefault();
                return true;
            }else {
                e.preventDefault();
                return false;
            }
        }else {
            alert("Please Select One Question...");
            // return false;
        }
    });

    $("#deleteSubQuestion").click(function(e){
        var n = $("input:checked").length;
        if(n > 0){
            var confirmVal = window.confirm("Are you sure that you want to delete this question?");
            if(confirmVal == true){
                e.preventDefault();
                return true;
            }else {
                e.preventDefault();
                return false;
            }
        }else {
            alert("Please Select One Question...");
            // return false;
        }
    });

    // $("#deleteQuestion").click(function(e){
    //     var n = $("input:checked").length;
    //     if(n > 0){
    //         confirm("Are you sure that you want to delete this question?");
    //         e.preventDefault();
    //         return false;
    //     } else {
    //         alert("Please Select One Question...");
    //         e.preventDefault();
    //         return false;
    //     }
    // });

    $("#submitForm").click(function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        var title = $("#title").val();
        var subject = $("#subject").val();
        var date = $('#datepicker').val();
        var duedate = $('#duedatepicker').val();
        var time = $('#time').val();
        if(branch=="none"){
            $("#branchError").html("<label for='branch' class='error'><span class='text-danger'>Please select branch.</span></label>");
            e.preventDefault();
            return false;
        }else{
            $("#branchError").html("");
            if(class_val=="none")
            {
                $("#classError").html("<label for='class' class='error'><span class='text-danger'>Please select class.</span></label>");
                e.preventDefault();
                return false;
                
            }else{
                if(section=="none"){
                    $("#sectionError").html("<label for='branch' class='error'><span class='text-danger'>Please select section.</span></label>");
                    e.preventDefault();
                    return false;
                }else {
                    if(title==""){
                        $("#titleError").html("<label for='title' class='error'><span class='text-danger'>Please enter title.</span></label>");
                        e.preventDefault();
                        return false;
                    }else {
                        if(subject==""){
                            $("#subjectError").html("<label for='subject' class='error'><span class='text-danger'>Please enter subject.</span></label>");
                            e.preventDefault();
                            return false;
                        }else {
                            var sel = document.getElementById('exam_type');
                            if (sel.options[sel.selectedIndex].value == 1) {
                            // console.log(objec_option);
                            // if (objec_option.value == null) {
                                // alert("Please select a card type");

                                $("textarea:input[name='que[]']").each(function( key, value ){
                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                        $(this).css("border","1px solid red");
                                        $(this).focus();
                                        e.preventDefault();
                                        return false;
                                    }else {
                                        $(this).css("border","1px solid #ff975b");
                                        $("input:text[name='option1[]']").each(function( key, value ){
                                            if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                $(this).css("border","1px solid red");
                                                $(this).focus();
                                                e.preventDefault();
                                                return false;
                                            }else {
                                                $(this).css("border","1px solid #ff975b");
                                                $("input:text[name='option2[]']").each(function( key, value ){
                                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                        $(this).css("border","1px solid red");
                                                        $(this).focus();
                                                        e.preventDefault();
                                                        return false;
                                                    }else {
                                                        $(this).css("border","1px solid #ff975b");
                                                        $("input:text[name='option3[]']").each(function( key, value ){
                                                            if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                                $(this).css("border","1px solid red");
                                                                $(this).focus();
                                                                e.preventDefault();
                                                                return false;
                                                            }else {
                                                                $(this).css("border","1px solid #ff975b");
                                                                $("input:text[name='option4[]']").each(function( key, value ){
                                                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                                                        $(this).css("border","1px solid red");
                                                                        $(this).focus();
                                                                        e.preventDefault();
                                                                        return false;
                                                                    }else{
                                                                        if(date==""){
                                                                            $("#dateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Date.</span></label>");
                                                                            e.preventDefault();
                                                                            return false;
                                                                        }else {
                                                                            if(duedate==""){
                                                                                $("#duedateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Due Date.</span></label>");
                                                                                e.preventDefault();
                                                                                return false;
                                                                            }else {
                                                                                if(time==""){
                                                                                    $("#timeError").html("<label for='time' class='error'><span class='text-danger'>Please Enter Time.</span></label>");
                                                                                    e.preventDefault();
                                                                                    return false;
                                                                                }else{
                                                                                    $(this).css("border","1px solid #ff975b");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else if(sel.options[sel.selectedIndex].value == 0){

                                $("textarea:input[name='subque[]']").each(function( key, value ){
                                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                                        $(this).css("border","1px solid red");
                                        $(this).focus();
                                        e.preventDefault();
                                        return false;
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }

        var sel = document.getElementById('exam_type');
        if (sel.options[sel.selectedIndex].value == 1) {
            $("table").find('input[name="id1[]"]').each(function(){
                    var id = $(this).attr('id')
                    var value = id.replace('answerA[','');
                    value = value.replace(']','');
                    var check = document.getElementById(id).checked;
                    if(check == false){
                        var check = document.getElementById('answerB['+value+']').checked;
                        if(check == false){
                            var check = document.getElementById('answerC['+value+']').checked;
                            if(check == false){
                                var check = document.getElementById('answerD['+value+']').checked;
                                if(check == false){
                                    console.log(value);
                                    $("#optionError_"+value).html("<label for='class' class='error'><span class='text-danger' style='color:#ff975b!important; margin-left:30px;'>Please select anser.</span></label>");
                                    e.preventDefault();
                                    return false;
                                }
                            }
                        }
                    }
               });
        }
    });

    $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                console.log(data);
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select--</option>');
                     $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });

    $("#class").on('change',function(){
        var idAndClass = $(this).val();
        var classIdName = idAndClass.split(",");
        console.log(classIdName);
        $.ajax({
            type : "POST",
            url : "{{ url('student/getSection') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Class : classIdName[1],
                Cla_Bra_Id : classIdName[0]
            },
            dataType : "JSON",
            success : function(data){
                    $(".class-sec option").each(function() {
                        $(this).remove();
                    });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                     });
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#section").append(items);
            },
            error :function(error){
                console.log(error);
            }
        });
    });

    $("#exam_year").on('change',function(){
        var examYear = $(this).val();
        // var classIdName = idAndClass.split(",");
        console.log(examYear);
        $.ajax({
            type : "POST",
            url : "{{ url('exam_title') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                exam_year : examYear
            },
            dataType : "JSON",
            success : function(data){
                    $(".exam_title option").each(function() {
                        $(this).remove();
                    });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" + this['exam_id'] + "'>" + this['exam_title'] + "</option>" );
                     });
                    $("#title").append('<option value="none">-- Select --</option>');
                    $("#title").append(items);
            },
            error :function(error){
                console.log(error);
            }
        });
    });

    $("#class").on('change',function(){
        var idAndSec = $(this).val();
        var SecIdName = idAndSec.split(",");
        var classIdName = $(this).val();
        console.log(SecIdName);
        $.ajax({
            type : "POST",
            url : "{{ url('getSubject') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Class : classIdName,
            },
            dataType : "JSON",
            success : function(data){
                $(".class-sub option").each(function() {
                    $(this).remove();
                });

                // var sub = JSON.parse(data.subject[0].Sub_Json_Data);
                // console.log(data[0].Sub_Name);
                // console.log(data.subject[0].);
                data = data.subject;
                var lcounter = 0;
                var items = [];
                $.each( data, function( key, val ) {
                    items.push( "<option value='" + data[lcounter].Sub_Name + "," + data[lcounter].Sub_Id + "'>" + data[lcounter].Sub_Name + "</option>" );
                    lcounter++;
                });
                $("#subject").append('<option value="none">-- Select --</option>');
                $("#subject").append(items);
            },
            error :function(error){
                console.log(error);
            }
        });
    });

    $('.only_number_allow').keypress(function (event) {
        var keycode = event.which;
        if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
            event.preventDefault();
        }
    });

    $('.only_number_allow_add').keypress(function (event) {
        var keycode = event.which;
        if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
            event.preventDefault();
        }
    });

    $(function() {
            $('#datetimepicker1').datetimepicker({
                format: 'MM/DD/YYYY HH:mm',
                minDate: moment().add(0, 'day')
            });
            $('#datetimepicker2').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'MM/DD/YYYY HH:mm'
            });
            $("#datetimepicker1").on("dp.change", function (e) {
                $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
                $('#datetimepicker2').data("DateTimePicker").clear();
            });
            // $("#datetimepicker2").on("dp.change", function (e) {
            //     $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
            // });


        $(".datetimepicker3").clockpicker();

    });
});
</script>


@endpush
@section('footer_link_and_scripts')