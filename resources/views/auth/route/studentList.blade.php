    <div class="row">
        <div class="col-lg-12">
                <div class="box box-primary">
                @if(count($studentList)>0)
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                        <h2 class="box-title"><b><input type="hidden" name="studentList[]" value="">Student List</b></h2>
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll()">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll()">UnCheck All</a>
                        </div>
                    </div>
                @endif
                    <div class="branch-box">
                    <?php $i=1; ?>
                    @forelse($studentList as $data)
                        <div class="col-md-3">
                            <input type="checkbox" name="student[]" id="ckbox{{ $i }}" class="css-checkbox std" value="{{ $data->Std_Id }}" 
                                @foreach($assignedRoute as $route)
                                    @if($route->Ass_Std_Id == $data->Std_Id)
                                        checked="true"
                                    @endif
                                @endforeach
                            ><label for="ckbox{{ $i }}" class="css-label">{{ $data->Std_Name }}</label>
                        </div>
                    <?php $i++; ?>
                    @empty
                        <div style="color: #ff975b;"> No Record Found. </div>
                    @endforelse
                    </div>
                </div>
            </div>
    </div>
    @if(count($studentList)>0)
    <div class="row">
            <div class="form-btn">
                <input type="submit" value="save" tabindex="10"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="11"  ></a>
            </div>
    </div>
    @endif

<script type="text/javascript">    
function selectAll(){ $(".std").prop('checked', true); }
function unselectAll(){ $(".std").prop('checked', false); }
</script>