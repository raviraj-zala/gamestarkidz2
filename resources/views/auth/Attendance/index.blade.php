@extends('auth.Master.master')

@section('title','Report Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('images/report_img.png') }}" alt="">Attendance Management</h2>      
    </div>
</div>
<div class="form-section">
    <form action="{{url('search_attendence')}}" method="get" enctype="multipart/form-data" autocomplete="off">
        <div class="row">
            <div class="col-lg-2">
                <input class="form-control" type="text" placeholder="Search"  id="" tabindex="1" name="search" value="@if(Request::input('search')!=''){{ Request::input('search') }}@endif"  maxlength="50">
                
            </div>
            <div class="col-lg-2">
                <input class="form-control" type="text" placeholder="From date"  id="from_date" tabindex="1" name="from_date" value="@if(Request::input('from_date')!=''){{ Request::input('from_date') }}@endif" readonly>
                
            </div>

            <div class="col-lg-1">
                <input type="submit" class="btn btn-info search-btn" value="Search" tabindex="5">
            </div>
        </div>
    </form>
</div>
<br>
<div class="clearfix"></div>
<br>
<div class="table-form">   
    <table id="example2">
        <tbody>
            <tr>
                <th>
                    <input type="checkbox" id="selectall" class="checked css-checkbox" />
                    <label for="selectall" class="css-label table-ckeckbox">
                        <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white">
                        </th>
                        {{-- <th class="td-left">No</th> --}}
                        <th class="td-left">Branch Name</th>
                        <th class="td-left">Class</th>
                        <th class="td-left">Section</th>
                        <th class="td-left">Date</th>
                        <th>Update</th>
            </tr>
            <?php $i = 1; ?>
            @forelse($attendance as $student)
            <tr>
                <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $student->Att_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
                
                <td class="td-left">{{ $student->branch_name }}</td>
                <td class="td-left">{{ $student->Cla_Class }}</td>
                <td class="td-left">{{ $student->Cla_Section }}</td>
                <td>{{ date("d-m-Y", strtotime($student->Att_CreatedAt)) }}</td>
                <td>
                    <a href="{{ url('edit_attendance',$student->Att_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a>
                </td>
            </tr>
            <?php $i++; ?>
            @empty
            <tr><td colspan="7">No Data Found.</td></tr>
            @endforelse

        </tbody>
    </table>
</div>
@php
    $tempQuery = Request::getQueryString(); 
    $tempQuery = explode("&",$tempQuery);
    $newQueryString = "";
    foreach ($tempQuery as $queryString) {
        if(explode("=",$queryString)[0]=="page"){
        } else{
            $newQueryString .= "&".$queryString;
        }
    }
@endphp

<div class="paggination-section">
   <nav>
@if($attendance->lastPage() > 1)
    <ul class="pagination">
        @if ($attendance->currentPage() != 1 && $attendance->lastPage() >= 5)
            <li class="page-item"><a class="page-link" href="{{ $attendance->url($attendance->url(1)).$newQueryString }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($attendance->currentPage() != 1)
            <li class="page-item">
                <a class="page-link" href="{{ $attendance->url($attendance->currentPage()-1).$newQueryString }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($attendance->currentPage()-2, 1); $i <= min(max($attendance->currentPage()-2, 1)+4,$attendance->lastPage()); $i++)
                <li class="page-item {{ ($attendance->currentPage() == $i) ? 'active' : '' }}">
                    <a class="page-link" href="{{ $attendance->url($i).$newQueryString }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($attendance->currentPage() != $attendance->lastPage())
            <li class="page-item">
                <a class="page-link" href="{{ $attendance->url($attendance->currentPage()+1).$newQueryString }}" >
                    >
                </a>
            </li>
        @endif
        @if ($attendance->currentPage() != $attendance->lastPage() && $attendance->lastPage() >= 5)
            <li class="page-item">
                <a class="page-link" href="{{ $attendance->url($attendance->lastPage()).$newQueryString }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</nav>
</div>
<script>    
$(document).ready(function(){
     $('#from_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,

    });
});
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
$('#btnSearch').on('change', function () {
    var Std_Name = $(this).val();
    if (Std_Name != "") {
        $.ajax({
            type: "POST",
            url: "{{ url('search_attendance') }}",
            data: {
                _token: '{{ csrf_token() }}',
                Std_Name: Std_Name
            },
            success: function (data) {
                $(".paggination-section").html('');
                $('table#example2').html(data);
            },
            error: function (error) {
            }
        });
    } else {
        window.location = "{{ url('Report Mgmt.') }}";
    }
});

$('#btnDelete').on('click', function () {
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete = confirm('Delete selected records???');
        if (ch_delete) {
            var ids = [];
            $('input:checked').each(function (i) {
                ids[i] = $(this).val();
            });
            $.ajax({
                type: "POST",
                url: "{{ url('delete_report') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    Rpt_Id: ids
                },
                success: function (data) {
                    $("input[type='checkbox']").each(function (i) {
                        $(this).prop('checked', false);
                    });
                    $(".eve_sts").each(function (i) {
                        $(this).prop('checked', false);
                    });
                    window.location.reload();
                }
            });
        }
    } else {
        alert('Please select atleast one record!');
    }
});
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')