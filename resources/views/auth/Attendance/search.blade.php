<table id="example2"> 
    <tbody>
        <tr>
            <tr>
                <th>
                    <input type="checkbox" id="selectall" class="checked css-checkbox" />
                    <label for="selectall" class="css-label table-ckeckbox">
                    <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white"></label>
                </th>
                {{-- <th class="td-left">No</th> --}}
                <th class="td-left">Branch Name</th>
                <th class="td-left">Class</th>
                <th class="td-left">Section</th>
                <th class="td-left">Date</th>
                <th>Update</th>
            </tr>
        </tr> 
        <?php $i = 1; ?>
            @forelse($attendance as $student)
            <tr>
                <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $student->Att_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
                
                <td class="td-left">{{ $student->branch_name }}</td>
                <td class="td-left">{{ $student->Cla_Class }}</td>
                <td class="td-left">{{ $student->Cla_Section }}</td>
                <td>{{ date("d-m-Y", strtotime($student->Att_CreatedAt)) }}</td>
                <td>
                    <a href="{{ url('edit_attendance',$student->Att_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a>
                </td>
            </tr>
            <?php $i++; ?>
            @empty
            <tr><td colspan="7">No Data Found.</td></tr>
            @endforelse
    </tbody>
</table>