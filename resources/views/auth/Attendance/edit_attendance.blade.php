@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text">
   <h2><img src="{{ asset('images/report_img.png') }}" alt="">Update Attendance</h2>   
 </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <form name="insert-report" role="form" method="POST" action="{{ url('update_attendance') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="branch-form">
            <input type="hidden" id="branch_id" name="student_id" >
            <input type="hidden" id="student_gr_no" name="student_gr_no" >
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="branch-form">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Select Branch <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="branch_id" style="z-index: auto"  id="branch_id" value="{{$attendance->branch_name}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Class <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                            <input type="text" name="class" id="class" value="{{$attendance->Cla_Class}}" style="z-index: auto"  readonly>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                          <h5>Section <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                        <input type="text" name="section" style="z-index: auto"  id="section" value="{{$attendance->Cla_Section}}" readonly>
                        </div>
                    </div>
                    @php
                        $today_date = date('d-m-Y');
                    @endphp
                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Date <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="date" id="datepicker" tabindex="3" value="@if(isset($attendance->Att_Date)){{date('d-m-Y',strtotime($attendance->Att_Date))}}@else{{$today_date}}@endif" readonly>
                            </div>
							 <span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
                            <!-- <div id="monthYearError"></div> -->
                            @if (Session::has('month_year'))
                            <label class="error" for="month_year">
                                <span class="text-danger">
                                    {{ Session::get('month_year') }}
                                </span>
                            </label>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <!-- </form> -->
</div>  

<div id="progress_report_data">
    
    <div class="line-1">        
        <div class="mangement-btn user-text">
            <h2><img src="{{ asset('images/report_img.png') }}" alt="">Student List</h2>      
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">

        @if(Session::has('success'))
        <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="main-btn">
               
            </div>
        </div>
    </div>
    <div class="table-form">   
        <table id="example2">
            <tbody>
                <tr>
                    <th class="td-left">Student GR No.</th>   
                    <th class="td-left">Student Name</th>
                    <th class="td-left">Present &nbsp;<input type="checkbox" id="presentall" class="checked css-checkbox" />
                    <label for="presentall" class="css-label table-ckeckbox">
                    <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
                    <th class="td-left">Absent &nbsp;<input type="checkbox" id="absentall" class="checked css-checkbox" />
                    <label for="absentall" class="css-label table-ckeckbox">
                    <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
                </tr>
                <?php $i = 1; ?>
                @forelse($student_data as $student)
                <tr>
                    <input type="hidden" name="Att_Cla_Id" value="{{ $student->Std_Cla_Id }}" >
                    <td>{{$student->Std_Gr_No}}</td>
                    <td class="td-left">{{ $student->Std_Name }}</td>
                    <td><input id="class{{ $i }}presentall" type="checkbox" onClick="checkbox_is_checked()" name="present_id[{{$student->at_id}}]" value="{{ $student->Std_Id }}" class="css-checkbox present-all present-all{{$student->Std_Id}} present_data" @if($student->Att_Pre_Abs==1) checked @endif><label for="class{{ $i }}presentall" class="css-label table-ckeckbox"></label></td>
                    <td><input id="class{{ $i }}absentall" type="checkbox" onClick="checkbox_is_checked()" name="absent_id[{{$student->at_id}}]" value="{{ $student->Std_Id }}" class="css-checkbox absent-all absent-all{{$student->Std_Id}} absent_data" @if($student->Att_Pre_Abs==0) checked @endif>
                        <label for="class{{ $i }}absentall" class="css-label table-ckeckbox"></label></td>
                </tr>
                <?php $i++; ?>
                @empty
                <tr><td colspan="7">No Data Found.</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
    @if(count($student_data)>0)
        <div>
            <div class="form-btn branch-form-btn">
                <input type="submit" value="Update" tabindex="3" onClick="return setText()" id="progressSubmitForm"><a href="{{ url('list_Attendance') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
            </div>    
        </div>
    @endif
</form>
</div>

@endsection

@push('footer_script')
<script>    
$(function () {
    /*$("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });*/
    $("#presentall").click(function () {
        if ($("#presentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', true);
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $("#absentall").click(function () {
        if ($("#absentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', true);
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $('.present-all').click(function(){
        // var presentid = $('input[name="present_id[]"]').val();
         var presentid = this.value;
         console.log(presentid);
         if(presentid == 'checked' ){
            $('.absent-all'+presentid).attr('checked',true);
         } else {
            $('.absent-all'+presentid).attr('checked',false);
         }
         // console.log(id);
    });
    $('.absent-all').click(function(){
        var absentid = this.value;
        if(absentid == 'checked'){
            $('.present-all'+absentid).attr('checked',true);
         } else {
            $('.present-all'+absentid).attr('checked',false);
         }
        // console.log(absentid);
        // var absentid = $('input[name="absent_id[]"]').val();
    });
});
    function setText()
    {
        var chkArray = [];
        
         // look for all checkboes that have a class 'chk' attached to it and check if it was checked 
        $(".present-all:checked").each(function() {

            console.log($(this).val());
            chkArray.push($(this).val());
        });
         // we join the array separated by the comma 
        var selected;
        selected = chkArray.join(',') ;
        
         // check if there is selected checkboxes, by default the length is 1 as it contains one single comma 
        if(selected.length > 0)
        {
            return true;
        }else{
             alert("Please at least check one of the student"); 
             return false;
        }
    }
    /* $("#section").on('change', function () {
            var type = $("#section").val();
            if (type === "2") {
                $("#attendancereport").css('display', 'none');
            } else {
                $("#attendancereport").css("display", "block");
            }
        });*/
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
