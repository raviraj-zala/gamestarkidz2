<div class="row">
        <div class="col-lg-12">
        <?php $i=1; $cls=""; $sid=""; ?>   
        @foreach($class as $data)
                <div class="box box-primary">
                @if($data->Cla_Class!=$cls)
                    <?php $cls=$data->Cla_Class; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="class[]" value="{{ $data->Cla_Class }}">{{ $data->Cla_Class }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll('{{ $data->Cla_Class }}')">Check All</a>|
                            <a href="javascript:void(0);" onclick="unselectAll('{{ $data->Cla_Class }}')">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="section[]" id="chckbox{{ $i }}" class="css-checkbox {{ 'sec'.$data->Cla_Class }}" value="{{ $data->Cla_Id,$data->Cla_Section }}"><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Section }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
</div>