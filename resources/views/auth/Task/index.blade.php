@extends('auth.Master.master')

@section('title','Task Management')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('task.png') }}" alt="branch-img">Task Management</h2>      
	</div>
</div>

<div class="clearfix"></div>
	<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <div class="search-box">
				<input id="btnSearch" name="Cla_Name" placeholder="Search" type="search" />
	      </div>
	    </div>

	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
	      			<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_task') }}'">
	      		</li>
	      		<li>
				<!-- <input type="button" id="active"  value="Active" class="active btnActInact" > -->
				</li>
	            <li>
	            <!-- <input type="button" id="inactive" value="In-Active" class="in-active btnActInact"> -->
				</li>
	      		<li><input type="submit" value="Delete" class="delete" id="btnDelete" ></li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white">
      </th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Teacher</th>
      <th class="td-left">Start Date</th>
      <!-- <th class="td-left">Time</th> -->
      <th>Status</th>
      <th>Edit</th>
      <th>Notes</th>
    </tr>

@if($task)
	<?php $i=1; ?>
	@foreach($task as $data)
	@php
		$date = date('d/m/Y', strtotime($data->Tas_Date));
	@endphp
    <tr>
      	<td><input id="class{{ $i }}" type="checkbox" name="id[]" value="{{ $data->Tas_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
      	<td class="td-left">{{ $data->Brn_Name }}</td>
      	<td class="td-left">{{ $data->Cla_Class }}</td>
      	<td class="td-left">{{ $data->Use_Name }}</td>
      	<td class="td-left">{{ $date }}</td>
      	<!-- <td class="td-left">{{ $data->Tas_Time }}</td> -->
      	<td>
			@if($data->Tas_Status == 0)
				Open
      		@elseif($data->Tas_Status == 1)
				Approve
			@elseif($data->Tas_Status == 5)
				Pending
			@elseif($data->Tas_Status == 3)
				Complete
			@elseif($data->Tas_Status == 2)
				Close
			@elseif($data->Tas_Status == 4)
				Reopen
			@elseif($data->Tas_Status == 6)
				Inprogress
			@endif
      	</td>
      	<td><a href="{{ url('edit_task',$data->Tas_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a></td>
      	<td><a href="{{ url('note_task',$data->Tas_Id) }}"><img src="{{ asset('images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
	<?php $i++; ?>
    @endforeach
@else
	No Data Found
@endif    

  
  </tbody></table>
</div>

<div class="paggination-section">
@if ($task->lastPage() > 1)
    <ul>
        @if ($task->currentPage() != 1 && $task->lastPage() >= 5)
            <li><a href="{{ $class->url($task->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($task->currentPage() != 1)
            <li>
                <a  href="{{ $task->url($task->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($task->currentPage()-2, 1); $i <= min(max($task->currentPage()-2, 1)+4,$task->lastPage()); $i++)
                <li>
                    <a class="{{ ($task->currentPage() == $i) ? 'active' : '' }}" href="{{ $task->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($task->currentPage() != $task->lastPage())
            <li>
                <a href="{{ $task->url($task->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($task->currentPage() != $task->lastPage() && $task->lastPage() >= 5)
            <li>
                <a href="{{ $task->url($task->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
</script>

<script type="text/javascript">
$('#btnSearch').on('change',function(){
	var Cla_Name = $(this).val();
	if(Cla_Name!=""){
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_class') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Cla_Name : Cla_Name
		        },
	        success : function(data){
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
    }else{
    	window.location = "{{ url('Class Mgmt.') }}"
    }
});


$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records?');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});	
			console.log(ids);	
			$.ajax({
                type : "POST",
                url : "{{ url('delete_task') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            ids : ids
			        },
                success : function(data){
                	// console.log(data);
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});

$("input.btnActInact").on('click',function(){
	var n = $("input:checked").length;
	var val = $(this).val();
	var ids = [];
	var ch_sts=false;
	var status=null;
	if( n >0 )
	{
		$('input:checked').each(function(i){ ids[i] = $(this).val(); });
		if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
    	if(status==1) { ch_sts=confirm('Activate selected records???'); }
    	else if(status==0) { ch_sts=confirm('In-activate selected records???'); }

    	if(ch_sts){
	    	$.ajax({
		        type: "POST",
		        url: "{{ url('classStatus') }}",
		        data: { 
		            id: ids, 
		            status: status,
		            _token:     '{{ csrf_token() }}'
		        },
		        success: function(data) {
		        	$("input[type='checkbox']").each(function(i){
							$(this).prop('checked', false);
					});
		        	window.location.reload();
		        },
		        error: function(result) {
		            console.log(result.responseText);
		        }
		    });
    	}
    }
    else
    {
    	alert('Please select atleast one record!');
    }
});
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')

<!-- http://www.codeinhouse.com/how-to-delete-multiple-rows-using-checkbox-in-php-laravel-using-eloquent-javascript/ -->