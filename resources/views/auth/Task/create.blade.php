@extends('auth.Master.master')

@section('title','Add Student')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text" style="display: block!important">
       <h2><img src="{{ asset('images/event-management.png') }}" alt="">@if(isset($task->Tas_Id)) Edit @else Add @endif Task</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif

<div class="form-section">
<form name="add-student" role="form" method="Post" action="{{ url('save_task') }}" enctype="multipart/form-data" id="taskForm">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="@if(isset($task->Tas_Id)){{$task->Tas_Id}}@endif">
    <div class="left-form">
            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                        <option value="none">-- Select --</option>
                        @foreach($branch as $data)
                            <option value="{{ $data->Brn_Id }}" @if(isset($task->Tas_Branch)) @if($task->Tas_Branch==$data->Brn_Id) selected @endif @endif>{{ $data->Brn_Name }}</option>
                        @endforeach
                    </select>
                @if (Session::has('branch'))
                <label class="error" for="branch">
                    <span class="text-danger">
                        {{ Session::get('branch') }}
                    </span>
                </label>
                @endif
                </div>
                <div id="branchError"></div>
                </div>    
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    @if(isset($task->Tas_Class ))
                        @foreach($class as $data)
                            <option value="{{ $data->Cla_Id }}" @if($task->Tas_Class == $data->Cla_Id) selected @endif>{{ $data->Cla_Class }}</option>
                        @endforeach
                    @else
                        <option value="none">-- Select--</option>
                    @endif
                  </select>
                </div>
                </div>
                <div id="classError"></div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Teacher <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="class-sec" name="teacher" id="section" tabindex="3">
                    @if(isset($task->Tas_Teacher ))
                        @foreach($teacher as $data)
                            <option value="{{ $data->Use_Id }}" @if($task->Tas_Teacher == $data->Use_Id) selected @endif>{{ $data->Use_Name }}</option>
                        @endforeach
                    @else
                        <option value="none">-- Select--</option>
                    @endif
                  </select>
                </div>
                </div>
                <div id="sectionError"></div>
                @if (Session::has('teacher'))
                <label class="error" for="teacher">
                    <span class="text-danger">
                        {{ Session::get('teacher') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Task <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="task" id="task" class="{{ $errors->has('task') ? ' is-invalid' : '' }}" required tabindex="6" value="@if(isset($task->Tas_Title)){{$task->Tas_Title}}@else{{ old('task') }}@endif" maxlength="15">
                    @if ($errors->has('task'))
                        <label class="error" for="task">
                            <span class="text-danger">
                                {{ $errors->first('task') }}
                            </span>
                        </label>
                    @endif
                    <div id="taskError"></div>
                </div>
            </div>

              <div class="form-box">
                    <div class="form-text">
                      <h5>Status<span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}"
                            name="status"
                            tabindex="4"
                            required
                    >
                        <option value="0" @if(isset($task->Tas_Status)) @if($task->Tas_Status==0) selected @endif @endif>Open</option>
                        <option value="1" @if(isset($task->Tas_Status)) @if($task->Tas_Status==1) selected @endif @endif>Approve</option>
                        <option value="3" @if(isset($task->Tas_Status)) @if($task->Tas_Status==3) selected @endif @endif>Completed</option>
                        <option value="5" @if(isset($task->Tas_Status)) @if($task->Tas_Status==5) selected @endif @endif>Pending</option>
                        <option value="4" @if(isset($task->Tas_Status)) @if($task->Tas_Status==4) selected @endif @endif>Reopen</option>
                        <option value="2" @if(isset($task->Tas_Status)) @if($task->Tas_Status==2) selected @endif @endif>Close</option>
                        <option value="6" @if(isset($task->Tas_Status)) @if($task->Tas_Status==2) selected @endif @endif>Inprogress</option>
                    </select>
                    </div>
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                    </div>    
                </div>           
        </div>
        @php 
            if(isset($task->Tas_Date)){
                $date = date('d/m/Y',strtotime($task->Tas_Date));
            }
        @endphp
        <div class="right-form">
            <div class="form-box">
                <div class="form-text">
                    <h5>Start Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="date" id="datepicker" tabindex="3"
                           value="@if(isset($task->Tas_Date)){{$date}}@endif" readonly>
						   <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <div id="dateError"></div>
                    @if (Session::has('date'))
                    <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            @php
                if(isset($task->Tas_Due_Date)){
                    $duedate = date('d/m/Y',strtotime($task->Tas_Due_Date));
                }
            @endphp
            <div class="form-box">
                <div class="form-text">
                    <h5>Due Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text"
                           name="duedate"
                           id="duedatepicker"
                           tabindex="3" value="@if(isset($task->Tas_Due_Date)){{ $duedate }}@endif" readonly>
						   <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <div id="duedateError"></div>
                    @if (Session::has('date'))
                        <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Time <span>*</span> :</h5>
                </div>
                <div class="form-typ-box datetimepicker3"> 
                    <input type="text" name="time" tabindex="3" id='time' value="@if(isset($task->Tas_Time)){{$task->Tas_Time}}@endif">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
					<div id="timeError"></div>
                    @if (Session::has('time'))
                    <label class="error" for="time">
                        <span class="text-danger">
                            {{ Session::get('time') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Description <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <textarea name="description" id="description" raws="3">@if(isset($task->Tas_Description)){!! $task->Tas_Description  !!}@endif</textarea> 
                    @if ($errors->has('description'))
                      <label class="error" for="description">
                        <span class="text-danger">
                            {{ $errors->first('description') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <!-- <div class="form-box">
                <div class="form-text ">
                    <h5>Notes <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="notes" class="{{ $errors->has('notes') ? ' is-invalid' : '' }}" tabindex="10" value="@if(isset($task->Tas_Note)){!! $task->Tas_Note  !!}@else{{ old('notes') }}@endif" >
                    @if ($errors->has('notes'))
                        <label class="error" for="notes">
                            <span class="text-danger">
                                {{ $errors->first('notes') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div> -->

            
</div>
            <div class="form-btn branch-form-btn">
            <input value="save" type="submit" tabindex="11" id="submitForm"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="12" ></a>
            </div>
</form>
</div>

@endsection

@section('footer')

@push('footer_script')

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('public/js/index.js') }}"></script>
    <script src="{{ url('js/clockpicker.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    var brnErr,clsErr,secErr;
    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault();
            return false;
        }else{
            if(class_val=="none")
            {
                alert("Please Select  Class...");
                e.preventDefault();
            return false;
                
            }else{
                if(section=="none"){
                    alert("Please Select  Section...");
                    e.preventDefault();
            return false;
                }
            }
        }
    });

    $("#submitForm").click(function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        var task = $('#task').val();
        var date = $('#datepicker').val();
        var duedate = $('#duedatepicker').val();
        var time = $('#time').val();
        console.log('time '+time);
        if(branch=="none"){
            $("#branchError").html("<label for='branch' class='error'><span class='text-danger'>Please Select Branch.</span></label>");
            e.preventDefault();
            return false;
        }else{
            $("#branchError").html("");
            if(class_val=="none")
            {
                $("#classError").html("<label for='class' class='error'><span class='text-danger'>Please Select Class.</span></label>");
                e.preventDefault();
                return false;
                
            }else{
                if(section=="none"){
                 $("#sectionError").html("<label for='branch' class='error'><span class='text-danger'>Please Select Teacher.</span></label>");
                    e.preventDefault();
                    return false;
                }else {
                    if(task==""){
                        $("#taskError").html("<label for='task' class='error'><span class='text-danger'>Please Enter Task.</span></label>");
                        e.preventDefault();
                        return false;
                    }else{
                        if(date==""){
                            $("#dateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Date.</span></label>");
                            e.preventDefault();
                            return false;
                        }else {
                            if(duedate==""){
                                $("#duedateError").html("<label for='datepicker' class='error'><span class='text-danger'>Please Select Due Date.</span></label>");
                                e.preventDefault();
                                return false;
                            }else {
                                if(time==""){
                                    $("#timeError").html("<label for='time' class='error'><span class='text-danger'>Please Enter Time.</span></label>");
                                    e.preventDefault();
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    });

        $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                    $.each( data, function( key, val ) {
                        items.push( "<option value='"+this['Cla_Id']+"'>" + this['Cla_Class'] + "</option>" );
                     });
                    $("#class").append('<option value="none">-- Select--</option>');
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });
        $("#class").on('change',function(){
            var Branch_Id = $('#branch').val();
            $.ajax({
                type : "POST",
                url : "{{ url('teacher') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Branch_Id : Branch_Id
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        console.log(data);
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Use_Id'] + "'>" + this['Use_Name'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });

    });
});

$(function() {
    $( "#datepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        minDate: 0,
        onSelect: function(selectedDate) {
            $('#duedatepicker').datepicker('option', 'minDate', selectedDate || '2013-09-10');
        }
    });
    $( "#duedatepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        minDate: 0,
        onSelect: function(selectedDate) {
            $('#duedatepicker').datepicker('option', 'minDate', selectedDate || '2013-09-10');
        }
        // minDate: parsed_date
    });
	
	$(".datetimepicker3").clockpicker();
});

</script>
@endpush
@section('footer_link_and_scripts')