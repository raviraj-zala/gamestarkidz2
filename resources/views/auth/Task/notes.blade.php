@extends('auth.Master.master')

@section('title','Task Notes')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text">
   <h2><img src="{{ asset('images/report_img.png') }}" alt="">Task Notes</h2>   
 </div>
</div>



@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <input type="hidden" name="id" value="@if(isset($task->Tas_Id)){{$task->Tas_Id}}@endif">
    <div class="left-form">
            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box">
                    <input disabled type="text" class="disabled" name="branch" id="branch" tabindex="6" readonly value="@if(isset($task->Brn_Name)){{$task->Brn_Name}}@endif" maxlength="15">
                
                <div id="branchError"></div>
                </div>    
            </div>

            

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                    <input disabled type="text" class="disabled" name="class" id="class" tabindex="6" readonly value="@if(isset($task->cla_Class)){{$task->cla_Class}}@endif" maxlength="15">
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Teacher <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <input type="text" name="teacher" id="teacher" tabindex="6" readonly value="@if(isset($task->Use_Name)){{$task->Use_Name}}@endif" maxlength="15">
                <div id="sectionError"></div>
                @if (Session::has('teacher'))
                <label class="error" for="teacher">
                    <span class="text-danger">
                        {{ Session::get('teacher') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Task <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="task" id="task" class="{{ $errors->has('task') ? ' is-invalid' : '' }}" tabindex="6" readonly value="@if(isset($task->Tas_Title)){{$task->Tas_Title}}@else{{ old('task') }}@endif" maxlength="15">
                    @if ($errors->has('task'))
                        <label class="error" for="task">
                            <span class="text-danger">
                                {{ $errors->first('task') }}
                            </span>
                        </label>
                    @endif
                    <div id="taskError"></div>
                </div>
            </div>
            @php
                if(isset($task->Tas_Status)){
                    if($task->Tas_Status==1){
                        $status="Approve";
                    }elseif($task->Tas_Status==2){
                        $status="Close";
                    }elseif($task->Tas_Status==3){
                        $status="Complete";
                    }elseif($task->Tas_Status==4){
                        $status="Reopen";
                    }elseif($task->Tas_Status==0){
                        $status="Open";
                    }elseif($task->Tas_Status==5){
                        $status="Pending";
                    }
                }
            @endphp
            <div class="form-box">
                    <div class="form-text">
                      <h5>Status<span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <input type="text" name="status" tabindex="6" readonly value="{{$status}}" maxlength="15">
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                    </div>    
                </div>           
        </div>
        @php 
            if(isset($task->Tas_Date)){
                $date = date('d/m/Y',strtotime($task->Tas_Date));
            }
        @endphp
        <div class="right-form">
            <div class="form-box">
                <div class="form-text">
                    <h5>Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="date" id="datepicker" tabindex="3" value="@if(isset($task->Tas_Date)){{$date}}@endif" readonly>
                    <div id="dateError"></div>
                    @if (Session::has('date'))
                    <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>
			
			 @php
                if(isset($task->Tas_Due_Date)){
                    $duedate = date('d/m/Y',strtotime($task->Tas_Due_Date));
                }
            @endphp

            <div class="form-box">
                <div class="form-text">
                    <h5>Due Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="date" id="datepicker" tabindex="3" value="@if(isset($task->Tas_Due_Date)){{$duedate}}@endif" readonly>
                    <div id="dateError"></div>
                    @if (Session::has('date'))
                    <label class="error" for="date">
                        <span class="text-danger">
                            {{ Session::get('date') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Time <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="time" name="time" tabindex="3" id='time' readonly value="@if(isset($task->Tas_Time)){{$task->Tas_Time}}@endif">
                    <div id="timeError"></div>
                    @if (Session::has('time'))
                    <label class="error" for="time">
                        <span class="text-danger">
                            {{ Session::get('time') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Description <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <textarea name="description" id="description" raws="3" readonly>@if(isset($task->Tas_Description)){!! $task->Tas_Description  !!}@endif</textarea> 
                    @if ($errors->has('description'))
                      <label class="error" for="description">
                        <span class="text-danger">
                            {{ $errors->first('description') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div> 
    </div>
</div>  

<div id="progress_report_data">
    
    <div class="line-1">        
        <div class="mangement-btn user-text">
            <h2><img src="{{ asset('images/report_img.png') }}" alt="">Notes</h2>      
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="main-btn">
            <ul>
                <li>
                    <input class="create" value="create" type="button" id="btnCreate">
                </li>
                <li><input type="submit" value="Delete" class="delete" id="btnDelete" ></li>
            </ul>
          </div>
        </div>
    </div>
    <div class="table-form">   
        <table id="example2">
            <thead>
                <tr>
                    <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
                        <label for="selectall" class="css-label table-ckeckbox">
                        <img src="{{ asset('images/checkbox-white.jpg') }}" alt="checkbox-white">
                    </th>
                    <th class="td-left">Assigne By</th>  
                    <th class="td-left">Note</th>  
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @if(count($notes))
                    @foreach($notes as $note)
                    <tr>
                        <td><input id="class{{ $i }}" type="checkbox" name="id[]" value="{{ $note->Tas_Not_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
                        <td class="td-left">{{$note->Use_Name}}</td>
                        <td class="td-left">{{$note->Note}}</td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                @else
                    <tr>
                    <td></td>
                    <td class="td-left"></td>
                    <td class="td-left"><form name="add-student" role="form" method="Post" action="{{ url('create_note') }}" enctype="multipart/form-data" id="taskForm">{{ csrf_field() }}<input type="hidden" name="id" value="@if(isset($task->Tas_Id)){{$task->Tas_Id}}@endif"><input type="text" class="text-style" name="note" id="note" tabindex="6" value="{{ old('note') }}"><input type="submit" id="submitForm" value="Add" class="add" ><div id="noteError"></div>@if($errors->has('note'))<span class="text-danger">{{ $errors->first('note') }}</span>@endif</form></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<style>
    .text-style{
        width: 85%; 
        height: auto; 
        margin: 5px; 
        display: inline-block; 
        color: #ff975b; 
        padding: 5px; 
        border: 1px solid #ff975b;
    }
    .add{
        display: inline-block;
        font-size: 18px;
        color: #fff;
        padding: 7px 28px;
        border-radius: 50px;
        text-transform: capitalize;
        font-weight: 600;
        margin: 0 3px;
        background: #5ade6a;
    }
    .text-danger {
        color: #ff975b;
    }
	

 input.disabled:hover {
    cursor:not-allowed
 }
 

</style>
@endsection

@push('footer_script')
<script>    

$("#submitForm").click(function(e){
        var note = $("#note").val();
        if(note==""){
            $("#noteError").html("<label for='note' class='error'><span class='text-danger'>Please Enter Notes.</span></label>");
            e.preventDefault();
            return false;
        }
    });

    $('#btnDelete').on('click',function(){
        var n = $("input:checked").length;
        if (n > 0)
        {
            var ch_delete=confirm('Delete selected records?');
            if(ch_delete){
                var ids = [];
                $('input:checked').each(function(i){
                    ids[i] = $(this).val();
                }); 
                console.log(ids);   
                $.ajax({
                    type : "POST",
                    url : "{{ url('delete_note_task') }}",
                    data : { 
                            _token:     '{{ csrf_token() }}',
                            ids : ids
                        },
                    success : function(data){
                        // console.log(data);
                        window.location.reload();
                    }
                });
            }
        }else{
            alert('Please select atleast one record!');
        }
    });

    $('#btnAdd').on('click',function(){
        var note = $('#note').val();
        var id = $('#id').val();
        if(note!=""){ 
            $.ajax({
                type : "POST",
                url : "{{ url('create_note') }}",
                data : { 
                    _token:     '{{ csrf_token() }}',
                    id : id,
                    note : note
                },
                success : function(data){
                    console.log(data);
                }
            });
        }else{
            alert('Please Enter Note!');
        }
    });

    $('#btnCreate').on('click',function(){
        var table = $('#example2');
        if($('#note').length){
        }else{  
            table.append('<tr><td></td><td></td><td><form name="add-student" role="form" method="Post" action="{{ url("create_note") }}" enctype="multipart/form-data" id="taskForm">{{ csrf_field() }}<input type="hidden" name="id" value="@if(isset($task->Tas_Id)){{$task->Tas_Id}}@endif"><input type="text" class="text-style" name="note" id="note" tabindex="6" value="{{ old("note") }}"><input type="submit" id="submitForm" value="Add" class="add" >@if($errors->has("note"))<span class="text-danger">{{ $errors->first("note") }}</span>@endif</form></td></tr>');
        }
    });
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
