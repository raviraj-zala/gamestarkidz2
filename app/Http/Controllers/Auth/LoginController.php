<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use App\User;
use App\Model\Users;
use App\Helper\Exception;
use Illuminate\Support\Facades\Input;

/**
 * @OA\Post(
 * path="/api/sign_In",
 * summary="Sign in",
 * description="Login by email, password",
 * operationId="authLogin",
 * tags={"auth"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"username","password"},
 *       @OA\Property(property="username", type="string", format="username", example="branchadmin2@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="12345678"),
 *       @OA\Property(property="_token", type="string", format="password", example="T48rttktwm8z3ygNS26ddGLFNbDufHWvWQbefZhM"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(){
    	return view('auth.login');
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'Use_Name';
    }

    public function password()
    {
        return 'Use_Password';
    }

    public function login(Request $request)
    {
        try 
        {
            $credentials = [
                'Use_Email' => $request['username'],
                'password' => $request['password'],
            ];

            $auth = Auth::attempt($credentials,false);
            
            if($auth)
              return redirect('home');
            else
                return redirect('login')->with('error',"Username and password does not match.");;
        } catch (Exception $e) {
            Exception::exception($e);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
