<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Helper\Notification;
use App\User;
use App\Model\Users;
use App\Model\Homework;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Homework_view;
use Auth;
use Input;
use Validator;
use DB;

class HomeworkservicesController extends Controller
{
   //Class Section List
	public function class_Section_List(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(User::find(Input::get('user_id')))
			{
				$class_id = Input::get('class_id');
				$className = ClassTbl::where('Cla_Id',$class_id)->first()->Cla_Class;
				$branch_id = ClassTbl::where('Cla_Id',$class_id)->first()->Cla_Bra_Id;
				$section = ClassTbl::where('Cla_Bra_Id',$branch_id)->where('Cla_Class',$className)->select(['Cla_Id','Cla_Section'])->distinct()->get();
				if($section)
				{
					ResponseMessage::success("Class List",$section);
				}else
				{
					ResponseMessage::error('Record Not Found');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}

		} 
		catch (Exception $e) {
			Exceptions::exception($e);
		}
	} 
     
    //Homework Create
	  
      /**
       * 
       * Create Homework Date: 23-05-2018
       * Updated By Jayesh Sukhadiya  To add one new field image(to upload all file)
       */
     
    //HomeWork Create
	public function create_Homework(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				'section' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}
			if(ClassTbl::where('Cla_Id',Input::get('class_id'))->where('Cla_Section',Input::get('section'))->exists())
			{
				$subject = Input::get('subject');
				$date = Input::get('date');

				if($request->file('image')){
	            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $isUpload = $request->file('image')->move(public_path('images/homework'), $imageName);
		        }else{
		            $imageName = "";
		        }

				$class_id = ClassTbl::where('Cla_Id',Input::get('class_id'))->where('Cla_Section',Input::get('section'))->first()->Cla_Id;

						$homework['Hmw_Cla_Id'] = $class_id;
						$homework['Hmw_Subject'] = Input::get('subject');
						$homework['Hmw_Chapter'] = "0";
						$homework['Hmw_Date'] = Input::get('date');
						$homework['Hmw_Page_No'] = "0";
						$homework['Hmw_File'] = $imageName;
						$homework['Hmw_Desscription'] = Input::get('description');
						$homework['Hmw_Class_Work'] = Input::get('class_work');
						$homework['Hmw_CreatedBy'] = Input::get('user_id');
						$homework['Hmw_UpdatedBy'] = Input::get('user_id');
						$homework['Hmw_CreatedAt'] = date('Y-m-d H:i:s');
						$homework['Hmw_UpdatedAt'] = date('Y-m-d H:i:s');

						$lastid = Homework::insertGetId($homework);
					
						$class_id = User::where('Use_Id',Input::get('user_id'))->first()->Use_Cla_Id;
						$teacher_name = User::where('Use_Id',Input::get('user_id'))->first()->Use_Name;
						$student = Student::where('Std_Cla_Id',Input::get('class_id'))->select('Std_Parent_Id')->get()->toArray();
						$parent_id = User::whereIn('Use_Id',$student)->get();
						foreach ($parent_id as $value) {

							$parent = new Homework_view;
							$parent->Hwv_Use_Id = $value->Use_Id;
							$parent->Hwv_Hmw_Id = $lastid;
							$parent->Hwv_Flag = "0" ;
							$parent->Hwv_CreatedBy = Input::get('user_id');
							$parent->Hwv_UpdatedBy = Input::get('user_id');
							$parent->Hwv_CreatedAt = date('Y-m-d H:i:s');
							$parent->Hwv_UpdatedAt = date('Y-m-d H:i:s');
							$parent->save();
							$student_name = Student::where("Std_Parent_Id",$value->Use_Id)->first()->Std_Name;

							if($value["Use_Token"]!=""){
						Notification::sendNotification($value["Use_Token"],$teacher_name." Assign Homework.","Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name,"HOMEWORK");
							}
							if($value["Use_Mother_Token"]!=""){
						Notification::sendNotification($value["Use_Mother_Token"],$teacher_name." Assign Homework.","Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name,"HOMEWORK");
							}
						}
						if($homework)
						{
							ResponseMessage::success("Homework Submitted",$homework);
						}else
						{
							ResponseMessage::error('Homework Submission Fail');
						}
			}else{
				ResponseMessage::error("Class And Section Not Found");
			}
		}
		catch (Exception $e) {
			Exceptions::exception($e);
		}
	} 
 
    // Parent Homework View
    public function view_Homework(Request $request)
	{
		try 
		{
			$rules = [
				'parent_id' => 'required',
				'homework_id' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Input::get('flag') == 1)
			{
				$flag = 1;
			}else{
				$flag = 0;
			}
                             
            $homework['Hwv_Use_Id'] = Input::get('parent_id');
            $homework['Hwv_Hmw_Id'] = Input::get('homework_id');
			$homework['Hwv_Flag'] = $flag;
			$homework['Hwv_UpdatedAt'] = date('Y-m-d H:i:s');
			$update =Homework_view::where('Hwv_Use_Id',Input::get('parent_id'))->where('Hwv_Hmw_Id',Input::get('homework_id'))->update($homework);

			if($update)
			{
				ResponseMessage::success("Homework View success",$homework);
			}else{
				ResponseMessage::error("Homework Not View");
			}
		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Teacher HomeWork List

	public function teacher_Homework_List(Request $request)
	{
		try
		{
			$rules = [
			'user_id' =>  'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			$homework = Homework::where('Hmw_CreatedBy',Input::get('user_id'))
			->select(array(
			DB::raw("homework_tbl.*,class_tbl.*"),
   			DB::raw("(SELECT COUNT(homework_view_tbl.Hwv_Flag) FROM homework_view_tbl WHERE homework_view_tbl.Hwv_Hmw_Id = homework_tbl.Hmw_Id AND Hwv_Flag = 1 GROUP BY homework_view_tbl.Hwv_Hmw_Id) as Hmw_View"),
    		DB::raw("(SELECT COUNT(homework_view_tbl.Hwv_Flag) FROM homework_view_tbl WHERE homework_view_tbl.Hwv_Hmw_Id = homework_tbl.Hmw_Id AND Hwv_Flag = 0 GROUP BY homework_view_tbl.Hwv_Hmw_Id) as Hmw_Not_View")))
			->join('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')
			
			->orderBy('Hmw_CreatedAt','DESC')
			->get();

			if($homework->isEmpty())
			{
				ResponseMessage::error('Homework List Not Found');
			}else{
				ResponseMessage::success('Homework List',$homework);
			}
		}catch (Exception $e) {
		Exceptions::exception($e);
		}
	}
// Teaher homework list monthwise
    public function teacher_Homework_Monthwise_List(Request $request)
	{
		try
		{
			$rules = [
			'user_id' =>  'required',
                        'month' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

                        $month = Input::get('month');
			$homework = Homework::where('Hmw_CreatedBy',Input::get('user_id'))
			->select(array(
			DB::raw("homework_tbl.*,class_tbl.*"),
   			DB::raw("(SELECT COUNT(homework_view_tbl.Hwv_Flag) FROM homework_view_tbl WHERE homework_view_tbl.Hwv_Hmw_Id = homework_tbl.Hmw_Id AND Hwv_Flag = 1 GROUP BY homework_view_tbl.Hwv_Hmw_Id) as Hmw_View"),
    		DB::raw("(SELECT COUNT(homework_view_tbl.Hwv_Flag) FROM homework_view_tbl WHERE homework_view_tbl.Hwv_Hmw_Id = homework_tbl.Hmw_Id AND Hwv_Flag = 0 GROUP BY homework_view_tbl.Hwv_Hmw_Id) as Hmw_Not_View")))
			->join('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')
			->whereRaw('MONTH(Hmw_Date) = ?',[$month])
			->orderBy('Hmw_CreatedAt','DESC')
			->get();

			if($homework->isEmpty())
			{
				ResponseMessage::error('Homework List Not Found');
			}else{
				ResponseMessage::success('Homework List',$homework);
			}
		}catch (Exception $e) {
		Exceptions::exception($e);
		}
	}
   
	//Parent Views Homework List
	public function parent_Homework_List(Request $request)
	{
		try
		{
			$rules = [
			'user_id' => 'required',
			'homework_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{	
				$homework_id = Input::get("homework_id");
				$homework_view = DB::table('homework_view_tbl')
							->where("Hwv_Hmw_Id",$homework_id)
							->join('homework_tbl','homework_tbl.Hmw_Id','=','homework_view_tbl.Hwv_Hmw_Id')
							->join('user_tbl','user_tbl.Use_Id','=','homework_view_tbl.Hwv_Use_Id')
							->join('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')
							->join('student_tbl',function($query){
								$query->on('student_tbl.Std_Cla_Id','=','homework_tbl.Hmw_Cla_Id')
								->on('student_tbl.Std_Parent_Id','=','user_tbl.Use_Id');
							})->select(['user_tbl.Use_Name','student_tbl.Std_Name','homework_view_tbl.Hwv_Flag'])->get();
				if($homework_view)
				{
					ResponseMessage::success('Homework List',$homework_view);
				}else{
					ResponseMessage::error('Homework List Not Found');
				}
			}else{
				ResponseMessage::error("User Not Found");
			}	
		} 
		catch (Exception $e) {
		Exceptions::exception($e);
		}
	}
	

	//Homework List
	  
      /**
       * 
       * Homework List 
       * Updated By Ravirajsinh Zala on send homework view or not particular parent
       *
       * @param integer $parent_id  parent id 
       * @return JSON
       */
	public function homework_List(Request $request)
	{
		try
		{
			$rules = [
				'parent_id' => 'required',
				'student_id' => '',
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(Student::where('Std_Parent_Id',Input::get('parent_id'))->exists())
			{
				if(Input::get('student_id')!=""){
					$class_id = Student::where('Std_Id',Input::get('student_id'))->first()->Std_Cla_Id;
				} else{
					$class_id = Student::where('Std_Parent_Id',Input::get('parent_id'))->first()->Std_Cla_Id;
				}

				$class = DB::table('class_tbl')->where('Cla_Id',$class_id)->first();

				$homework = Homework::with(array('Section' => function($query) use ($class_id){
				$query->where('Cla_Id',$class_id);
				}))->where('Hmw_Cla_Id',$class_id)->join('homework_view_tbl',function($query){
					$query->on('homework_view_tbl.Hwv_Hmw_Id','=','homework_tbl.Hmw_Id')
					->where('homework_view_tbl.Hwv_Use_Id','=',Input::get('parent_id'));
				})->orderBy('Hmw_CreatedAt','DESC')->select('homework_tbl.*','homework_view_tbl.Hwv_Flag')->get();

				if($homework){
					ResponseMessage::success('Homework List',$homework);
				}else{
					ResponseMessage::error('Homework List Not Found');
				}
			}else{
				ResponseMessage::error("Parent Not Found");
			}	
		} 
		catch (Exception $e) {
		Exceptions::exception($e);
		}
	}

         // Month wise Homework List
	public function homework_Monthwise_List(Request $request)
	{
		try
		{
			$rules = [
				'parent_id' => 'required',
				'month' => 'required',
				'student_id' => ''
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			$month = Input::get('month');

			if(Student::where('Std_Parent_Id',Input::get('parent_id'))->exists())
			{	
				if(Input::get('student_id')!=""){
					$class_id = Student::where('Std_Id',Input::get('student_id'))->first()->Std_Cla_Id;
				}else{
					$class_id = Student::where('Std_Parent_Id',Input::get('parent_id'))->first()->Std_Cla_Id;
				}

				$class = DB::table('class_tbl')->where('Cla_Id',$class_id)->first();

                                $homework = Homework::with(array('Section' => function($query) use ($class_id){
				$query->where('Cla_Id',$class_id);
				}))->where('Hmw_Cla_Id',$class_id)->join('homework_view_tbl',function($query){
					$query->on('homework_view_tbl.Hwv_Hmw_Id','=','homework_tbl.Hmw_Id')
					->where('homework_view_tbl.Hwv_Use_Id','=',Input::get('parent_id'));
				})->orderBy('Hmw_CreatedAt','DESC')->whereRaw('MONTH(Hmw_Date) = ?',[$month])->select('homework_tbl.*','homework_view_tbl.Hwv_Flag')->get();

				if($homework){
					ResponseMessage::success('Homework List',$homework);
				}else{
					ResponseMessage::error('Homework List Not Found');
				}
			}else{
				ResponseMessage::error("Parent Not Found");
			}	
		} 
		catch (Exception $e) {
		Exceptions::exception($e);
		}
	}
}
