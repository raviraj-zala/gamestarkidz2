<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\User;
use App\Model\Student;
use App\Model\Branch;
use App\Helper\Exceptions;
use App\Model\Payment;
use Validator;
use App\Model\ClassTbl;
use Exception;

class PaymentController extends Controller
{
    public function get_payment_details(Request $request)
    {
    	try {
    		$rules = [
                'student_id' => 'required',
            ];
            $customeMessage = [
                'student_id.required' => 'Please sent student id.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else{
	        	$student_id = trim(strip_tags($request->student_id));
                if(Student::where('Std_Id',$student_id)->exists()){
                    $studentDetails = Student::where('Std_Id',$student_id)->get()->first();
                    if($studentDetails){
                        $payemts = Payment::select("Pay_Type","Pay_Date","Pay_Amount","Pay_Status")->where('Pay_Std_Id',$student_id)->get();
                        $fee = array("fee"=>$studentDetails->Std_Fee,"payment"=>$payemts);

                        ResponseMessage::success($studentDetails->Std_Fee,$payemts);
                        // $response = array();
                        // $fee = $studentDetails->Std_Fee;
                        // $paidAmount = 0;
                        // $payemts = Payment::where('Pay_Std_Id',$student_id)->get();
                        // if(count($payemts)!=0){
                        //     $duedate = count($payemts);
                        //     $countr = 1;
                        //     foreach ($payemts as $payment) {
                        //         $paidAmount = (int)($paidAmount) + (int)($payment->Pay_Amount);
                        //         if($duedate==$countr){
                        //             $duedate = $payment->Pay_Date;
                        //         }
                        //         $countr++;
                        //     }
                        //     $response["total_fee"] = $fee;
                        //     if($fee==$paidAmount){
                        //         $response["remaining"] = 0;
                        //         $response["paid"] = $paidAmount;
                        //         $response["duedate"] = 0;
                        //         $response["type"] = "complated";
                        //         ResponseMessage::success("Full Payment",$response);
                        //     } else if($fee>$paidAmount){
                        //         $remainingAmount = (int)$fee - (int)$paidAmount;
                        //         $response["remaining"] = $remainingAmount;
                        //         $response["paid"] = $paidAmount;
                        //         $response["duedate"] = $duedate;
                        //         $response["type"] = "remaining";
                        //         ResponseMessage::success("Remaining Payment",$response);
                        //     } else{
                        //         ResponseMessage::success("unknown");
                        //     }
                        // } else{
                        //     ResponseMessage::error("Record not found.");         
                        // }
                    } else{
                        ResponseMessage::error("Student not found, please try again.");     
                    }
                } else{
                    ResponseMessage::error("Student not found, please try again.");	
                }
	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
