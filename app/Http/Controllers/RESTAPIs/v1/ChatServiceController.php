<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\User;
use App\Model\Student;
use App\Model\Branch;
use App\Helper\Exceptions;
use App\Model\Payment;
use Validator;
use App\Model\ClassTbl;
use App\Model\GroupMaster;
use App\Model\GroupDetail;
use App\Model\Group;
use App\Model\Chat;
use Exception;
use DB;

class ChatServiceController extends Controller
{
    public function restore_chat(Request $request)
    {
        try {
            $rules = [
                'user_id' => 'required',
            ];
            $customeMessage = [
                'user_id.required' => 'Please sent user id.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else{
                $user_id = trim(strip_tags($request->user_id));
                if(User::where('Use_Id',$user_id)->exists()){

                    // if(isset($request->debug) && $request->debug=="true"){
                    //     $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage',DB::raw('MAX(chat_tbl.Cha_CreatedAt) AS max_Cha_CreatedAt'))
                    //             ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                    //             ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                    //             ->orderBy('max_Cha_CreatedAt','DESC')
                    //             ->groupBy('chat_tbl.Cha_Receiver_Id')
                    //             ->get();

                    //     ResponseMessage::success('Debug',$chat_history);
                    //     exit();
                    // }

                    
                    $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage',DB::raw('MAX(chat_tbl.Cha_CreatedAt) AS Cha_CreatedAt'),DB::raw('"" as Grp_Cha_Mas_Id'))
                                ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                                ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                                ->orderBy('Cha_CreatedAt','DESC')
                                ->groupBy('chat_tbl.Cha_Receiver_Id')
                                ->get()
                                ->toArray();

                    $groupMasterIds = GroupDetail::select('Grp_Det_Mas_Id')->where('Grp_Det_Mem_Id',$user_id)->orwhere('Grp_Det_CreatedBy',$user_id)->get()->toArray();

                    $gchat_history = Group::select('group_chat_tbl.Grp_Cha_Id as Cha_Id','group_chat_tbl.Grp_Cha_Sender_Id as Cha_Sender_Id','group_chat_tbl.Grp_Cha_Mas_Id','group_chat_tbl.Grp_Cha_Message as Cha_Message','group_chat_tbl.Grp_Cha_Video as Cha_Video','group_chat_tbl.Grp_Cha_Image as Cha_Image','group_chat_tbl.Grp_Cha_Audio as Cha_Audio','group_chat_tbl.Grp_Cha_Document as Cha_Document','group_chat_tbl.Grp_Cha_Type as Cha_Type','group_chat_tbl.Cha_Receiver_Id as Cha_Receiver_Id','group_chat_tbl.Grp_Cha_CreatedBy as Cha_CreatedBy','group_chat_tbl.Grp_Cha_CreatedAt as Cha_CreatedAt','group_chat_tbl.Grp_Cha_UpdatedBy as Cha_UpdatedBy','group_chat_tbl.Grp_Cha_UpdatedAt as Cha_UpdatedAt','group_master_tbl.Grp_Mas_Name as receiverName','group_master_tbl.Grp_Mas_Image as receiverImage',DB::raw('MAX(group_chat_tbl.Grp_Cha_CreatedAt) AS Cha_CreatedAt'))
                                ->whereIn('group_chat_tbl.Grp_Cha_Mas_Id',$groupMasterIds)
                                ->join('user_tbl','user_tbl.Use_Id','=','group_chat_tbl.Grp_Cha_Sender_Id')
                                ->join('group_master_tbl','group_master_tbl.Grp_Mas_Id','=','group_chat_tbl.Grp_Cha_Mas_Id')
                                ->orderBy('Cha_CreatedAt','DESC')
                                ->groupBy('group_chat_tbl.Grp_Cha_Mas_Id')
                                ->get()
                                ->toArray();

                    $chat_history = array_merge($chat_history,$gchat_history);

                    usort($chat_history,function($a, $b)
                    {
                        $t1 = strtotime($a['Cha_CreatedAt']);
                        $t2 = strtotime($b['Cha_CreatedAt']);
                        return $t2 - $t1;
                    });

                    // usort($chat_history, function($a, $b) {
                    //     return $a['Cha_CreatedAt'] <=> $b['Cha_CreatedAt'];
                    // });

                    // rsort($chat_history);

                    // $chat_history_group = Group::select('group_chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage',DB::raw('MAX(group_chat_tbl.Grp_Cha_CreatedAt) AS Grp_Cha_CreatedAt'))
                    //             ->where('group_chat_tbl.Grp_Cha_Sender_Id',$user_id)
                    //             ->join('user_tbl','user_tbl.Use_Id','=','group_chat_tbl.Grp_Cha_Sender_Id')
                    //             ->orderBy('Grp_Cha_CreatedAt','DESC')
                    //             ->groupBy('group_chat_tbl.Grp_Cha_Sender_Id')
                    //             ->get()
                    //             ->toArray();
                    // $chat_history_all =  array_merge($chat_history,$chat_history_group);
                                
                    // $group  = array();                               
                    // foreach ($chat_history_group as $chat_history_group) {
                    //     $group['Cha_Id'] = $chat_history_group->Grp_Cha_Id;
                    // }
                    
                    // $chat_history =  array_merge($chat_history,$group);
                    // array_push($chat_history, $chat_history_group);            
                    // array_push($chat_history, $chat_history_group);
                    ResponseMessage::success('Chat History',$chat_history);
                        exit();


                    // $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage')
                    //                     ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                    //                     // ->where('Cha_Receiver_Id','!=',$user_id)
                    //                     ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                    //                     ->where('user_tbl.Use_Id','!=',null)
                    //                     ->groupBy('chat_tbl.Cha_Receiver_Id')
                    //                     ->get();
                    // ResponseMessage::success('Chat History',$chat_history);
                } else{
                    ResponseMessage::error("User not found, please try again.");     
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
