<?php

namespace App\Http\Controllers\RESTAPIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\StudentExamAns;
use App\Model\Student;
use Validator;
use App\Helper\ResponseMessage;
use Auth;

class ExamController extends Controller
{
    public function examList(Request $request){
        try {
            $rules = [
                'class_id' => 'required',
            ];
            $customeMessage = [
                'class_id.required' => 'Please enter class id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            // dd($request->all());
            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                // dd(QuestionClass::get());
                // if(QuestionClass::where('Que_Cla_Id', $request->class_id)->exists()){
                    $exams = QuestionClass::join('branch_tbl','question_class_tbl.Cla_Bra_Id','branch_tbl.Brn_Id')
                                    ->select('question_class_tbl.Que_Cla_Id',
                                        'question_class_tbl.Exam_Title',
                                        'question_class_tbl.Exam_Subject',
                                        'question_class_tbl.Exm_Date',
                                        'question_class_tbl.Exm_Due_Date',
                                        'question_class_tbl.Exm_Time',
                                        'question_class_tbl.Cla_Id',
                                        'question_class_tbl.Cla_Sec_Id',
                                        'branch_tbl.Brn_Name')
                                    ->orderBy('Que_Cla_CreatedAt', 'DESC')
                                    ->where('Que_Cla_Status',1)
                                    // ->where('question_class_tbl.Que_Cla_Id', $request->class_id)
                                    ->get();

                    if(count($exams)){
                        $exam_array = array();
                        foreach($exams as $exam){
                            $class = explode(',',$exam->Cla_Id);
                            $class_id = $class[1];
                            $branch_id = $class[0];
                            if (ClassTbl::where('Cla_Id',$request->class_id)->exists()) {
                                $class_data = ClassTbl::where('Cla_Id',$request->class_id)->first();
                                $class_branch = $class_data->Cla_Bra_Id.','.$class_data->Cla_Class;
                                    // dd($class_branch);
                                if ($class_branch == $exam->Cla_Id) {
                                    $exam->Cla_Id = $class[0];
                                   
                                    $exam_array_single = array(
                                            'Cla_Id'  => $request->class_id,
                                            'Que_Cla_Id'  => $exam->Que_Cla_Id,
                                            'Exam_Title' => $exam->Exam_Title,
                                            'Exm_Date' => $exam->Exm_Date,
                                            'Exm_Due_Date' => $exam->Exm_Due_Date,
                                            'Exm_Time' => $exam->Exm_Time,
                                            'Exam_Subject' => $exam->Exam_Subject,
                                            'Cla_Sec_Id' => $exam->Cla_Sec_Id,
                                            'Brn_Name' => $exam->Brn_Name,
                                        );
                                    array_push($exam_array, $exam_array_single);
                                }
                            }
                        }
                        if (count($exam_array)) {
                            ResponseMessage::success('Success', $exam_array);
                        }else{
                            ResponseMessage::error("Class not found.");
                        }
                    }else{
                        ResponseMessage::error("Data not found.");
                    }
                // }else{
                //     ResponseMessage::error("Class not found.");
                // }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }



    public function questionList(Request $request){
        try {
            $rules = [
                'exam_id' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->where('Que_Cla_Status',1)->exists()){
                    $questions = Question::where('Que_Cla_Id',$request->exam_id)->get();
                    ResponseMessage::success('Success', $questions);
                }else{
                    ResponseMessage::error("Exam not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function submit(Request $request){
        try {
            $rules = [
                'exam_id' => 'required',
                'student_id' => 'required',
                'questions' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
                'student_id.required' => 'Please enter student id',
                'questions.required' => 'Please enter question',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->where('Que_Cla_Status',1)->exists()){
                    if(Student::where('Std_Id',$request->student_id)->where('Std_Status',1)->exists()){
                        $test = new StudentExamAns();
                        $test->Que_Cla_Id = $request->exam_id;
                        $test->Stu_Id = $request->student_id;
                        $test->Ques_Ans = $request->questions;
                        $test->save();
                        ResponseMessage::success('Success', $test);
                    }else{
                        ResponseMessage::error("Student not found.");
                    }
                }else{
                    ResponseMessage::error("Exam not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function result(Request $request) {
        try {
            $rules = [
                'class_id' => 'required',
                'student_id' => 'required',
                'exam_id' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
                'student_id.required' => 'Please enter student id',
                'class_id.required' => 'Please enter class id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(ClassTbl::where('Cla_Id', $request->class_id)->where('Cla_Status',1)->exists()){
                    if(Student::where('Std_Id', $request->student_id)->where('Std_Cla_Id',$request->class_id)->where('Std_Status',1)->exists()){
                        if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->exists()){
                            $exam_result = QuestionClass::where('Que_Cla_Id',$request->exam_id)
                                        ->Select('Cla_Bra_Id',
                                            'Cla_Id','Cla_Sec_Id',
                                            'Exam_Title',
                                            'Exam_Subject',
                                            'Exm_Date',
                                            'Exm_Due_Date',
                                            'Exm_Time'
                                        )
                                        ->first();
                            $student = Student::Select('Std_Gr_No','Std_Name')
                                        ->where('Std_Id', $request->student_id)
                                        ->first();
                            $exam_result->Std_Gr_No = $student->Std_Gr_No;
                            $exam_result->Std_Name = $student->Std_Name;
                            $questions = Question::select('Que_Id','Que_Question','Que_Option_1','Que_Option_2','Que_Option_3','Que_Option_4','Que_Ans')
                                        ->where('Que_Cla_Id',$request->exam_id)
                                        ->get();
                            $std_ans = StudentExamAns::where('Que_Cla_Id',$request->exam_id)
                                        ->where('Stu_Id',$request->student_id)
                                        ->first();
                            $decode = json_decode($std_ans->Ques_Ans);
                            // dd($decode);
                            $result = 0;
                            foreach ($questions as $question) {
                                foreach($decode as $key => $value){
                                    if($key == $question->Que_Id){
                                        $question->Std_Ans = $value;
                                        if($value == $question->Que_Ans){
                                            $result++;
                                        }
                                    }
                                }
                            }
                            $exam_result->correct_result = $result;
                            $exam_result->wrong_result = count($questions)-$result;
                            $exam_result->questions= $questions;
                            ResponseMessage::success('Success', $exam_result);
                        }else{
                            ResponseMessage::error("Exam not found.");    
                        }
                    }else{
                        ResponseMessage::error("Student not found.");
                    }
                }else{
                    ResponseMessage::error("Class not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function resultRating(Request $request) {
        try {
            $rules = [
                'class_id' => 'required',
                'branch_id' => 'required',
                'student_id' => 'required',
            ];
            $customeMessage = [
                'class_id.required' => 'Please enter class id',
                'branch_id.required' => 'Please enter branch id',
                'student_id.required' => 'Please enter student id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                $subject=[];
                if(Student::where('Std_Id', $request->student_id)->where('Std_Cla_Id',$request->class_id)->where('Std_Status',1)->exists()){
                    $class = ClassTbl::where('Cla_Id',$request->class_id)
                                    ->where('Cla_Bra_Id',$request->branch_id)
                                    ->first(['Cla_Class']);
                    $cla_branch = $request->branch_id.','.$class->Cla_Class;
                    $exam = Student::join('class_tbl','class_tbl.Cla_Id','student_tbl.Std_Cla_Id')
                            ->join('question_class_tbl','question_class_tbl.Cla_Bra_Id','class_tbl.Cla_Bra_Id')
                            ->Select('question_class_tbl.*')
                            ->where('student_tbl.Std_Id',$request->student_id)
                            ->where('question_class_tbl.Cla_id',$cla_branch)
                            ->get();
                    $sub=[];
                    $grand_total = $que_total = 0;
                     $exam_array = array();
                    foreach ($exam as $ex){
                        $questions = Question::select('Que_Id','Que_Ans')
                                        ->where('Que_Cla_Id',$ex->Que_Cla_Id)
                                        ->get();
                        $std_ans = StudentExamAns::where('Que_Cla_Id',$ex->Que_Cla_Id)
                                    ->where('Stu_Id',$request->student_id)
                                    ->first();
                        if($std_ans){
                            $decode = json_decode($std_ans->Ques_Ans);
                            $result = 0;
                            // dd($decode);
                            if (is_array($decode)) {
                                foreach ($questions as $question) {
                                    foreach($decode as $key => $value){
                                        if($key == $question->Que_Id){
                                            if($value == $question->Que_Ans){
                                                $result++;
                                            }
                                        }
                                    }
                                }
                            }
                            if(!array_key_exists($ex->Exam_Subject, $subject)){
                                $subject[$ex->Exam_Subject]=$result;
                                $sub[$ex->Exam_Subject]['que']=count($questions);
                                $sub[$ex->Exam_Subject]['total']=$result;
                                $sub[$ex->Exam_Subject]['percent']=number_format($sub[$ex->Exam_Subject]['total']/$sub[$ex->Exam_Subject]['que']*100, 2);
                                $grand_total = $grand_total+$result;
                                $que_total = $que_total+count($questions);
                            }else{
                                $total = $result+$subject[$ex->Exam_Subject];
                               
                                // dd($subject);
                                foreach ($subject as $key => $value) {
                                    // dd($value);
                                    $exam_array_single = array(
                                            'total'  => $total,
                                            'que'  => $sub[$ex->Exam_Subject]['que']+count($questions),
                                            'percent' => number_format($sub[$ex->Exam_Subject]['total']/$sub[$ex->Exam_Subject]['que']*100, 2),
                                            'sub' => $key,
                                        );
                                        array_push($exam_array, $exam_array_single);
                                    // if($ex->Exam_Subject == $key){
                                        

                                    //     // $exam_array['total'][] = $total;
                                    //     // $exam_array['que'] = $sub[$ex->Exam_Subject]['que']+count($questions);
                                    //     // $exam_array[]['percent'][] = number_format($sub[$ex->Exam_Subject]['total']/$sub[$ex->Exam_Subject]['que']*100, 2);

                                    //     $sub[$ex->Exam_Subject]['que']=$sub[$ex->Exam_Subject]['que']+count($questions);
                                    //     $subject[$ex->Exam_Subject] = $total;
                                    //     $sub[$ex->Exam_Subject]['total'] = $total;
                                    //     $sub[$ex->Exam_Subject]['percent']=number_format($sub[$ex->Exam_Subject]['total']/$sub[$ex->Exam_Subject]['que']*100, 2);
                                    //     $grand_total = $grand_total+$result;
                                    //     $que_total = $que_total+count($questions);
                                    // }
                                }
                            }
                        }
                    }
                    // dd($grand_total);
                    if ($grand_total!=0) {
                        $total_percentage = $grand_total/$que_total*100;
                    }else{
                        $total_percentage = 0;
                    }
                    $rating['total_percent'] = number_format($total_percentage,2);
                    // dd($exam_array);
                    $rating['subject'] = $exam_array;
                        // dd($total_percentage);
                    // array_push($subject, $sub);
                    ResponseMessage::success('Success', $rating);
                }else{
                    ResponseMessage::error("Student not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

}

