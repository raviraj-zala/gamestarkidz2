<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Notification;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Student;
use App\Model\Attendence;
use App\Model\ClassTbl;
use Auth;
use Input;
use Validator;
use DB;

class AttendenceservicesController extends Controller
{

        /**
	   * 
	   * Student Month Wise Attedence Report
	   *
	   * @author Jayesh Sukhadiya
	   *
	   * @param integer parent_id =  Parent Id
	   * @param integer class_id =  Student Class Id
	   * @param integer month =   parent select month
	   * @param integer year = parent select year
	   * @param $student = To get student detials using parend id 
	   * @return JSON Student Attedence List With Absent Or Present
	   */

	public function student_Attendence_List(Request $request)
	{
		try
		{
			$rules = [
				'parent_id' => 'required',
				'month' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Users::where('Use_Id',Input::get('parent_id'))->exists())
			{
				$month = Input::get('month');
				$year = date('Y');


				$student = Student::where('Std_Parent_Id',Input::get('parent_id'))->first();
				$attendenceMonth = Attendence::select('Att_Stu_Id','Att_Pre_Abs','Att_Date')->where('Att_Stu_Id',$student->Std_Id)->whereRaw('MONTH(Att_Date) = ?',[$month])->whereRaw('YEAR(Att_Date) = ?',[$year])->get();

				$present = Attendence::select('Att_Pre_Abs')->where('Att_Stu_Id',$student->Std_Id)->where('Att_Pre_Abs',1)->where('Att_Cla_Id',$student->Std_Cla_Id)->whereRaw('MONTH(Att_Date) = ?',[$month])->whereRaw('YEAR(Att_Date) = ?',[$year])->get()->count();

				$absent = Attendence::select('Att_Pre_Abs')->where('Att_Stu_Id',$student->Std_Id)->where('Att_Pre_Abs',0)->where('Att_Cla_Id',$student->Std_Cla_Id)->whereRaw('MONTH(Att_Date) = ?',[$month])->whereRaw('YEAR(Att_Date) = ?',[$year])->get()->count();
				$attendence = array(['image'=>$student->Std_Image,'present'=>$present,'absent'=>$absent,'month'=>$attendenceMonth]);

				if($attendence){
					ResponseMessage::success("Student Attendence list",$attendence);
				}else{
					ResponseMessage::error("No Record Found");
				}
			}else{
				ResponseMessage::error("User Not Found");
			}

		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

         /**
	   * 
	   * Student Date Wise Attedence Report
	   *
	   * @author Jayesh Sukhadiya Date : 08-05-2018
	   *
	   * @param integer parent_id =  Parent Id
	   * @param integer date =   selected date
	   * @var $student = To get student detials using parend id 
	   * @var $attendence = Date wise record to show 
	   * @return JSON Student Attedence List With Absent / Present
	   */
     
    public function attendence_Datewise_List(Request $request)
	{
		try
		{
			$rules = [
				'parent_id' => 'required',
				'date' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Users::where('Use_Id',Input::get('parent_id'))->exists())
			{
				$date = Input::get('date');

				$student = Student::where('Std_Parent_Id',Input::get('parent_id'))->first();

				$attendence = Attendence::select('Att_Date','Att_Pre_Abs')->where('Att_Stu_Id',$student->Std_Id)->where('Att_Cla_Id',$student->Std_Cla_Id)->where('Att_Date',$date)->get();

				if($attendence){
					ResponseMessage::success("Attendence list Successfully",$attendence);
				}else{
					ResponseMessage::error("No Record Found");
				}
			}else{
				ResponseMessage::error("User Not Found");
			}

		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

        /**
	   * 
	   * Student Attedence Month Wise
	   *
	   * @author Jayesh Sukhadiya Date : 09-05-2018
	   *
	   * @param integer parent_id =  Parent Id
	   * @param integer month =   selected month
	   * @var $student = To get student detials using parend id 
	   * @var $attendence = Date wise record to show 
	   * @return JSON Student Attedence List With Absent / Present
	   */
     
        public function attendence_Monthwise_List(Request $request)
		{
		try
		{
			$rules = [
				'parent_id' => 'required',
				'month' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Users::where('Use_Id',Input::get('parent_id'))->exists())
			{
				$month = Input::get('month');

				$student = Student::where('Std_Parent_Id',Input::get('parent_id'))->first();

				$attendence = Attendence::select('Att_Date','Att_Pre_Abs')->where('Att_Stu_Id',$student->Std_Id)->where('Att_Cla_Id',$student->Std_Cla_Id)->whereRaw('MONTH(Att_Date) = ?',[$month])->get();

				if($attendence){
					ResponseMessage::success("Attendence list Successfully",$attendence);
				}else{
					ResponseMessage::error("No Record Found");
				}
			}else{
				ResponseMessage::error("User Not Found");
			}

		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	
	//Get Student Absent Presetnt Current Date List
	public function get_student_List(Request $request){
		try{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$date = date('Y-m-d');

			if(Users::where('Use_Id',Input::get('user_id'))->exists())
			{
				$student = Student::with(['StdAttedence' => function($query) use($date){
                $query->where('Att_CreatedBy',Input::get('user_id'));
		 		$query->where('Att_Cla_Id',Input::get('class_id'));
				$query->where('Att_Date',$date);
				$query->select('Att_Pre_Abs','Att_Stu_Id','Att_Date');
		 	}])->where('Std_Cla_Id',Input::get('class_id'))->get();

				if($student){
					ResponseMessage::success("Student List Successfully",$student);
				}else{
				 	ResponseMessage::error("Record Not Found");
				}
			}else{
				ResponseMessage::error("User Not Found");
			}

		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}

	 /**
	   * 
	   * Selected Student Absent Or Present
	   *
	   * @author Ravirajsinh Zala
	   *
	   * @param integer $user_id  Teacher Id
	   * @param integer $class_id  Teacher
	   * @param integer $date  Date of Attendance Day
	   * @param integer $falg  0 = Student_id get there are Present And Other Student Of Class is Absent 1 = Student_id get there are Absent And Other Student Of Class is Present
	   * @param string $student_id  Get Student Id In Comma Sepreted Value
	   * @return JSON Student List With Absent Or Present
	   */

	public function absent_present_Student(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				'date' => 'required',
				'flag' => 'required',
				'student_id' => ''
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$std = Input::get('student_id');
			$student_id = explode(',', $std);

			$getAllStudentFromClass = Student::where('Std_Cla_Id',Input::get('class_id'))->get();
			$getAllStudentFromClassIds = Student::where('Std_Cla_Id',Input::get('class_id'))->select('Std_Id')->get()->toArray();
			if(Input::get('flag') == 1)
			{
				if($getAllStudentFromClass)
				{
					if(Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->exists())
					{
						Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->delete();
					}
						foreach ($getAllStudentFromClass as $student) 
						{
								$parent_id = Student::where("Std_Id",$student->Std_Id)->first();
								$parent_token = User::where("Use_Id",$parent_id["Std_Parent_Id"])->first(); 
								if(in_array($student->Std_Id, $student_id))
								{
									$att = new Attendence;
									$att['Att_Cla_Id'] = $student->Std_Cla_Id;
									$att['Att_Stu_Id'] = $student->Std_Id;
									$att['Att_Pre_Abs'] = 1;
									$att['Att_Flag'] = 2;
									$att['Att_Date'] = Input::get('date');
									$att['Att_CreatedBy'] = Input::get('user_id');
									$att['Att_UpdatedBy'] = Input::get('user_id');
									$att['Att_CreatedAt'] = date('Y-m-d H:i:s');
									$att['Att_UpdatedAt'] = date('Y-m-d H:i:s');
									if($att->save()){
										Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
										Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
									}
								}else{
									$att = new Attendence;
									$att['Att_Cla_Id'] = $student->Std_Cla_Id;
									$att['Att_Stu_Id'] = $student->Std_Id;
									$att['Att_Pre_Abs'] = 0;
									$att['Att_Flag'] = 2;
									$att['Att_Date'] = Input::get('date');
									$att['Att_CreatedBy'] = Input::get('user_id');
									$att['Att_UpdatedBy'] = Input::get('user_id');
									$att['Att_CreatedAt'] = date('Y-m-d H:i:s');
									$att['Att_UpdatedAt'] = date('Y-m-d H:i:s');
									if($att->save()){
										Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
										Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
									}
								}
						}
						
						$student = Attendence::with('AttStudent')
							->where(function($query){
							$query->where('Att_Cla_Id',Input::get('class_id'));
							$query->where('Att_Date',Input::get('date'));
						})->get();

						if($student!=""){
							ResponseMessage::success("Attendence Submit",$student);
						}else{
							ResponseMessage::error('Attendence Submission Fail');
						}
				}else{
					ResponseMessage::error('Student Not Found');
				}
			}
			if(Input::get('flag') == 2)
			{
				if($getAllStudentFromClass)
				{
					if(Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->exists())
					{
						Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->delete();
					}

						foreach ($getAllStudentFromClass as $student) 
						{
								$parent_id = Student::where("Std_Id",$student->Std_Id)->first();
								$parent_token = User::where("Use_Id",$parent_id["Std_Parent_Id"])->first(); 
								if(in_array($student->Std_Id, $student_id))
								{
									$att = new Attendence;
									$att['Att_Cla_Id'] = $student->Std_Cla_Id;
									$att['Att_Stu_Id'] = $student->Std_Id;
									$att['Att_Pre_Abs'] = 0;
									$att['Att_Flag'] = 2;
									$att['Att_Date'] = Input::get('date');
									$att['Att_CreatedBy'] = Input::get('user_id');
									$att['Att_UpdatedBy'] = Input::get('user_id');
									$att['Att_CreatedAt'] = date('Y-m-d H:i:s');
									$att['Att_UpdatedAt'] = date('Y-m-d H:i:s');
									if($att->save()){
										Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
										Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
									}
									
								}else{
									$att = new Attendence;
									$att['Att_Cla_Id'] = $student->Std_Cla_Id;
									$att['Att_Stu_Id'] = $student->Std_Id;
									$att['Att_Pre_Abs'] = 1;
									$att['Att_Flag'] = 2;
									$att['Att_Date'] = Input::get('date');
									$att['Att_CreatedBy'] = Input::get('user_id');
									$att['Att_UpdatedBy'] = Input::get('user_id');
									$att['Att_CreatedAt'] = date('Y-m-d H:i:s');
									$att['Att_UpdatedAt'] = date('Y-m-d H:i:s');
									if($att->save()){
										Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
										Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
									}
									
								}
						}
						
						$student = Attendence::with('AttStudent')
							->where(function($query){
							$query->where('Att_Cla_Id',Input::get('class_id'));
							$query->where('Att_Date',Input::get('date'));
						})->get();

						if($student!=""){
							ResponseMessage::success("Attendence Submit",$student);
						}else{
							ResponseMessage::error('Attendence Submission Fail');
						}
				}else{
					ResponseMessage::error('Student Not Found');
				}
			}			
			if(Input::get('flag') == 3)
			{
				$student_att_list = Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->whereIn('Att_Stu_Id',$student_id)->get();
				foreach ($student_att_list as $attendance) {
					$parent_id = Student::where("Std_Id",$attendance->Att_Stu_Id)->first();
					$parent_token = User::where("Use_Id",$parent_id["Std_Parent_Id"])->first(); 
					if($attendance->Att_Pre_Abs == "0")
					{
						$att = Attendence::find($attendance->Att_Id);
						$att->Att_Pre_Abs = "1" ;
						$att->Att_Flag = "3";
						$att->Att_UpdatedBy = Input::get('user_id');
						$att->Att_UpdatedAt = date('Y-m-d H:i:s');
						if($att->update()){
							Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
							Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Present.");
						}

					}
					if($attendance->Att_Pre_Abs == "1")
					{
						$att = Attendence::find($attendance->Att_Id);
						$att->Att_Pre_Abs = "0" ;
						$att->Att_Flag = "3";
						$att->Att_UpdatedBy = Input::get('user_id');
						$att->Att_UpdatedAt = date('Y-m-d H:i:s');
						if($att->update()){
							Notification::sendNotification($parent_token["Use_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
							Notification::sendNotification($parent_token["Use_Mother_Token"],"Today Attendence.",$parent_id["Std_Name"]." Is Absent.");
						}
					}
				}

				$studentDetails = Attendence::where('Att_CreatedBy',Input::get('user_id'))
						->where('Att_Cla_Id',Input::get('class_id'))
						->where('Att_Date',Input::get('date'))
						->whereIn('Att_Stu_Id',$student_id)->get();

				if($studentDetails != ""){
					ResponseMessage::success("Attendence Update.",$studentDetails);
				}else{
					ResponseMessage::error('Attendence Update Fail');
				}

			}
		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function absent_present_Check(Request $request)
	{
		try 
		{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				'date' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Attendence::where('Att_CreatedBy',Input::get('user_id'))->where('Att_Cla_Id',Input::get('class_id'))->where('Att_Date',Input::get('date'))->exists())
				{
					$student = 1;
				}else{
					$student = 0;
				}

				if($student)
				{
					ResponseMessage::success("Attendence",$student);
				}else
				{
					ResponseMessage::success("Attendence",$student);
				}
		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
		
	}
	//Student Attendance List
	public function absent_present_List(Request $request)
	{
		try
		{
			$rules = [
				'class_id' => 'required',
				'date' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$student = Attendence::with('AttStudent')
				->where(function($query){
				$query->where('Att_Cla_Id',Input::get('class_id'));
				$query->where('Att_Date',Input::get('date'));
			})->get();

			if($student)
			{
				ResponseMessage::success("Attendence List",$student);
			}else
			{
				ResponseMessage::error('Record Not Found');
			}
		}
		catch (\Exception $e) {	
                   Exceptions::exception($e);
		}
	}

}
