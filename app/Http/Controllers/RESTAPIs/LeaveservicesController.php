<?php

namespace App\Http\Controllers\RESTAPIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Leaverequest;
use App\Model\Student;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use Validator;
use Input;
use DB;

class LeaveservicesController extends Controller
{
    public function send_leave(Request $request)
    {
    	try {
    		$rules = [
				'leave_type' => 'required',
				'user_type' => 'required',	
				'from_date' => 'required',
				'reason' => 'required',
				'leave_id' => 'required',
				'user_id' => 'required',
				'to_date' => '',
				'from_time' => '',
				'to_time' => '',
				];
			$customeMessage = [
				"leave_type.required"=>"Please enter leave type.",
				"user_type.required"=>"Please enter user type.",
				"from_date.required"=>"Please enter from date .",
				"reason.required"=>"Please enter reason.",
				"leave_id.required"=>"Please enter user or student id.",
				"user_id.required"=>"Please enter user id.",
			];

			$validator = Validator::make($request->all(), $rules, $customeMessage);

			if($validator->fails()){
				$errors = $validator->errors();
				ResponseMessage::error($errors->first());
				exit;
			}else{
				$leave_type = $request->leave_type;
				$user_type = $request->user_type;
				$reason = $request->reason;
				$from_date = $request->from_date;
				$leave_id = $request->leave_id;
				$user_id = $request->user_id;

				if($leave_type=="fullday"){
					$to_date = $request->to_date;
					$leaveReauest = new Leaverequest;
	                $leaveReauest->Lea_Type = $leave_type;
	                $leaveReauest->Lea_User_Type = $user_type;
	                $leaveReauest->Lea_From_Date = $from_date;
	                if($to_date==""){
	                    $leaveReauest->Lea_To_Date = $from_date;
	                }else{
	                    $leaveReauest->Lea_To_Date = $to_date;
	                }
	                $leaveReauest->Lea_User_Id = $leave_id;
	                $leaveReauest->Lea_Reason = $reason;
	                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
	                $leaveReauest->Lea_CreatedAt = date('Y-m-d H:i:s');
	                $leaveReauest->Lea_CreatedBy = $user_id;
	                if($leaveReauest->save()) 
	                {
	                    ResponseMessage::success("Leave Request Send Successfully",$leaveReauest);
	                    exit;
	                }else{
	                    ResponseMessage::error("Failed to send leave");
	                    exit;
	                }
				}else if($leave_type=="halfday"){
					$to_date = $request->to_date;
	                $from_time = $request->from_time;
	                $to_time = $request->to_time;

	                $leaveReauest = new Leaverequest;
	                $leaveReauest->Lea_Type = $leave_type;
	                $leaveReauest->Lea_User_Type = $user_type;
	                $leaveReauest->Lea_From_Date = $from_date;
	                if($to_date==""){
	                    $leaveReauest->Lea_To_Date = $from_date;
	                }else{
	                    $leaveReauest->Lea_To_Date = $to_date;
	                }
	                $leaveReauest->Lea_From_Time = $from_time;
	                $leaveReauest->Lea_To_Time = $to_time;
	                $leaveReauest->Lea_User_Id = $leave_id;
	                $leaveReauest->Lea_Reason = $reason;
	                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
	                $leaveReauest->Lea_CreatedAt = date('Y-m-d H:i:s');
	                $leaveReauest->Lea_CreatedBy = $user_id;
	                if($leaveReauest->save())  
	                {
	                    ResponseMessage::success("Leave Request Send Successfully",$leaveReauest);
	                    exit;
	                }else{
	                    ResponseMessage::error("Failed to send leave");
	                    exit;
	                }
				}else{
					ResponseMessage::error("Leave type must be 'fullday' or 'halfday'.");
					exit;
				}
				dd($request->all());
			}
    	}catch (\Exception $e) {
			Exceptions::exception($e);
		}
    }

    public function teacher_leave_report(Request $request)
    {
    	try {
    		$rules = [
				'user_id' => 'required',
				];
			$customeMessage = [
				"user_id.required"=>"Please enter teacher id."
			];

			$validator = Validator::make($request->all(), $rules, $customeMessage);

			if($validator->fails()){
				$errors = $validator->errors();
				ResponseMessage::error($errors->first());
				exit;
			}else{
				$user_id = $request->user_id;
				if(User::where('Use_Id',$user_id)->where('Use_Type','2')->exists())
				{
					$leaves = Leaverequest::where('Lea_User_Type','teacher')->where('Lea_User_Id',$user_id)->select('leave_request.*',DB::raw("CASE WHEN leave_request.Lea_User_Type = 'teacher' THEN  user_tbl.Use_Name END as leave_user_name "))
	                                    ->leftjoin('user_tbl','user_tbl.Use_Id','=','leave_request.Lea_User_Id')
	                                    ->orderBy('Lea_Id','DESC')
	                                    ->get();
	                if($leaves)  
	                {
	                    ResponseMessage::success("Teacher Leave Request List.",$leaves);
	                    exit;
	                }else{
	                    ResponseMessage::error("Failed to send leave");
	                    exit;
	                }
                }else{
                	ResponseMessage::error("Teacher Not Found.");
	                exit;
                }
			}	
    	}catch (\Exception $e) {
			Exceptions::exception($e);
		}
    }

    public function student_leave_report(Request $request)
    {
    	try {
    		$rules = [
				'student_id' => 'required',
				];
			$customeMessage = [
				"student_id.required"=>"Please enter student id."
			];

			$validator = Validator::make($request->all(), $rules, $customeMessage);

			if($validator->fails()){
				$errors = $validator->errors();
				ResponseMessage::error($errors->first());
				exit;
			}else{
				$student_id = $request->student_id;
				if(Student::where('Std_Id',$student_id)->exists()){
					$leaves = Leaverequest::where('Lea_User_Type','student')->where('Lea_User_Id',$student_id)->select('leave_request.*',DB::raw("CASE  WHEN leave_request.Lea_User_Type = 'student' THEN  student_tbl.Std_Name END as leave_user_name "))
                                    ->leftjoin('student_tbl','student_tbl.Std_Id','=','leave_request.Lea_User_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->get();
                	if($leaves)  
	                {
	                    ResponseMessage::success("Student Leave Request List.",$leaves);
	                    exit;
	                }else{
	                    ResponseMessage::error("Failed to send leave");
	                    exit;
	                }
				}else {
					ResponseMessage::error("Student Not Found.");
	                exit;
				}
			}
    	}catch (\Exception $e) {
			Exceptions::exception($e);
		}
    }
}
