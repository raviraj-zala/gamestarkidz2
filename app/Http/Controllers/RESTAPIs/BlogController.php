<?php

namespace App\Http\Controllers\RESTAPIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\Model\UserLikeBlog;
use App\Model\Users;
use App\User;
use Validator;
use Input;
use App\Helper\ResponseMessage;
use Auth;
use DB;






class BlogController extends Controller
{
    public function blogList() {
    	try {
    		$blogs = Newsfeed::with(['images'=>function($query){
                $query->select('Nfi_Nfd_Unique_Id','Nfi_Id','Nfi_Path');
            }])->where('Nfd_Status','1')
    						->orderBy('Nfd_CreatedAt','DESC')
    						->get(['Nfd_Id','Nfd_Title','Nfd_Description','Nfd_Unique_Id','Nfd_CreatedAt']);
       
    		if(count($blogs)){
	    		foreach($blogs as $blog){
                    foreach($blog->images as $image){
                       $image->blogLike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                            ->where('Img_Vid_Id',$image->Nfi_Id)
                                            ->count(); 
                    }
                    $bloglike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                        ->where('Img_Vid_Id',null)
                                        ->count();
                    $blog->bloglike = $bloglike;
	    		}
	    	}
            $image=env('Blog_Image');
    		ResponseMessage::successwithimage('Success', $image, $blogs);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function blogImage(Request $request){
        try {
            $rules = [
                'blog_id' => 'required',
                'user_id' => 'required',
                'image_id'=> 'required',
            ];
            $customeMessage = [
                'blog_id.required' => 'Please enter blog id',
                'user_id.required' => 'Please enter user id',
                'image_id.required' => 'Please enter image id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(User::where('Use_Id',Input::get('user_id'))->exists())
                {

                    $image = UserLikeBlog::where('user_id',Input::get('user_id'))->where('Blog_Id',Input::get('blog_id'))->where('Img_Vid_Id',Input::get('image_id'))->get();

                    if($image)
                    {   
                        ResponseMessage::success("Blog Image Like List",$image);
                    }else{
                        ResponseMessage::error("Blog Like not Found");
                    }
                }else{
                    ResponseMessage::error('User Not Found');
                }
                // if(Newsfeed::where('Nfd_Status',"1")->where('Nfd_Id',$request->blog_id)->exists()){
                //     if(Newsfeedinfo::where('Nfi_Nfd_Unique_Id', $request->image_id)->exists()){
                //         $images = Newsfeedinfo::where('Nfi_Nfd_Unique_Id', $request->image_id)->first(['Nfi_Nfd_Unique_Id','Nfi_Path']);
                //         $images->bloglike = UserLikeBlog::where('Blog_Id',$request->blog_id)
                //                             ->where('Img_Vid_Id',$request->image_id)
                //                             ->where('user_id',$request->user_id)
                //                             ->count();
                //         $image=env('Blog_Image');
                //         ResponseMessage::successwithimage('Success', $image, $images);
                //     }else{
                //         ResponseMessage::error("Image not found.");
                //     }
                // }else{
                //     ResponseMessage::error("Blog not found."); 
                // }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function blogLike(Request $request){
    	try {
    		$rules = [
    			'blog_id' => 'required',
    			'user_id' => 'required',
                'flag' => 'required',
    		];
    		$customeMessage = [
    			'blog_id.required' => 'Please enter blog id',
    			'user_id.required' => 'Please enter user id',
                'flag.required' => 'Please enter flag',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
            	if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)){
            		if(Newsfeed::where('Nfd_Status',"1")->where('Nfd_Id',$request->blog_id)->exists())
                    {
            			$image=null;
            			if(isset($request->image_id)){
            				if(Newsfeedinfo::where('Nfi_Nfd_Unique_Id',$request->image_id)->exists()){
            					$image = $request->image_id;
            				}else{
            					ResponseMessage::error("Image not found."); 
            					exit;
            				}
            			}
                        if ($request->flag==1) {
                			$like = new UserLikeBlog();
                			$like->Blog_Id = $request->blog_id;
                			$like->User_Id = $request->user_id;
                            $like->Blog_Like = $request->flag;
                			$like->Img_Vid_Id = $image;
                			$like->save();
                        }else{
                            $like = UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->first();
                            if ($like) {
                                UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->delete();
                            }
                        }
            			ResponseMessage::success('Success', $like);
            		}else{
            			ResponseMessage::error("Blog not found."); 
            		}
            	}else{
	    			ResponseMessage::error("User not found."); 
	    		}
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function blogImageLike(Request $request){
        try {
            $rules = [
                'blog_id' => 'required',
                'user_id' => 'required',
                'flag' => 'required',
                'image_id' => 'required',
            ];
            $customeMessage = [
                'blog_id.required' => 'Please enter blog id',
                'user_id.required' => 'Please enter user id',
                'flag.required' => 'Please enter flag',
                'image_id.required' => 'Please enter image id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)){
                    if(Newsfeed::where('Nfd_Status',"1")->where('Nfd_Id',$request->blog_id)->exists())
                    {
                        
                        $image = $request->image_id;
                        
                        if ($request->flag==1) {
                            $like = new UserLikeBlog();
                            $like->Blog_Id = $request->blog_id;
                            $like->User_Id = $request->user_id;
                            $like->Blog_Like = $request->flag;
                            $like->Img_Vid_Id = $image;
                            $like->save();
                        }else{
                            $like = UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->where('Img_Vid_Id',$image)->first();
                            if ($like) {
                                UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->where('Img_Vid_Id',$image)->delete();
                            }
                        }
                        ResponseMessage::success('Success', $like);
                    }else{
                        ResponseMessage::error("Blog not found."); 
                    }
                }else{
                    ResponseMessage::error("User not found."); 
                }
            }
            // dd('dd');
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
