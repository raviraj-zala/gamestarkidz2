<?php

namespace App\Http\Controllers\RESTAPIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Task;
use App\Model\Module;
use App\Model\Users;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\TaskNotes;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use Validator;
use App\Helper\ResponseMessage;
use Auth;

class TaskController extends Controller
{
    public function taskList(){
    	try {
    		$task = Task::get(['Tas_Id','Tas_Title','Tas_Status','Tas_Description','Tas_Time','Tas_Date','Tas_CreatedAt']);
    		ResponseMessage::success('Success', $task);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    	
    }
    public function taskDetail(Request $request){
    	try {   		
    		$rules = [
    			'task_id' => 'required',
    		];
    		$customeMessage = [
    			'task_id.required' => 'Please enter task id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
	    		if(Task::where('Tas_Id',$request->task_id)->exists()){
	    			$data['task'] = Task::where('Tas_Id',$request->task_id)
	    						->Select('Tas_Id','Tas_Title','Tas_Status','Tas_Description','Tas_Time','Tas_Date','Tas_CreatedAt')
	    						->first();
                    if($data['task']){
    	    			$data['task']->notes = TaskNotes::where('Tas_id',$request->task_id)
    	    								->Select('Note','Assignee')
    	    								->get();
                    }
    	    		ResponseMessage::success('Success', $data);
	    		}else{
	    			ResponseMessage::error("Task not found."); 
	    		}
	    	}
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
    public function createNotes(Request $request){
    	try {   		
    		$rules = [
    			'task_id' => 'required',
    			'notes' => 'required',
    			'user_id' => 'required',
    			'status' => 'required',
    		];
    		$customeMessage = [
    			'task_id.required' => 'Please enter task id',
    			'notes.required' => 'Please enter notes',
    			'user_id.required' => 'Please enter user id',
    			'status.required' => 'Please select status',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
            	if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)->exists()){
	            	if(Task::where('Tas_Id',$request->task_id)->exists()){
	            		$task = Task::where('Tas_Id',$request->task_id)->first();
	            		$task->Tas_Status = $request->status;
	            		$task->save();

	            		$note = new TaskNotes();
	            		$note->Tas_Id = $request->task_id;
	            		$note->Note = $request->notes;
	            		$note->Assignee = $request->user_id;
	            		if($note->save()){
		    				ResponseMessage::success('Success', $note);
		    			}
	            	}else{
		    			ResponseMessage::error("Task not found.");
		    		}
		    	}else{
		    		ResponseMessage::error("User not found.");
		    	}
            }
        } catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }



}
