<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Payment;
use Auth;
use Input;
use Validator;
use DB;

class PaymentservicesController extends Controller
{
   //user create payment
	public function create_Paymment(Request $request)
	{
		try
		{
			$rules = [
			'parent_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('parent_id'))->exists())
			{
				$parent_name = User::where('Use_Id',Input::get('parent_id'))->first()->Use_Name;

				$payment = new Payment;
				$payment->Pay_Stu_Name  = Input::get('student_name');
				$payment->Pay_Parent_Name = $parent_name;
				$payment->Pay_Parent_Id = Input::get('parent_id');
				$payment->Pay_Type = Input::get('type');
				$payment->Pay_Date = Input::get('date');
				$payment->Pay_Amount = Input::get('amount');
				$payment->Pay_CreatedBy = Input::get('parent_id');
				$payment->Pay_UpdatedBy = Input::get('parent_id');
				$payment->Pay_CreatedAt = date('Y-m-d H:i:s');
				$payment->Pay_UpdatedAt = date('Y-m-d H:i:s');
				$payment->save();
				$payment->Pay_Id;

				if($payment)
				{
					ResponseMessage::success('Payment Done Successfully',$payment);
				}else{
					ResponseMessage::error('Not Creation Payment');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	//Paymenmt List
	public function payment_List(Request $request)
	{
		try 
		{
			$rules = [
			'user_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{
				$payment = Payment::where('Pay_Parent_Id',Input::get('user_id'))->get();

				if($payment)
				{
					ResponseMessage::success('Payment List',$payment);
				}else{
					ResponseMessage::error('Payment List Not Found');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}
}
