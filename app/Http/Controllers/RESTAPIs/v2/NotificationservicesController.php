<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Notification;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Student;
use App\Model\Attendence;
use App\Model\ClassTbl;
use Auth;
use Input;
use Validator;
use DB;

/**
 * @OA\get(
 * path="/api/v2/notification_list",
 * summary="User Offline Notification List",
 * description="User Offline Notification List",
 * operationId="authLogin",
 * tags={"notification"},
 * @OA\Parameter(
 *    description="ID of user",
 *    in="query",
 *    name="user_id",
 *    required=true,
 *    example="322",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

class NotificationservicesController extends Controller
{

    public function notificationGet(Request $request)
    {
        try
        {
            $rules = [
                'user_id' => 'required',
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter user id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                $notificaions = \App\Model\Notification::where('user_id',$request->user_id)->select('type')->get();

                if($notificaions)
                {
                    ResponseMessage::success('Notification List',$notificaions);
                }else{
                    ResponseMessage::error('Users List Not Found');
                }

                $delete_notificaions = \App\Model\Notification::where('user_id',$request->user_id)->delete();
            }
        }
        catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
