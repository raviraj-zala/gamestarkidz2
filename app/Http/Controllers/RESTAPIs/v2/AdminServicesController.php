<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Model\Admin_assign_branch;
use App\Model\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\StudentExamAns;
use App\Model\Student;
use Validator;
use App\Helper\ResponseMessage;
use Auth;



/**
 * @OA\get(
 * path="/api/v2/branch_list",
 * summary="Branch List",
 * description="Branch List",
 * operationId="authLogin",
 * tags={"admin"},
 * @OA\Parameter(
 *    description="ID of class",
 *    in="query",
 *    name="admin_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/class_list",
 * summary="Class List",
 * description="Class List",
 * operationId="authLogin",
 * tags={"admin"},
 * @OA\Parameter(
 *    description="ID of Branch",
 *    in="query",
 *    name="branch_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */


/**
 * @OA\Post(
 * path="/api/v2/branch_wise_class_list",
 * summary="Branch Wise Class List",
 * description="Branch Wise Class List",
 * operationId="authLogin",
 * tags={"admin"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"admin_id"},
 *       @OA\Property(property="admin_id", type="integer", format="number", example="659"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\get(
 * path="/api/v2/teacher_list",
 * summary="Teacher List",
 * description="Teacher List",
 * operationId="authLogin",
 * tags={"admin"},
 * @OA\Parameter(
 *    description="ID of Branch",
 *    in="query",
 *    name="branch_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ), @OA\Parameter(
 *    description="ID of Class",
 *    in="query",
 *    name="class_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */



class AdminServicesController extends Controller
{
    public function branchList(Request $request) {
        try {
            $rules = [
                'admin_id' => 'required',
            ];
            $customeMessage = [
                'admin_id.required' => 'Please enter admin id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            // dd($request->all());
            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                 if(true){
                    $branch = Branch::leftjoin('admin_assign_branch_tbl', 'admin_assign_branch_tbl.Aab_Brn_Id','=', 'branch_tbl.Brn_Id')
                                    ->where('Aab_Use_Id', $request->admin_id)
                                    ->select('Brn_Id','Brn_Name','Brn_Code')
                                    ->get();
                     ResponseMessage::success("Branch Found.", $branch);
                 }else{
                     ResponseMessage::error("Branch not found.");
                 }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function branchWiseClassList(Request $request) {
        try {
            $rules = [
                'admin_id' => 'required',
            ];
            $customeMessage = [
                'admin_id.required' => 'Please enter admin id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            // dd($request->all());
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(true){

                    $branch = Branch::leftjoin('admin_assign_branch_tbl', 'admin_assign_branch_tbl.Aab_Brn_Id','=', 'branch_tbl.Brn_Id')
                        ->where('Aab_Use_Id', $request->admin_id)
                        ->select('Brn_Id')
                        ->get();

                    $classdata = array();

                    foreach ($branch as $branchlist){
                        $classdata[] =  $branchlist->Brn_Id;
                    }

                        $class = ClassTbl::whereIn('Cla_Bra_Id',$classdata)
                            ->select('Cla_Id','Cla_Class')
                            ->get();

                    $data["class"] =$class;

                    ResponseMessage::success("Branch Found.", $data);

                }else{
                    ResponseMessage::error("Branch not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function classList(Request $request){
        try {
            $rules = [
                'branch_id' => 'required',
            ];
            $customeMessage = [
                'branch_id.required' => 'Please enter branch id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                 if(ClassTbl::where('Cla_Bra_Id', $request->branch_id)->exists()){

                    $class = ClassTbl::where('Cla_Bra_Id',$request->branch_id)
                                    ->select('Cla_Id','Cla_Bra_Id','Cla_Class','Cla_Section','Cla_Status')
                                    ->get();
                     ResponseMessage::success("Class Found.", $class);
                 }else{
                     ResponseMessage::error("Class not found.");
                 }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function teacherList(Request $request){
        try {
            $rules = [
                'branch_id' => 'required',
                'class_id' => 'required',
            ];
            $customeMessage = [
                'branch_id.required' => 'Please enter branch id',
                'class_id.required' => 'Please enter class id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(true) {
                    $teacher = Users::join('teacher_assign_class_tbl', 'teacher_assign_class_tbl.Tac_Use_Id', 'user_tbl.Use_Id')
                        ->where('teacher_assign_class_tbl.Tac_Brn_Id', $request->branch_id)
                        ->where('teacher_assign_class_tbl.Tac_Cla_Id', $request->class_id)
                        ->where('Use_Type', 2)
                        ->where('Use_Status', 1)
                        ->distinct()
                        ->get(['Use_Id', 'Use_Name']);

                    ResponseMessage::success("Teacher Found.", $teacher);
                }else{
                    ResponseMessage::error("Teacher Not Found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

}

