<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Model\UserLikeBlog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


/**
 * @OA\Post(
 * path="/api/v2/create_newsfeed",
 * summary="Create Newsfeed",
 * description="Create Newsfeed",
 * operationId="authLogin",
 * tags={"newsfeed"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"title","description","user_id","tag_type"},
 *       @OA\Property(property="title", type="string", format="string", example="New Title"),
 *       @OA\Property(property="description", type="string", format="string", example="New Descprition"),
 *       @OA\Property(property="tag_type", type="string", format="string", example="class"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="imageOrVideo", type="file", format="file", example="examla.jpg"),
 *       @OA\Property(property="branch", type="integer", format="number", example="5"),
 *       @OA\Property(property="imageOrVideoURL", type="string", format="string", example="URl"),
 *       @OA\Property(property="class_id", type="integer", format="number", example="10"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/teacher_newsfeed_list",
 * summary="Teacher Newsfeed list",
 * description="Teacher Newsfeed list",
 * operationId="authLogin",
 * tags={"newsfeed"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"user_id"},
 *       @OA\Property(property="user_id", type="integer", format="number", example="226"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/parent_newsfeed",
 * summary="Parent Newsfeed list",
 * description="Parent Newsfeed list",
 * operationId="authLogin",
 * tags={"newsfeed"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"student_id"},
 *       @OA\Property(property="student_id", type="integer", format="number", example="218"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/newsfeed_list",
 * summary="Admin Newsfeed list",
 * description="Admin Newsfeed list",
 * operationId="authLogin",
 * tags={"newsfeed"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"admin_id"},
 *       @OA\Property(property="admin_id", type="integer", format="number", example="332"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

class NewsfeedserviceController extends Controller
{
    public function createNewsfeed(Request $request)
    {
    	try {
    		$rules = [
    			'title' => 'required',
    			'description' => 'required',
    			'imageOrVideo' => 'max:25600',
    			'tag_type' => 'required',
    			'user_id' => 'required',
    			'classes' => '',
    			'students' => '',
    		];
    		$customeMessage = [
    			'title.required' => 'Please enter title',
    			'description.required' => 'Please enter description.',
    			'imageOrVideo.mimes' => 'Invalid image or video type.',
    			'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
    			'tag_type.required' => 'Please send tag type',
    			'user_id.required' => 'Please send user id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	$picture = array();
            	$images = $request->file('imageOrVideo');
            	$error_size = false;
	    		if($images)
				{
                    foreach ($images as $image)
					{
						if($image->getClientSize() > 26214400)
						{
							$error_size = true;
						}
					}
                }
                if($error_size == true)
                {
                   ResponseMessage::error("Do Not Upload More Than 25MB File");
                   exit;
                }
                else
                {
					if($images)
					{
						$i = 0;
//						dd();
			            foreach($images as $file)
			            {
//                            dd('hel');

					        $filename = $file->getClientOriginalName();
					        $extension = $file->getClientOriginalExtension();
					        $mime = $file->getMimeType();
                            if(strstr($mime, "video/")){
                                $mediaType = "2";
                            }else if(strstr($mime, "image/")){
                                $mediaType = "1";
                            }
	                        $picture[] = ['path' => $i.time().".".$extension,'type'=>$mediaType];
					        $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                          	$i++;
//                          	dd($i);
						}
			        }else{
			            $picture = "";
			        }
		        }


            	$title = trim($request->title);
            	$description = trim($request->description);
            	$status = "0";
            	$user_type = User::where('Use_Id', $request->user_id)->select('Use_Type')->first()->Use_Type;
//            	dd($user+);
            	$user_id = $request->user_id;
                $imgvdourl = $request->imageOrVideoURL;
            	$tag_type = $request->tag_type;
            	$branch_id = $request->branch;
//                dd($branch_id);
            	$created_at = date('Y-m-d H:i:s');
            	$created_by = $user_id;
                $unique_id = "NFD".time();
                $classes = trim($request->class_id);
                $students = trim($request->students);



                if($user_type =="1" || $user_type =="5"){

                    if($request->imageOrVideoURL != null){
                        $newsfeedInfo = New Newsfeedinfo;
                        $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                        $newsfeedInfo->Nfi_Path = $imgvdourl;
                        $newsfeedInfo->Nfi_Type = "3";
                        $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                        $newsfeedInfo->save();
                    }



                    $newsfeed = New Newsfeed;
                    $newsfeed->Nfd_Unique_Id = $unique_id;
                    $newsfeed->Nfd_Brn_Id = $branch_id;
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = "1";
                    $newsfeed->Nfd_User_Type = $user_type;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_CreatedAt = $created_at;
                    $newsfeed->Nfd_CreatedBy = $created_by;
                    $newsfeed->save();


                    if($tag_type == "class"){
                        $class_ids = explode(",", $classes);
                        $student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
//                        dd('hello');

                        foreach ($class_ids as $class)
                        {
//                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                            $newsfeedTag = New Newsfeedtag;
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
//                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $class;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
//                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                        }
                        ResponseMessage::success("Newsfeed Created Successfully", $newsfeedTag);
                    }
//                    elseif($tag_type == "student"){
//
//                        $student_ids = explode(",", $students);
//                        $student_list = Student::whereIn('Std_Id',$student_ids)->get();
//                        foreach ($student_list as $student)
//                        {
////                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
//                            $newsfeedTag = New Newsfeedtag;
//                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
//                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
//                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
//                            $newsfeedTag->Nft_Brn_Id = $branch_id;
//                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
//                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
//                            $newsfeedTag->save();
//                        }
//                        ResponseMessage::success("Newsfeed Created Successfully", $newsfeedTag);
//                    }

                } elseif($user_type == "2" ){

                    if($request->imageOrVideoURL != null) {
                        $newsfeedInfo = new Newsfeedinfo;
                        $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                        $newsfeedInfo->Nfi_Path = $imgvdourl;
                        $newsfeedInfo->Nfi_Type = "3";
                        $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                        $newsfeedInfo->save();
                    }

                    $newsfeed = New Newsfeed;
                    $newsfeed->Nfd_Unique_Id = $unique_id;
                    $newsfeed->Nfd_Brn_Id = $branch_id;
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = $status;
                    $newsfeed->Nfd_User_Type = $user_type;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_CreatedAt = $created_at;
                    $newsfeed->Nfd_CreatedBy = $created_by;
                    $newsfeed->save();
                    ResponseMessage::success("Newsfeed Created Successfully",$newsfeed);
                }
                if($newsfeed->save()){
//                    dd(count($picture));
                	if(count($picture)!=0){

	            		foreach ($picture as $pic) {
		            		$newsfeedInfo = New Newsfeedinfo;
		            		$newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
		            		$newsfeedInfo->Nfi_Path = $pic['path'];
		            		$newsfeedInfo->Nfi_Type = $pic['type'];
		            		$newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
		            		$newsfeedInfo->save();
	            		}
                        ResponseMessage::success("Newsfeed Created Successfully",$newsfeedInfo);
                    }
                }
            	else{
        			ResponseMessage::error("Newsfeed not create please try again.");
        			exit;
            	}
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }


    public function update_newsfeed(Request $request)
    {
        try {
            $rules = [
                'title' => 'required',
                'description' => 'required',
                'imageOrVideo' => 'max:25600',
                'classes' => '',
                'students' => '',
            ];
            $customeMessage = [
                'title.required' => 'Please enter title',
                'description.required' => 'Please enter description.',
                'imageOrVideo.mimes' => 'Invalid image or video type.',
                'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                foreach ($errors->all() as $message) {
                    ResponseMessage::error($message);
                    exit;
                }
            }else{
                $picture = array();
                $images = $request->file('imageOrVideo');
                $error_size = false;
                if($images)
                {
                    foreach ($images as $image)
                    {
                        if($image->getClientSize() > 26214400)
                        {
                            $error_size = true;
                        }
                    }
                }
                if($error_size == true)
                {
                    ResponseMessage::error("Do Not Upload More Than 25MB File");
                    exit;
                }
                else
                {
                    if($images)
                    {
                        $i = 0;
                        foreach($images as $file)
                        {
                            $filename = $file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $mime = $file->getMimeType();
                            if(strstr($mime, "video/")){
                                $mediaType = "2";
                            }else if(strstr($mime, "image/")){
                                $mediaType = "1";
                            }
                            $picture[] = ['path' => $i.time().".".$extension,'type'=>$mediaType];
                            $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                            $i++;
                        }
                    }else{
                        $picture = "";
                    }
                }

                $title = trim($request->title);
                $description = trim($request->description);
                $status = "0";
                $user_type = "2";
                $imgvdourl = $request->imageOrVideoURL;
                $tag_type = $request->tag_type;
                $created_at = date('Y-m-d H:i:s');
                $unique_id = $request->newsfeed_unique_id;
                $classes = trim($request->classes);
                $students = trim($request->students);


                if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5"){

                    $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                    $newsfeed->Nfd_Unique_Id = $unique_id;
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = $status;
                    $newsfeed->Nfd_User_Type = $user_type;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_CreatedAt = $created_at;
                    $newsfeed->save();

                    if($tag_type == "class"){
                        $class_ids = explode(",", $classes);
                        $student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
                        foreach ($student_list as $student)
                        {
                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get('Cla_Bra_Id')->first();
                            $newsfeedTag = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->first();
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                        }
                        ResponseMessage::success("Newsfeed Updated Successfully",$newsfeed);
                    }elseif($tag_type == "student"){
                        $student_ids = explode(",", $students);
                        $student_list = Student::whereIn('Std_Id',$student_ids)->get();
                        foreach ($student_list as $student)
                        {
                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get('Cla_Bra_Id')->first();
                            $newsfeedTag = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->first();
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                        }
                        ResponseMessage::success("Newsfeed Updated Successfully",$newsfeed);
                    }

                }elseif(Auth::user()->Use_Type == "2" ){

                    $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                    $newsfeed->Nfd_Unique_Id = $unique_id;
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = $status;
                    $newsfeed->Nfd_User_Type = $user_type;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_CreatedAt = $created_at;

                }

                if($newsfeed->save()){
                    if(count($picture)!=0){
                        foreach ($picture as $pic) {
                            $newsfeedInfo = New Newsfeedinfo;
                            $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                            $newsfeedInfo->Nfi_Path = $pic['path'];
                            $newsfeedInfo->Nfi_Type = $pic['type'];
                            $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedInfo->save();
                        }
                        ResponseMessage::success("Newsfeed Updated Successfully",$newsfeedInfo);

                    }

                }
                else{
                    ResponseMessage::error("Newsfeed not update please try again.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function teacherNewsfeedList(Request $request) {
        try {
            $rules = [
                'user_id' => 'required'
            ];
            $customeMessage = [
                'user_id.required' => 'Please send user id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                foreach ($errors->all() as $message) {
                    ResponseMessage::error($message);
                    exit;
                }
            }else{
                $user_id = $request->user_id;
                if(User::where('Use_Id',$user_id)->where('Use_Type','2')->exists())
                {
                    $teacherAssignClass = UserRights::teacherApi($user_id,'AssignClass');
                    // $teacherAssignBranch = UserRights::teacherApi($user_id,'branchAccess');
                    $tagList =  Newsfeedtag::whereIn('Nft_Cla_Id',$teacherAssignClass)->select('Nft_Nfd_Unique_Id')->groupBy('Nft_Nfd_Unique_Id')->get();
                     $newsfeed = Newsfeed::with(['newsfeed_details'=>function($query){
                         $query->select('Nfi_Nfd_Unique_Id','Nfi_Id','Nfi_Path','Nfi_CreatedAt','Nfi_Type');
                      }])->with('newsfeed_details')
                      // ->whereIn('Nfd_Unique_Id',$tagList)->where('Nfd_Brn_Id',$teacherAssignBranch)
                                ->where('Nfd_Status',"1")
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                                ->select('newsfeed.*','user_tbl.Use_Name')
                                ->orderBy('Nfd_Id','DESC')
                                ->get();

                    if(count($newsfeed)){
                        foreach($newsfeed as $blog){
                            foreach($blog->newsfeed_details as $image){
                                $image->blogImgaeLike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                    ->where('Img_Vid_Id',$image->Nfi_Id)
                                    ->count();
                            }
                            $bloglike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                ->where('Img_Vid_Id',null)
                                ->count();
                            $blog->bloglike = $bloglike;
                            $blog_like = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)->where('User_Id',$request->user_id)->get();
                            $blog->blog_like = $blog_like;
                        }
                    }
                    return ResponseMessage::success("Newsfeed list",$newsfeed);
                }else{
                    ResponseMessage::error("Teacher Not Found");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function parentNewsList(Request $request) {
        try {
            $rules = [
                'student_id' => 'required'
            ];
            $customeMessage = [
                'student_id.required' => 'Please send student id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                foreach ($errors->all() as $message) {
                    ResponseMessage::error($message);
                    exit;
                }
            }else{
                $student_id = $request->student_id;
                $user_id = $request->user_id;
                if(Student::where('Std_Id',$student_id)->exists())
                {
                    $studentClass = Student::where('Std_Id',$student_id)->select('Std_Cla_Id')->first();
                    //$studentBranch = Student::where('Std_Id',$student_id)->select('Std_Cla_Id')->first();
                    //$teacherAssignBranch = UserRights::teacherApi($student_id,'branchAccess');
                    $tagList =  Newsfeedtag::whereIn('Nft_Cla_Id',$studentClass)->select('Nft_Nfd_Unique_Id')->groupBy('Nft_Nfd_Unique_Id')->get();
                    $newsfeed = Newsfeed::with(['newsfeed_details'=>function($query){
                        $query->select('Nfi_Nfd_Unique_Id','Nfi_Id','Nfi_Path','Nfi_CreatedAt','Nfi_Type');
                    }])->with('newsfeed_details')
                    // ->where('Nfd_Brn_Id',$studentBranch)
                    ->where('Nfd_Status',"1")
                    ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                    ->select('newsfeed.*','user_tbl.Use_Name')
                    ->orderBy('Nfd_Id','DESC')
                    ->get();
                    if(count($newsfeed)){
                        foreach($newsfeed as $blog){
                            foreach($blog->newsfeed_details as $image){
                                $image->blogImgaeLike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                    ->where('Img_Vid_Id',$image->Nfi_Id)
                                    ->count();
                            }
                            $bloglike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                ->where('Img_Vid_Id',null)
                                ->count();
                            $blog->bloglike = $bloglike;
                            $blog_like = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)->where('User_Id',$request->user_id)->get();
                            $blog->blog_like = $blog_like;
                        }
                    }
                    return ResponseMessage::success("Newsfeed list",$newsfeed);
                }else{
                    ResponseMessage::error("User Not Found");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function newsfeedList(Request $request) {
    	try {
    		$rules = [
    			'admin_id' => 'required'
    		];
    		$customeMessage = [
    			'admin_id.required' => 'Please send Admin Id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	if(true)
            	{
                    $newsfeed = Newsfeed::with(['newsfeed_details'=>function($query){
                        $query->select('Nfi_Nfd_Unique_Id','Nfi_Id','Nfi_Path','Nfi_CreatedAt','Nfi_Type');
                    }])->with('newsfeed_details')
                        ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                        ->select('newsfeed.*','user_tbl.Use_Name')
                        ->orderBy('Nfd_Id','DESC')
                        ->get();

                    if(count($newsfeed)){
                        foreach($newsfeed as $blog){
                            foreach($blog->newsfeed_details as $image){
                                $image->blogImgaeLike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                    ->where('Img_Vid_Id',$image->Nfi_Id)
                                    ->count();
                            }
                            $bloglike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                ->where('Img_Vid_Id',null)
                                ->count();
                            $blog->bloglike = $bloglike;
                            $blog_like = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)->where('User_Id',$request->admin_id)->get();
                            $blog->blog_like = $blog_like;
                        }
                    }
                    return ResponseMessage::success("Newsfeed list",$newsfeed);

	            }else{
	            	ResponseMessage::error("Blog Not Found");
	            }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function parent_newsfeed_list(Request $request)
    {
    	try
    	{
    		$rules = [
    			'parent_id' => 'required',
    			'student_id' => 'required'
    		];
    		$customeMessage = [
    			'parent_id.required' => 'Please send parent id',
    			'student_id.required' => 'Please send student id'
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	$parent_id = $request->parent_id;
            	$student_id = $request->student_id;
            	if(User::where('Use_Id',$parent_id)->where('Use_Type','4')->exists())
            	{
            		if(Student::where('Std_Id',$student_id)->where('Std_Parent_Id',$parent_id)->exists())
            		{
            			$studentInfo = Student::where('Std_Id',$student_id)->get()->first();
//            			dd($studentInfo);
		            	$studentAssignBranch = ClassTbl::where('Cla_Id',$studentInfo->Std_Cla_Id)->first()->Cla_Bra_Id;
//		            	dd($studentAssignBranch);
		            	$tagList =  Newsfeedtag::where('Nft_Cla_Id',$studentInfo->Std_Cla_Id)
		            				->where('Nft_Std_Id',$student_id)
		            				->where('Nft_Parent_Id',$studentInfo->Std_Parent_Id)
		            				->select('Nft_Nfd_Unique_Id')
		            				->groupBy('Nft_Nfd_Unique_Id')
		            				->get();

		                $newsfeed = Newsfeed::with('newsfeed_details')
		                            ->whereIn('Nfd_Unique_Id',$tagList)
		                            ->where('Nfd_Brn_Id',$studentAssignBranch)
		                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
		                            ->select('newsfeed.*','user_tbl.Use_Name')
		                            ->orderBy('Nfd_Id','DESC')
		                            ->get();

		                ResponseMessage::success("Newsfeed list",$newsfeed);
            		}else{
            			ResponseMessage::error("Student Not Found");
            		}
	            }else{
	            	ResponseMessage::error("Parent Not Found");
	            }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
