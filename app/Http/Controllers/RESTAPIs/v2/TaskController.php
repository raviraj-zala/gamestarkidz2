<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Helper\Notification;
use App\Helper\UserNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Task;
use App\Model\Module;
use App\Model\Users;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\TaskNotes;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
//use DB;
use App\Helper\ResponseMessage;
//use Illuminate\Support\Facades\Auth;

/**
 * @OA\Post(
 * path="/api/v2/create_task",
 * summary="Create Task",
 * description="Create Task",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"branch_id","teacher_id","user_id"},
 *       @OA\Property(property="branch_id", type="integer", format="number", example="5"),
 *       @OA\Property(property="class_id", type="integer", format="number", example="13"),
 *       @OA\Property(property="teacher_id", type="integer", format="number", example="648"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="task_name", type="string", format="string", example="Task Title"),
 *       @OA\Property(property="task_status", type="integer", format="number", example="1"),
 *       @OA\Property(property="task_date", type="integer", format="date", example="2020-11-20"),
 *       @OA\Property(property="task_due_date", type="integer", format="date", example="2020-11-22"),
 *       @OA\Property(property="task_time", type="integer", format="date", example="17:49:00"),
 *       @OA\Property(property="task_description", type="string", format="string", example="Task Descprition"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/update_task",
 * summary="Update Task",
 * description="Update Task",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"task_id","branch_id","teacher_id","user_id"},
 *       @OA\Property(property="task_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="branch_id", type="integer", format="number", example="5"),
 *       @OA\Property(property="class_id", type="integer", format="number", example="13"),
 *       @OA\Property(property="teacher_id", type="integer", format="number", example="648"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="task_name", type="string", format="string", example="Task Title"),
 *       @OA\Property(property="task_status", type="integer", format="number", example="1"),
 *       @OA\Property(property="task_date", type="integer", format="date", example="2020-11-20"),
 *       @OA\Property(property="task_due_date", type="integer", format="date", example="2020-11-22"),
 *       @OA\Property(property="task_time", type="integer", format="date", example="17:49:00"),
 *       @OA\Property(property="task_description", type="string", format="string", example="Task Descprition"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/create_notes",
 * summary="Create Notes",
 * description="Create Notes",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"task_id","user_id","notes","status"},
 *       @OA\Property(property="task_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="notes", type="string", format="string", example="Notes Title"),
 *       @OA\Property(property="status", type="integer", format="number", example="1"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/create_note",
 * summary="Create Note",
 * description="Create Note",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"task_id","user_id","notes","status"},
 *       @OA\Property(property="task_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="notes", type="string", format="string", example="Notes Title"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */

/**
 * @OA\Post(
 * path="/api/v2/teacher_task_status",
 * summary="Teacher Task Status Update",
 * description="Teacher Task Status Update",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"task_id","user_id","notes","status"},
 *       @OA\Property(property="task_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="teacher_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="status", type="integer", format="number", example="1"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */


/**
 * @OA\get(
 * path="/api/v2/taskList",
 * summary="Task List for Teacher",
 * description="Task List",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Teacher",
 *    in="query",
 *    name="teacher_id",
 *    required=true,
 *    example="648",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */


/**
 * @OA\get(
 * path="/api/v2/task_admin_list",
 * summary="Task List for Admin",
 * description="Task List",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Admin",
 *    in="query",
 *    name="admin_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */


/**
 * @OA\get(
 * path="/api/v2/task_detail",
 * summary="Task Details",
 * description="Task Details",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Task",
 *    in="query",
 *    name="task_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/notes_list",
 * summary="Notes Details",
 * description="Notes Details",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Task",
 *    in="query",
 *    name="task_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/branch_list_task",
 * summary="Branch List",
 * description="Branch List",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Task",
 *    in="query",
 *    name="task_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),@OA\Parameter(
 *    description="ID of Admin",
 *    in="query",
 *    name="admin_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/class_list_task",
 * summary="Class List",
 * description="Class List",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Task",
 *    in="query",
 *    name="task_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/teacher_list_task",
 * summary="Teacher List",
 * description="Teacher List",
 * operationId="authLogin",
 * tags={"task"},
 * @OA\Parameter(
 *    description="ID of Task",
 *    in="query",
 *    name="task_id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */





class TaskController extends Controller
{
    // Teachere id check
    public function taskList(Request $request){
    	try {
            $rules = [
                'teacher_id' => 'required',
            ];
            $customeMessage = [
                'teacher_id.required' => 'Please enter Teacher id',
            ];

            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(Task::where('Tas_Teacher',$request->teacher_id)->exists()){

                    $task = Task::leftjoin('task_notes_tbl','task_tbl.Tas_Id', '=', 'task_notes_tbl.Tas_Id' )
                        ->where('Tas_Teacher',$request->teacher_id)
                        ->select('task_tbl.Tas_Id',
                            'task_tbl.Tas_Title',
                            'task_tbl.Tas_Status',
                            'task_tbl.Tas_Description',
                            'task_tbl.Tas_Time',
                            'task_tbl.Tas_Date',
                            'task_tbl.Tas_Due_Date',
                            'task_tbl.Tas_CreatedAt',
                            'task_notes_tbl.Note as Tas_Note'
                        )
                        ->distinct('Tas_Id')
                        ->get();

                    ResponseMessage::success('Success', $task);
                }else{
                    ResponseMessage::error("Task not found.");
                }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    	
    }

    public function notesList(Request $request){
    	try {
            $rules = [
                'task_id' => 'required',
            ];
            $customeMessage = [
                'task_id.required' => 'Please enter Task id',
            ];

            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(TaskNotes::where('Tas_id',$request->task_id)->exists()){

                    $notes = TaskNotes::where('Tas_id',$request->task_id)
                        ->Select('Note')
                        ->get();

                    ResponseMessage::success('Success', $notes);
                }else{
                    ResponseMessage::error("Task not found.");
                }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}

    }


// Admin Id Check
    public function taskAdminList(Request $request){
    	try {
            $rules = [
                'admin_id' => 'required',
            ];
            $customeMessage = [
                'admin_id.required' => 'Please enter Admin',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(Task::where('Tas_CreatedBy',$request->admin_id)->exists()){
                    $task = Task::leftjoin('user_tbl','task_tbl.Tas_Teacher', '=', 'user_tbl.Use_Id' )
                        ->where('Tas_CreatedBy',$request->admin_id)
                        ->select('task_tbl.Tas_Id',
                            'task_tbl.Tas_Title',
                            'task_tbl.Tas_Status',
                            'task_tbl.Tas_Description',
                            'task_tbl.Tas_Time',
                            'task_tbl.Tas_Date',
                            'task_tbl.Tas_Due_Date',
                            'task_tbl.Tas_CreatedAt',
                            'user_tbl.Use_Name as Tas_Teacher_Name'
                        )
                    ->get();
                    ResponseMessage::success('Success', $task);
                }else{
                    ResponseMessage::error("Task not found.");
                }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}

    }

    public function taskDetail(Request $request){
    	try {   		
    		$rules = [
    			'task_id' => 'required',
    		];
    		$customeMessage = [
    			'task_id.required' => 'Please enter task id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
	    		if(Task::where('Tas_Id',$request->task_id)->exists()){
	    			$data['task'] = Task::where('Tas_Id',$request->task_id)
	    						->Select('Tas_Id','Tas_Title','Tas_Status','Tas_Description','Tas_Time','Tas_Date','Tas_CreatedAt')
	    						->first();
                    if($data['task']){
    	    			$data['task']->notes = TaskNotes::where('Tas_id',$request->task_id)
    	    								->Select('Note','Assignee')
    	    								->get();
                    }
    	    		ResponseMessage::success('Success', $data);
	    		}else{
	    			ResponseMessage::error("Task not found."); 
	    		}
	    	}
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function createNotes(Request $request){
    	try {   		
    		$rules = [
    			'task_id' => 'required',
    			'notes' => 'required',
    			'user_id' => 'required',
    			'status' => 'required',
    		];
    		$customeMessage = [
    			'task_id.required' => 'Please enter task id',
    			'notes.required' => 'Please enter notes',
    			'user_id.required' => 'Please enter user id',
    			'status.required' => 'Please select status',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
            	if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)->exists()){
	            	if(Task::where('Tas_Id',$request->task_id)->exists()){
	            		$task = Task::where('Tas_Id',$request->task_id)->first();
	            		$task->Tas_Status = $request->status;
	            		$task->save();

	            		$note = new TaskNotes();
	            		$note->Tas_Id = $request->task_id;
	            		$note->Note = $request->notes;
	            		$note->Assignee = $request->user_id;
	            		if($note->save()){
		    				ResponseMessage::success('Success', $note);
		    			}
	            	}else{
		    			ResponseMessage::error("Task not found."); 
		    		}
		    	}else{
		    		ResponseMessage::error("User not found."); 
		    	}
            }
        } catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }


    public function createNote(Request $request){
        try {
            $rules = [
                'task_id' => 'required',
                'notes' => 'required',
                'user_id' => 'required',
            ];
            $customeMessage = [
                'task_id.required' => 'Please enter task id',
                'notes.required' => 'Please enter notes',
                'user_id.required' => 'Please enter user id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)->exists()){
                    if(Task::where('Tas_Id',$request->task_id)->exists()){

                        $note = new TaskNotes();
                        $note->Tas_Id = $request->task_id;
                        $note->Note = $request->notes;
                        $note->Assignee = $request->user_id;
                        if($note->save()){
                            ResponseMessage::success('Success', $note);
                        }
                    }else{
                        ResponseMessage::error("Task not found.");
                    }
                }else{
                    ResponseMessage::error("User not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function teacherTaskStatus(Request $request){
    	try {
    		$rules = [
    			'task_id' => 'required',
    			'teacher_id' => 'required',
    			'status' => 'required',
    		];
    		$customeMessage = [
    			'task_id.required' => 'Please enter task id',
    			'teacher_id.required' => 'Please enter user id',
    			'status.required' => 'Please select status',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				ResponseMessage::error($errors->first());
            }else{
            	if(Users::where('Use_Id',$request->teacher_id)->where('Use_Status',1)->exists()){
	            	if(Task::where('Tas_Id',$request->task_id)->exists()){
	            		$task = Task::where('Tas_Id',$request->task_id)->first();
	            		$task->Tas_Status = $request->status;
	            		$task->save();

                        ResponseMessage::success('Success', $task);
	            	}else{
		    			ResponseMessage::error("Task not found.");
		    		}
		    	}else{
		    		ResponseMessage::error("Teacher not found.");
		    	}
            }
        } catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }



    public function createTasks(Request $request){
        try {
            $rules = [
                'branch_id' => 'required',
                'class_id' => 'required',
                'teacher_id' => 'required',
                'user_id' => 'required',
                'task_name' => 'required',
                'task_status' => 'required',
                'task_date' => 'required',
                'task_due_date' => 'required',
                'task_time' => 'required',
                'task_description' => 'required',
            ];
            $customeMessage = [
                'branch_id.required' => 'Please enter Branch Id',
                'class_id.required' => 'Please enter Class Id',
                'teacher_id.required' => 'Please enter Teacher Id',
                'user_id.required' => 'Please enter User',
                'task_name.required' => 'Please enter Task Title',
                'task_status.required' => 'Please enter Task Status',
                'task_date.required' => 'Please enter Task Date',
                'task_due_date.required' => 'Please enter Task Due Date',
                'task_time.required' => 'Please enter Task Time',
                'task_description.required' => 'Please enter Task Description',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
                exit;
            }else{

                $task = New Task();
                $task->Tas_Title = $request->task_name;
                $task->Tas_Branch = $request->branch_id;
                $task->Tas_Class = $request->class_id;
                $task->Tas_Teacher = $request->teacher_id;
                $task->Tas_Status = $request->task_status;
                $task_date =  str_replace('/', '-', $request->task_date);
//                $task->Tas_Date = date('Y-m-d', strtotime($task_date));
                $task->Tas_Date = $request->task_date;
                $task_due_date =  str_replace('/', '-', $request->task_due_date);
//                $task->Tas_Due_Date = date('Y-m-d', strtotime($task_due_date));
                $task->Tas_Due_Date = $request->task_due_date;
                $task->Tas_Time = $request->task_time;
                $task->Tas_Description = $request->task_description;
                $task->Tas_CreatedBy = $request->user_id;
                $task->Tas_CreatedAt = date('Y-m-d H:i:s');
                $task->Tas_UpdatedBy = $request->user_id;
                $task->Tas_UpdatedAt = date('Y-m-d H:i:s');
                $task->save();

                     $user = User::where('Use_Id',$request->user_id)->first()->Use_Name;
                    $title = "Task Add";
                    $message = "Task Added and Assign by ". ucfirst($user);
                    $value = User::where('Use_Id',$request->teacher)->first();
                    Notification::sendNotification($value["Use_Token"],$title.".",$message." ", "TASK_TEACHER");
                    UserNotification::storeNotification($value->Use_Id,"TASK_TEACHER");
//                }


                ResponseMessage::success('Task Create Successfully', $task);

            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function updateTasks(Request $request){
        try {
            $rules = [
                'task_id' => 'required',
                'branch_id' => 'required',
                'class_id' => 'required',
                'teacher_id' => 'required',
                'user_id' => 'required',
                'task_name' => 'required',
                'task_status' => 'required',
                'task_date' => 'required',
                'task_due_date' => 'required',
                'task_time' => 'required',
                'task_description' => 'required',
            ];
            $customeMessage = [
                'task_id.required' => 'Please enter Task Id',
                'branch_id.required' => 'Please enter Branch Id',
                'class_id.required' => 'Please enter Class Id',
                'teacher_id.required' => 'Please enter Teacher Id',
                'user_id.required' => 'Please enter User',
                'task_name.required' => 'Please enter Task Title',
                'task_status.required' => 'Please enter Task Status',
                'task_date.required' => 'Please enter Task Date',
                'task_due_date.required' => 'Please enter Task Due Date',
                'task_time.required' => 'Please enter Task Time',
                'task_description.required' => 'Please enter Task Description',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
                exit;
            }else{

                if(Task::where('Tas_Id', $request->task_id)->exists()){

                    $task = Task::where('Tas_Id', $request->task_id)->first();
                    $task->Tas_Title = $request->task_name;
                    $task->Tas_Branch = $request->branch_id;
                    $task->Tas_Class = $request->class_id;
                    $task->Tas_Teacher = $request->teacher_id;
                    $task->Tas_Status = $request->task_status;
                    $task_date =  str_replace('/', '-', $request->task_date);
                    $task->Tas_Date = $request->task_date;
                    $task_due_date =  str_replace('/', '-', $request->task_due_date);
                    $task->Tas_Due_Date = $request->task_due_date;
                    $task->Tas_Time = $request->task_time;
                    $task->Tas_Description = $request->task_description;
                    $task->Tas_CreatedBy = $request->user_id;
                    $task->Tas_CreatedAt = date('Y-m-d H:i:s');
                    $task->Tas_UpdatedBy = $request->user_id;
                    $task->Tas_UpdatedAt = date('Y-m-d H:i:s');
                    $task->save();

                    $user = User::where('Use_Id',$request->user_id)->first()->Use_Name;
                    $title = "Task Update";
                    $message = "Task Updated and Assign by ". ucfirst($user);
                    $value = User::where('Use_Id',$request->teacher)->first();
                    Notification::sendNotification($value["Use_Token"],$title.".",$message." ", "TASK_TEACHER");
                    UserNotification::storeNotification($value->Use_Id,"TASK_TEACHER");

                    ResponseMessage::success('Task Update Successfully', $task);
                }else{
                    ResponseMessage::error('Task Not Found');
                }

            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function branchListAdmin(Request $request) {
        try {
            $rules = [
                'admin_id' => 'required',
                'task_id' => 'required',
            ];
            $customeMessage = [
                'admin_id.required' => 'Please enter admin id',
                'task_id.required' => 'Please enter admin id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(true){
                    $task_branch = Task::where('Tas_Id', $request->task_id)->first()->Tas_Branch;

                    $branch = Branch::leftjoin('admin_assign_branch_tbl', 'admin_assign_branch_tbl.Aab_Brn_Id','=', 'branch_tbl.Brn_Id')
                        ->where('Aab_Use_Id', $request->admin_id)
                        ->select('Brn_Id','Brn_Name','Brn_Code')
                        ->orderByRaw("FIELD(Brn_Id , '$task_branch') DESC")
                        ->get();
                    ResponseMessage::success("Branch Found.", $branch);
                }else{
                    ResponseMessage::error("Branch not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function classTaskList(Request $request){
        try {
            $rules = [
                'task_id' => 'required',
                'branch_id' => 'required',
            ];
            $customeMessage = [
                'task_id.required' => 'Please enter task id',
                'branch_id.required' => 'Please enter branch id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(true){

                    $task_branch = Task::where('Tas_Id', $request->task_id)->first()->Tas_Branch;
                    $task_class = Task::where('Tas_Id', $request->task_id)->first()->Tas_Class;

                    $class = ClassTbl::where('Cla_Bra_Id',$request->branch_id)
                        ->orderByRaw("FIELD(Cla_Id , '$task_class') DESC")
                        ->select('Cla_Id','Cla_Bra_Id','Cla_Class','Cla_Section','Cla_Status')
                        ->get();


                    ResponseMessage::success("Class Found.", $class);
                }else{
                    ResponseMessage::error("Class not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function teacherTaskList(Request $request){
        try {
            $rules = [
                'task_id' => 'required',
                'branch_id' => 'required',
                'class_id' => 'required',
            ];
            $customeMessage = [
                'task_id.required' => 'Please enter task id',
                'branch_id.required' => 'Please enter branch id',
                'class_id.required' => 'Please enter class id',
           ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(true) {

                    $task_branch = Task::where('Tas_Id', $request->task_id)->first()->Tas_Branch;
                    $task_class = Task::where('Tas_Id', $request->task_id)->first()->Tas_Class;
                    $task_teacher = Task::where('Tas_Id', $request->task_id)->first()->Tas_Teacher;


                    $teacher = Users::join('teacher_assign_class_tbl', 'teacher_assign_class_tbl.Tac_Use_Id', 'user_tbl.Use_Id')
                        ->where('teacher_assign_class_tbl.Tac_Brn_Id', $request->branch_id)
                        ->where('teacher_assign_class_tbl.Tac_Cla_Id', $request->class_id)
                        ->orderByRaw("FIELD(Tac_Use_Id , '$task_teacher') DESC")
                        ->where('Use_Type', 2)
                        ->where('Use_Status', 1)
                        ->distinct()
                        ->get(['Use_Id', 'Use_Name']);


                    ResponseMessage::success("Teacher Found.", $teacher);
                }else{
                    ResponseMessage::error("Teacher Not Found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
