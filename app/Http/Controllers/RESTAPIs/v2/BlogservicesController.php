<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\Model\UserLikeBlog;
use App\Model\Users;
use App\User;
use Validator;
use Input;
use App\Helper\ResponseMessage;
use Auth;
use DB;

/**
 * @OA\Post(
 * path="/api/v2/blog_list",
 * summary="Blog List",
 * description="Blog List",
 * operationId="blog_list",
 * tags={"blog"},
 * @OA\RequestBody(
 *    description="",
 *    @OA\JsonContent(
 *    ),
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 * 
*/

/**
 * @OA\Post(
 * path="/api/v2/blog_image",
 * summary="Blog Images Like",
 * description="Blog Images Like",
 * operationId="blog_image",
 * tags={"blog"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"blog_id","user_id","flag"},
 *       @OA\Property(property="blog_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="659"),
 *       @OA\Property(property="image_id", type="integer", format="number", example="1"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * 
*/

/**
 * @OA\Post(
 * path="/api/v2/blog_like",
 * summary="Blog Like",
 * description="Blog Like",
 * operationId="blog_like",
 * tags={"blog"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"blog_id","user_id","flag"},
 *       @OA\Property(property="blog_id", type="integer", format="number", example="68"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="659"),
 *       @OA\Property(property="flag", type="integer", format="number", example="1"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * 
*/

/**
 * @OA\Post(
 * path="/api/v2/blog_image_like",
 * summary="Blog Image Like",
 * description="Blog Image Like",
 * operationId="authLogin",
 * tags={"blog"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"blog_id","user_id","flag","image_id"},
 *       @OA\Property(property="blog_id", type="integer", format="number", example="38"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="659"),
 *       @OA\Property(property="flag", type="integer", format="number", example="1"),
 *       @OA\Property(property="image_id", type="integer", format="number", example="44"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 *
*/

/**
 * @OA\Get(
 * path="/api/v2/blogImageLikeCount",
 * summary="Blog Image Like Count",
 * description="Blog Image Like Count",
 * operationId="blogImageLikeCount",
 * tags={"blog"},
 * @OA\RequestBody(
 *    description="",
 *    @OA\JsonContent(
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * 
*/


class BlogservicesController extends Controller
{
    public function blogList() {
    	try {
    		$blogs = Newsfeed::with(['images'=>function($query){
                $query->select('Nfi_Nfd_Unique_Id','Nfi_Id','Nfi_Path');
            }])->where('Nfd_Status','1')
    						->orderBy('Nfd_CreatedAt','DESC')
    						->get(['Nfd_Id','Nfd_Title','Nfd_Description','Nfd_Unique_Id','Nfd_CreatedAt']);
       
    		if(count($blogs)){
	    		foreach($blogs as $blog){
                    foreach($blog->images as $image){
                       $image->blogImgaeLike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                            ->where('Img_Vid_Id',$image->Nfi_Id)
                                            ->count();
                    }
                    $bloglike = UserLikeBlog::where('Blog_Id',$blog->Nfd_Id)
                                        ->where('Img_Vid_Id',null)
                                        ->count();
                    $blog->bloglike = $bloglike;
	    		}
	    	}
            $image=env('Blog_Image');
    		ResponseMessage::successwithimage('Success', $image, $blogs);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function blogImage(Request $request){
        try {
            $rules = [
                'blog_id' => 'required',
                'user_id' => 'required',
                'image_id'=> 'required',
            ];
            $customeMessage = [
                'blog_id.required' => 'Please enter blog id',
                'user_id.required' => 'Please enter user id',
                'image_id.required' => 'Please enter image id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                $images['images'] = array();
                $images['likes'] = array();
                // dd($images);
                if(User::where('Use_Id',Input::get('user_id'))->exists())
                {
                    $images['images'] = UserLikeBlog::where('user_id',Input::get('user_id'))->where('Blog_Id',Input::get('blog_id'))->where('Img_Vid_Id',Input::get('image_id'))->first();

                }else{
                    ResponseMessage::error('User Not Found');
                }

                $images['likes'] = UserLikeBlog::where('Blog_Id',$request->blog_id)
                    ->where('Img_Vid_Id',$request->image_id)
                    ->count();

                ResponseMessage::success("Blog Image Like List", $images);

            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function blogLike(Request $request){
    	try {
    		$rules = [
    			'blog_id' => 'required',
    			'user_id' => 'required',
                'flag' => 'required',
    		];
    		$customeMessage = [
    			'blog_id.required' => 'Please enter blog id',
    			'user_id.required' => 'Please enter user id',
                'flag.required' => 'Please enter flag',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
				ResponseMessage::error($errors->first());
            }else{
            	if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)){
            		if(Newsfeed::where('Nfd_Status',"1")->where('Nfd_Id',$request->blog_id)->exists())
                    {
            			$image=null;
            			if(isset($request->image_id)){
            				if(Newsfeedinfo::where('Nfi_Nfd_Unique_Id',$request->image_id)->exists()){
            					$image = $request->image_id;
            				}else{
            					ResponseMessage::error("Image not found."); 
            					exit;
            				}
            			}
                        if ($request->flag==1) {
                			$like = new UserLikeBlog();
                			$like->Blog_Id = $request->blog_id;
                			$like->User_Id = $request->user_id;
                            $like->Blog_Like = $request->flag;
                			$like->Img_Vid_Id = $image;
                			$like->save();
                        }else{
                            $like = UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->first();
                            if ($like) {
                                UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->delete();
                            }
                        }
            			ResponseMessage::success('Success', $like);
            		}else{
            			ResponseMessage::error("Blog not found."); 
            		}
            	}else{
	    			ResponseMessage::error("User not found."); 
	    		}
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function blogImageLike(Request $request){
        try {
            $rules = [
                'blog_id' => 'required',
                'user_id' => 'required',
                'flag' => 'required',
                'image_id' => 'required',
            ];
            $customeMessage = [
                'blog_id.required' => 'Please enter blog id',
                'user_id.required' => 'Please enter user id',
                'flag.required' => 'Please enter flag',
                'image_id.required' => 'Please enter image id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(Users::where('Use_Id',$request->user_id)->where('Use_Status',1)){
                    if(Newsfeed::where('Nfd_Status',"1")->where('Nfd_Id',$request->blog_id)->exists())
                    {
                        
                        $image = $request->image_id;
                        
                        if ($request->flag==1) {
                            $like = new UserLikeBlog();
                            $like->Blog_Id = $request->blog_id;
                            $like->User_Id = $request->user_id;
                            $like->Blog_Like = $request->flag;
                            $like->Img_Vid_Id = $image;
                            $like->save();
                        }else{
                            $like = UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->where('Img_Vid_Id',$image)->first();
                            if ($like) {
                                UserLikeBlog::where('Blog_Id',$request->blog_id)->where('User_Id',$request->user_id)->where('Img_Vid_Id',$image)->delete();
                            }
                        }
                        ResponseMessage::success('Success', $like);
                    }else{
                        ResponseMessage::error("Blog not found."); 
                    }
                }else{
                    ResponseMessage::error("User not found."); 
                }
            }
            // dd('dd');
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function blogImageLikeCount(){
        try {
                if(true){
//                    $likes = UserLikeBlog::
                }else{
                    ResponseMessage::error("Image not found.");
                }
//            }
            // dd('dd');
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
