<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Model\Teacher_assign_class;
use App\Model\ClassTbl;
use App\User;
use App\Model\Users;
use Auth;
use Input;
use Validator;
use DB;

/**
	* @OA\Post(
	* path="/api/v2/sign_Up",
	* summary="User Sign Up",
	* description="User Sign Up",
	* operationId="sign_Up",
	* tags={"user"},
	* @OA\RequestBody(
	* 		required=true,
    * 		@OA\MediaType(
 	* 		mediaType="multipart/form-data",
	* 			@OA\Schema(
	*			required={"name","email","password","mobile_no","type","image"},
    *			type="object",
    *				@OA\Property(
	*					property="name",
	*					type="text",
	*				),
	*				@OA\Property(
	*					property="email",
	*					type="text",
	*				),
	*				@OA\Property(
	*					property="password",
	*					type="password",
	*				),
	*				@OA\Property(
	*					property="mobile_no",
	*					type="number",
	*				),
	*				@OA\Property(
	*					property="type",
	*					type="number",
	*				),
	*				@OA\Property(
	*					property="image",
	*					type="file",
	*				),
	*			)
	*		)
	*),
	* @OA\Response(
 	*    response=400,
 	*    description="success",
 	*    @OA\JsonContent(
 	*       @OA\Property(property="message", type="string", example="success")
 	*        )
 	*     )
 	* )
*/
/**
 	* @OA\Post(
	* path="/api/v2/profile_Update",
	* summary="User Profile Update",
	* description="User Profile Update",
	* operationId="profile_Update",
	* tags={"user"},
	* @OA\RequestBody(
	* 		required=true,
    * 		@OA\MediaType(
 	* 		mediaType="multipart/form-data",
	* 			@OA\Schema(
	*			required={"user_id"},
    *			type="object",
    *				@OA\Property(
	*					property="user_id",
	*					type="number",
	*				),
    *				@OA\Property(
	*					property="name",
	*					type="text",
	*				),
	*				@OA\Property(
	*					property="email",
	*					type="text",
	*				),
	*				@OA\Property(
	*					property="mobile_no",
	*					type="number",
	*				),
	*				@OA\Property(
	*					property="image",
	*					type="file",
	*				),
	*			)
	*		)
	*),
	* @OA\Response(
 	*    response=400,
 	*    description="success",
 	*    @OA\JsonContent(
 	*       @OA\Property(property="message", type="string", example="success")
 	*        )
 	*     )
 	* )
*/
/**
 	* @OA\Post(
	* path="/api/v2/email_Mobile_Profile",
	* summary="Get User Profile Using  Email Or Mobile",
	* description="Get User Profile Using  Email Or Mobile",
	* operationId="email_Mobile_Profile",
	* tags={"user"},
	* @OA\RequestBody(
	*    required=true,
	*    description="Get User Profile Using  Email Or Mobile",
	*    @OA\JsonContent(
	*       @OA\Property(property="email", type="string", format="string", example="admin@gmail.com"),
	*       @OA\Property(property="mobile_no", type="number", format="number", example="1234567890"),
	*       @OA\Property(property="_token", type="string", format="password", example="T48rttktwm8z3ygNS26ddGLFNbDufHWvWQbefZhM"),
	*    ),
	* ),
	* @OA\Response(
 	*    response=400,
 	*    description="success",
 	*    @OA\JsonContent(
 	*       @OA\Property(property="message", type="string", example="success")
 	*        )
 	*     )
 	* )
*/
/**
 	* @OA\Post(
	* path="/api/v2/teacher_Assign_Class",
	* summary="Get Teacher Class List Using user_id",
	* description="Get Teacher Class List Using user_id",
	* operationId="teacher_Assign_Class",
	* tags={"user"},
	* @OA\RequestBody(
	*    required=true,
	*    description="Get User Profile Using  Email Or Mobile",
	*    @OA\JsonContent(
	*       required={"user_id"},
	*       @OA\Property(property="user_id", type="number", format="number", example="320"),
	*       @OA\Property(property="_token", type="string", format="password", example="T48rttktwm8z3ygNS26ddGLFNbDufHWvWQbefZhM"),
	*    ),
	* ),
	* @OA\Response(
 	*    response=400,
 	*    description="success",
 	*    @OA\JsonContent(
 	*       @OA\Property(property="message", type="string", example="success")
 	*        )
 	*     )
 	* )
 	* 
*/
class ProfileservicesController extends Controller
{
  	//User Registration
	public function sign_Up(Request $request)
	{
		try{
			if(User::where('Use_Email',Input::get('email'))->exists()){
				$user = User::where('Use_Email',Input::get('email'))->get();
				ResponseMessage::error("Email Already Exist");
			}else if(User::where('Use_Mobile_No',Input::get('mobile_no'))->exists()){
				$user = User::where('Use_Mobile_No',Input::get('mobile_no'))->get();
				ResponseMessage::error("Mobile Number Already Exist");	
			}else{
				$user = new User;
				if($request->file('image')){
		            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
		            $isUpload = $request->file('image')->move(public_path('images/profile'), $imageName);
		        }else{
		            $imageName = "";
		        }
		        $user->Use_Image = $imageName;
				$user->Use_Name = Input::get('name');
				$user->Use_Mobile_No = Input::get('mobile_no');
				$user->Use_Email = Input::get('email');
				$user->Use_Password = bcrypt(Input::get('password'));
				$user->Use_Type = Input::get('type');
				$user->Use_Register_Type = Input::get('reg_type');
				$user->Use_Token = Input::get('token');
				$user->Use_Cla_Id = Input::get('class');
				$user->Use_CreatedAt = date('Y-m-d H:i:s');
				$user->Use_UpdatedAt  = date('Y-m-d H:i:s');

				$user->save();
				$user->Use_Id;
		
				if($user){
					$user1['Use_CreatedBy'] = $user->Use_Id;
					$user1['Use_UpdatedBy'] = $user->Use_Id;

					User::where('Use_Id',$user->Use_Id)->update($user1);

					ResponseMessage::success("Registration Successfully",$user);
				}else{
					ResponseMessage::error("Registration Fail, Please Try Again");
				}
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//User Profile Updatation
	public function profile_Update(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if($request->file('image')){
	            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $isUpload = $request->file('image')->move(public_path('images/profile'), $imageName);
	        }else{
	            $imageName = "";
	        }
	        if(Input::get('name')){
        		$update_req['Use_Name'] = Input::get('name');
        		$update_req['Use_UpdatedBy'] = Input::get('user_id');
        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
        		$update = User::where('Use_Id',Input::get('user_id'))->update($update_req);
        	}else if(Input::get('mobile_no')){
        		$update_req['Use_Mobile_No'] = Input::get('mobile_no');
        		$update_req['Use_UpdatedBy'] = Input::get('user_id');
        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
        		$update = User::where('Use_Id',Input::get('user_id'))->update($update_req);
        	}else if(Input::get('email')){
        		$update_req['Use_Email'] = Input::get('email');
        		$update_req['Use_UpdatedBy'] = Input::get('user_id');
        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
        		$update = User::where('Use_Id',Input::get('user_id'))->update($update_req);
        	}else if($request->file('image')){
        		$update_req['Use_Image'] = $imageName;
        		$update_req['Use_UpdatedBy'] = Input::get('user_id');
        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
        		$update = User::where('Use_Id',Input::get('user_id'))->update($update_req);
        	}

			if($update){
				ResponseMessage::success("Profile Updated Successfully",$update_req);
			}else{
				ResponseMessage::error("Profile Updation Fail");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	/**
	   * 
	   * To check user Email Id/Mobile No.
	   * @author Jayesh Sukhadiya 
	   * input field = email 
	   * input field = mobile_no
	   */
	//user already register using email id / mobile no check

	public function email_Mobile_Profile(Request $request)
	{
		try
		{
			$mobile = "";
			if(User::where('Use_Email',Input::get('email'))->exists()){
				$user = User::where('Use_Email',Input::get('email'))->get();
				ResponseMessage::error("Email Already Exist");
			}else if(User::where('Use_Mobile_No',Input::get('mobile_no'))->exists()){
				$user = User::where('Use_Mobile_No',Input::get('mobile_no'))->get();
				ResponseMessage::error("Mobile Number Already Exist");
			}else{
				ResponseMessage::success("Successfully",$mobile);
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function teacher_Assign_Class(Request $request)
	{
		try 
		{
			$rules = [
				'user_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$teacher = Teacher_assign_class::where('Tac_Use_Id',Input::get('user_id'))->select('Tac_Cla_Id')->get()->toArray();

			$class = ClassTbl::whereIn('Cla_Id',$teacher)->get();

			if($class)
			{
				ResponseMessage::success("Teacher Class List",$class);
			}else
			{
				ResponseMessage::error('Record Not Found');
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}
