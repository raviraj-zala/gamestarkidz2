<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Teacher_assign_class;
use App\Model\Student;
use App\Model\Report;
use Auth;
use Input;
use Validator;
use DB;

class ReportservicesController extends Controller
{
   //Progress Report
	public function report_List(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}
			if(Teacher_assign_class::where('Tac_Use_Id',Input::get('user_id'))->where('Tac_Cla_Id',Input::get('class_id'))->exists())
			{
				// $class_id = User::where('Use_Id',Input::get('user_id'))->first()->Use_Cla_Id;
				$student_list = Student::where("Std_Cla_Id",Input::get('class_id'))->select(['Std_Id'])->get()->toArray();

				$report = Report::whereIn("Rpt_Std_Id",$student_list)->join('student_tbl','student_tbl.Std_Id','=','report_tbl.Rpt_Std_Id')->leftjoin('subject_tbl','subject_tbl.Sub_Id','=','report_tbl.Rpt_Sub_Id')->select(['report_tbl.*','student_tbl.Std_Image','subject_tbl.Sub_Json_Data'])->orderBy("Rpt_Date","DESC")->get();

				if($report)
				{
					ResponseMessage::success('Report',$report);
				}else{
					ResponseMessage::error('Report Not Found');
				}
			}else{
				ResponseMessage::error('Teacher and Class Not Found');
			}
		} 
		catch (Exception $e) {
			Exceptions::exception($e);
		}
	}
	public function report_Update(Request $request){
		try {
			$rules = [
				'user_id' => 'required',
				'report_id' => 'required',
				'report_data' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{
				if(Report::where('Rpt_Id',Input::get('report_id'))->exists())
				{
					$report = Report::find(Input::get('report_id'));
	                $report->Rpt_Data = Input::get('report_data');
	                $report->Rpt_UpdatedBy = Input::get('user_id');
	                $report->Rpt_UpdatedAt = date("Y-m-d H:i:s");
	                if ($report->update()) {
	                    ResponseMessage::success('Report Successfully Update.',$report);
	                } else {
	                    ResponseMessage::error('Report Not Update.');
	                }
				}
				else{
					ResponseMessage::error('Report Not Found');
				}

			}else{
				ResponseMessage::error('Teacher and Class Not Found');
			}

		} catch (\Exception $e) {
			Exceptions::exception($e);	
		}
	}

	public function parent_report_List(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',
				'student_id' => '',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->where("Use_Type",4)->exists())
			{
				if(Input::get('student_id')!=""){
					$std_id = Input::get('student_id');
				} else{
					$std_id = Student::where('Std_Parent_Id',Input::get('user_id'))->first()->Std_Id;
				}

				$report = Report::where("Rpt_Std_Id",$std_id)->join('student_tbl','student_tbl.Std_Id','=','report_tbl.Rpt_Std_Id')->leftjoin('subject_tbl','subject_tbl.Sub_Id','=','report_tbl.Rpt_Sub_Id')->select(['report_tbl.*','student_tbl.Std_Image','subject_tbl.Sub_Json_Data'])->orderBy("Rpt_Date","DESC")->get();

				if($report)
				{
					ResponseMessage::success('Report',$report);
				}else{
					ResponseMessage::error('Report Not Found');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		} 
		catch (Exception $e) {
			Exceptions::exception($e);
		}
	}

	// Month wise student report. student patent wise
	public function parent_report_Mothwise_List(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',
				'month' => 'required',
				'student_id' => '',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$month = Input::get('month');
			$year = date('Y');

			if(User::where('Use_Id',Input::get('user_id'))->where("Use_Type",4)->exists())
			{
				if(Input::get('student_id')!=""){
					$std_id = Input::get('student_id');
				} else{
					$std_id = Student::where('Std_Parent_Id',Input::get('user_id'))->first()->Std_Id;
				}
				$report = Report::where("Rpt_Std_Id",$std_id)->whereRaw('MONTH(Rpt_Date) = ?',[$month])->whereRaw('YEAR(Rpt_Date) = ?',[$year])->join('student_tbl','student_tbl.Std_Id','=','report_tbl.Rpt_Std_Id')->leftjoin('subject_tbl','subject_tbl.Sub_Id','=','report_tbl.Rpt_Sub_Id')->select(['report_tbl.*','student_tbl.Std_Image','subject_tbl.Sub_Json_Data'])->orderBy("Rpt_Date","DESC")->get();

				if($report)
				{
					ResponseMessage::success('Report',$report);
				}else{
					ResponseMessage::error('Report Not Found');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		} 
		catch (Exception $e) {
			Exceptions::exception($e);
		}
	}
}
