<?php

namespace App\Http\Controllers\RESTAPIs\v2;

use App\Model\ExamTitle;
use App\Model\StudentSubExamAns;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\StudentExamAns;
use App\Model\Student;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Helper\ResponseMessage;
use App\Model\StudentMark;
//use Auth;

/**
 * @OA\get(
 * path="/api/v2/exam_list",
 * summary="Exam List",
 * description="Exam List",
 * operationId="authLogin",
 * tags={"exam"},
 * @OA\Parameter(
 *    description="ID of class",
 *    in="query",
 *    name="class_id",
 *    required=true,
 *    example="13",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ), @OA\Parameter(
 *    description="ID of Student",
 *    in="query",
 *    name="student_id",
 *    required=true,
 *    example="218  ",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */


/**
 * @OA\get(
 * path="/api/v2/question_list",
 * summary="Question List",
 * description="Question List",
 * operationId="authLogin",
 * tags={"exam"},
 * @OA\Parameter(
 *    description="ID of exam",
 *    in="query",
 *    name="exam_id",
 *    required=true,
 *    example="22",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */

/**
 * @OA\get(
 * path="/api/v2/stu_question_ans_list",
 * summary="Question List",
 * description="Studen Question Answer List",
 * operationId="authLogin",
 * tags={"exam"},
 * @OA\Parameter(
 *    description="ID of exam",
 *    in="query",
 *    name="exam_id",
 *    required=true,
 *    example="22",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),@OA\Parameter(
 *    description="ID of Student",
 *    in="query",
 *    name="student_id",
 *    required=true,
 *    example="218",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */


/**
 * @OA\get(
 * path="/api/v2/result_rating",
 * summary="Results",
 * description="Results Rating",
 * operationId="authLogin",
 * tags={"exam"},
 * @OA\Parameter(
 *    description="ID of Class",
 *    in="query",
 *    name="class_id",
 *    required=true,
 *    example="13",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),@OA\Parameter(
 *    description="ID of Student",
 *    in="query",
 *    name="student_id",
 *    required=true,
 *    example="218",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),@OA\Parameter(
 *    description="ID of Student",
 *    in="query",
 *    name="branch_id",
 *    required=true,
 *    example="5",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=400,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success")
 *        )
 *     )
 * )
 */



/**
 * @OA\Post(
 * path="/api/v2/stu_sub_question_ans",
 * summary="Submit Student Subjective Question Answer",
 * description="Create Notes",
 * operationId="authLogin",
 * tags={"exam"},
 * @OA\RequestBody(
 *    required=true,
 *    description="",
 *    @OA\JsonContent(
 *       required={"task_id","user_id","notes","status"},
 *       @OA\Property(property="task_id", type="integer", format="number", example="1"),
 *       @OA\Property(property="user_id", type="integer", format="number", example="332"),
 *       @OA\Property(property="notes", type="string", format="string", example="Notes Title"),
 *       @OA\Property(property="status", type="integer", format="number", example="1"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 * */


class ExamController extends Controller
{
    public function examList(Request $request){
//      dd('hi');
        try {
            $rules = [
                'class_id' => 'required',
                'student_id' => 'required',
            ];
            $customeMessage = [
                'class_id.required' => 'Please enter class id',
                'student_id.required' => 'Please enter student id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
//             dd($request->all());
            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                    $exams = QuestionClass::join('branch_tbl','question_class_tbl.Cla_Bra_Id','branch_tbl.Brn_Id')
                                    ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                                    ->select('question_class_tbl.Que_Cla_Id',
                                        'question_class_tbl.Exam_Title',
                                        'question_class_tbl.Exam_Subject',
                                        'question_class_tbl.Exm_Date',
                                        'question_class_tbl.Exm_Type',
                                        'question_class_tbl.Exm_Due_Date',
                                        'question_class_tbl.Exm_Time',
                                        'question_class_tbl.Cla_Id',
                                        'question_class_tbl.Cla_Sec_Id',
                                        'question_class_tbl.Exm_Complete_Status',
                                        'exam_title_tbl.exam_title',
                                        'branch_tbl.Brn_Name')
                                    ->orderBy('Que_Cla_CreatedAt', 'DESC')
                                    ->where('Que_Cla_Status',1)
                                    ->get();


                $stu_exms = StudentExamAns::where('Stu_Id', $request->student_id)->select('Stu_Id', 'Que_Cla_Id')->get();


                if(count($exams)){
                    $exam_array['compeleted_exam'] = array();
                    $exam_array['upcoming_exam'] = array();
                    $upcomin_exam = array();
                    foreach($exams as $exam){
                        $class = explode(',',$exam->Cla_Id);
                        $class_id = $class[1];
                        $branch_id = $class[0];
                        if (ClassTbl::where('Cla_Id', $request->class_id)->exists()) {
                            $class_data = ClassTbl::where('Cla_Id', $request->class_id)->first();
                            $class_branch = $class_data->Cla_Bra_Id . ',' . $class_data->Cla_Class;
                            if ($class_branch == $exam->Cla_Id) {
                                $upcomin_exam[] = $exam;
                            }
                        }
                    }

                    if(count($upcomin_exam)) {//4,3
                            //dump($stu_exmsnew);
                            foreach($upcomin_exam as $exam) {
                                $stu_exmsnew = StudentExamAns::where('Stu_Id', $request->student_id)
                                    ->where('Que_Cla_Id', $exam->Que_Cla_Id)
                                    ->get();

                                if (count($stu_exmsnew)) {
                                foreach ($stu_exmsnew as $stu_exm) {
                                    if (($stu_exm->Que_Cla_Id == $exam->Que_Cla_Id)) {

                                        $exam_array_single = array(
                                            'Cla_Id' => $request->class_id,
                                            'Que_Cla_Id' => $exam->Que_Cla_Id,
                                            'Exam_Title' => $exam->exam_title,
                                            'Exm_Date' => $exam->Exm_Date,
                                            'Exm_Due_Date' => $exam->Exm_Due_Date,
                                            'Exm_Type' => $exam->Exm_Type,
                                            'Exm_Time' => $exam->Exm_Time,
                                            'Exam_Subject' => $exam->Exam_Subject,
                                            'Cla_Sec_Id' => $exam->Cla_Sec_Id,
                                            'Exam_Complete_status' => 1,
                                            'Brn_Name' => $exam->Brn_Name,
                                        );
                                        array_push($exam_array['compeleted_exam'], $exam_array_single);
                                    }
                                }
                            }
                                else {
                                    if(Question::where('Que_Cla_Id',$exam->Que_Cla_Id)->exists()){

                                        if($exam->Exm_Due_Date < date('Y-m-d') ){
                                            $time   = explode(":", $exam->Exm_Time);
                                            $hour   = $time[0] * 60 * 60 * 1000;
                                            $minute = $time[1] * 60 * 1000;
                                            $second_new = $time[2];
                                            $result = $hour + $minute +  $second_new;

                                            $exam_array_single = array(
                                                'Cla_Id' => $request->class_id,
                                                'Que_Cla_Id' => $exam->Que_Cla_Id,
                                                'Exam_Title' => $exam->exam_title,
                                                'Exm_Date' => $exam->Exm_Date,
                                                'Exm_Due_Date' => $exam->Exm_Due_Date,
                                                'Exm_Type' => $exam->Exm_Type,
                                                'Exm_Time' => $result,
                                                'Exam_Subject' => $exam->Exam_Subject,
                                                'Cla_Sec_Id' => $exam->Cla_Sec_Id,
                                                'Exam_Complete_status' => 0,
                                                'Brn_Name' => $exam->Brn_Name,
                                            );
                                            array_push($exam_array['compeleted_exam'], $exam_array_single);
                                        }else if($exam->Exm_Due_Date >= date('Y-m-d')){
                                            $time   = explode(":", $exam->Exm_Time);
                                            $hour   = $time[0] * 60 * 60 * 1000;
                                            $minute = $time[1] * 60 * 1000;
                                            $second_new = $time[2];
                                            $result = $hour + $minute +  $second_new;

                                            $exam_array_single = array(
                                                'Cla_Id' => $request->class_id,
                                                'Que_Cla_Id' => $exam->Que_Cla_Id,
                                                'Exam_Title' => $exam->exam_title,
                                                'Exm_Date' => $exam->Exm_Date,
                                                'Exm_Due_Date' => $exam->Exm_Due_Date,
                                                'Exm_Type' => $exam->Exm_Type,
                                                'Exm_Time' => $result,
                                                'Exam_Subject' => $exam->Exam_Subject,
                                                'Cla_Sec_Id' => $exam->Cla_Sec_Id,
                                                'Exam_Complete_status' => 0,
                                                'Brn_Name' => $exam->Brn_Name,
                                            );
                                            array_push($exam_array['upcoming_exam'], $exam_array_single);
                                        }
                                    }
                                }
                        }
                    }
                }
                    ResponseMessage::success('Success', $exam_array);
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function questionList(Request $request){
        try {
            $rules = [
                'exam_id' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->where('Que_Cla_Status',1)->exists()){

                    if(Question::where('Que_Cla_Id',$request->exam_id)->exists()){
                        $questions = Question::where('Que_Cla_Id',$request->exam_id)->get();
                        ResponseMessage::success('Success', $questions);

                    }else{
                        ResponseMessage::error("Question Not Found.");
                    }
                }else{
                    ResponseMessage::error("Exam not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function stuQuestionAnsList(Request $request){
        try {
            $rules = [
                'exam_id' => 'required',
                'student_id' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
                'student_id.required' => 'Please enter Student id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->where('Que_Cla_Status',1)->exists()){

                    $questions = Question::select('Que_Id','Que_Question','Que_Option_1','Que_Option_2','Que_Option_3','Que_Option_4','Que_Ans','Que_Marks')
                        ->where('Que_Cla_Id',$request->exam_id)
                        ->get();
                    $std_ans = StudentExamAns::where('Que_Cla_Id',$request->exam_id)
                        ->where('Stu_Id',$request->student_id)
                        ->first();
                    $decode = json_decode($std_ans->Ques_Ans);
                    $std_marks = StudentMark::where('Stu_Id',$request->student_id)->where('Que_Cla_Id',$request->exam_id)->first();
                    if(!empty($std_marks)) {
                        $ans_decode = json_decode($std_marks->stu_marks);
                    } else {
                        $ans_decode = 0;
                    }

                    $stu_marks=0;
                    $total_marks=0;
                    $i=0;
                    foreach ($questions as $question) {
                        $total_marks += $question->Que_Marks;
                        foreach($decode as $key => $value){
                            if($key == $question->Que_Id){
                                if($question->Que_Ans==$value){
                                    $stu_marks += $question->Que_Marks;
                                }
                                $questions[$i]->Std_Ans = $value;
                            }
                        }
                        if(!empty($stu_marks)) {
                            $student_marks = $stu_marks;
                        } else {
                            $student_marks = !empty($ans_decode) ? array_sum($ans_decode) : $ans_decode;
                        }
                        $i++;
                    }
                    $data = 0;
                    foreach ($ans_decode as $question) {
                        $data += $question;
                    }
                    $questions = array('questions'=>$questions,'total_marks'=>$total_marks,'student_marks'=>$student_marks);
                    ResponseMessage::success('Success', $questions);
                }else{
                    ResponseMessage::error("Exam not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function submit(Request $request){
        try {
            $rules = [
                'exam_id' => 'required',
                'student_id' => 'required',
                'questions' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
                'student_id.required' => 'Please enter student id',
                'questions.required' => 'Please enter question',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->where('Que_Cla_Status',1)->exists()){
                    if(Student::where('Std_Id',$request->student_id)->where('Std_Status',1)->exists()){
                        $test = new StudentExamAns();
                        $test->Que_Cla_Id = $request->exam_id;
                        $test->Stu_Id = $request->student_id;
                        $test->Ques_Ans = $request->questions;
                        $test->Exm_Status = $request->exm_status;
                        $test->save();

                        $exam = QuestionClass::where('Que_Cla_Id',$request->exam_id)->first()->Exm_Complete_Status;
                        $exam->Exm_Complete_Status = 1;
                        $exam->save();

                        ResponseMessage::success('Success', $test);
                    }else{
                        ResponseMessage::error("Student not found.");
                    }
                }else{
                    ResponseMessage::error("Exam not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function result(Request $request) {
        try {
            $rules = [
                'class_id' => 'required',
                'student_id' => 'required',
                'exam_id' => 'required',
            ];
            $customeMessage = [
                'exam_id.required' => 'Please enter exam id',
                'student_id.required' => 'Please enter student id',
                'class_id.required' => 'Please enter class id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();   
                ResponseMessage::error($errors->first());
            }else{
                if(ClassTbl::where('Cla_Id', $request->class_id)->where('Cla_Status',1)->exists()){
                    if(Student::where('Std_Id', $request->student_id)->where('Std_Cla_Id',$request->class_id)->where('Std_Status',1)->exists()){
                        if(QuestionClass::where('Que_Cla_Id',$request->exam_id)->exists()){
                            $exam_result = QuestionClass::where('Que_Cla_Id',$request->exam_id)
                                        ->Select('Cla_Bra_Id',
                                            'Cla_Id','Cla_Sec_Id',
                                            'Exam_Title',
                                            'Exam_Subject',
                                            'Exm_Date',
                                            'Exm_Type',
                                            'Exm_Due_Date',
                                            'Exm_Time'
                                        )
                                        ->first();
                            $student = Student::Select('Std_Gr_No','Std_Name')
                                        ->where('Std_Id', $request->student_id)
                                        ->first();
                            $exam_result->Std_Gr_No = $student->Std_Gr_No;
                            $exam_result->Std_Name = $student->Std_Name;
                            $questions = Question::select('Que_Id','Que_Question','Que_Option_1','Que_Option_2','Que_Option_3','Que_Option_4','Que_Ans')
                                        ->where('Que_Cla_Id',$request->exam_id)
                                        ->get();
                            $std_ans = StudentExamAns::where('Que_Cla_Id',$request->exam_id)
                                        ->where('Stu_Id',$request->student_id)
                                        ->first();
                            $decode = json_decode($std_ans->Ques_Ans);
                            // dd($decode);
                            $result = 0;
                            foreach ($questions as $question) {
                                foreach($decode as $key => $value){
                                    if($key == $question->Que_Id){
                                        $question->Std_Ans = $value;
                                        if($value == $question->Que_Ans){
                                            $result++;
                                        }
                                    }
                                }
                            }
                            $exam_result->correct_result = $result;
                            $exam_result->wrong_result = count($questions)-$result;
                            $exam_result->questions= $questions;
                            ResponseMessage::success('Success', $exam_result);
                        }else{
                            ResponseMessage::error("Exam not found.");    
                        }
                    }else{
                        ResponseMessage::error("Student not found.");
                    }
                }else{
                    ResponseMessage::error("Class not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function resultRating(Request $request) {
        try {
            $rules = [
                'class_id' => 'required',
                'branch_id' => 'required',
                'student_id' => 'required',
            ];
            $customeMessage = [
                'class_id.required' => 'Please enter class id',
                'branch_id.required' => 'Please enter branch id',
                'student_id.required' => 'Please enter student id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            }else{
                $title=[];
                $subject=[];
                if(Student::where('Std_Id', $request->student_id)->where('Std_Cla_Id',$request->class_id)->where('Std_Status',1)->exists()){
                    $class = ClassTbl::where('Cla_Id',$request->class_id)
                        ->where('Cla_Bra_Id',$request->branch_id)
                        ->first(['Cla_Class']);
                    $cla_branch = $request->branch_id.','.$class->Cla_Class;
                    $exam = Student::join('class_tbl','class_tbl.Cla_Id','student_tbl.Std_Cla_Id')
                        ->join('question_class_tbl','question_class_tbl.Cla_Bra_Id','class_tbl.Cla_Bra_Id')
                        ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                        ->Select('question_class_tbl.*','exam_title_tbl.exam_title','exam_title_tbl.exam_year','exam_title_tbl.exam_id')
                        ->where('Exm_Type',1)
                        ->where('student_tbl.Std_Id', $request->student_id)
                        ->where('question_class_tbl.Cla_id', $cla_branch)
//                        ->groupBy('exam_title_tbl.exam_title')
                        ->get();
//                     dd($exam);
                    $sub=[];
                    $grand_total = $que_total = 0;
                    $array_exam_per = array();
                    $exam_title = array();
                    $array_exam = array();
                    $rating['exam'] = array();

                    foreach ($exam as $ex){
                        $questions = Question::leftjoin('question_class_tbl', 'question_class_tbl.Que_Cla_Id','question_tbl.Que_Cla_Id')
                            ->select('question_tbl.Que_Id',
                                'question_tbl.Que_Ans',
                                'question_tbl.Que_Marks',
                                'question_tbl.Que_Cla_Id',
                                'question_class_tbl.Exam_Title',
                                'question_class_tbl.Exam_Sub_Id',
                                'question_class_tbl.Exam_Subject'
                            )
                            ->where('question_tbl.Que_Cla_Id',$ex->Que_Cla_Id)
                            ->get();

                        $exm_title = ExamTitle::where('exam_status',1)->get();
                        $std_ans = StudentExamAns::where('Que_Cla_Id', $ex->Que_Cla_Id)
                            ->where('Stu_Id',$request->student_id)
                            ->where('Exm_Status',1)
                            ->first();

                        if($std_ans){
                            $decode = json_decode($std_ans->Ques_Ans, true);
                            $result = 0;
                            $stu_marks=0;
                            $total_marks=0;

                            foreach ($questions as $que) {
                                $total_marks += $que->Que_Marks;
                            }
                            if (is_array($decode)) {
                                foreach ($questions as $question) {
                                    foreach ($decode as $key => $value) {
                                        if ($key == $question->Que_Id) {
                                            if ($value == $question->Que_Ans) {
                                                $stu_marks += $que->Que_Marks;
                                                $result++;
                                            }
                                        }
                                    }
                                }

                                $array_exam=array();
                                $exam_title_name = $ex->exam_title."-".$ex->exam_year;

                                $sub_count = 0;
                                $stu_mark = 0;
                                foreach ($questions as $question) {
                                    foreach ($decode as $key => $value) {
                                        if ($key == $question->Que_Id) {
                                            if ($value == $question->Que_Ans) {
                                                $stu_mark += $question->Que_Marks;
                                                $result++;
                                            }
                                        }
                                    }
                                    $exam_array_single = array(
                                        'percent' => round(($stu_mark/$total_marks) * 100, 2),
                                        'sub' => $question->Exam_Subject,
                                    );
                                }

                                array_push($array_exam,  $exam_array_single);
                                // dd('hello');
                                $temp['title'] =  $ex->exam_title."-".$ex->exam_year;

                                array_push($array_exam_per,  $exam_array_single);

                                $temp['subject'] = $array_exam;

                                array_push($rating['exam'], $temp );
                                array_push($exam_title, $exam_title_name);

                            }
                        }
                    }
                    $data_summ = array();

                    $subj = array();
                    foreach ($rating['exam'] as $key=>$value) {

                        $data_summ[$value['title']][] = $value['title'];
                        $data_summ[$value['title']]['subject'][] =  $value['subject'][0];
                        //$data_summ[$value['title']]['subject'][] =  $value['subject'];

                    }

                    $finalArray=array();
                    $tempArray=array();

                    foreach ( $data_summ as $key=>$list ) {
                        $finalArray['title']=$list[0];
                        $finalArray['subject']=$list['subject'];
                        array_push($tempArray, $finalArray);

                    }

                    $rating['exam']=$tempArray;

                    $percentagsss = 0;
                    $exam_subject_number = 0;
                    foreach ($array_exam_per as $key => $value){
                        $percentag =  $value['percent'];
                        $percentagsss += $percentag;
                        $exam_subject_number++;
                    }
                    $total_percentage = ($percentagsss)/$exam_subject_number;

                    $rating['total_percent'] = number_format($total_percentage,2);

                    ResponseMessage::success('Success', $rating);

                }else{
                    ResponseMessage::error("Student not found.");
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

}

