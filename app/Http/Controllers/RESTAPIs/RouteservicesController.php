<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\RouteTbl;
use App\Model\Student;
use App\Model\RouteAssign;
use Auth;
use Input;
use Validator;
use DB;

class RouteservicesController extends Controller
{

	/**
	   * 
	   * In this api to dispaly driver route
	   *
	   * @author Jayesh Sukhadiya 
	   *
	   * @param  user_id = Driver Id
	   *
	   * @return JSON to driver list
	   */
   //Driver Route 
	public function driver_Routes(Request $request)
	{
		try 
		{
			$rules = [
				'user_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{
				if(RouteTbl::where('Rou_Use_Id',Input::get('user_id'))->exists())
				{
					$driver_from = RouteTbl::where('Rou_Use_Id',Input::get('user_id'))->first()->Rou_From;

					$place = $driver_from;
					$place = urlencode($place);
					$url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
					$ch = curl_init();//initiating curl
					curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch,CURLOPT_PROXYPORT,3128);
					curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
					$response = curl_exec($ch);

					$response = json_decode($response);
					$lat = $response->results[0]->geometry->location->lat;
					$lng = $response->results[0]->geometry->location->lng;

					$route_from['from'] = $response->results[0]->geometry->location;

					$driver_to = RouteTbl::where('Rou_Use_Id',Input::get('user_id'))->first()->Rou_To;

					$place = $driver_to;
					$place = urlencode($place);
					$url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
					$ch = curl_init();//initiating curl
					curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch,CURLOPT_PROXYPORT,3128);
					curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
					$response = curl_exec($ch);

					$response = json_decode($response);
					$lat = $response->results[0]->geometry->location->lat;
					$lng = $response->results[0]->geometry->location->lng;

					$route_to['to'] = $response->results[0]->geometry->location;

					$route = array_merge($route_from,$route_to);

					if($place)
					{
						ResponseMessage::success('Route Create Successfully',$route);
					}else{
						ResponseMessage::error('Fail Creation to Route');
					}
				}else{
					ResponseMessage::error('Driver Route Not Assigned');
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
			
		} catch (\Exception $e) {
			Exceptions::exception($e);
			
		}
	}

         /**
	   * 
	   * In this api parent show route his son/daughter
	   *
	   * @author Jayesh Sukhadiya
	   *
	   * @param integer $parent  Parent Id
	   *
	   * @return JSON Display student route 
           * Note : parent_id par thi student id aav che,student_id par thi assign_tbl mathi route id aav che te route id par thi student nu route aav che
	   */


	public function parent_Route(Request $request)
	{
		try 
		{
			$rules = [
				'parent_id' => 'required',
				'student_id' => ''
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Student::where('Std_Parent_Id',Input::get('parent_id'))->exists())
			{
				if(Input::get('student_id')!=""){
			  		$student_id = Input::get('student_id');
				} else{
			  		$student_id = Student::where('Std_Parent_Id',Input::get('parent_id'))->first()->Std_Id;
				}
			 if(RouteAssign::where('Ass_Std_Id',$student_id)->exists())
			{
				$route_assign = RouteAssign::where('Ass_Std_Id',$student_id)->first()->Ass_Rou_Id;

			  $route = RouteTbl::select('Rou_Use_Id','Rou_From','Rou_To')->where('Rou_Id',$route_assign)->first();

		 		$place = $route->Rou_From;
				$place = urlencode($place);
				$url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
				$ch = curl_init();//initiating curl
				curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_PROXYPORT,3128);
				curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
				$response = curl_exec($ch);

				$response = json_decode($response);
				$lat = $response->results[0]->geometry->location->lat;
				$lng = $response->results[0]->geometry->location->lng;

				$route_from['from'] = $response->results[0]->geometry->location;

				$place = $route->Rou_To;
				$place = urlencode($place);
				$url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
				$ch = curl_init();//initiating curl
				curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_PROXYPORT,3128);
				curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
				$response = curl_exec($ch);

				$response = json_decode($response);
				$lat = $response->results[0]->geometry->location->lat;
				$lng = $response->results[0]->geometry->location->lng;

				$route_to['to'] = $response->results[0]->geometry->location;

				$driver_id['driver_id'] = RouteTbl::select('Rou_Use_Id as Driver_Id')->where('Rou_Id',$route_assign)->first();

				$route = array_merge($route_from,$route_to,$driver_id);

				if($place)
				{
					ResponseMessage::success('Parent Route Create Successfully',$route);
				}else{
					ResponseMessage::error('Fail Creation to Route');
				}
			}else{
				ResponseMessage::error("Fail Creation to Route");
			}
			  
			}else{
				ResponseMessage::error("Parent Not Found");
			}
		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	  /**
	   * 
	   * In this api to dispaly all driver route
	   *
	   * @author Jayesh Sukhadiya Date : 28-05-2018
	   *
	   * @param No any parameter reqiired
	   *
	   * @return JSON to routes list 
	   */

	public function teachers_Routes_List(Request $request)
	{
		try
		{
			$rules = [
				'branch_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}
			$route = RouteTbl::where('Rou_Brn_Id',Input::get('branch_id'))->get();

			if($route)
			{
				ResponseMessage::success('Teacher Route List Successfully',$route);
			}else{
				ResponseMessage::error("Record Not Found");
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}
