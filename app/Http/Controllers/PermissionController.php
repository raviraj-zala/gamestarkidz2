<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class PermissionController extends Controller
{
    public function index()
    {
    	if(Auth::user()) {
    		return view('permission.permission');
    	} else {
			return redirect('login');
		}
    }
}
