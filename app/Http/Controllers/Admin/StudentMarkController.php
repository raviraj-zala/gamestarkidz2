<?php

namespace App\Http\Controllers\Admin;

use App\Model\ExamTitle;
use App\Model\StudentExamAns;
use App\Model\StudentMark;
use App\Model\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\Student;
use App\User;
use Auth;
use App\Helper\Notification;
use Illuminate\Support\Carbon;
use Redirect;

class StudentMarkController extends Controller
{
    public function store(Request $request){
        try{
            if(Auth::user()) {
                $edit = StudentMark::where('stu_id',$request->stu_id)->where('que_cla_id',$request->que_cla_id)->first();
                if($edit) {
                    $edit->exam_type = !empty($request->exm_status) ? $request->exm_status : $edit->exam_type;
                    $edit->exam_id = !empty($request->exam_title) ? $request->exam_title : $edit->exam_id;
                    $edit->exam_subject = !empty($request->exam_subject) ? $request->exam_subject : $edit->exam_subject;
                    $edit->que_cla_id = !empty($request->que_cla_id) ? $request->que_cla_id : $edit->que_cla_id;
                    $edit->stu_id = !empty($request->stu_id) ? $request->stu_id : $edit->stu_id;

                    if(!empty($request->submarks)) {
                        $stu_marks = json_encode($request->submarks,JSON_FORCE_OBJECT);
                        $edit->stu_marks = $stu_marks;    
                    } else {
                        $edit->stu_marks = $edit->stu_marks;
                    }
                    $edit->Stu_Mark_UpdatedBy = Auth::user()->Use_Id;
                    $edit->Stu_Mark_UpdatedAt = date('Y-m-d H:i:s');
                    $edit->save();
                } else {

                    $marks = new StudentMark();
                    $marks->exam_type = $request->exm_status;
                    $marks->exam_id = $request->exam_title;
                    $marks->exam_subject = $request->exam_subject;
                    $marks->que_cla_id = $request->que_cla_id;
                    $marks->stu_id = $request->stu_id;

                    $stu_marks = json_encode($request->submarks,JSON_FORCE_OBJECT);
                    $marks->stu_marks = $stu_marks;

                    $marks->Stu_Mark_CreatedAt = date('Y-m-d H:i:s');
                    $marks->Stu_Mark_CreatedBy = Auth::user()->Use_Id;
                    $marks->Stu_Mark_UpdatedBy = Auth::user()->Use_Id;
                    $marks->Stu_Mark_UpdatedAt = date('Y-m-d H:i:s');
                    $marks->save();
                }

                $question_marks = Question::where('que_cla_id',$request->que_cla_id)->pluck('Que_Marks');
                $i=0;
                foreach($question_marks as $old_key => $old_marks) {
                    foreach($request->submarks as $new_key  => $new_marks) {
                        if($request->submarks[$i] > $question_marks[$i]) {
                            return redirect('student_ans_sheet/'.$request->stu_exam_id)->with('error', 'Please enter marks less then equal to '.$question_marks[$i].'.');
                        }
                    }
                    $i++;
                }
                return redirect('Exam Mgmt.');
            } else {
                return redirect('login');
            }
        }catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

}
