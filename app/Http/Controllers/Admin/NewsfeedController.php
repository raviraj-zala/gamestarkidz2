<?php

namespace App\Http\Controllers\Admin;
 
use App\Helper\UserNotification;
use App\Model\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\NewsfeedTagTbl;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\Model\UserLikeBlog;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
//use Auth;
use DB;

class NewsfeedController extends Controller
{
    public function index()
    {
    	try {
            if(Auth::user()) {
        		if(Auth::user()->Use_Type=="1"){
                    $data['newsfeed'] = Newsfeed::leftjoin('branch_tbl','branch_tbl.Brn_Id','=','newsfeed.Nfd_Brn_Id')
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                                ->select('newsfeed.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                                ->orderBy('Nfd_Id','DESC')
                                ->paginate(10);
        		}else if(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $tagList =  Newsfeedtag::whereIn('Nft_Cla_Id',$teacher["AssignClass"])->select('Nft_Nfd_Unique_Id')->groupBy('Nft_Nfd_Unique_Id')->get();
                    $data['newsfeed'] = Newsfeed::
                                whereIn('Nfd_Unique_Id',$tagList)
                                ->where('Nfd_Brn_Id',$teacher['branchAccess'])
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                                ->select('newsfeed.*','user_tbl.Use_Name')
                                ->orderBy('Nfd_Id','DESC')
                                ->paginate(10);
        		}else if(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
        			$data['newsfeed'] = Newsfeed::
                                whereIn('Nfd_Brn_Id',$branch['branchAccess'])
                                ->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','newsfeed.Nfd_Brn_Id')
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                                ->select('newsfeed.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                                ->orderBy('Nfd_Id','DESC')
                                ->paginate(10);
        		}
        		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.newsfeed.index')->with($data);
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function create($branchId = 0)
    {
	
    	try {
            if(Auth::user()) {
                if ($branchId != 0) {
                    $data['classList'] = ClassTbl::where('Cla_Bra_Id', $branchId)->get();
                }
    			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
        		if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status','1')->get();
                    //    			$data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                    				/* $data['getbranch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                                ->select('zone_tbl.*','branch_tbl.*')
                                                ->where('zone_tbl.Zon_Status','=',1)
                                                ->where('branch_tbl.Brn_Status','=',1)
                                                ->orderBy('branch_tbl.Brn_Name')
                                                ->get();*/

                                $data['getbranch'] = Branch::where('Brn_Status','1')->get();
    							    $data['classes'] = Branch::where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                    // $data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
    				  $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
        		}elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['students'] = Student::wherein('Std_Cla_Id',$teacher['AssignClass'])->get();
        			$data['branch'] = Branch::whereIn('Cla_Id',$teacher['AssignClass'])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
        		
    				 $data['getbranch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                ->where('Brn_Id',$teacher['branchAccess'])
                                ->select('zone_tbl.*','branch_tbl.*')
                                ->where('zone_tbl.Zon_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('branch_tbl.Brn_Name')
                                ->get();
    							
    				$branch_id = UserRights::teacher();
                    $data['classes'] = Branch::where('Brn_Id',$branch_id['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();
    				    $moduleAccess = UserRights::rights();
                    $data['module'] = Module::where('Mod_Parent_Id','!=',0)->whereIn("Mod_Id",$moduleAccess)->get();
    			}elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
        			$data['branch'] = Branch::whereIn('Cla_Bra_Id',$branch['branchAccess'])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
    				   $branch = UserRights::branchAdmin();
                    $data['getbranch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                ->whereIn('Brn_Id',$branch["branchAccess"])
                                ->select('zone_tbl.*','branch_tbl.*')
                                ->where('zone_tbl.Zon_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('branch_tbl.Brn_Name')
                                ->get();

                    $branch_ids = UserRights::branchAdmin();
                    $data['classes'] = Branch::whereIn('Brn_Id',$branch_ids['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();
    			}
        		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
        		return view('auth.newsfeed.create')->with($data);
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function save(Request $request)
    {
    	try {
            if(Auth::user()) {
        		$rules = [
        			'title' => 'required',
        			'description' => 'required',
        			'imageOrVideo' => 'max:25600',
        		];
        		$customeMessage = [
        			'title.required' => 'Please enter title',
        			'description.required' => 'Please enter description.',
        			'imageOrVideo.mimes' => 'Invalid image or video type.',
        			'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
        		];
        		$validator = Validator::make($request->all(),$rules,$customeMessage);
              
                if($validator->fails()){
                    return back()->with('errors',$validator->errors())->withInput();
                }else{


                    //                dd($request->all());
                    //            	if($request->hasFile('imageOrVideo'))
                    //            	{
                    //            		$imagesOrVideo = $request->file('imageOrVideo');
                    //            		$image_count = 0;
                    //            		$error_size = false;
                    //            		foreach ($imagesOrVideo as $image)
                    //                	{
                    //                		if($image->getClientSize() > 26214400)
                    //	                    {
                    //	                        $error_size = true;
                    //	                    }
                    //	                    if($image_count >= 50)
                    //	                    {
                    //	                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 50 Image');
                    //	                       break;
                    //	                    }
                    //                	}
                    //                	if($error_size == true)
                    //	                {
                    //	                   return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');
                    //	                }else{
                    //	                	$i = 0;
                    //	                	foreach($imagesOrVideo as $file)
                    //	                	{
                    //	                		$filename = $file->getClientOriginalName();
                    //                        	$extension = $file->getClientOriginalExtension();
                    //                        	$mime = $file->getMimeType();
                    //	                        if(strstr($mime, "video/")){
                    //	                            $mediaType = "2";
                    //	                        }else if(strstr($mime, "image/")){
                    //	                            $mediaType = "1";
                    //	                        }
                    //	                        $picture[] = ['path' => $i.time().".".$extension,
                    //	                                            'type'=>$mediaType];
                    //	                        $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                    //							$i++;
                    //                    	}
                    //                    }
                    //            	}else{
                    //            		$picture = array();
                    //            	}
                	$title = trim($request->title);
                	$description = trim($request->description);
                    if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5"){
                	   $status = trim($request->status);
                    }else if(Auth::user()->Use_Type=="2"){
                        $status = "0";
                    }
                    $imgvdourl = $request->imageOrVideoURL;
                    $user_type = Auth::user()->Use_Type;
                	$tag_type = $request->tag_type;
                	$created_at = date('Y-m-d H:i:s');
                	$created_by = Auth::user()->Use_Id;
                    $unique_id = "NFD".time();
              
                    if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
                    {
                        if($request->has('class_id')){
                            $class_ids = $request->class_id;
                            $branch_ids = ClassTbl::where('Cla_Id',$class_ids)->select('Cla_Bra_Id')->first();

                            foreach ($class_ids as $class) {
                                $newsfeed = New Newsfeedtag;
                                $newsfeed->Nft_Nfd_Unique_Id = $unique_id;
                                $newsfeed->Nft_Cla_Id = $class;
                                $newsfeed->Nft_Brn_Id = $request->branch;
                                $newsfeed->save();
                            }
                        }

                        $class_ids = $request->class_id;
                        $newsfeed = New Newsfeed;
                        $newsfeed->Nfd_Unique_Id = $unique_id;
                        $newsfeed->Nfd_Brn_Id = $request->branch;
                        $newsfeed->Nfd_Title = $title;
                        $newsfeed->Nfd_Description = $description;
                        $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                        $newsfeed->Nfd_Status = $status;
                        $newsfeed->Nfd_User_Type = $user_type;
                        $newsfeed->Nfd_Tag_Type = $tag_type;
                        $newsfeed->Nfd_CreatedAt = $created_at;
                        $newsfeed->Nfd_CreatedBy = $created_by;
    					$newsfeed->Nfd_Brnch_Id = $request->teacherBranch;
    					$newsfeed->Nfd_Cla_Id = $request->brnchclass;
    					$newsfeed->Nfd_Sec_Id = $request->section;
    					
    					$newsfeed->Nfd_Br_UserType = $request->userType;
                        $newsfeed->save();

                        //  if($newsfeed->Nfd_Status=="1"){
                        //      $users = User::where('Use_Status',"1")->get();
                        //      foreach($users as $value){
                        //          $title = "Create";
                        //           $message = $newsfeed->Nfd_Title." New Blog";
                        //           Notification::sendNotification($value["Use_Token"],$title." Blog","Date : ".$created_at."\nMessage : ".$message,"BLOG_ADMIN");
                        //      }
                        //  }

                        if($newsfeed->Nfd_Status=="1"){
                            $users = User::where('Use_Type',2)->where('Use_Status',"1")->get();
                            foreach($users as $value){
                                $title = "Create";
                                $message = $newsfeed->Nfd_Title." New Blog";
                                Notification::sendNotification($value["Use_Token"],$title." Blog","Date : ".$created_at."\nMessage : ".$message,"BLOG_TEACHER");
                                UserNotification::storeNotification($value->Use_Id,"BLOG_TEACHER");

                            }
                        }
                        if($newsfeed->Nfd_Status=="1"){
                            $users = User::where('Use_Type',4)->where('Use_Status',"1")->get();
                            foreach($users as $value){
                                $title = "Create";
                                $message = $newsfeed->Nfd_Title." New Blog";
                                Notification::sendNotification($value["Use_Token"],$title." Blog","Date : ".$created_at."\nMessage : ".$message,"BLOG_PARENT");
                                UserNotification::storeNotification($value->Use_Id,"BLOG_PARENT");

                            }
                        }
                        if($newsfeed->Nfd_Status=="1"){
                            $users = User::where('Use_Type',5)->where('Use_Status',"1")->get();
                            foreach($users as $value){
                                $title = "Create";
                                $message = $newsfeed->Nfd_Title." New Blog";
                                Notification::sendNotification($value["Use_Token"],$title." Blog","Date : ".$created_at."\nMessage : ".$message,"BLOG_ADMIN");
                                UserNotification::storeNotification($value->Use_Id,"BLOG_ADMIN");

                            }
                        }
                            foreach($request->class_id as $key=>$class){
                              
                               $classes_details = \App\Model\ClassTbl::where('Cla_Id',$class)->first()->Cla_Bra_Id;
                            
                            $newsfeedtagtbl= new NewsfeedTagTbl;
                            $newsfeedtagtbl->NfTag_Soa_Id=$classes_details;
                            $newsfeedtagtbl->NfTag_Cla_Id=$class;
                            $newsfeedtagtbl->NfTag_Soa_Unique_Id=$unique_id;
                            $newsfeedtagtbl->save();
                                
                            }
                            
                    }elseif(Auth::user()->Use_Type == "2" ){
                        $teacher = UserRights::teacher();
                        $newsfeed = New Newsfeed;
                        $newsfeed->Nfd_Unique_Id = $unique_id;
                        $newsfeed->Nfd_Brn_Id =$request->branch;
                        $newsfeed->Nfd_Title = $title;
                        $newsfeed->Nfd_Description = $description;
                        $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                        $newsfeed->Nfd_Status = $status;
                        $newsfeed->Nfd_User_Type = $user_type;
                        $newsfeed->Nfd_Tag_Type = $tag_type;
                        $newsfeed->Nfd_CreatedAt = $created_at;
                        $newsfeed->Nfd_CreatedBy = $created_by;
                        $newsfeed->save();
                    }

                    if($request->hasFile('imageOrVideo'))
                    {
                        $imagesOrVideo = $request->file('imageOrVideo');
                        $image_count = 0;
                        $error_size = false;
                        foreach ($imagesOrVideo as $image)
                        {
                            if($image->getClientSize() > 26214400)
                            {
                                $error_size = true;
                            }
                            if($image_count >= 50)
                            {
                                return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 50 Image');
                                break;
                            }
                        }
                        if($error_size == true)
                        {
                            return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');
                        }
                        else
                        {
                            $i = 0;
                            foreach($imagesOrVideo as $file)
                            {
                                $filename = $file->getClientOriginalName();
                                $extension = $file->getClientOriginalExtension();
                                $mime = $file->getMimeType();
                                if(strstr($mime, "video/")){
                                    $mediaType = "2";
                                }else if(strstr($mime, "image/")){
                                    $mediaType = "1";
                                }
                                $picture[] = ['path' => $i.time().".".$extension,
                                    'type'=>$mediaType];
                                $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                                $i++;
                            }
                            if(count($picture)!=0){
                                foreach ($picture as $pic) {
                                    $newsfeedInfo = New Newsfeedinfo;
                                    $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                                    $newsfeedInfo->Nfi_Path = $pic['path'];
                                    $newsfeedInfo->Nfi_Type = $pic['type'];
                                    $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                                    $newsfeedInfo->save();
                                    if($newsfeed->Nfd_Status=="1"){
                                        $users = User::where('Use_Status',"1")->get();
                                        foreach($users as $value){
                                            $title = "Blog";
                                            $message = $newsfeed->Nfd_Title." New Blog";
                                            // Notification::sendNotification($value["Use_Token"],$title,$message);
                                        }
                                    }
                                    // return redirect('Blog Mgmt.');
                                }
                            }
                        }
                    }

                    // if($newsfeed){ //->save()
                    // $nfd_id = $newsfeed->Nfd_Id;
                    //     if(count($picture)!=0){
                    //         foreach ($picture as $pic) {
                    //             $newsfeedInfo = New Newsfeedinfo;
                    //             $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                    //             $newsfeedInfo->Nfi_Path = $pic['path'];
                    //             $newsfeedInfo->Nfi_Type = $pic['type'];
                    //             $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                    //             $newsfeedInfo->save();
                    //             if($newsfeed->Nfd_Status=="1"){
                    //                 $users = User::where('Use_Status',"1")->get();
                    //                 foreach($users as $value){
                    //                     $title = "Blog";
                    //                     $message = $newsfeed->Nfd_Title." New Blog";
                    //                     Notification::sendNotification($value["Use_Token"],$title,$message);
                    //                 }
                    //             }
                    //         return redirect('Blog Mgmt.');
                    //         }
                    //     }
        	    			return redirect('Blog Mgmt.');
                    //            	}else{
                    //            		return back()->withInput();
                    //            	}
    			}
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }

    public function edit($unique_id)
    {
        try {
            if(Auth::user()) {
                $data['newsfeed'] = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
    			
                if($data['newsfeed']->Nfd_User_Type == "2" )
                {
                    $data['created_user_type'] = "teacher";
                    $teacherAssignClass = UserRights::teacherApi($data['newsfeed']->Nfd_CreatedBy,'AssignClass');
                    if($data['newsfeed']->Nfd_Tag_Type == "class"){
                        $data['tag_type'] = "class";
                        $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
                    }else if($data['newsfeed']->Nfd_Tag_Type == "student"){
                        $data['tag_type'] = "student";
                        $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Std_Id')->groupBy('Nft_Std_Id')->get();
                    }
                    
                    $data['branch'] = Branch::whereIn('Cla_Id',$teacherAssignClass)->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                    $data['students'] = Student::wherein('Std_Cla_Id',$teacherAssignClass)->get();
                }elseif($data['newsfeed']->Nfd_User_Type == "5"){
                    $data['created_user_type'] = "branchAdmin";
                    $branchAccess = UserRights::branchAdminApi($data['newsfeed']->Nfd_CreatedBy,'branchAccess');
                    $data['branch'] = Branch::whereIn('Cla_Bra_Id',$branchAccess)->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                    $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
                }else if($data['newsfeed']->Nfd_User_Type == "1"){
                    $data['created_user_type'] = "admin";
                    $data['branch'] = Branch::where('Brn_Status','1')->get();
                    $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
                    $data['sel_section'] = NewsfeedTagTbl::where('NfTag_Soa_Unique_Id',$unique_id)->distinct()->select(['NfTag_Soa_Id'])->get();
                    $data['sel_class'] = NewsfeedTagTbl::where('NfTag_Soa_Unique_Id',$unique_id)->distinct()->select(['NfTag_Cla_Id','NfTag_Soa_Id'])->get();
    				$data['getbranch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                    ->select('zone_tbl.*','branch_tbl.*')
                                    ->where('zone_tbl.Zon_Status','=',1)
                                    ->where('branch_tbl.Brn_Status','=',1)
                                    ->orderBy('zone_tbl.Zon_Id')
                                    ->get();
    				$data['tec_class'] = ClassTbl::where('Cla_Status',1)->join('branch_tbl','branch_tbl.Brn_Id','=','class_tbl.Cla_Bra_Id')->where('Cla_Bra_Id',$data['newsfeed']->Nfd_Brnch_Id)->get();
                   $data['tec_section'] = ClassTbl::where('Cla_Status',1)->where("Cla_Id",$data['newsfeed']->Nfd_Brnch_Id)->get();
    			   $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
                }
                $data['newsfeed_info'] = Newsfeedinfo::where('Nfi_Nfd_Unique_Id',$unique_id)->get();
               
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();

                $data['CURight']=UserRights::rights();
                return view('auth.newsfeed.edit')->with($data);
            } else {
                return redirect('login');
            }
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
    public function update(Request $request)
    {
        try {
            if(Auth::user()) {
                $rules = [
                    'title' => 'required',
                    'description' => 'required',
                    'imageOrVideo' => 'max:25600'
                ];
                $customeMessage = [
                    'title.required' => 'Please enter title',
                    'description.required' => 'Please enter description.',
                    'imageOrVideo.mimes' => 'Invalid image or video type.',
                    'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
                ];
                $validator = Validator::make($request->all(),$rules,$customeMessage);

                if($validator->fails()){
                    return back()->with('errors',$validator->errors())->withInput();
                }else{
                    $unique_id = trim($request->newsfeed_unique_id);
                    $title = trim($request->title);
                    $description = trim($request->description);
                    $status = $request->status;
                    $imgvdourl = $request->imageOrVideoURL;
                    $tag_type = $request->tag_type;
                    $updated_at = date('Y-m-d H:i:s');
                    $updated_by = Auth::user()->Use_Id;

                    $old_newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();

                    $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                    if($newsfeed->Nfd_User_Type == "1" || $newsfeed->Nfd_User_Type == "5")
                    {
    					
                        $existing_branch_ids = Newsfeed::where('Nfd_Unique_Id',$unique_id)->select('Nfd_Brn_Id')->get()->toArray();
                        $existing_ids = array();
                        foreach ($existing_branch_ids as $key => $value) {
                            array_push($existing_ids, $value['Nfd_Brn_Id']);
                        }
                        
    					$temp = explode(",", $request->brnchclass);
                        $cls = end($temp);
                        $claId = ClassTbl::where('Cla_Bra_Id',$request->teacherBranch)->where('Cla_Class',$cls)->where('Cla_Section',$request->section)->first();
                         
    					//echo ClassTbl::where('Cla_Bra_Id',$request->teacherBranch)->where('Cla_Class',$cls)->where('Cla_Section',$request->section)->count();
    					
    					foreach ($existing_ids as $existsKey=>$existsValue) 
                        {
                            $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->where('Nfd_Brn_Id',$existsValue)->first();
                            $newsfeed->Nfd_Title = $title;
                            $newsfeed->Nfd_Description = $description;
                            $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                            $newsfeed->Nfd_Status = $status;
    						$newsfeed->Nfd_Cla_Id = $temp[0];
    						$newsfeed->Nfd_Brn_Id = $request->branch;
    						$newsfeed->Nfd_Br_UserType = $request->userType;
    						$newsfeed->Nfd_Sec_Id = $request->section;
                            $newsfeed->Nfd_UpdatedAt = $updated_at;
                            $newsfeed->Nfd_UpdatedBy = $updated_by;
                            $newsfeed->save();
                        }


                    }elseif($newsfeed->Nfd_User_Type == "2"){
    				
                        $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                        $newsfeed->Nfd_Title = $title;
                        $newsfeed->Nfd_Description = $description;
                        $newsfeed->Nfd_Brn_Id = $request->branch;
                        $newsfeed->Nfd_Status = $status;
                        $newsfeed->Nfd_Img_Vdo_URL = $imgvdourl;
                        $newsfeed->Nfd_Tag_Type = $tag_type;
                        $newsfeed->Nfd_UpdatedAt = $updated_at;
                        $newsfeed->Nfd_UpdatedBy = $updated_by;
                        if($newsfeed->save())
                        {
                            if($newsfeed->Nfd_Status=="1"){
                                $users = User::where('Use_Status',"1")->get();
                                foreach($users as $value){
                                    $title = "Blog";
                                    $message = $newsfeed->Nfd_Title." New Blog";
                                    Notification::sendNotification($value["Use_Token"],$title."Create Blog","Date : ".$date."\nMessage : ".$message,"BLOG_TEACHER");
                                    UserNotification::storeNotification($value->Use_Id,"BLOG_TEACHER");
                                }
                            }
                        }

                    }

                    if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
                    {
                        if($request->has('class_id')){
                            $class_ids = $request->class_id;
                            $old_branch_ids = ClassTbl::where('Cla_Id',$class_ids)->select('Cla_Bra_Id')->first();

                            $oldIds = Newsfeedtag::where('Nft_nfd_Unique_Id',$unique_id)->select('Nft_Id')->get();
                            Newsfeedtag::where('Nft_nfd_Unique_Id',$unique_id)->delete();
                            $i = 0;
                            foreach ($class_ids as $class) {
                                $newsfeed = new Newsfeedtag;
                                // $newsfeed->Nft_Id = $oldIds->Nft_Id[$i];
                                $newsfeed->Nft_Nfd_Unique_Id = $unique_id;
                                $newsfeed->Nft_Cla_Id = $class;
                                $newsfeed->Nft_Brn_Id = $request->branch;
                                $newsfeed->save();
                                $i++;
                            }

                        }


                    }
                    
                    //update the class
                    $data = array();
                     foreach ($request->class_id as $key => $class) {
                             $classes_details = ClassTbl::where("Cla_Id",$class)->first()->Cla_Bra_Id;
                             $unique_id = trim($request->newsfeed_unique_id);
                             $updated_at = date('Y-m-d H:i:s');
                             $data = $classes_details;
                             $newsfeedtagtbl = NewsfeedTagTbl::where("NfTag_Soa_Unique_Id",$unique_id)->first();

                             $newsfeedtagtbl->NfTag_Soa_Id = $classes_details;
                             $newsfeedtagtbl->NfTag_Cla_Id = $class;
                             $newsfeedtagtbl->NfTag_Soa_Unique_Id = $unique_id;
                             $newsfeedtagtbl->NfTag_UpdatedAt = $updated_at;

                             $newsfeedtagtbl->save();
                        }
                    if($request->hasFile('imageOrVideo'))
                    {
                        $imagesOrVideo = $request->file('imageOrVideo');
                        $image_count = 0;
                        $error_size = false;
                        foreach ($imagesOrVideo as $image)
                        {
                            if($image->getClientSize() > 26214400)
                            {
                                $error_size = true;
                            }
                            if($image_count >= 50)
                            {
                               return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 50 Image');
                               break;  
                            }
                        }
                        if($error_size == true)
                        {
                           return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');
                        }
                        else
                        {
                            $i = 0;
                            foreach($imagesOrVideo as $file)
                            {
                                $filename = $file->getClientOriginalName();
                                $extension = $file->getClientOriginalExtension();
                                $mime = $file->getMimeType();
                                if(strstr($mime, "video/")){
                                    $mediaType = "2";
                                }else if(strstr($mime, "image/")){
                                    $mediaType = "1";
                                }
                                $picture[] = ['path' =>  $i.time().".".$extension,
                                                    'type'=>$mediaType];
                                $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                                $i++;
                            }
                            if(count($picture)!=0){
                                foreach ($picture as $pic) {
                                    $newsfeedInfo = New Newsfeedinfo;   
                                    $newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
                                    $newsfeedInfo->Nfi_Path = $pic['path'];
                                    $newsfeedInfo->Nfi_Type = $pic['type'];
                                    $newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
                                    $newsfeedInfo->save();
                                    if($newsfeed->Nfd_Status=="1"){
                                        $users = User::where('Use_Status',"1")->get();
                                        foreach($users as $value){
                                            $title = "Blog";
                                            $message = $newsfeed->Nfd_Title." New Blog";
                                            // Notification::sendNotification($value["Use_Token"],$title,$message);
                                        }
                                    }
                                    // return redirect('Blog Mgmt.');
                                }
                            }
                        }
                    }
    				
                    return redirect('Blog Mgmt.');
                }
            } else {
                return redirect('login');
            }
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }

    public function view($id){
        try {
            if(Auth::user()) {
                if(Auth::user()->Use_Type == "1"){
                    $users = UserLikeBlog::join('user_tbl','user_tbl.Use_Id','blog_like.User_Id')
                                    ->Select('user_tbl.Use_Name','user_tbl.Use_Type')
                                    ->where('Blog_id',$id)
                                    ->orderBy('Blog_id','DESC')
                                    ->paginate(10);
                    foreach($users as $user){
                        if($user->Use_Type==1){
                            $user->Use_Type = 'Admin';
                        }else if($user->Use_Type==2){
                            $user->Use_Type = 'Teacher';                    
                        }else if($user->Use_Type==3){
                            $user->Use_Type = 'Driver';                    
                        }else{
                            $user->Use_Type = 'Parent';                    
                        }
                    }
                }elseif(Auth::user()->Use_Type == "2"){
                    $users = UserLikeBlog::join('user_tbl','user_tbl.Use_Id','blog_like.User_Id')
                                    ->Select('user_tbl.Use_Name','user_tbl.Use_Type')
                                    ->where('Blog_id',$id)
                                    ->orderBy('Blog_id','DESC')
                                    ->paginate(10);
                    foreach($users as $user){
                        if($user->Use_Type==1){
                            $user->Use_Type = 'Admin';
                        }else if($user->Use_Type==2){
                            $user->Use_Type = 'Teacher';                    
                        }else if($user->Use_Type==3){
                            $user->Use_Type = 'Driver';                    
                        }else{
                            $user->Use_Type = 'Parent';                    
                        }
                    }
                }elseif(Auth::user()->Use_Type == "5"){
                   $users = UserLikeBlog::join('user_tbl','user_tbl.Use_Id','blog_like.User_Id')
                                    ->Select('user_tbl.Use_Name','user_tbl.Use_Type')
                                    ->where('Blog_id',$id)
                                    ->orderBy('Blog_id','DESC')
                                    ->paginate(10);
                    foreach($users as $user){
                        if($user->Use_Type==1){
                            $user->Use_Type = 'Admin';
                        }else if($user->Use_Type==2){
                            $user->Use_Type = 'Teacher';                    
                        }else if($user->Use_Type==3){
                            $user->Use_Type = 'Driver';                    
                        }else{
                            $user->Use_Type = 'Parent';                    
                        }
                    }
                }
                $data['users'] = $users;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.newsfeed.user_like_list')->with($data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function tagList($id){
        try {
            if(Auth::user()) {
                $data['i'] = 1;
                //admin user
                $class_id = Newsfeedtag::where('Nft_Nfd_Unique_Id',$id)->select('Nft_Cla_Id')->get()->toArray();
                $class_ids = Newsfeedtag::where('Nft_Nfd_Unique_Id',$id)->select('Nft_Cla_Id')->get();
                $branch_id = Newsfeed::where('Nfd_Unique_Id',$id)->select('Nfd_Brn_Id')->first()->Nfd_Brn_Id;

                $data['classList'] = ClassTbl::whereIn('Cla_Id',$class_id)->paginate(10);
                $data['teacher'] = Users::join('teacher_assign_class_tbl', 'teacher_assign_class_tbl.Tac_Use_Id', 'user_tbl.Use_Id')
                                            ->select('teacher_assign_class_tbl.*','user_tbl.Use_Id','user_tbl.Use_Name')
                                            ->whereIn('Tac_Cla_Id',$class_id)
                                            ->get();

                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();\
                return view('auth.newsfeed.tag_list',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }

    }

    public function activeBlog(Request $request){
        try {
            if(Auth::user()) {
                $count = count($request->ids);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $newsfeed = Newsfeed::where('Nfd_Id',$request->ids[$i])->first();
                        $newsfeed->Nfd_Status = "1";
                        $newsfeed->save();
                        $users = User::where('Use_Status',"1")->get();
                        foreach($users as $value){
                            $title = "Blog";
                            $message = $newsfeed->Nfd_Title." Blog active";
                            Notification::sendNotification($value["Use_Token"],$title,$message);
                            // UserNotification::storeNotification($value->Use_Id,"BLOG_TEACHER");
                        }
                    }
                    return 'active';
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function inActiveBlog(Request $request){
        try {
            if(Auth::user()) {
                $count = count($request->ids);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $newsfeed = Newsfeed::where('Nfd_Id',$request->ids[$i])->first();
                        $newsfeed->Nfd_Status = "2";
                        $newsfeed->save();
                    }
                    return 'in_active';
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function deleteBlog(Request $request){
        try {
            if(Auth::user()) {
                $count = count($request->ids);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $newsfeed = Newsfeed::where('Nfd_Id',$request->ids[$i])->first();
                        $newsfeed->delete();
                    }
                    return 'true';
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function imageVideoremove($id){
        try {
            if(Auth::user()) {
                Newsfeedinfo::where('Nfi_Id',$id)->delete();
                return back();
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

}
