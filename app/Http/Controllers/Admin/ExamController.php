<?php

namespace App\Http\Controllers\Admin;

use App\Model\ExamTitle;
use App\Model\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\StudentExamAns;
use App\Model\Student;
use App\User;
use Auth;
use App\Helper\Notification;
use Illuminate\Support\Carbon;
use App\Model\StudentMark;

class ExamController extends Controller
{
    public function index(){
        try {
            if(Auth::user()) {
                $data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['class'] = QuestionClass::join('branch_tbl','question_class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id')
                    ->select('question_class_tbl.*','branch_tbl.Brn_Name','exam_title_tbl.exam_title')
                    ->orderBy('Que_Cla_CreatedAt', 'DESC')
                    ->paginate(10);
                    
                    
                $data['title'] = ExamTitle::select('*')
                    ->orderBy('Exm_CreatedAt', 'DESC')
                    ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                return view('auth.Exam.index',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create(){
        try{
            if(Auth::user()) {
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['title'] = ExamTitle::where('exam_status',1)->select('exam_year')->distinct()->get();

                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                    $data['subject'] = Subject::orderBy('Sub_Id','DESC')
                        ->leftjoin('branch_tbl','branch_tbl.Brn_Code','=','subject_tbl.Sub_Brn_Code')
                        ->orderBy('Sub_Id','DESC')
                        ->get();
                    $data['title'] = ExamTitle::where('exam_status',1)->select('exam_year')->distinct()->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $branchCode = Branch::whereIn('Brn_Id',$branch["branchAccess"])->select(['Brn_Code'])->get()->toArray();
                    $data['subject'] = Subject::whereIn('Sub_Brn_Code',$branchCode)
                        ->leftjoin('branch_tbl','branch_tbl.Brn_Code','=','subject_tbl.Sub_Brn_Code')
                        ->orderBy('Sub_Id','DESC')
                        ->get();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['title'] = ExamTitle::where('exam_status',1)->select('exam_year')->distinct()->get();
                }else{
                    abort('404', 'Your are Not Authorize to Create an Exam');
                }
                $data['CURight']=UserRights::rights();
                // dd($data);
                return view('auth.Exam.create',$data);
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function edit($id){
        try{
            if(Auth::user()) {
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                
                $data['title'] = ExamTitle::where('exam_status',1)->get();

                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)->first();
                    $data['ques_mod']= Question::where('Que_Cla_Id', $id)->get();
                    $data['class_id'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->orderBy('Cla_Class')->get();
                    for($i=0; $i<count($data['class_id']);$i++){
                        $data['class_id'][$i]['Cla_branchId']=$data['class_mod']->Cla_Bra_Id.",".$data['class_id'][$i]->Cla_Class;
                    }
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)->first();
                    $data['ques_mod']= Question::where('Que_Cla_Id', $id)->get();
                    $data['class_id'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->orderBy('Cla_Class')->get();
                    for($i=0; $i<count($data['class_id']);$i++){
                        $data['class_id'][$i]['Cla_branchId'] = $data['class_mod']->Cla_Bra_Id.",".$data['class_id'][$i]->Cla_Class;
                    }
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)->first();
                    $data['ques_mod']= Question::where('Que_Cla_Id', $id)->get();
                    $data['class_id'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->orderBy('Cla_Class')->get();
                    for($i=0; $i<count($data['class_id']);$i++){
                        $data['class_id'][$i]['Cla_branchId']=$data['class_mod']->Cla_Bra_Id.",".$data['class_id'][$i]->Cla_Class;
                    }
                }
                $data['CURight']=UserRights::rights();
                return view('auth.Exam.edit',$data);
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function store(Request $request){
        try {
            if(Auth::user()) {
                // dd($request->all());
                $questions = $request->que;
                $suquestions = $request->subque;
                $option1 = $request->option1;
                $option2 = $request->option2;
                $option3 = $request->option3;
                $option4 = $request->option4;
                if($request->id1){
                    $answer1 = $request->id1;
                }
                if($request->id2){
                    $answer2 = $request->id2;
                }
                if($request->id3){
                    $answer3 = $request->id3;
                }
                if($request->id4){
                    $answer4 = $request->id4;
                }
                if($request->id!=''){
                    $que_class = QuestionClass::where('Que_Cla_Id', $request->id)->first();
                    $old_status=$que_class->Que_Cla_Status;
                } else {
                    $que_class = new QuestionClass();
                }
                $class = explode(',',$request->class);
                $cl_name = $class[1];
                // $user = User::join('student_tbl','student_tbl.Std_Parent_Id','user_tbl.Use_Id')
                //             ->join('class_tbl','class_tbl.Cla_Class','Std_Cla_Id')
                //             ->where('class_tbl.Cla_Class',$cl_name)
                //             ->
                // $users = User::where('Use_Status',"1")->where('Use_Type',4)->count();
                // dd($users);
                $subject_id_name = explode(',', $request->subject);
                // dd($subject_id_name[1]);
                $que_class->Exam_Title = $request->title;
                $que_class->Exam_Subject = $subject_id_name[0];
                $que_class->Exam_Sub_Id = $subject_id_name[1];
                $que_class->Cla_Bra_Id = $request->branch;
                $que_class->Cla_Id = $request->class;
                $que_class->Exm_Type = $request->exam_type;

                $exam_date =  str_replace('/', '-', $request->date);
                // $que_class->Exm_Date = date('Y-m-d', strtotime($exam_date));
                $que_class->Exm_Date =  Carbon::parse($request->date)->format('Y-m-d H:i:s');
                $exam_due_date =  str_replace('/', '-', $request->duedate);
                // $que_class->Exm_Due_Date = date('Y-m-d', strtotime($exam_due_date));
                $que_class->Exm_Due_Date =  Carbon::parse($request->duedate)->format('Y-m-d H:i:s');
                $que_class->Exm_Time = $request->time;

                $que_class->Cla_Sec_Id = $request->section;
                $que_class->Que_Cla_Status = $request->status;
                $que_class->Que_Cla_CreatedBy = Auth::user()->Use_Id;
                $que_class->Que_Cla_CreatedAt = date('Y-m-d H:i:s');
                $que_class->Que_Cla_UpdatedBy = Auth::user()->Use_Id;
                $que_class->Que_Cla_UpdatedAt = date('Y-m-d H:i:s');
                // dd($que_class);
                if($que_class->save())
                {
                    // dd($request->id);
                    // if($request->id){
                        // $ques_ans = Question::where('Que_Cla_Id', $que_class->Que_Cla_Id)->get();
                        // foreach($ques_ans as $queAns){
                            // $queAns->delete();
                        // }
                    // }
                    if($request->exam_type == 1){
                        // dd('he');
                        for($i=0;$i<count($questions);$i++){
                            $que = new Question();
                            $que->Que_Cla_Id = $que_class->Que_Cla_Id;
                            $que->Que_Question = $questions[$i];
                            $que->Que_Marks =  $request->objmarks[$i];
                            $que->Que_Option_1 = $option1[$i];
                            $que->Que_Option_2 = $option2[$i];
                            $que->Que_Option_3 = $option3[$i];
                            $que->Que_Option_4 = $option4[$i];
                            if($request->id1){
                                foreach ($answer1 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id2){
                                foreach ($answer2 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id3){
                                foreach ($answer3 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id4){
                                foreach ($answer4 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            $que->Que_CreatedAt = date('Y-m-d H:i:s');
                            $que->Que_UpdatedAt = date('Y-m-d H:i:s');
                            $que->save();
                        }
                    } elseif($request->exam_type == 0){
                        // dd('hello');
                        for($i=0;$i<count($suquestions);$i++){
                            $que = new Question();

                            $que->Que_Cla_Id = $que_class->Que_Cla_Id;
                            $que->Que_Question = $suquestions[$i];
                            $que->Que_Marks =  $request->submarks[$i];


                            $que->Que_CreatedAt = date('Y-m-d H:i:s');
                            $que->Que_UpdatedAt = date('Y-m-d H:i:s');
                            $que->save();
                        }
                    }

                    return redirect('Exam Mgmt.');
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


    public function update(Request $request){
        try {
            if(Auth::user()) {
                // dd($request->date);
                $questions = $request->que;
                $suquestions = $request->subque;
                $option1 = $request->option1;
                $option2 = $request->option2;
                $option3 = $request->option3;
                $option4 = $request->option4;

                if($request->id1){
                    $answer1 = $request->id1;
                }
                if($request->id2){
                    $answer2 = $request->id2;
                }
                if($request->id3){
                    $answer3 = $request->id3;
                }
                if($request->id4){
                    $answer4 = $request->id4;
                }
                if($request->id!=''){
                    $que_class = QuestionClass::where('Que_Cla_Id', $request->id)->first();
                    $old_status=$que_class->Que_Cla_Status;
                } else {
                    $que_class = new QuestionClass();
                }
                $class = explode(',',$request->class);
                $cl_name = $class[1];
                // $user = User::join('student_tbl','student_tbl.Std_Parent_Id','user_tbl.Use_Id')
                //             ->join('class_tbl','class_tbl.Cla_Class','Std_Cla_Id')
                //             ->where('class_tbl.Cla_Class',$cl_name)
                //             ->
                // $users = User::where('Use_Status',"1")->where('Use_Type',4)->count();
                // dd($users);

                $subject_id_name = explode(',', $request->subject);
                // dd($subject_id_name[1]);
                $que_class->Exam_Title = $request->title;
                $que_class->Exam_Subject = $subject_id_name[0];
                $que_class->Exam_Sub_Id = $subject_id_name[1];

                $que_class->Cla_Bra_Id = $request->branch;
                $que_class->Cla_Id = $request->class;
                $que_class->Exm_Type = $request->exam_type;

                // $exam_date =  str_replace('/', '-', $request->);
                // $que_class->Exm_Date = date('Y-m-d', strtotime($exam_date));
                $que_class->Exm_Date = Carbon::parse($request->date)->format('Y-m-d H:i:s');
                $exam_due_date =  str_replace('/', '-', $request->duedate);
                // $que_class->Exm_Due_Date = date('Y-m-d', strtotime($exam_due_date));
                $que_class->Exm_Due_Date = Carbon::parse($request->duedate)->format('Y-m-d H:i:s');
                $que_class->Exm_Time = $request->time;

                $que_class->Cla_Sec_Id = $request->section;
                $que_class->Que_Cla_Status = $request->status;
                $que_class->Que_Cla_CreatedBy = Auth::user()->Use_Id;
                $que_class->Que_Cla_CreatedAt = date('Y-m-d H:i:s');
                $que_class->Que_Cla_UpdatedBy = Auth::user()->Use_Id;
                $que_class->Que_Cla_UpdatedAt = date('Y-m-d H:i:s');
                // dd($que_class);
                if($que_class->save())
                {
                    // dd($request->id);
                    if($request->id){
                        $ques_ans = Question::where('Que_Cla_Id', $que_class->Que_Cla_Id)->get();
                        foreach($ques_ans as $queAns){
                            $queAns->delete();
                        }
                    }

                    if($request->exam_type == 1){
                        // dd('he');
                        for($i=0;$i<count($questions);$i++){
                            $que = new Question();
                            $que->Que_Cla_Id = $que_class->Que_Cla_Id;
                            $que->Que_Question = $questions[$i];
                            $que->Que_Marks =  $request->objmarks[$i];

                            $que->Que_Option_1 = $option1[$i];
                            $que->Que_Option_2 = $option2[$i];
                            $que->Que_Option_3 = $option3[$i];
                            $que->Que_Option_4 = $option4[$i];
                            if($request->id1){
                                foreach ($answer1 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id2){
                                foreach ($answer2 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id3){
                                foreach ($answer3 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            if($request->id4){
                                foreach ($answer4 as $ans) {
                                    $str = explode('_', $ans);
                                    if($str[1]==$i){
                                        $que->Que_Ans =$str[0];
                                    }
                                }
                            }
                            $que->Que_CreatedAt = date('Y-m-d H:i:s');
                            $que->Que_UpdatedAt = date('Y-m-d H:i:s');
                            $que->save();
                        }
                    } elseif($request->exam_type == 0){
                        // dd('hello');
                        for($i=0;$i<count($suquestions);$i++){
                            $que = new Question();

                            $que->Que_Cla_Id = $que_class->Que_Cla_Id;
                            $que->Que_Question = $suquestions[$i];
                            $que->Que_Marks =  $request->submarks[$i];


                            $que->Que_CreatedAt = date('Y-m-d H:i:s');
                            $que->Que_UpdatedAt = date('Y-m-d H:i:s');
                            $que->save();
                        }
                    }
                    return redirect('Exam Mgmt.');
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function view($id){
        try {
            if(Auth::user()) {
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)
                        ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                        ->first();
                    $class = explode(',',$data['class_mod']->Cla_Id);
                    $data['exam'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->where('Cla_Section',$data['class_mod']->Cla_Sec_Id)->where('Cla_Class', $class[1])->first();
                    $ques = Question::where('Que_Cla_Id', $id)->get();
                    $branch = Branch::where('Brn_Id',$data['class_mod']->Cla_Bra_Id)->first();
                    $data['class_mod']['branch']=$branch->Brn_Name;
                    $data['class_mod']['class']=$class[1];
                    $data['students']=StudentExamAns::where('stu_exam_ans_tbl.Que_Cla_Id',$data['class_mod']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach($data['students'] as $student){
                        $student->stu_marks=StudentMark::where('Stu_Id',$student->Stu_Id)->where('Que_Cla_Id',$student->Que_Cla_Id)->pluck('stu_marks')->first();
                        $stu= Student::where('Std_Id', $student->Stu_Id)->first();
                        $encode = json_decode($student->Ques_Ans,true);
                        $answer_encode = json_decode($student->stu_marks,true);
                        $data['students'][$i]->Std_Gr_No = $stu->Std_Gr_No;
                        $data['students'][$i]->Std_Name = $stu->Std_Name;
                        $data['students'][$i]->attempt = count($encode);
                        $data['students'][$i]->total_ques = count($ques);
                        $correct_ans=0;
                        $stu_marks=0;
                        $total_marks=0;

                        foreach ($ques as $que) {
                            $total_marks += $que->Que_Marks;
                        }
                        foreach($encode as $key=>$value){
                            foreach ($ques as $que) {
                                if($key==$que->Que_Id){
                                    if($que->Que_Ans==$value){
                                        $stu_marks += $que->Que_Marks;
                                        $correct_ans++;
                                    }
                                }
                            }
                        }
                        
                        $data['students'][$i]->correct_ans = $correct_ans;
                        if(!empty($stu_marks)) {
                            $data['students'][$i]->stu_marks = $stu_marks;
                        } else {
                            $data['students'][$i]->stu_marks = array_sum($answer_encode);
                        }
                        $data['students'][$i]->total_marks = $total_marks;
                        $i++;
                    }
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)
                        ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                        ->first();
                    $class = explode(',',$data['class_mod']->Cla_Id);
                    $data['exam'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->where('Cla_Section',$data['class_mod']->Cla_Sec_Id)->where('Cla_Class', $class[1])->first();
                    $ques = Question::where('Que_Cla_Id', $id)->get();
                    $branch = Branch::where('Brn_Id',$data['class_mod']->Cla_Bra_Id)->first();
                    $data['class_mod']['branch']=$branch->Brn_Name;
                    $data['class_mod']['class']=$class[1];
                    $data['students']=StudentExamAns::where('stu_exam_ans_tbl.Que_Cla_Id',$data['class_mod']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach($data['students'] as $student){
                        $student->stu_marks=StudentMark::where('Stu_Id',$student->Stu_Id)->where('Que_Cla_Id',$student->Que_Cla_Id)->pluck('stu_marks')->first();
                        $stu= Student::where('Std_Id', $student->Stu_Id)->first();
                        $encode = json_decode($student->Ques_Ans,true);
                        $answer_encode = json_decode($student->stu_marks,true);
                        $data['students'][$i]->Std_Gr_No = $stu->Std_Gr_No;
                        $data['students'][$i]->Std_Name = $stu->Std_Name;
                        $data['students'][$i]->attempt = count($encode);
                        $data['students'][$i]->total_ques = count($ques);
                        $correct_ans=0;
                        $stu_marks=0;
                        $total_marks=0;

                        foreach ($ques as $que) {
                            $total_marks += $que->Que_Marks;
                        }
                        foreach($encode as $key=>$value){
                            foreach ($ques as $que) {
                                if($key==$que->Que_Id){
                                    if($que->Que_Ans==$value){
                                        $stu_marks += $que->Que_Marks;
                                        $correct_ans++;
                                    }
                                }
                            }
                        }
                        
                        $data['students'][$i]->correct_ans = $correct_ans;
                        if(!empty($stu_marks)) {
                            $data['students'][$i]->stu_marks = $stu_marks;
                        } else {
                            $data['students'][$i]->stu_marks = array_sum($answer_encode);
                        }
                        $data['students'][$i]->total_marks = $total_marks;
                        $i++;
                    }
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['class_mod'] = QuestionClass::where('Que_Cla_Id', $id)
                        ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                        ->first();
                    $class = explode(',',$data['class_mod']->Cla_Id);
                    $data['exam'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['class_mod']->Cla_Bra_Id)->where('Cla_Section',$data['class_mod']->Cla_Sec_Id)->where('Cla_Class', $class[1])->first();
                    $ques = Question::where('Que_Cla_Id', $id)->get();
                    $branch = Branch::where('Brn_Id',$data['class_mod']->Cla_Bra_Id)->first();
                    $data['class_mod']['branch']=$branch->Brn_Name;
                    $data['class_mod']['class']=$class[1];
                    $data['students']=StudentExamAns::where('stu_exam_ans_tbl.Que_Cla_Id',$data['class_mod']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach($data['students'] as $student){
                        $student->stu_marks=StudentMark::where('Stu_Id',$student->Stu_Id)->where('Que_Cla_Id',$student->Que_Cla_Id)->pluck('stu_marks')->first();
                        $stu= Student::where('Std_Id', $student->Stu_Id)->first();
                        $encode = json_decode($student->Ques_Ans,true);
                        $answer_encode = json_decode($student->stu_marks,true);
                        $data['students'][$i]->Std_Gr_No = $stu->Std_Gr_No;
                        $data['students'][$i]->Std_Name = $stu->Std_Name;
                        $data['students'][$i]->attempt = count($encode);
                        $data['students'][$i]->total_ques = count($ques);
                        $correct_ans=0;
                        $stu_marks=0;
                        $total_marks=0;

                        foreach ($ques as $que) {
                            $total_marks += $que->Que_Marks;
                        }
                        foreach($encode as $key=>$value){
                            foreach ($ques as $que) {
                                if($key==$que->Que_Id){
                                    if($que->Que_Ans==$value){
                                        $stu_marks += $que->Que_Marks;
                                        $correct_ans++;
                                    }
                                }
                            }
                        }
                        
                        $data['students'][$i]->correct_ans = $correct_ans;
                        if(!empty($stu_marks)) {
                            $data['students'][$i]->stu_marks = $stu_marks;
                        } else {
                            $data['students'][$i]->stu_marks = array_sum($answer_encode);
                        }
                        $data['students'][$i]->total_marks = $total_marks;
                        $i++;
                    }
                }
                $data['CURight']=UserRights::rights();
               // dd($data['students']);
                return view('auth.Exam.view',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function AnswerSheet($id){
        try {
            if(Auth::user()) {
                // dd($id);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                    $data['student']=StudentExamAns::join('student_tbl','student_tbl.Std_Id','stu_exam_ans_tbl.Stu_Id')
                        ->join('question_class_tbl', 'question_class_tbl.Que_Cla_Id','stu_exam_ans_tbl.Que_Cla_Id')
                        ->select('stu_exam_ans_tbl.*','student_tbl.*','question_class_tbl.Exam_Title','question_class_tbl.Exam_Subject', 'question_class_tbl.Exm_Type','question_class_tbl.Cla_Id','question_class_tbl.Cla_Sec_Id')
                        ->where('Stu_Exam_Id',$id)
                        ->first();
                    $class = explode(',', $data['student']->Cla_Id);
                    $data['student']->Cla_Id=$class[1];
                    $Stu_Id = $data['student']->Stu_Id;
                    $Que_Cla_Id = $data['student']->Que_Cla_Id;
                    $data['student']['stu_marks']=StudentMark::where('Stu_Id',$Stu_Id)->where('Que_Cla_Id',$Que_Cla_Id)->pluck('stu_marks')->first();
                    $decode = json_decode($data['student']->Ques_Ans,true);
                    $decode_marks = json_decode($data['student']->stu_marks,true);
                    $ques= Question::where('Que_Cla_Id', $data['student']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach ($ques as $que) {
                        foreach ($decode as $key => $value) {
                            if($key == $que->Que_Id) {
                                $ques[$i]['Std_Ans']=$value;
                            }
                        }
                        if(!empty($decode_marks)) {
                            foreach ($decode_marks as $key => $value) {
                                if($key == $i) {
                                    $ques[$i]['Std_Disc_Ans']=$value;
                                }
                            }
                        } else {
                            $ques[$i]['Std_Disc_Ans']=0;
                        }
                        $i++;
                    }
                    $data['ques_mod']=$ques;
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['student']=StudentExamAns::join('student_tbl','student_tbl.Std_Id','stu_exam_ans_tbl.Stu_Id')
                        ->join('question_class_tbl', 'question_class_tbl.Que_Cla_Id','stu_exam_ans_tbl.Que_Cla_Id')
                        ->select('stu_exam_ans_tbl.*','student_tbl.Std_Name','question_class_tbl.Exam_Title','question_class_tbl.Exam_Subject', 'question_class_tbl.Exm_Type', 'question_class_tbl.Cla_Id','question_class_tbl.Cla_Sec_Id')
                        ->where('Stu_Exam_Id',$id)
                        ->first();
                    $class = explode(',', $data['student']->Cla_Id);
                    $data['student']->Cla_Id=$class[1];
                    $Stu_Id = $data['student']->Stu_Id;
                    $Que_Cla_Id = $data['student']->Que_Cla_Id;
                    $data['student']['stu_marks']=StudentMark::where('Stu_Id',$Stu_Id)->where('Que_Cla_Id',$Que_Cla_Id)->pluck('stu_marks')->first();
                    $decode = json_decode($data['student']->Ques_Ans,true);
                    $decode_marks = json_decode($data['student']->stu_marks,true);
                    $ques= Question::where('Que_Cla_Id', $data['student']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach ($ques as $que) {
                        foreach ($decode as $key => $value) {
                            if($key == $que->Que_Id) {
                                $ques[$i]['Std_Ans']=$value;
                            }
                        }
                        if(!empty($decode_marks)) {
                            foreach ($decode_marks as $key => $value) {
                                if($key == $i) {
                                    $ques[$i]['Std_Disc_Ans']=$value;
                                }
                            }
                        } else {
                            $ques[$i]['Std_Disc_Ans']=0;
                        }
                        $i++;
                    }
                    $data['ques_mod']=$ques;
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                    $data['student']=StudentExamAns::join('student_tbl','student_tbl.Std_Id','stu_exam_ans_tbl.Stu_Id')
                        ->join('question_class_tbl', 'question_class_tbl.Que_Cla_Id','stu_exam_ans_tbl.Que_Cla_Id')
                        ->select('stu_exam_ans_tbl.*','student_tbl.Std_Name','question_class_tbl.Exam_Title','question_class_tbl.Exam_Subject', 'question_class_tbl.Exm_Type', 'question_class_tbl.Cla_Id','question_class_tbl.Cla_Sec_Id')
                        ->where('Stu_Exam_Id',$id)
                        ->first();
                    $class = explode(',', $data['student']->Cla_Id);
                    $data['student']->Cla_Id=$class[1];
                    $Stu_Id = $data['student']->Stu_Id;
                    $Que_Cla_Id = $data['student']->Que_Cla_Id;
                    $data['student']['stu_marks']=StudentMark::where('Stu_Id',$Stu_Id)->where('Que_Cla_Id',$Que_Cla_Id)->pluck('stu_marks')->first();
                    $decode = json_decode($data['student']->Ques_Ans,true);
                    $decode_marks = json_decode($data['student']->stu_marks,true);
                    $ques= Question::where('Que_Cla_Id', $data['student']->Que_Cla_Id)
                        ->get();
                    $i=0;
                    foreach ($ques as $que) {
                        foreach ($decode as $key => $value) {
                            if($key == $que->Que_Id) {
                                $ques[$i]['Std_Ans']=$value;
                            }
                        }
                        if(!empty($decode_marks)) {
                            foreach ($decode_marks as $key => $value) {
                                if($key == $i) {
                                    $ques[$i]['Std_Disc_Ans']=$value;
                                }
                            }
                        } else {
                            $ques[$i]['Std_Disc_Ans']=0;
                        }
                        $i++;
                    }
                    $data['ques_mod']=$ques;
                }
                $data['CURight']=UserRights::rights();
                return view('auth.Exam.ans_sheet',$data);
            } else {
                return redirect('login');
            }             
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function deleteQuestion(Request $request){
        try {
            if(Auth::user()) {
                $count = count($request->Cls_Id);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $class_mod = QuestionClass::where('Que_Cla_Id', $request->Cls_Id[$i])->first();
                        $questions = Question::where('Que_Cla_Id',$request->Cls_Id[$i])->get();
                        for($j=0;$j<count($questions);$j++){
                            $question = Question::where('Que_Id', $questions[$j]->Que_Id)->first();
                            $question->delete();
                        }
                        $class_mod->delete();
                    }
                    return 'true';
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
