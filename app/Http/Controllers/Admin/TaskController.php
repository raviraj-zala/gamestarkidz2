<?php

namespace App\Http\Controllers\Admin;

use App\Helper\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Task;
use App\Model\Module;
use App\Model\Users;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\TaskNotes;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use Validator;
use App\Helper\ResponseMessage;
use Auth;
use App\User;
use App\Helper\Notification;

class TaskController extends Controller
{
    public function index(){
    	try {
            if(Auth::user()) {
        		$data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['task'] = Task::join('branch_tbl','task_tbl.Tas_Branch','=','branch_tbl.Brn_Id')
                				->join('class_tbl','task_tbl.Tas_Class','=','class_tbl.Cla_Id')
                				->join('user_tbl','task_tbl.Tas_Teacher','=','user_tbl.Use_Id')
                				->select('task_tbl.*','branch_tbl.Brn_Name', 'class_tbl.Cla_Class','user_tbl.Use_Name')
                				->orderBy('Tas_Id', 'DESC')
                                ->paginate(10);              
        		return view('auth.Task.index',$data);
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function create(){
    	try {
            if(Auth::user()) {
        		$data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                }
        		return view('auth.Task.create',$data);
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function edit($id){
    	try {
            if(Auth::user()) {
        		$data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['task'] = Task::where('Tas_id',$id)->first();
                $data['class'] = ClassTbl::Select('Cla_Id', 'Cla_Class')->where('Cla_Bra_Id',$data['task']->Tas_Branch)->get();
                $data['teacher'] = Users::join('teacher_assign_class_tbl','teacher_assign_class_tbl.Tac_Use_Id','user_tbl.Use_Id')->where('teacher_assign_class_tbl.Tac_Brn_Id',$data['task']->Tas_Branch)->where('Use_Type',2)->where('Use_Status',1)->distinct()->get(['Use_Id','Use_Name']);
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                }
        		return view('auth.Task.create',$data);
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function store(Request $request){
    	try {
            if(Auth::user()) {
                date_default_timezone_set('Asia/Kolkata');
        		if(isset($request->id)){
        			$task = Task::where('Tas_Id', $request->id)->first();
        		}else {
        			$task = New Task();
        		}

                if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5"){

                    $task->Tas_Title = $request->task;
                    $task->Tas_Branch = $request->branch;
                    $task->Tas_Class = $request->class;
                    $task->Tas_Teacher = $request->teacher;
                    $task->Tas_Status = $request->status;
                    $task_date =  str_replace('/', '-', $request->date);
                    $task->Tas_Date = date('Y-m-d', strtotime($task_date));
                    $task_due_date =  str_replace('/', '-', $request->duedate);
                    $task->Tas_Due_Date = date('Y-m-d', strtotime($task_due_date));
                    $task->Tas_Time = $request->time;
                    $task->Tas_Description = $request->description;
                    $task->Tas_Note = $request->notes;
                    $task->Tas_CreatedBy = Auth::user()->Use_Id;
                    $task->Tas_CreatedAt = date('Y-m-d H:i:s');
                    $task->Tas_UpdatedBy = Auth::user()->Use_Id;
                    $task->Tas_UpdatedAt = date('Y-m-d H:i:s');
                    if($task->save()){

                        if(isset($request->id)){
                            $title = "Task Updated ";
                            $message = "Task Updated and Assign by ".ucfirst(Auth::user()->Use_Name);

                            $task = Task::where('Tas_Id', $request->id)->first()->Tas_CreatedBy;
                            $value = User::where('Use_Id',$task)->first();
                            Notification::sendNotification($value["Use_Token"],$title.".",$message." ", "TASK_ADMIN");
                            UserNotification::storeNotification($value->Use_Id,"TASK_ADMIN");
                        }else{
                            $title = "Task Add";
                            $message = "Task Added and Assign by ".ucfirst(Auth::user()->Use_Name);
                            $value = User::where('Use_Id',$request->teacher)->first();
                            Notification::sendNotification($value["Use_Token"],$title.".",$message." ", "TASK_TEACHER");
                            UserNotification::storeNotification($value->Use_Id,"TASK_TEACHER");
                        }

                        return redirect('Task Mgmt.');
                    }else{
                        return back();
                    }

                }
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function notesTask($id){
    	try {
            if(Auth::user()) {
        		$data['notes'] = TaskNotes::join('user_tbl','user_tbl.Use_Id','task_notes_tbl.Assignee')->select('task_notes_tbl.*','user_tbl.Use_Name')->where('Tas_Id', $id)->get();
        		$data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['task'] = Task::join('class_tbl','class_tbl.Cla_Id','task_tbl.Tas_Class')
                					->join('branch_tbl','branch_tbl.Brn_Id','task_tbl.Tas_Branch')
                					->join('user_tbl','user_tbl.Use_Id','task_tbl.Tas_Teacher')
                					->Select('task_tbl.*','class_tbl.cla_Class','branch_tbl.Brn_Name','user_tbl.Use_Name')
                					->where('Tas_id',$id)->first();
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                }
        		return view('auth.Task.notes',$data);
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function deleteTask(Request $request){
    	try {
            if(Auth::user()) {
                $count = count($request->ids);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $task = Task::where('Tas_Id',$request->ids[$i])->first();
                        $task->delete();
                    }
                    return 'true';
                }
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function createNotes(Request $request){
    	try {
            if(Auth::user()) {
        		$rules = [
        			'note' => 'required',
        		];
        		$customeMessage = [
        			'note.required' => 'Please Enter Note',
        		];
        		$validator = Validator::make($request->all(),$rules, $customeMessage);

                if( $validator->fails() ) {
    	            return back()->withInput()->withErrors($validator->errors());
    	        } else {
    	            $note = new TaskNotes();
    	            $note->Tas_Id = $request->id;
    	            $note->Note = $request->note;
    	            $note->Assignee = Auth::user()->Use_Id;
    	            if($note->save()){
    	            	return redirect()->back();
    	            }
    	        }
            } else {
                return redirect('login');
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function deleteNotes(Request $request){
    	try {
    		$count = count($request->ids);
            if($count>0){
                for($i=0;$i<$count;$i++){
                    $task = TaskNotes::where('Tas_Not_Id',$request->ids[$i])->first();
                    $task->delete();
                }
                return 'true';
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function teacher(Request $request){
    	try {
    		// return $request->Cla_Id;
            $teacher = Users::join('teacher_assign_class_tbl','teacher_assign_class_tbl.Tac_Use_Id','user_tbl.Use_Id')
                            ->where('teacher_assign_class_tbl.Tac_Brn_Id',$request->Branch_Id)
                            ->where('Use_Type',2)
                            ->where('Use_Status',1)
                            ->distinct()
                            ->get(['Use_Id','Use_Name']);
            return $teacher;
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

}
