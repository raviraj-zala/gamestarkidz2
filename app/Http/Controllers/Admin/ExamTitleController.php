<?php

namespace App\Http\Controllers\Admin;

use App\Model\ExamTitle;
use App\Model\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Question;
use App\Model\QuestionClass;
use App\Helper\Exceptions;
use App\Model\Branch;
use App\Model\StudentExamAns;
use App\Model\Student;
use App\User;
use Auth;
use App\Helper\Notification;
use Illuminate\Support\Carbon;

class ExamTitleController extends Controller
{

    public function index(){
        try {
            if(Auth::user()) {
                $data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['class'] = QuestionClass::join('branch_tbl','question_class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                    ->leftjoin('exam_title_tbl','question_class_tbl.Exam_Title', '=', 'exam_title_tbl.exam_id' )
                    ->select('question_class_tbl.*','branch_tbl.Brn_Name','exam_title_tbl.exam_title')
                    ->orderBy('Que_Cla_CreatedAt', 'DESC')
                    ->paginate(10);

                $data['title'] = ExamTitle::select('*')
                    ->orderBy('Exm_CreatedAt', 'DESC')
                    ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                return view('auth.Exam.indextitle',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }

    }

    public function examTitle(Request $request){
        try {
            if(Auth::user()) {
                $data = ExamTitle::where('exam_year',$request->exam_year)->where('exam_status',1)->get();
                return $data;
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }

    }

    public function titleCreate(){
        try {
            if(Auth::user()) {
                $data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                }
                return view('auth.Exam.titleCreate',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }



    public function store(Request $request){
        try {
            if(Auth::user()) {
                $title = new ExamTitle();
                // $title->branch_id = $request->branch;
                // $title->class_id = $request->class;
                $title->exam_title = $request->title;
                $title->exam_year = $request->date;
                $title->exam_status = $request->status;

                $title->Exam_CreatedBy = Auth::user()->Use_Id;
                $title->Exm_CreatedAt = date('Y-m-d H:i:s');
                $title->Exam_UpdatedBy = Auth::user()->Use_Id;
                $title->Exm_UpdatedAt = date('Y-m-d H:i:s');
                $title->save();
                return redirect('Exam Title Mgmt.');
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function titleEdit($id){
        try {
            if(Auth::user()) {
                $data['i'] = 1;
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                $data['title'] = ExamTitle::where('exam_id',$id)->first();
                $data['class_id'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['title']->class_id)->orderBy('Cla_Class')->get();

                if(Auth::user()->Use_Type == "1"){
                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "2"){
                    $teacher = UserRights::teacher();
                    $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
                }elseif(Auth::user()->Use_Type == "5"){
                    $branch = UserRights::branchAdmin();
                    $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                }
                return view('auth.Exam.titleEdit',$data);
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function update(Request $request){
        try {
            if(Auth::user()) {
                $oldtitle = ExamTitle::where('exam_id',$request->id)->first()->exam_id;
                ExamTitle::where('exam_id', $request->id)->delete();

                $title = new ExamTitle();
                $title->exam_id = $oldtitle;
                // $title->branch_id = $request->branch;
                // $title->class_id = $request->class;
                $title->exam_title = $request->title;
                $title->exam_year = $request->date;
                $title->exam_status = $request->status;

                $title->Exam_CreatedBy = Auth::user()->Use_Id;
                $title->Exm_CreatedAt = date('Y-m-d H:i:s');
                $title->Exam_UpdatedBy = Auth::user()->Use_Id;
                $title->Exm_UpdatedAt = date('Y-m-d H:i:s');
                $title->save();
                return redirect('Exam Title Mgmt.');
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function deleteExam(Request $request){

        try {
            if(Auth::user()) {
                $count = count($request->Cls_Id);
                if($count>0){
                    for($i=0;$i<$count;$i++){
                        $class_mod = ExamTitle::where('exam_id', $request->Cls_Id[$i])->first();
                        $class_mod->delete();
                    }
                    return 'true';
                }
            } else {
                return redirect('login');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }


}
