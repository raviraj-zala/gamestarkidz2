<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helper\Exceptions;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Zone;
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Helper\Ajax;
use App\User;
use Auth;
use App\Model\Student;
use URL;

class StudentController extends Controller
{
    public $rules = [
            'branch' =>  'required',
            'studentname'   =>  'required|regex:/^[A-Za-z ]+$/',
            'parentname'    =>  'required|regex:/^[A-Za-z ]+$/',
            'status'    =>  'required',
            'studentfee' => 'required|numeric'
        ];
    public function index(){
        try{
            if(Auth::user()) {
                $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name')
                                ->orderBy('Std_Id','DESC')
                                ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.student.index',$data);
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function create(){
    	try{
            if(Auth::user()) {
        		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
        		$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
    			$data['CURight']=UserRights::rights();
    			return view('auth.student.create',$data);
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }
    public function search(Request $request)
    {
        try {
            if(Auth::user()) {
                return view('auth.student.search',Ajax::searchStudent($request));
            } else {
                return redirect('login');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function edit($id){
        try{
            if(Auth::user()) {
                $classId = Student::where('Std_Id',$id)->first();
                $data['branch'] = Branch::where('Brn_Status',1)->get();

                $branchId = ClassTbl::where('Cla_Id',$classId->Std_Cla_Id)->first();

                $data['std_class'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$branchId->Cla_Bra_Id)->get();

                $data['std_section'] = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$branchId->Cla_Class)->where('Cla_Bra_Id',$branchId->Cla_Bra_Id)->get(); 

                $data['student'] = Student::where('Std_Id',$id)
                                ->join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->select(['student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName'])
                                ->first();

                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.student.edit',$data);
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function getParent(Request $request){
        $searchParentName = $request->get('term');
        $data = User::select('Use_Name')->where('Use_Type',4)->where('Use_Name','LIKE','%'.$searchParentName.'%')->get();
        $result=array();
        foreach ($data as $value){
                $result[] = ['value' => $value->Use_Name];
                }
        return response()->json($result);
    }
    public function store(Request $request){
        try {
            if(Auth::user()) {
                $this->rules['grno'] =  'required|unique:student_tbl,Std_Gr_No';
                $this->rules['image'] =  'required|image';
                // dd($this->rules);
                $validator = Validator::make($request->all(),$this->rules);
     
                if($validator->fails()){
                    return back()->with('errors',$validator->errors())->withInput();
                }else{
                    if($request->branch=="none"){
                        return back()->with('branch',"Please Select Branch")->withInput();
                    }
                    if($request->class=="none"){
                        return back()->with('class',"Please Select Class")->withInput();
                    }
                    if($request->section=="none"){
                        return back()->with('section',"Please Select Section")->withInput();
                    }
                    if($request->has('image')){
                        $image = $request->file('image');
                        $imageName = time().$image->getClientOriginalName();
                        $image->move(public_path('images/profile'),$imageName);
                    }
                    $branchId = $request->branch;
                    $classAndId = explode(",", $request->class);
                    $class = end($classAndId);
                    $section = $request->section;
                    $classId = ClassTbl::select(['Cla_Id'])->where('Cla_Bra_Id',$branchId)->where('Cla_Class',$class)->where('Cla_Section',$section)->first();
                    // $classId->Cla_Id

                    $parentId = User::select(['Use_Id'])->where('Use_Name',$request->parentname)->first();
                    //$parentId->Use_Id
                    if($parentId){
                        $student = new Student;
                        $student->Std_Cla_Id = $classId->Cla_Id;
                        $student->Std_Gr_No = $request->grno;
                        $student->Std_Name = $request->studentname;
                        $student->Std_Parent_Id = $parentId->Use_Id;
                        $student->Std_Image = $imageName;
                        $student->Std_Fee = $request->studentfee;
                        if($request->transport!="") 
                            $student->Std_Transport = 1 ;
                        $student->Std_Status = $request->status;
                        $student->Std_CreatedBy = Auth::user()->Use_Id;
                        $student->Std_CreatedAt = date('Y-m-d H:i:s');
                        $student->Std_UpdatedBy = Auth::user()->Use_Id;
           		        $student->Std_UpdatedAt = date('Y-m-d H:i:s');
                        if($student->save()){
                            return redirect('Student Mgmt.');
                        }else{
                            return back();
                        }
                    }else{
                        return back()->with('error',"<script>alert('Parent Does Not Exists Please Add Perent First.')</script>")->withInput();
                    }
                }
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function update(Request $request){
        try{
            if(Auth::user()) {
            	$std_grno = Student::where('Std_Id',$request->id)->select('Std_Gr_No')->first();
            	if($std_grno['Std_Gr_No']==$request->grno){
            	}else{
            		$this->rules['grno'] =  'required|unique:student_tbl,Std_Gr_No';
            	}
            	
            	$validator = Validator::make($request->all(),$this->rules);
         
                if($validator->fails()){
                    return back()->with('errors',$validator->errors())->withInput();
                }else{
                    if($request->branch=="none"){
                        return back()->with('branch',"Please Select Branch")->withInput();
                    }
                    if($request->class=="none"){
                        return back()->with('class',"Please Select Class")->withInput();
                    }
                    if($request->section=="none"){
                        return back()->with('section',"Please Select Section")->withInput();
                    }
                    $branchId = $request->branch;
                    $classAndId = explode(",", $request->class);
                    $class = end($classAndId);
                    $section = $request->section;
                    $classId = ClassTbl::select(['Cla_Id'])->where('Cla_Bra_Id',$branchId)->where('Cla_Class',$class)->where('Cla_Section',$section)->first();
                    // $classId->Cla_Id

                    $parentId = User::select(['Use_Id'])->where('Use_Name',$request->parentname)->first();
                    //$parentId->Use_Id

                    $student = Student::find($request->id);
                    $student->Std_Cla_Id = $classId->Cla_Id;
                    $student->Std_Gr_No = $request->grno;
                    $student->Std_Name = $request->studentname;
                    $student->Std_Parent_Id = $parentId->Use_Id;
                    if($request->has('image')){
                        $image = $request->file('image');
                        $imageName = time().$image->getClientOriginalName();
                        $image->move(public_path('images/profile'),$imageName);
                        $student->Std_Image = $imageName;
                    }
                    $student->Std_Fee = $request->studentfee;
                    if($request->transport!="") 
                        $student->Std_Transport = 1 ;
                    else
                        $student->Std_Transport = 0 ;
                        $student->Std_Status = $request->status;
                        $student->Std_UpdatedBy = Auth::user()->Use_Id;
                        $student->Std_UpdatedAt = date('Y-m-d H:i:s');
                }

                if($student->update()){
                    return redirect('Student Mgmt.');
                }else{
                    return back();
                }
            } else {
                return redirect('login');
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function destroy(Request $request)
    {
        try {
            if(Auth::user()) {
                $all_data = $request->except('_token');

                foreach ($all_data as $ids) 
                {
                    foreach($ids as $id) 
                    {
                        Student::where('Std_Id',$id)->delete();
                    }
                }
                return redirect()->back();
            } else {
                return redirect('login');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getClass(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$request->Cla_Bra_Id)->distinct()->get(['Cla_Id','Cla_Class','Cla_Bra_Id']);
        return $data;
    }

    public function getStudentClass(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$request->Cla_Bra_Id)->distinct()->get(['Cla_Id','Cla_Class','Cla_Bra_Id']);
        // $data->push($request->Cla_Bra_Id);
        return view('auth.newsfeed.class_list')->with('data',$data);

    }

    public function getSection(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$request->Cla_Class)
            ->where('Cla_Bra_Id',$request->Cla_Bra_Id)->get();
        return $data;
    }
    public function active(Request $request)
    {
        try {
            if(Auth::user()) {
                $all_data = $request->except('_token');
                $chk_update = Ajax::studentStatus($request);       
                $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
                if(is_numeric($whatIWant))
                    $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName')
                                ->paginate(10,['*'],'page',$whatIWant);
                else
                    $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName')
                                ->paginate(10);
                return view('Auth.student.active',$data);
            } else {
                return redirect('login');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

}
