<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Exceptions;
use App\Model\QuestionClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Model\Subject;
use Auth;
use App\Model\Module;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class SubjectController extends Controller
{
    public function index()
    {
    	try {
            if(Auth::user()) {
    	    	if(Auth::user()->Use_Type == "1")
    	    	{
    	    		$data['subject'] = Subject::orderBy('Sub_Id','DESC')
    	    				->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','subject_tbl.Sub_Brn')
    	        			->orderBy('Sub_Id','DESC')
    	        			->paginate(10);

                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
    	    	}
    	    	elseif(Auth::user()->Use_Type == "2")
    	        {
    	        	$teacher = UserRights::teacher();
    	        	$branchCode = Branch::where('Brn_Id',$teacher["branchAccess"])->first()->Brn_Name;
    	        	$data['subject'] = Subject::where('Sub_Brn',$branchCode)
    	        				->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','subject_tbl.Brn_Name')
    	        				->orderBy('Sub_Id','DESC')
    	        				->paginate(10);

                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
    	        }
    	        elseif(Auth::user()->Use_Type == "5")
    	        {
    	        	$branch = UserRights::branchAdmin();
    	        	$branchCode = Branch::whereIn('Brn_Id',$branch["branchAccess"])->select(['Brn_Name'])->get()->toArray();
    	        	$data['subject'] = Subject::whereIn('Sub_Brn',$branchCode)
    	        				->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','subject_tbl.Sub_Brn')
    	        				->orderBy('Sub_Id','DESC')
    	        				->paginate(10);

                    $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
    	        }
    	        $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
    	        $data['CURight'] = UserRights::rights();
    	    	return view('auth.subject.index')->with($data);
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e){
			Exceptions::exception($e);
		}
    }

    public function create(){
        try{
            if(Auth::user()) {
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                return view('auth.subject.create')->with($data);
            } else {
                return redirect('login');
            }
        }catch (\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function store(Request $request){
        try{
            if(Auth::user()) {
                if(Subject::where('Sub_Class', $request->class)
                    ->where('Sub_Section',$request->section)
                    ->where('Sub_Brn',$request->branch)
                    ->where('Sub_Name',$request->subName)
                    ->exists()){

                    return redirect()->back()->with('subName', 'Subject Already Exists');
                }else{
                    $subject = New Subject();

                    $subject->Sub_Name = $request->subName;
                    $subject->Sub_Code = $request->subCode;
                    $subject->Sub_Brn = $request->branch;
                    $subject->Sub_Status = $request->status;
                    $subject->Sub_Class = $request->class;
                    $subject->Sub_Section = $request->section;
                    $subject->Sub_Month_Year = $request->date;
                    $subject->save();

                    return redirect("Subject Mgmt.");
                }
            } else {
                return redirect('login');
            }
        }catch (\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function edit($subject){
        try{
            if(Auth::user()) {
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                $data['subject'] = Subject::where('Sub_Id',$subject)->first();

                return view('auth.subject.edit')->with($data);
            } else {
                return redirect('login');
            }
        }catch (\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function update(Request $request, $sub_id){
        try{
            if(Auth::user()) {
                $oldSubData = Subject::where('Sub_Id',$sub_id)->first();
                Subject::where('Sub_Id',$sub_id)->delete();

                $subject = new Subject;
                $subject->Sub_Id = $oldSubData->Sub_Id;
                $subject->Sub_Name = $request->subName;
                $subject->Sub_Code = $request->subCode;
                $subject->Sub_Brn = $request->branch;
                $subject->Sub_Status = $request->status;
                $subject->Sub_Class = $request->class;
                $subject->Sub_Section = $request->section;
                $subject->Sub_Month_Year = $request->date;
                $subject->save();

                return redirect("Subject Mgmt.");
            } else {
                return redirect('login');
            }
        }catch (\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function getSubject(Request $request){
        $data['subject'] = Subject::where('Sub_Class', $request->Cla_Class)
            ->select('Sub_Name','Sub_Id')->get();
        return $data;
    }


    public function viewSubjectFile($id)
    {
    	try {
            if(Auth::user()) {
        		$data['filedata'] = Subject::where('Sub_Id',$id)->first()->Sub_Json_Data;
        		return view('auth.subject.filedata')->with($data);
            } else {
                return redirect('login');
            }
    	}catch(\Exception $e){
			Exceptions::exception($e);
		}
    }
    public function destory(Request $request)
    {
    	try {
            if(Auth::user()) {
                // dd($request->Sub_Id);
                // foreach ($request->Sub_Id as $item) {
                //   dd($item);
                    if(QuestionClass::whereIn('Exam_Sub_Id',$request->Sub_Id )
                        ->exists()){
                        return response()->json([
                            'success' => 'You Can not Delete subject because you have created exam on the same.'
                        ]);
                    }
                // if(QuestionClass::where('Exam_Sub_Id',$item )
                // ->exists()){
                // break;
                // }
                    else{
                        $all_data = $request->except('_token');

                        foreach ($all_data as $ids)
                        {
                            foreach($ids as $id)
                            {
                                Subject::where('Sub_Id',$id)->delete();
                            }
                        }
                        return response()->json([
                            'error' => 'Record has been deleted successfully!'
                        ]);
                    }
                //}
            } else {
                return redirect('login');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
