/* INCLUDE REQUIRE PACKAGES */
var app = require("express")();
//var https = require("https").Server(app);
var https = require('https');
var mysql = require("mysql");
var FCM = require('fcm-node');
var fs = require('fs');
var https_server = https.createServer({
    key: fs.readFileSync('/var/www/rideapp/socket/certi/private.key'),
    cert: fs.readFileSync('/var/www/rideapp/socket/certi/certificate.crt'),
    requestCert: true,
    rejectUnauthorized: false
},app);
var io = require('socket.io')(https_server);
/*var server = require('https').createServer({
    key: fs.readFileSync('../socket/certi/privkey.pem'),
    cert: fs.readFileSync('../socket/certi/cert.pem'),
    
    requestCert: true,
    rejectUnauthorized: false
},app);*/
/*Notification*/
/* Android */
var serverKeyDriver = 'AAAAQmmLJqc:APA91bGDc5p2YdMHVK2LQj3OGZDJ7cJsCXwkPaKFli4ceiCTPfDMivAWiJWiJxr-BKkZ_ZVGiLrt5GIeLCqoI15rcUJUZEFdQCpzzS18MN4GoLbXuCB2ghDRQKdSlAGDZaW75LZRjzvJ'; //put your server key here
var collapseKeyDriver = 'AIzaSyA3ysv8OntTK-CJ-ZDjq-cRZDTjyFJg8ws';
var fcmDriver = new FCM(serverKeyDriver);
var serverKeyCustomer = 'AAAAq99xXgs:APA91bEgo6il4WwKjABXW1j8vv-Mh4NCmIGvYwfVf5PJKcYgjm52lb2eszZ0rQ5lulFh3XIvd30FOGSLD5ZOlL9s22Fl8TaFH1Ycx6AJL_L5o_uOR5ad_NOcwhXYtbQJdn5SGdco22JG'; //put your server key here
var collapseKeyCustomer = 'AIzaSyApM1WUrmcOJ0NHUcSXsYs8l9Yq0MQeVu4';
var fcmCustomer = new FCM(serverKeyCustomer);

/* Ios */
var serverKeyDriverIos = 'AAAAQmmLJqc:APA91bGDc5p2YdMHVK2LQj3OGZDJ7cJsCXwkPaKFli4ceiCTPfDMivAWiJWiJxr-BKkZ_ZVGiLrt5GIeLCqoI15rcUJUZEFdQCpzzS18MN4GoLbXuCB2ghDRQKdSlAGDZaW75LZRjzvJ'; //put your server key here
var collapseKeyDriverIos = 'AIzaSyA3ysv8OntTK-CJ-ZDjq-cRZDTjyFJg8ws';
var fcmDriverIos = new FCM(serverKeyDriverIos);
var serverKeyCustomerIos = 'AAAAq99xXgs:APA91bEgo6il4WwKjABXW1j8vv-Mh4NCmIGvYwfVf5PJKcYgjm52lb2eszZ0rQ5lulFh3XIvd30FOGSLD5ZOlL9s22Fl8TaFH1Ycx6AJL_L5o_uOR5ad_NOcwhXYtbQJdn5SGdco22JG'; //put your server key here
var collapseKeyCustomerIos = 'AIzaSyApM1WUrmcOJ0NHUcSXsYs8l9Yq0MQeVu4';
var fcmCustomerIos = new FCM(serverKeyCustomerIos);


/*SOCKET PORT*/
const LISTEN_PORT = 3080;
const  RADIUS = 10;

/* GLOBALS VARIABLE */
var userlist = [];

/* GLOBALS FUNCTION */
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/*DATABASE CONNECTION*/
con = mysql.createConnection({ host : "rideapp.caru4mgyyuzj.us-west-2.rds.amazonaws.com", user : "root", password : "XgyApXRn9N", database : "rideapp_dev" });
app.get("/",function(req,res){	res.sendFile(__dirname+"/index.html");	});
app.get("/customer",function(req,res){	res.sendFile(__dirname+"/customer.html");	});
app.get("/driver",function(req,res){	res.sendFile(__dirname+"/driver.html");	});
io.on("connection",function(socket){

	socket.on('disconnect', function() {
		var socket_id = socket.id;
		if(socket_id!=""){
			/*DRIVER SOCKET ID NULL IF CLOSE APP AND ALSO SET DRIVER OFF*/
			var driverSocketGetQuery = "SELECT `id` FROM `drivers` WHERE `socket_id`='"+socket_id+"' limit 1 ";
			var driverSocketGetId = con.query(driverSocketGetQuery,function(err,driver_res){
				if(err!=null){ console.log("ERROR IN driverSocketGetId ::--"+err);
				} else{
					if(driver_res!="" && driver_res[0]["id"]!=""){
						var driverSocketId = "UPDATE `drivers` SET `socket_id`='' WHERE `id`='"+driver_res[0]["id"]+"' ";
						var socketUpdateDriver = con.query(driverSocketId,function(err,res){
							if(err!=null) console.log("ERROR IN socketUpdateDriver ::--"+err);
						});
						var availableDriverSocketUpdate = "UPDATE `available_drivers` SET `status`='off' WHERE `driver_id`='"+driver_res[0]["id"]+"' ";
						var availableSocketUpdate = con.query(availableDriverSocketUpdate,function(err,res){
							if(err!=null) console.log("ERROR IN availableSocketUpdate ::--"+err);
						});
					}
				}
			});

			/*CUSTOMER SOCKET ID SET NULL IF THAY CLOSE APP*/
			var customerSocketId = "UPDATE `users` SET `socket_id`='' WHERE `socket_id`='"+socket_id+"' ";
			var socketUpdateCustomer = con.query(customerSocketId,function(err,res){
				if(err!=null) console.log("ERROR IN socketUpdateCustomer ::--"+err);
			});
		}
	});

	socket.on('testing',async(data)=>{
		// asyncCall(data);
	});

	/* DRIVER CURRENT STATUS UPDATE */
	/* PARAMETERS : driver_id:"1",latitude:lat,longitude:lon,vehicle_type:"1",vehicle_category:"1",status:"on"*/
	socket.on('driver_status',async(data)=>{
		console.log("==============DRIVER STATUS===============");
		console.log(data);
		/*DRIVER CURRENT LOCATION UPDATE*/
		var driver_id = data.driver_id;
		var latitude = data.latitude;
		var longitude = data.longitude;
		var vehicle_type = data.vehicle_type;
		var vehicle_category = data.vehicle_category;
		var status = data.status;

		if(driver_id!="" && latitude!="" && longitude!="" && vehicle_type!="" && vehicle_category!="" && status!=""){

			var driverCheckQuery = "SELECT * FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
			var driverExistsCheck = con.query(driverCheckQuery,function(err,driver_res){
				if(err!=null){ console.log("ERROR IN driverExistsCheck ::--"+err);
				} else{
					console.log(" DRIVER STATUS ===== "+driver_res[0]["status"]);
					if(driver_res[0]["id"]!=""){
						console.log(" DRIVER STATUS ===== "+driver_res[0]["status"]);
						if(driver_res[0]["status"]!="active"){
							io.to(socket.id).emit("driver_not_active",{"message":"Driver not active."});
						} else{
							var statusQueryParameters = " '"+driver_id+"','"+latitude+"','"+longitude+"','"+vehicle_type+"','"+vehicle_category+"','"+status+"' ";
							var driverStatusQuery = "REPLACE INTO `available_drivers`(`driver_id`,`latitude`,`longitude`,`vehicle_type`,`vehicle_category`,`status`)VALUES("+statusQueryParameters+")";
							var is_saved = con.query(driverStatusQuery,function(err,res){
								if(err!=null){
									console.log("ERROR IN driverStatusQuery ::--"+err);
								}
							});
							/*DRIVER SOCKET ID UPDATE*/
							var socketQueryParameters = " '"+driver_id+"','"+socket.id+"' ";
							var driverSocketQuery = "SELECT `socket_id` FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
							var driverSocketId = con.query(driverSocketQuery,function(err,res){
								if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
								} else{
									if(res[0]["socket_id"]=="" || res[0]["socket_id"]!=socket.id ){
										var driverSocketUpdate = "UPDATE `drivers` SET `socket_id`='"+socket.id+"' WHERE `id`='"+driver_id+"' ";
										var socketUpdate = con.query(driverSocketUpdate,function(err,res){
											if(err!=null) console.log("ERROR IN socketUpdate ::--"+err);
										});
									}
								}
							});
						}
					} else{
						console.log("Driver id not exists :- "+driver_id);
					}
				}
			});

			

		}
	});

	/* CUSTOMER SEND REQUEST DRIVER TO RIDE */
	/* PARAMETERS : driver_id:"1",trip_key:"adFKJHDSfsdfs8df7ZUd7sa6d7ygddf7dgsdfDGg8GGdaagFD" */
	socket.on("send_driver_request",async(data)=>{
		/* VARIABLE */
		var customer_id=data.customer_id;
		var driver_id=data.driver_id;
		var trip_key=data.trip_key;

		/* CHECK DRIVER AVAILABLE OR NOT QUERY */
		var driverSocketIdQuery = "SELECT * FROM `available_drivers` WHERE `driver_id`="+driver_id+" AND `status`='on' limit 1 ";
		var driverSocketId = con.query(driverSocketIdQuery,function(err,res){
			if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
			} else{
				if(res.length){
					/*DRIVER AVAILABLE AND SEND REQUEST*/
					var tripDetailQuery = "SELECT trip_details.*,users.name,users.country_code,users.contact_no,users.rating,available_drivers.latitude as driver_latitude,available_drivers.longitude as driver_longitude FROM `trip_details` LEFT JOIN `users` ON trip_details.customer_id=users.id LEFT JOIN `available_drivers` ON available_drivers.driver_id=trip_details.driver_id WHERE `key`='"+trip_key+"'  limit 1 ";
					var tripDetail = con.query(tripDetailQuery,function(err,trip_res){
						if(err!=null){ console.log("ERROR IN tripDetail ::--"+err);
						} else{
							if(trip_res.length){
								/*FIND DRIVER SOCKET ID*/
								var driverSocketIdQuery = "SELECT `socket_id`,`device_token`,`name` FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
								var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
									if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
									} else{
										if(driver_res[0]["socket_id"]!=""){
											io.to(driver_res[0]["socket_id"]).emit("driver_get_request",trip_res);
											if(driver_res[0]["device_token"]!=""){
												sendDriverNotification(driver_id,"New Request.","",".DashboardActivity");
												// sendDriverNotification(driver_res[0]["device_token"],"New Request.","");
												createNotification('customer',customer_id,'Send Request to '+driver_res[0]["name"]+" for "+trip_res[0]["drop_address"]+" trip.",'yes');
												createNotification('driver',driver_id,'New Request Arrived for '+trip_res[0]["drop_address"]+" trip.",'yes');
											}
										}
									}
								});
							} else{
								console.log("NOT FOUND");
							}
						}
					});
				} else{
					/* DRIVER NOT AVAILABLE */
					console.log("Driver not available");
				}
			}
		});
	});

	/*DRIVER DECLINE REQUEST AND SEND OTHER DRIVER FOR TRIP */
	socket.on("driver_decline_request",async(data)=>{
		/* VARIABLE */
		var customer_id=data.customer_id;
		var driver_id=data.driver_id;
		var trip_key=data.trip_key;

		if(driver_id!="" && trip_key!=""){
			/* CHECK DRIVER AVAILABLE OR NOT QUERY */
			var driverSocketIdQuery = "SELECT * FROM `available_drivers` WHERE `driver_id`="+driver_id+" AND `status`='on' limit 1 ";
			var driverSocketId = con.query(driverSocketIdQuery,function(err,res){
				if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
				} else{
					if(res.length){
						/*DRIVER AVAILABLE AND SEND REQUEST*/
						var tripDetailQuery = "SELECT trip_details.*,users.name,users.country_code,users.contact_no,users.rating,countrys.currency_sign,available_drivers.latitude as driver_latitude,available_drivers.longitude as driver_longitude FROM trip_details LEFT JOIN users ON trip_details.customer_id=users.id LEFT JOIN `available_drivers` ON available_drivers.driver_id=trip_details.driver_id JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE `key`='"+trip_key+"'  limit 1 ";
						var tripDetail = con.query(tripDetailQuery,function(err,trip_res){
							if(err!=null){ console.log("ERROR IN tripDetail ::--"+err);
							} else{
								if(trip_res.length){
									/*FIND DRIVER SOCKET ID*/
									var driverSocketIdQuery = "SELECT `socket_id` FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
									var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
										if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
										} else{
											if(driver_res[0]["socket_id"]!=""){
												/*EMIT TO CUSTOMER FOR DRIVER DETAILS*/
												var customerSocketIdQuery = "SELECT `socket_id`,`device_token` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
												var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
													if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
													} else{
														if(cust_res[0]["socket_id"]!=""){
															var customerTripDetails = "SELECT drivers.id as driver_id,trip_details.key,trip_details.pickup_time,trip_details.status,trip_details.drop_address, drivers.name, drivers.rating, drivers.country_code, drivers.contact_no, drivers.vehicle_number, drivers.profile, available_drivers.latitude, available_drivers.longitude, vehicle_categorys.name as vehicle_category_name,countrys.currency_sign,trip_details.total_price,trip_details.pickup_latitude,trip_details.pickup_longitude FROM trip_details LEFT JOIN drivers ON drivers.id = '"+driver_id+"' LEFT JOIN available_drivers ON available_drivers.driver_id = '"+driver_id+"' LEFT JOIN vehicle_categorys ON drivers.vehicle_category = vehicle_categorys.id JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE trip_details.key = '"+trip_key+"' ";
															var customerTrip = con.query(customerTripDetails,function(err,cust_trip_res){
																if(err!=null){ console.log("ERROR IN customerTrip ::--"+err);
																} else{
																	io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",cust_trip_res);
																	createNotification('customer',customer_id,'Send Request to '+cust_res[0]["name"]+" for "+cust_res[0]["drop_address"]+" trip.",'yes');
																}
															});
														}
													}
												});
												io.to(driver_res[0]["socket_id"]).emit("driver_get_request",trip_res);
												sendDriverNotification(driver_res[0]["device_token"],"New Requsest.","");
												sendDriverNotification(driver_id,"New Requsest.","",".DashboardActivity");
											}
										}
									});
								} else{
									console.log("NOT FOUND");
								}
							}
						});
					} else{
						/* DRIVER NOT AVAILABLE */
						console.log("Driver not available");
					}
				}
			});
		} else{
			var customerSocketIdQuery = "SELECT `socket_id` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
			var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
				if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
				} else{
					if(cust_res[0]["socket_id"]!=""){
						io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",[]);
						var tripDetailQuery = "SELECT trip_details.drop_address FROM trip_details  WHERE `key`='"+trip_key+"'  limit 1 ";
						var tripDetail = con.query(tripDetailQuery,function(err,trip_res){
							if(err!=null){ console.log("ERROR IN tripDetail ::--"+err);
							} else{
								if(trip_res.length){
									createNotification('customer',customer_id,"Near driver not found for"+trip_res[0]["drop_address"]+" trip.",'yes');
								}
							}
						});
					} else{
					}
				}
			});
		}
	});

	/*DRIVER SEND RESPONSE TO CUSTOMER*/
	/*PARAMETERS { customer_id:"",driver_id:"",trip_key:"",response:"" } */
	socket.on("driver_send_response",async(data)=>{
		/* VARIABLE */
		var customer_id=data.customer_id;
		var driver_id=data.driver_id;
		var trip_key=data.trip_key;
		var response=data.response; //response must be accept or decline
		/*IF DRIVER ACCEPT REQUEST*/
		if(response=='accept'){
			/*UPDATE DRIVER ID IN TRIP_DETAILS TABLE*/
			var driverAcceptReq = "UPDATE `trip_details` SET `driver_id`='"+driver_id+"',`status`='"+'booked'+"' WHERE `key`='"+trip_key+"' ";
			var updateDriverTrip = con.query(driverAcceptReq,function(err,res){
				if(err!=null){
					console.log("ERROR IN updateDriverTrip ::--"+err);
				} else{
					/*SEND RESPONSE TO CUSTOMER*/
					var customerSocketIdQuery = "SELECT `socket_id`,`device_token` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
					var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
						if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
						} else{
							var customerTripDetails = "SELECT trip_details.driver_id,trip_details.drop_address , trip_details.fare_duration ,trip_details.status,trip_details.pickup_time, drivers.name, drivers.rating, drivers.country_code, drivers.contact_no, drivers.vehicle_number, drivers.profile, available_drivers.latitude, available_drivers.longitude, vehicle_categorys.name as vehicle_category_name,countrys.currency_sign,trip_details.total_price,trip_details.pickup_latitude,trip_details.pickup_longitude FROM trip_details LEFT JOIN drivers ON drivers.id = trip_details.driver_id LEFT JOIN available_drivers ON available_drivers.driver_id = trip_details.driver_id LEFT JOIN vehicle_categorys ON drivers.vehicle_category = vehicle_categorys.id JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE trip_details.key = '"+trip_key+"'";
							var customerTrip = con.query(customerTripDetails,function(err,trip_res){
								if(err!=null){ console.log("ERROR IN customerTrip ::--"+err);
								} else{
									if(cust_res[0]["socket_id"]!=""){
										io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",trip_res);
									}
										
									if(cust_res[0]["device_token"]!=""){
										/* send notification on accept request */
										var notificationQuery = "select trip_details.key,trip_details.driver_id,trip_details.pickup_address,trip_details.pickup_latitude,trip_details.pickup_longitude,trip_details.drop_address,trip_details.drop_latitude,trip_details.status,trip_details.payment_mode,trip_details.pickup_time,trip_details.drop_time,trip_details.total_price,trip_details.drop_longitude,vehicle_categorys.name as vehicle_category_name,vehicle_types.name as vehicle_type_name,countrys.currency_sign,drivers.name as drivers_name,drivers.country_code,drivers.contact_no,drivers.profile,drivers.rating,drivers.vehicle_number,friends_details.pickup_latitude as friend_latitude,friends_details.pickup_longitude as friend_longitude,available_drivers.latitude as driver_latitude,available_drivers.longitude as driver_longitude,countrys.currency_sign FROM trip_details LEFT JOIN vehicle_categorys ON trip_details.vehicle_category_id = vehicle_categorys.id LEFT JOIN vehicle_types ON trip_details.vehicle_type_id = vehicle_types.id LEFT JOIN drivers ON trip_details.driver_id = drivers.id LEFT JOIN friends_details ON trip_details.key = friends_details.trip_id JOIN countrys ON trip_details.currency_code=countrys.currency_code LEFT JOIN available_drivers ON available_drivers.driver_id = trip_details.driver_id WHERE trip_details.key='"+trip_key+"' limit 1 ";
										var tripDetail = con.query(notificationQuery,function(err,trip_noti_res){
											if(err!=null){ console.log("ERROR IN notificationQuery ::--"+err);
											} else{
												sendCustomerNotification(customer_id,trip_res[0]["name"]+" accept your request.","Veh. No :- "+trip_res[0]["vehicle_number"],trip_noti_res[0],'.MainActivity');
											}
										});

										var driverSocketIdQuery = "SELECT `name` FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
										var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
											if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
											} else{
												createNotification('driver',driver_id,'You confirm request for '+trip_res[0]["drop_address"]+" trip.",'yes');
												createNotification('customer',customer_id,driver_res[0]["name"]+" Accept your "+trip_res[0]["drop_address"]+" trip",'yes');
											}
										});
									}
								}
							});

						}
					});
				}
			});
		} else if(response=='decline'){
			/*IF DRIVER DECLINE REQUEST*/
		}
	});

	/* CUSTOMER SOCKET ID SEND IF CUSTOMER REGISTER LOGIN AND OPEN APPLICATION TIME */
	/* PARAMETERS : customer_id:"1" */
	socket.on("customer_socket_id",async(data)=>{
		/*VARIABEL*/
		var customer_id=data.customer_id;
		var socket_id = socket.id;
		/*CUSTOMER SOCKET ID UPDATE*/
		var socketQueryParameters = " '"+customer_id+"','"+socket_id+"' ";
		var customerSocketQuery = "SELECT `socket_id` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
		var customerSocketId = con.query(customerSocketQuery,function(err,res){
			if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
			} else{
				if(res.length){
					if(res[0]["socket_id"]=="" || res[0]["socket_id"]!=socket_id ){
						var customerSocketId = "UPDATE `users` SET `socket_id`='"+socket_id+"' WHERE `id`='"+customer_id+"' ";
						var socketUpdate = con.query(customerSocketId,function(err,res){
							if(err!=null) console.log("ERROR IN socketUpdate ::--"+err);
						});
					}
				}
			}
		});
	});

	/*DRIVER PICKUP CUSTOMER*/
	/* PARAMETERS : customer_id:"",driver_id:"",trip_id:"",latitude:"",longitude:"",datetime:"" */
	socket.on("driver_pickup_customer",async(data)=>{
		/*VARIABEL*/
		var customer_id=data.customer_id;
		var driver_id=data.driver_id;
		var trip_id=data.trip_id;
		var latitude=data.latitude;
		var longitude=data.longitude;
		// var datetime= data.datetime;
		var datetime = new Date();
		var trip_det_res = "";

		/* CHECK TRIP DETAILS AVAILABLE OR NOT */
		var tripDetailsQuery = "SELECT * FROM trip_details WHERE `key`='"+trip_id+"' limit 1 ";
		var tripDetail = con.query(tripDetailsQuery,function(err,trip_res){
			if(err!=null){ console.log("ERROR IN tripDetailsQuery ::--"+err);
			} else{
				if(trip_res.length){
					/*UPDATE PICKUP LAT LONG AND TIME IN TRIP*/
					var pickupUpdateQuery = "UPDATE trip_details SET `pickup_latitude`='"+latitude+"',`pickup_longitude`='"+longitude+"',`pickup_time`='"+datetime.toISOString()+"',`status`='"+'ongoing'+"' WHERE `key`='"+trip_id+"' ";
					var pickupUpdate = con.query(pickupUpdateQuery,function(err,res){
						if(err!=null){
							console.log("ERROR IN pickupUpdate ::--"+err);
						} else{
							/*ADD NOTIFICATION*/
							createNotification('driver',driver_id,'Start ride for '+trip_res[0]["drop_address"]+" trip.",'yes');

							/*SEND RESPONSE TO CUSTOMER*/
							var customerSocketIdQuery = "SELECT `socket_id`,`device_token` FROM users WHERE `id`="+customer_id+" limit 1 ";
							var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
								if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
								} else{
									var customerTripDetails = "SELECT trip_details.pickup_latitude,trip_details.fare_duration,trip_details.pickup_longitude,trip_details.drop_latitude,trip_details.drop_longitude,trip_details.drop_address,trip_details.driver_id, trip_details.pickup_time, drivers.name, drivers.rating, drivers.country_code, drivers.contact_no, drivers.vehicle_number, drivers.profile, available_drivers.latitude, available_drivers.longitude, vehicle_categorys.name as vehicle_category_name,friends_details.pickup_latitude as friend_pickup_latitude,friends_details.pickup_longitude as friend_pickup_longitude FROM trip_details LEFT JOIN drivers ON drivers.id = trip_details.driver_id LEFT JOIN available_drivers ON available_drivers.driver_id = trip_details.driver_id LEFT JOIN vehicle_categorys ON drivers.vehicle_category = vehicle_categorys.id LEFT JOIN friends_details ON friends_details.trip_id = trip_details.key WHERE trip_details.key = '"+trip_id+"' limit 1";
									var customerTrip = con.query(customerTripDetails,function(err,trip_det_res){
										if(err!=null){ console.log("ERROR IN customerTrip ::--"+err);
										} else{
											if(cust_res[0]["socket_id"]!=""){
												io.to(cust_res[0]["socket_id"]).emit("driver_start_ride",trip_det_res);
											}
											createNotification('customer',customer_id,trip_det_res[0]["name"]+" pickup you from "+trip_det_res[0]["drop_address"],'yes');

											/* SEND PUSH NOTIFICATION */
											if(cust_res[0]["device_token"]!=""){
												var notificationQuery = "select trip_details.key,trip_details.driver_id,trip_details.pickup_address,trip_details.pickup_latitude,trip_details.pickup_longitude,trip_details.drop_address,trip_details.drop_latitude,trip_details.status,trip_details.payment_mode,trip_details.pickup_time,trip_details.drop_time,trip_details.total_price,trip_details.drop_longitude,vehicle_categorys.name as vehicle_category_name,vehicle_types.name as vehicle_type_name,countrys.currency_sign,drivers.name as drivers_name,drivers.country_code,drivers.contact_no,drivers.profile,drivers.rating,drivers.vehicle_number,friends_details.pickup_latitude as friend_latitude,friends_details.pickup_longitude as friend_longitude,available_drivers.latitude as driver_latitude,available_drivers.longitude as driver_longitude,countrys.currency_sign FROM trip_details LEFT JOIN vehicle_categorys ON trip_details.vehicle_category_id = vehicle_categorys.id LEFT JOIN vehicle_types ON trip_details.vehicle_type_id = vehicle_types.id LEFT JOIN drivers ON trip_details.driver_id = drivers.id LEFT JOIN friends_details ON trip_details.key = friends_details.trip_id JOIN countrys ON trip_details.currency_code=countrys.currency_code LEFT JOIN available_drivers ON available_drivers.driver_id = trip_details.driver_id WHERE trip_details.key='"+trip_id+"' limit 1 ";
												var tripDetail = con.query(notificationQuery,function(err,trip_noti_res){
													if(err!=null){ console.log("ERROR IN notificationQuery ::--"+err);
													} else{
														sendCustomerNotification(customer_id,trip_noti_res[0]["drivers_name"]+" Start a ride.","Veh. No :- "+trip_noti_res[0]["vehicle_number"],trip_noti_res[0],'.MainActivity');
													}
												});
											}
										}
									});
									var driverTripDetails = "SELECT trip_details.key as trip_id,trip_details.pickup_address, trip_details.pickup_latitude, trip_details.pickup_longitude, trip_details.drop_address, trip_details.drop_latitude, trip_details.drop_longitude, trip_details.customer_id, trip_details.pickup_time, trip_details.fare_duration, users.name, users.rating, users.country_code, users.contact_no, users.profile,friends_details.pickup_latitude as friend_pickup_latitude,friends_details.pickup_longitude as friend_pickup_longitude FROM trip_details LEFT JOIN users ON users.id = trip_details.customer_id LEFT JOIN friends_details ON friends_details.trip_id = trip_details.key WHERE trip_details.key = '"+trip_id+"' ";
									var driverTrip = con.query(driverTripDetails,function(err,driver_trip_res){
										if(err!=null){ console.log("ERROR IN driverTrip ::--"+err);
										} else{											
											io.to(socket.id).emit("driver_trip_response",driver_trip_res);
										}
									});
								}
							});
						}
					});
				}
			}
		});
	});

	/* TRIP ROUTE LOG */
	/* PARAMETERS : trip_id:"",driver_id:"",customer_id:"",latitude:"",longitude:"",datetime:"" */
	// socket.on("live_track_driver",async(data)=>{
	// });

	/* TRIP ROUTE LOG */
	/* PARAMETERS : trip_id:"",driver_id:"",customer_id:"",latitude:"",longitude:"",datetime:"" */
	socket.on("live_track_driver",async(data)=>{
		/*VARIABEL*/
		var trip_id=data.trip_id;
		var driver_id=data.driver_id;
		var customer_id=data.customer_id;
		var latitude=data.latitude;
		var longitude=data.longitude;
		var bearing = data.bearing;
		var datetime= data.datetime;

		// var res = calculateBearing(trip_id,latitude,longitude);
		// console.log("LAT  ::: "+res['lat1']);

		/* create function in sql */
		// var breringFunction = "DELIMITER $$ DROP FUNCTION IF EXISTS `initial_bearing`$$ CREATE FUNCTION `initial_bearing`(lat1 DOUBLE, lon1 DOUBLE,lat2 DOUBLE, lon2 DOUBLE) RETURNS DOUBLE NO SQL DETERMINISTIC COMMENT 'Returns the initial bearing, in degrees, to follow the great circle route from point (lat1,lon1), to point (lat2,lon2)' BEGIN RETURN (360.0 + DEGREES(ATAN2( SIN(RADIANS(lon2-lon1))*COS(RADIANS(lat2)), COS(RADIANS(lat1))*SIN(RADIANS(lat2))-SIN(RADIANS(lat1))*COS(RADIANS(lat2))* COS(RADIANS(lon2-lon1)) )) ) % 360.0; END$$ DELIMITER ; SET @bearing = initial_bearing("++","++","++","++");";
		

		/* INSERT LAT LONG OF ROUTE */
		var parameters = " '"+trip_id+"','"+driver_id+"','"+customer_id+"','"+latitude+"','"+longitude+"','"+bearing+"','"+datetime+"' ";
		var routeDetailQuery = " INSERT INTO trip_route_log(`trip_id`,`driver_id`,`customer_id`,`latitude`,`longitude`,`bearing`,`created_at`) VALUES("+parameters+") ";
		var routeInsert = con.query(routeDetailQuery,function(err,route_res){
			if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
			} else{
				/* SEND RESPONSE TO CUSTOMER */
				var customerSocketIdQuery = "SELECT `socket_id` FROM users WHERE `id`="+customer_id+" limit 1 ";
				var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
					if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
					} else{
						if(cust_res[0]["socket_id"]!=""){
							io.to(cust_res[0]["socket_id"]).emit("customer_track_response",{"latitude":latitude,"longitude":longitude,"bearing":bearing});
						}
					}
				});
			}
		});
	});

	/* DRIVER TRIP COMPLATE */
	/* PARAMETERS : trip_id:"",customer_id:"" */
	socket.on("driver_trip_complate",async(data)=>{
		/*VARIABEL*/
		var customer_id=data.customer_id;
		var trip_id=data.trip_id;
		
		/* GET TRIP DETAILS FROM DATABASE */
		var tripDetailsQuery = "SELECT trip_details.*,countrys.currency_sign FROM trip_details JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE trip_details.key='"+trip_id+"' limit 1 ";
		var tripDetail = con.query(tripDetailsQuery,function(err,trip_res){
			if(err!=null){ console.log("ERROR IN tripDetailsQuery ::--"+err);
			} else{
				if(trip_res.length){
					/*ADD NOTIFICATION*/
					createNotification('driver',trip_res[0]["driver_id"],'Trip Completed for '+trip_res[0]["drop_address"]+" trip.",'yes');
					/*SEND RESPONSE TO CUSTOMER*/
					var customerSocketIdQuery = "SELECT `socket_id` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
					var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
						if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
						} else{
							if(cust_res[0]["socket_id"]!=""){
								// io.to(cust_res[0]["socket_id"]).emit('driver_cancel_trip_customer','123');
								io.to(cust_res[0]["socket_id"]).emit("trip_complate_response",trip_res);
							}
						}
					});
				} else{
					console.log("TRIP DETAILS NOT FOUND.");
				}	
			}
		});
	});

	/* NEAR CAB FOR CUSTOMER */
	/* PARAMETERS : customer_id:"1",latitude:"22.12345678",longitude:"72.12345678" */
	socket.on("nearbycab_cust",async(data)=>{
		/*VARIABLE*/
		var customer_id = data.customer_id;
		var latitude = data.latitude;
		var longitude = data.longitude;

		var nearByCabQuery = "select `latitude`, `longitude` from `available_drivers` where status = 'on' AND (6371 * acos( cos( radians("+latitude+") ) * cos( radians( latitude ) )  *  cos( radians( longitude ) - radians("+longitude+") ) + sin( radians("+latitude+") ) * sin(  radians( latitude ) ) ) ) < "+RADIUS+"  limit 5";
		var nearByCabList = con.query(nearByCabQuery,function(err,near_res){
			if(err!=null){ console.log("ERROR IN nearByCabList ::--"+err);
			} else{
				var customerSocketIdQuery = "SELECT `socket_id` FROM users WHERE `id`="+customer_id+" limit 1 ";
				var customerSocketId = con.query(customerSocketIdQuery,function(err,res){
					if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
					} else{
						if(res!="" && res[0]["socket_id"]!=""){
							io.to(res[0]["socket_id"]).emit("nearbycab_cust_res",near_res);
						}
					}
				});
			}
		});
	});

	/*CUSTOMER CANCEL TRIP SEDN DRIVER RESPONSE*/
	/*PARAMETERS : customer_id:"1",driver_id:"1",trip_id:"" */
	socket.on("customer_cancel_trip",async(data)=>{
		/*VARIABLE*/
		var customer_id = data.customer_id;
		var driver_id = data.driver_id;
		var trip_id = data.trip_id;

		/*SEND RESPONSE TO DRIVER FOR CANCEL TRIP*/
		var driverSocketIdQuery = "SELECT `socket_id` FROM drivers WHERE `id`="+driver_id+" limit 1 ";
		var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
			if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
			} else{
				if(driver_res[0]["socket_id"]!=""){
					io.to(driver_res[0]["socket_id"]).emit("customer_cancel_trip_res",{"message":"Customer cancel trip.","trip_id":trip_id});
				}
			}
		});
	});

	socket.on("check_trip_cancel",async(data)=>{
		/*VARIABLE*/
		var customer_id = data.customer_id;
		// var driver_id = data.driver_id;
		var trip_id = data.trip_key;
		var tripDetailQuery = "SELECT trip_details.*FROM trip_details  WHERE `key`='"+trip_key+"' WHERE `status`= 'cancel'  limit 1 ";
		/*SEND RESPONSE TO DRIVER FOR CANCEL TRIP*/
		var driverSocketIdQuery = "SELECT `socket_id` FROM users WHERE `id`="+customer_id+" limit 1 ";
		var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
			if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
			} else{
				if(driver_res[0]["socket_id"]!=""){
					io.to(driver_res[0]["socket_id"]).emit("driver_cancel_trip_res",{"message":"Customer cancel trip.","trip_id":trip_id});
				}
			}
		});
	});

	/*DRIVER CANCEL TRIP SEDN CUSTOMER RESPONSE*/
	/*PARAMETERS : customer_id:"1",driver_id:"1",trip_id:"" */
	socket.on("driver_cancel_trip",async(data)=>{
		/* VARIABLE */
		// io.to('vXoCBwJjYpBd3ZymAAAR').emit('driver_cancel_trip_customer','123');
		var customer_id=data.customer_id;
		var driver_id=data.driver_id;
		var trip_key=data.trip_id;
		// socket.emit("driver_cancel_trip_res",'123');
		if(driver_id!="" && trip_key!=""){
			/* CHECK DRIVER AVAILABLE OR NOT QUERY */
			var driverSocketIdQuery = "SELECT * FROM `available_drivers` WHERE `driver_id`="+driver_id+" AND `status`='on' limit 1 ";
			var driverSocketId = con.query(driverSocketIdQuery,function(err,res){
				if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
				} else{
					if(res.length){
						/*DRIVER AVAILABLE AND SEND REQUEST*/
						var tripDetailQuery = "SELECT trip_details.*,users.name,users.country_code,users.contact_no,users.rating,countrys.currency_sign,available_drivers.latitude as driver_latitude,available_drivers.longitude as driver_longitude FROM trip_details LEFT JOIN users ON trip_details.customer_id=users.id LEFT JOIN `available_drivers` ON available_drivers.driver_id=trip_details.driver_id JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE `key`='"+trip_key+"'  limit 1 ";
						var tripDetail = con.query(tripDetailQuery,function(err,trip_res){
							if(err!=null){ console.log("ERROR IN tripDetail ::--"+err);
							} else{
								if(trip_res.length){
									/*FIND DRIVER SOCKET ID*/
									var driverSocketIdQuery = "SELECT `socket_id` FROM `drivers` WHERE `id`="+driver_id+" limit 1 ";
									var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
										if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
										} else{
											if(driver_res[0]["socket_id"]!=""){
												/*EMIT TO CUSTOMER FOR DRIVER DETAILS*/
												var customerSocketIdQuery = "SELECT `socket_id`,`device_token` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
												var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
													if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
													} else{
														// socket.emit("driver_cancel_trip_res",cust_res);
														if(cust_res[0]["socket_id"]!=""){
															var customerTripDetails = "SELECT drivers.id as driver_id,trip_details.status,trip_details.pickup_time,trip_details.drop_address, drivers.name, drivers.rating, drivers.country_code, drivers.contact_no, drivers.vehicle_number, drivers.profile, available_drivers.latitude, available_drivers.longitude, vehicle_categorys.name as vehicle_category_name,countrys.currency_sign,trip_details.total_price,trip_details.pickup_latitude,trip_details.pickup_longitude FROM trip_details LEFT JOIN drivers ON drivers.id = '"+driver_id+"' LEFT JOIN available_drivers ON available_drivers.driver_id = '"+driver_id+"' LEFT JOIN vehicle_categorys ON drivers.vehicle_category = vehicle_categorys.id JOIN countrys ON trip_details.currency_code=countrys.currency_code WHERE trip_details.key = '"+trip_key+"' ";
															var customerTrip = con.query(customerTripDetails,function(err,cust_trip_res){
																if(err!=null){ console.log("ERROR IN customerTrip ::--"+err);
																} else{
																	io.to(cust_res[0]["socket_id"]).emit("driver_cancel_trip_res",{trip_key:trip_res[0].key,message:'Trip cancel'});
																	// io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",cust_trip_res);
																	// createNotification('customer',customer_id,'Send Request to '+cust_res[0]["name"]+" for "+cust_res[0]["drop_address"]+" trip.",'yes');
																}
															});
														}
													}
												});
												// io.to(driver_res[0]["socket_id"]).emit("driver_get_request",trip_res);
												// // sendDriverNotification(driver_res[0]["device_token"],"New Requsest.","");
												// sendDriverNotification(driver_id,"New Requsest.","",".DashboardActivity");
											} else{
												io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",[]);
											}
										}
									});
								} else{
									console.log("NOT FOUND");
								}
							}
						});
					} else{
						/* DRIVER NOT AVAILABLE */
						console.log("Driver not available");
					}
				}
			});
		} else{
			var customerSocketIdQuery = "SELECT `socket_id` FROM `users` WHERE `id`="+customer_id+" limit 1 ";
			var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
				if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
				} else{
					if(cust_res[0]["socket_id"]!=""){
						io.to(cust_res[0]["socket_id"]).emit("driver_pickup_you",[]);
						var tripDetailQuery = "SELECT trip_details.drop_address FROM trip_details  WHERE `key`='"+trip_key+"'  limit 1 ";
						var tripDetail = con.query(tripDetailQuery,function(err,trip_res){
							if(err!=null){ console.log("ERROR IN tripDetail ::--"+err);
							} else{
								if(trip_res.length){
									createNotification('customer',customer_id,"Near driver not found for"+trip_res[0]["drop_address"]+" trip.",'yes');
								}
							}
						});
					}
				}
			});
		}
	});
	

	/*CUSTOMER GET RATING*/
	/*PARAMETERS : customer_id:"1" */
	socket.on("customer_rating",async(data)=>{
		/*VARIABLE*/
		var customer_id = data.customer_id;

		/*SEND RESPONSE TO CUSTOMER FOR RATING*/
		var customerSocketIdQuery = "SELECT `socket_id`,`rating` FROM users WHERE `id`="+customer_id+" limit 1 ";
		var customerSocketId = con.query(customerSocketIdQuery,function(err,cust_res){
			if(err!=null){ console.log("ERROR IN customerSocketId ::--"+err);
			} else{
				if(cust_res[0]["socket_id"]!=""){
					io.to(cust_res[0]["socket_id"]).emit("customer_rating_get",cust_res);
				}
			}
		});
	});

	/*DRIVER GET RATING*/
	/*PARAMETERS : driver_id:"1"*/
	socket.on("driver_rating",async(data)=>{
		/*VARIABLE*/
		var driver_id = data.driver_id;

		/*SEND RESPONSE TO DRIVER FOR RATING*/
		var driverSocketIdQuery = "SELECT `socket_id`,`rating` FROM drivers WHERE `id`="+driver_id+" limit 1 ";
		var driverSocketId = con.query(driverSocketIdQuery,function(err,driver_res){
			if(err!=null){ console.log("ERROR IN driverSocketId ::--"+err);
			} else{
				if(driver_res[0]["socket_id"]!=""){
					console.log(driver_res);
					io.to(driver_res[0]["socket_id"]).emit("driver_rating_get",driver_res);
				}
			}
		});
	});
	
});


/* calculate bearing */
function calculateBearing(trip_id,lat2,lon2)
{
	var lat1='',lon1='';
	/* find trip previous lat long */
	var previousLatLongQuery = "SELECT `latitude`,`longitude` FROM trip_route_log WHERE `trip_id`='"+trip_id+"' order by created_at DESC limit 1 ";
	var previousLatLong = con.query(previousLatLongQuery,function(err,trip_info){
		if(err!=null){ console.log("ERROR IN previousLatLongQuery on Bearing ::--"+err);
		} else{
			if(trip_info[0]["latitude"]!="" && trip_info[0]["longitude"]!=""){
				lat1 = trip_info[0]["latitude"];
				lon1 = trip_info[0]["longitude"];
			} else{
				lat1 = lat2;
				lon2 = lon2;
			}
		}
	});
	var res = new Array();
	// res["lat1"=>lat1];
	// res["lon2"=>lon2];
	return res;
}

function sendDriverNotification(id,title="",body="",data="",click_action="")
{
	/* variable */
	var token = "";
	var device_type = "";
	/* get customer details */
	var driverGetQueryNofi = "SELECT * FROM `drivers` WHERE `id`="+id+" limit 1 ";
	var driverSocketId = con.query(driverGetQueryNofi,function(err,driver_res){
		if(err!=null){ console.log("ERROR IN driverGetQueryNofi ::--"+err);
		} else{
			if(driver_res[0]["device_token"]!=""){
				token = driver_res[0]["device_token"];
				if(driver_res[0]["device_type"]=="Android"){
					var message = { 
							to : token, 
							collapse_key : collapseKeyCustomer, 
							notification : { title: title, body: body ,click_action:click_action },
					    	data : {
					    		data : data
					    	}
						};
					fcmDriver.send(message, function(err, response){if (err) {  console.log("Something has gone wrong!"+err); }});
				} else if(driver_res[0]["device_type"]=="Ios"){
					var message = { 
								to : token, 
								collapse_key : collapseKeyCustomerIos, 
								notification : { title: title, body: body ,click_action:click_action },
						    	data : {
						    		data : data
						    	}
						};
					fcmDriverIos.send(message, function(err, response){if (err) {  console.log("Something has gone wrong!"+err); }});
				} else{
					device_type = "";
				}
			}
		}
	});
}
function sendCustomerNotification(id,title="",body="",data="",click_action="")
{
	/* variable */
	var token = "";
	var device_type = "";
	/* get customer details */
	var customerGetQueryNofi = "SELECT * FROM `users` WHERE `id`="+id+" limit 1 ";
	var customerSocketId = con.query(customerGetQueryNofi,function(err,cust_res){
		if(err!=null){ console.log("ERROR IN customerGetQueryNofi ::--"+err);
		} else{
			if(cust_res[0]["device_token"]!=""){
				token = cust_res[0]["device_token"];
				if(cust_res[0]["device_type"]=="Android"){
					var message = { 
							to : token, 
							collapse_key : collapseKeyCustomer, 
							notification : { title: title, body: body ,click_action:click_action },
					    	data : {
					    		data : data
					    	}
						};
					fcmCustomer.send(message, function(err, response){if (err) {  console.log("Something has gone wrong!"+err); }});
				} else if(cust_res[0]["device_type"]=="Ios"){
					var message = { 
								to : token, 
								collapse_key : collapseKeyCustomerIos, 
								notification : { title: title, body: body ,click_action:click_action },
						    	data : {
						    		data : data
						    	}
						};
					fcmCustomerIos.send(message, function(err, response){if (err) {  console.log("Something has gone wrong!"+err); }});
				} else{
					device_type = "";
				}
			}
		}
	});
}

function createNotification(type,id,title,is_delivered='yes'){
	var datetime = new Date().toISOString();
	var parameters = " '"+type+"','"+id+"','"+title+"','"+is_delivered+"','"+datetime+"','' ";
	var notificationQuery = " INSERT INTO notification_log(`user_type`,`user_id`,`title`,`is_delivered`,`created_at`,`updated_at`) VALUES("+parameters+") ";
	var createNotification = con.query(notificationQuery,function(err,not_res){
		if(err!=null){ console.log("ERROR IN createNotification ::--"+err);
		} else{
		}
	});
}
function CancelTrip(data){

	if (data.status=='cancel') {
		var parameters = " '"+123+"','"+'2'+"','"+'Credited for trip cancel'+"','"+123+"','"+'INR'+"','"+'$'+"','"+'credit'+"','' ";
		var notificationQuery = " INSERT INTO payment_transaction(`tnx_id`,`user_id`,`description`,`amount`,`currency_code`,`currency_sign`,`tnx_type`) VALUES("+parameters+") ";
		var createNotification = con.query(notificationQuery,function(err,not_res){
			if(err!=null){ console.log("ERROR IN createNotification ::--"+err);
			} else{
			}
		});
	}
}
/* EXAMPLE 
function resolveAfter2Seconds(data) {
  return new Promise(resolve => {
    setTimeout(() => {
    	var d = new Date();
		var s = d.getSeconds();
		var m = d.getMilliseconds();
		resolve(data+' '+s+' '+m+'  resolved');
		});
  });
}
async function asyncCall(data) {
	console.log(data + '  calling');
  	var result = await resolveAfter2Seconds(data);
  	console.log(result);
}
*/

https_server.listen(LISTEN_PORT,function(){
	console.log("Server Created Successfully... With "+LISTEN_PORT);
});