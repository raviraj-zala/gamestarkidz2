<?php
namespace App\Helper;

use App\User;
use App\Model\Notification;


class UserNotification {

	public static function storeNotification($userId,$type)
	{
	    try{
	        if(User::where('Use_Id',$userId)->where('Use_Status', 1)->where('socket_id', null)->orwhere('socket_id', ' ')->exists()){

                    $note = new Notification();
                    $note->user_id = $userId;
                    $note->type = $type;
                    $note->Notification_CreatedAt =  date('Y-m-d H:i:s');
                    $note->Notification_UpdatedAt =  date('Y-m-d H:i:s');
                    $note->save();

                    return response()->json(['success' => 'Notification sent']);
            }

        }catch (\Exception $e) {
            Exceptions::exception($e);
        }

	}




}