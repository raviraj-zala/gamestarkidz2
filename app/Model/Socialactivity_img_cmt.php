<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Socialactivity_img_cmt extends Model
{
    const CREATED_AT = 'Sic_CreatedAt';
	const UPDATED_AT = 'Sic_UpdatedAt';

    protected $table = "socialactivity_img_cmt_tbl";
    protected $primaryKey = "Sic_Id";

    public function SicComment()
    {
	 return $this->hasMany('App\Model\Users','Use_Id','Sic_Use_Id');
    }
}
