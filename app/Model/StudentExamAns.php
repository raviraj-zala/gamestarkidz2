<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentExamAns extends Model
{
    const CREATED_AT = 'Exam_CreatedAt';
	const UPDATED_AT = 'Exam_UpdatedAt';

	protected $table = "stu_exam_ans_tbl";
	protected $primaryKey = "Stu_Exam_Id";
}
