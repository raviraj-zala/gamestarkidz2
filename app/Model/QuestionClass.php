<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionClass extends Model
{
    const CREATED_AT = 'Que_Cla_CreatedAt';
	const UPDATED_AT = 'Que_Cla_UpdatedAt';

	protected $table = "question_class_tbl";
	protected $primaryKey = "Que_Cla_Id";

}
