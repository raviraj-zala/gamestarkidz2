<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const CREATED_AT = 'Pay_CreatedAt';
	const UPDATED_AT = 'Pay_UpdatedAt';

    protected $table = "payment_tbl";
    protected $primaryKey = "Pay_Id";
}
