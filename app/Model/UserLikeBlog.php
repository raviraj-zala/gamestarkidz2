<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ClassTbl;

class UserLikeBlog extends Model
{
    public $timestamps = false;
	protected $table = "blog_like";

}
