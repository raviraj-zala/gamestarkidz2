<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ClassTbl;

class Socialactivity_comment extends Model
{
    const CREATED_AT = 'Sac_CreatedAt';
	const UPDATED_AT = 'Sac_UpdatedAt';

	protected $table = "socialactivity_cmnt_tbl";

	public function SoaComment()
	{
		 return $this->hasMany('App\Model\Users','Use_Id','Sac_Use_Id');
	}

}
