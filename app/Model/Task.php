<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const CREATED_AT = 'Tas_CreatedAt';
	const UPDATED_AT = 'Tas_UpdatedAt';

	protected $table = "task_tbl";
	protected $primaryKey = "Tas_Id";
}
