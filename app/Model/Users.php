<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    const CREATED_AT = 'Use_CreatedAt';
	const UPDATED_AT = 'Use_UpdatedAt';
	
   	protected $table = "user_tbl";
    
    public function Use_Student()
	{
		 return $this->hasmany('App\Model\Student','Std_Cla_Id','Use_Cla_Id');
	}

	public function Use_Teacher()
	{
		 return $this->hasmany('App\Model\Users','Use_Cla_Id','Use_Cla_Id');
	}
}
