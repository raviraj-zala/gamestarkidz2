<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteTbl extends Model
{
    protected $table = "route_tbl";
    protected $primaryKey = 'Rou_Id';

    const CREATED_AT = 'Rou_CreatedAt';
	const UPDATED_AT = 'Rou_UpdatedAt';
}
