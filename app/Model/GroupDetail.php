<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupDetail extends Model
{
    protected $table = "group_detail_tbl";
    protected $primaryKey = "Grp_Det_Id";

    const CREATED_AT = "Grp_Det_CreatedAt";
    const UPDATED_AT = "Grp_Det_UpdatedAt";
}
