<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsfeedTagTbl extends Model
{
	const CREATED_AT = 'NfTag_CreatedAt';
	const UPDATED_AT = Null;
	protected $primaryKey = "Nfd_tg_tbl"; 
	protected $table = "newsfeed_tag_tbl";

}
