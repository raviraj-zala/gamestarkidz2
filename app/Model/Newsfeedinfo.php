<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newsfeedinfo extends Model
{
	const CREATED_AT = 'Nfi_CreatedAt';
	const UPDATED_AT = Null;
	protected $primaryKey = "Nfi_Id"; 
	protected $table = "newsfeed_info";
}
