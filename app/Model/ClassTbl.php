<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassTbl extends Model
{
    //
    const CREATED_AT = 'Cla_CreatedAt';
	const UPDATED_AT = 'Cla_UpdatedAt';

	public $primaryKey = 'Cla_Id'; 

	protected $table = "class_tbl";

	public function Section()
	{
		return $this->hasOne('App\Model\ClassTbl','Cla_Class','Cla_Class');
	}
}
