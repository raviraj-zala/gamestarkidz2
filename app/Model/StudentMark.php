<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentMark extends Model
{
    const CREATED_AT = 'Stu_Mark_CreatedAt';
	const UPDATED_AT = 'Stu_Mark_UpdatedAt';

	protected $table = "stu_mark_tbl";
	protected $primaryKey = "stu_mark_id";

}
