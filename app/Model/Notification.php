<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const CREATED_AT = 'Notification_CreatedAt';
    const UPDATED_AT = 'Notification_UpdatedAt';

   	protected $table = "user_notification";

}
