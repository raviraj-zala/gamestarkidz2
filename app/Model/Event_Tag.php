<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Event_Tag extends Model
{
    //
    const id = 'Evt_Id';
    const CREATED_AT = 'Evt_CreatedAt';
	const UPDATED_AT = 'Evt_UpdatedAt';
	
    protected $table = "event_tag_tbl";
    
}
