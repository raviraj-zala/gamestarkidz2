<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    const id = 'Bra_Zon_Id';
    const CREATED_AT = 'Brn_CreatedAt';
	const UPDATED_AT = 'Brn_UpdatedAt';
	
    protected $table = "branch_tbl";
    
    public function zone()
    {
    	return $this->hasOne('App\Model\Zone','Zon_Id','Brn_Zon_Id');
    }
}
