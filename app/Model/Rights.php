<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rights extends Model
{
    //
    const id = 'Usr_Id';
    const CREATED_AT = 'Usr_CreatedAt';
	const UPDATED_AT = 'Usr_UpdatedAt';

    protected $table = "user_rights_tbl";
}
