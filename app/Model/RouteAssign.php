<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteAssign extends Model
{
    protected $table = "route_assign_tbl";
    public $timestamps = false;
    protected $primaryKey = 'Ass_Id';
}
