<?php



namespace App\Model;



use Illuminate\Database\Eloquent\Model;



class Socialactivitytag extends Model

{

	const CREATED_AT = 'Tag_CreatedAt';
	const UPDATED_AT = 'Tag_UpdatedAt';

	protected $table = "socialactivity_tag_tbl";

	public function TagParent()
	{
		 return $this->hasMany('App\Model\Users','Use_Id','Tag_Par_Id');
	}

	public function TagTeacher()
	{
		 return $this->hasMany('App\Model\Users','Use_Id','Tag_Tea_Id');
	}

	public function TagStudent()
	{
		 return $this->hasMany('App\Model\Student','Std_Id','Tag_Stu_Id');
	}
}

