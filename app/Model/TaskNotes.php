<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaskNotes extends Model
{
    const CREATED_AT = 'Tas_Note_CreatedAt';
	const UPDATED_AT = 'Tas_Note_UpdatedAt';

	protected $table = "task_notes_tbl";
	protected $primaryKey = "Tas_Not_Id";
}
