<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teacher_assign_class extends Model
{
    //
    const id = 'Tac_Id';
    const CREATED_AT = 'Tac_CreatedAt';
	const UPDATED_AT = 'Tac_UpdatedAt';
	
    protected $table = "teacher_assign_class_tbl";
    
}
