<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExamTitle extends Model
{
    const CREATED_AT = 'Exm_CreatedAt';
	const UPDATED_AT = 'Exm_UpdatedAt';

	protected $table = "exam_title_tbl";
	protected $primaryKey = "exam_id";

}
