<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model {

    protected $table = "subject_tbl";
    protected $primarykey = 'Sub_Id';

    const CREATED_AT = 'Sub_CreatedAt';

    const UPDATED_AT = 'Sub_UpdatedAt';
}
