<?php



namespace App\Model;



use Illuminate\Database\Eloquent\Model;

use App\Model\ClassTbl;



class Count extends Model

{

    const CREATED_AT = 'Cnt_CreatedAt';

	const UPDATED_AT = 'Cnt_UpdatedAt';

	protected $table = "count_tbl";

	public function Cnt_user()
	{
		 return $this->hasmany('App\Model\Users','Use_Id','Cnt_User_Id');
	}

}

