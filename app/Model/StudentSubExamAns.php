<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentSubExamAns extends Model
{
    const CREATED_AT = 'Exam_CreatedAt';
	const UPDATED_AT = 'Exam_UpdatedAt';

	protected $table = "stu_sub_exam_ans_tbl";
	protected $primaryKey = "Stu_Exam_Id";
}
