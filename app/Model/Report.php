<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = "report_tbl";

    const CREATED_AT = 'Rpt_CreatedAt';
	const UPDATED_AT = 'Rpt_UpdatedAt';

	 protected $primaryKey = "Rpt_Id";
}
