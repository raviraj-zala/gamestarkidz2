<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const CREATED_AT = 'Que_CreatedAt';
	const UPDATED_AT = 'Que_UpdatedAt';

	protected $table = "question_tbl";
	protected $primaryKey = "Que_Id";
    protected $fillable = ['Que_Cla_Id', '	Que_Question', 'Que_Ans', 'Que_Option_1', 'Que_Option_2','Que_Option_3','Que_Option_4'];
}
