<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ClassTbl;

class Homework_view extends Model
{
    //
    const CREATED_AT = 'Hwv_CreatedAt';
	const UPDATED_AT = 'Hwv_UpdatedAt';

	protected $table = "homework_view_tbl";
}
